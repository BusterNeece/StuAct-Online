<?php
require_once dirname(__FILE__) . '/../app/bootstrap.php';
$application->bootstrap();

error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 1);
ini_set('memory_limit', -1);
ini_set('error_log', DF_INCLUDE_TEMP.'/synchronize_hourly.txt');

$sync = new \StuAct\Synchronization\Manager;
$sync->hourly();