(function($) {
    /* DF ModalForm plugin */
    $.fn.ModalForm = function(btn_name, form_url, title) {
        var form_name = btn_name.substring(btn_name.indexOf('btn_')+4);
        if ((title == '') || (title == undefined)) title = 'Edit';
        return this.each(function() {
            if( !$(this).is('a') )
                throw new Error("ModalForm can only be applied to an anchor element, just attempted to apply to a " + this.tagName);

            $(this).click(function(e) {
                e.preventDefault();
                var is_search = $(this).hasClass('search');
                openFormDialog(form_name, form_url, title, is_search);
            });
        });
    };
    
    var openFormDialog = function(form_name,form_url,title, is_search) {

        var div_id = '#'+form_name+'_form_dialog';

        if ($(div_id).length == 0)
        {
            $('body').append('<div class="modal" id="'+div_id.substring(1)+'"><div class="modal-header"><a class="close" data-dismiss="modal"><i class="icon-remove"></i></a><h3>'+title+'</h3></div><div class="modal-body"><span class="loading_msg"></span></div><div class="modal-footer"></div></div>');
            $(div_id).modal({
                show: false,
                keyboard: true,
                backdrop: 'static'
            });
            $(div_id + ' .close').click(function(){$(div_id).modal('hide')});
        }

        if ($(div_id+' form').length > 0)
        {
            $(div_id).modal('show');
        }
        else
        {
            $(div_id+' .loading_msg').addClass('loading').html('loading form...');
            $(div_id).modal('show');
            $.ajax({
                url: form_url,
                dataType: "html",
                success: function(data, textStatus)
                {
                    
                        $(div_id+' .loading_msg').removeClass('loading').html('');

                        $(div_id+ ' .modal-body').append(data);

                        /* Add a submit button to the footer. */
                        var submit_btn_txt = 'Save Changes';
                        if (is_search) submit_btn_txt = 'Search';
                        var submit_btn = $('<a />')
                            .attr('href', '#')
                            .attr('style', 'margin-right: 10px;')
                            .text(submit_btn_txt)
                            .addClass('btn btn-primary')
                            .click(function() {
                                $(div_id+' form').submit();
                            });
                        
                        /* Add a cancel button to the footer. */
                        var cancel_btn = $('<a />')
                            .attr('href', '#')
                            .attr('style', 'margin-right: 10px;')
                            .attr('data-dismiss', 'modal')
                            .text('Cancel')
                            .addClass('btn')
                            .click(function() {
                                $(div_id).modal('hide');
                            });

                        $(div_id+' .modal-footer').append(cancel_btn);
                        $(div_id+' .modal-footer').append(submit_btn);

                        $(div_id+' form').submit(function(){
                            $(div_id+' input[type=submit]').attr('disabled', 'disabled')
                                   .addClass('disabled')
                                     .val('Working...');
                        });
                },
                error: function(data, textStatus, errorThrown)
                {
                    $(div_id+' .loading_msg').removeClass('loading').html('Error loading form: '+errorThrown);
                }
            });
        }
        return false;
    }

})(jQuery);