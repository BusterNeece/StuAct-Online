<?php
class Default_IndexTest extends \DF\Test\TestCase
{
	public function testIndexLoads()
	{
		$this->dispatch('/');

		$this->assertController('index');
		$this->assertAction('index');
		$this->assertResponseCode(200);

		$this->assertQueryContentContains('h1.title', 'Welcome!');
	}


}