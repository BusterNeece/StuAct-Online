<?php
/**
 * This file loads the necessary files and configuration to run the Doctrine CLI
 * tools.
 */

define('DF_APPLICATION_ENV', 'production');
require dirname(__FILE__).'/doctrine.php';