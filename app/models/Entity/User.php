<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @Table(name="users", indexes={
 *   @index(name="search_idx", columns={"uin","firstname","lastname","username"})
 * })
 * @Entity
 * @HasLifecycleCallbacks
 */
class User extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->roles = new ArrayCollection;
        $this->batchgroups = new ArrayCollection;
        $this->officer_positions = new ArrayCollection;
        $this->logs = new ArrayCollection;
        $this->event_attendance = new ArrayCollection;
        
        $this->date_created = $this->date_modified = time();
    }
    
    /** @PreUpdate */
    public function _setUpdateTimestamp()
    {
        $this->date_modified = time();
    }

    /**
     * @Column(name="user_id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="uin", type="integer") */
    protected $uin;

    /** @Column(name="username", type="string", length=45, nullable=true) */
    protected $username;

    /** @Column(name="date_created", type="integer", length=4, nullable=true) */
    protected $date_created;

    /** @Column(name="date_modified", type="integer", length=4, nullable=true) */
    protected $date_modified;

    /** @Column(name="date_logged_in", type="integer", length=4, nullable=true) */
    protected $date_logged_in;

    /** @Column(name="firstname", type="string", length=255, nullable=true) */
    protected $firstname;

    /** @Column(name="lastname", type="string", length=255, nullable=true) */
    protected $lastname;

    public function getName($with_uin = true)
    {
        if ($with_uin)
            return $this->firstname.' '.$this->lastname.' ('.$this->uin.')';
        else
            return $this->firstname.' '.$this->lastname;
    }

    /** @Column(name="title", type="string", length=255, nullable=true) */
    protected $title;

    /** @Column(name="email", type="string", length=255, nullable=true) */
    protected $email;

    public function setEmail($email)
    {
        $this->email = $email;
        $this->email_is_confirmed = true;
    }

    /** @Column(name="email_is_confirmed", type="boolean", nullable=true) */
    protected $email_is_confirmed;

    /** @Column(name="email_confirmed_date", type="integer", nullable=true) */
    protected $email_confirmed_date;

    /** @Column(name="email2", type="string", length=255, nullable=true) */
    protected $email2;

    /** @Column(name="phone", type="string", length=255, nullable=true) */
    protected $phone;

    /** @Column(name="addr1", type="string", length=255, nullable=true) */
    protected $addr1;

    /** @Column(name="addr2", type="string", length=255, nullable=true) */
    protected $addr2;

    /** @Column(name="addrcity", type="string", length=255, nullable=true) */
    protected $addrcity;

    /** @Column(name="addrstate", type="string", length=255, nullable=true) */
    protected $addrstate;

    /** @Column(name="addrzip", type="string", length=12, nullable=true) */
    protected $addrzip;

    public function getAddress($separator = '<br>')
    {
        $city_line = array(
            ($this->addrcity) ? $this->addrcity.',' : '',
            $this->addrstate,
            $this->addrzip,
        );

        $address_parts = array(
            $this->addr1,
            $this->addr2,
            implode(' ', array_filter($city_line)),
        );
        return implode($separator, array_filter($address_parts));
    }

    /** @Column(name="strength1", type="integer", length=2, nullable=true) */
    protected $strength1;

    /** @Column(name="strength2", type="integer", length=2, nullable=true) */
    protected $strength2;

    /** @Column(name="strength3", type="integer", length=2, nullable=true) */
    protected $strength3;

    /** @Column(name="strength4", type="integer", length=2, nullable=true) */
    protected $strength4;

    /** @Column(name="strength5", type="integer", length=2, nullable=true) */
    protected $strength5;

    /** @Column(name="flag_training", type="integer", length=1, nullable=true) */
    protected $flag_training;

    public function setFlagTraining($new_flag)
    {
        $old_flag = $this->flag_training;
        $this->flag_training = $new_flag;

        if ($new_flag != 0)
        {
            $this->flag_training_notification = time();
            $this->flag_training_timestamp = time();
        }
    }

    /** @Column(name="flag_training_timestamp", type="integer", nullable=true) */
    protected $flag_training_timestamp;

    public function getFlagTrainingExpiration()
    {
        if ($this->flag_training_timestamp)
        {
            $org_settings = Organization::loadSettings();
            return $this->flag_training_timestamp + $org_settings['lifespan_training'];
        }

        return 0;
    }

    /** @Column(name="flag_training_notification", type="integer", nullable=true) */
    protected $flag_training_notification;

    /** @Column(name="gpr_last_checked", type="string", length=10, nullable=true) */
    protected $gpr_last_checked;

    /** @Column(name="conduct_last_checked", type="integer", length=4, nullable=true) */
    protected $conduct_last_checked;

    /** @Column(name="config_skin", type="string", length=50, nullable=true) */
    protected $config_skin;

    /** @Column(name="info_allow_extra_email", type="integer", length=1, nullable=true) */
    protected $info_allow_extra_email;

    /** @Column(name="info_willing_to_volunteer", type="integer", length=1, nullable=true) */
    protected $info_willing_to_volunteer;

    /** @Column(name="chat_username", type="string", length=45, nullable=true) */
    protected $chat_username;

    /** @Column(name="chat_password", type="string", length=45, nullable=true) */
    protected $chat_password;

    /** @Column(name="staff_profile", type="json", nullable=true) */
    protected $staff_profile;

    /** @Column(name="preferences", type="json", nullable=true) */
    protected $preferences;

    /**
     * @ManyToMany(targetEntity="Entity\Role", inversedBy="users")
     * @JoinTable(name="user_has_role",
     *      joinColumns={@JoinColumn(name="user_id", referencedColumnName="user_id")},
     *      inverseJoinColumns={@JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    protected $roles;
    
    /** @ManyToMany(targetEntity="Entity\OrganizationBatchGroup", mappedBy="managers") */
    protected $batchgroups;
    
    /** @OneToMany(targetEntity="Entity\OrganizationOfficer", mappedBy="user") */
    protected $officer_positions;

    /** @OneToMany(targetEntity="Entity\EventAttendee", mappedBy="user") */
    protected $event_attendance;

    /**
     * @OneToMany(targetEntity="Entity\Log", mappedBy="user")
     * @OrderBy({"timestamp" = "DESC"})
     */
    protected $logs;

    public function setPreference($pref_name, $pref_value, $save_mode = TRUE)
    {
        $prefs = (array)$this->preferences;
        $prefs[$pref_name] = $pref_value;

        $this->preferences = $prefs;
        if ($save_mode)
            $this->save();
    }

    public function getPreference($pref_name, $default = NULL)
    {
        $prefs = (array)$this->preferences;
        if (isset($prefs[$pref_name]))
            return $prefs[$pref_name];
        else
            return $default;
    }

    public function getAvatar($size = 50)
    {
        return \DF\Service\Gravatar::get($this->email, $size);
    }
    
    public function getLeaderStatus()
    {
        $leader_status = array();
        $config = \Zend_Registry::get('config');
        
        // Get all organization affiliations.
        $em = \Zend_Registry::get('em');
        $user_associations_raw = $em->createQuery('SELECT oo, o, obg FROM \Entity\OrganizationOfficer oo JOIN oo.organization o LEFT JOIN o.batch_group obg WHERE oo.user_id = :user_id')
            ->setParameter('user_id', $this->id)
            ->getArrayResult();
        
        $user_categories = array();
        $user_positions = array();
        $user_batch_groups = array();
        
        foreach($user_associations_raw as $position)
        {
            $user_positions[] = $position['position'];
            $user_categories[] = $position['organization']['category'];
            
            if ($position['organization']['batch_group'])
                $user_batch_groups[] = $position['organization']['batch_group']['short_name'];
        }
        
        $leader_status['batch_groups'] = $user_batch_groups;
        
        // Determine the highest level of user leader status.
        $officer_groups = $config->organizations->officer_groups->toArray();
        
        if (count(array_intersect($user_positions, $officer_groups['advisors'])) > 0)
            $leader_status['type'] = "advisor";
        else if (count(array_intersect($user_positions, $officer_groups['officers'])) > 0)
            $leader_status['type'] = "officer";
        else
            $leader_status['type'] = "none";
        
        // Determine the highest level of user categorization.
        if (in_array('Sponsored', $user_categories))
            $leader_status['category'] = 'Sponsored';
        else if (in_array('Affiliated', $user_categories))
            $leader_status['category'] = 'Affiliated';
        else
            $leader_status['category'] = 'Registered';
        
        $leader_status['experienced'] = ((int)$this->flag_training_timestamp != 0);
        $leader_status['current'] = ((int)$this->flag_training != 0);
        $leader_status['expires'] = $this->flag_training_timestamp + $config->organizations->lifespan_training;
        
        return $leader_status;
    }
    
    public function assignFromDirectory()
    {
        if ($this->isProfileMissingRequiredFields())
        {
            $api = new \DF\Service\DoitApi;
            $student_record = $api->getStudentRecord($this->uin);

            if ($student_record)
            {
                $replace_values = array(
                    'firstname'     => $student_record['firstname']['val'],
                    'lastname'      => $student_record['lastname']['val'],
                    'email'         => $student_record['tamu_email']['val']
                );
                
                if ($student_record['local_phone_number'])
                {
                    $replace_values['phone'] = $student_record['local_phone_number']['val'];
                }
                else
                {
                    $replace_values['phone'] = $student_record['perm_phone_number']['val'];
                }
                
                if ($student_record['local_street1'])
                {
                    $replace_values['addr1'] = $student_record['local_street1']['val'];
                    $replace_values['addr2'] = $student_record['local_street2']['val'];
                    $replace_values['addrcity'] = $student_record['local_city']['val'];
                    $replace_values['addrstate'] = $student_record['local_state']['val'];
                    $replace_values['addrzip'] = $student_record['local_zip']['val'];
                }
                else
                {
                    $replace_values['addr1'] = $student_record['perm_street1']['val'];
                    $replace_values['addr2'] = $student_record['perm_street2']['val'];
                    $replace_values['addrcity'] = $student_record['perm_city']['val'];
                    $replace_values['addrstate'] = $student_record['perm_state']['val'];
                    $replace_values['addrzip'] = $student_record['perm_zip']['val'];
                }
                
                foreach($replace_values as $replace_local => $replace_value)
                {
                    $replace_current = $this->{$replace_local};
                    if (empty($replace_current) && !empty($replace_value))
                        $this->{$replace_local} = $replace_value;
                }
            }
        }

        // Check again in case previous process supplied needed information.
        if ($this->isProfileMissingRequiredFields())
        {
            $directory = \DF\Service\TamuRest::getByUin($this->uin);
        
            if ($directory)
            {
                // Special replacement rules for NetID.
                if ($this->username == $this->uin && isset($directory['tamuEduPersonNetID'][0]))
                    $this->username = $directory['tamuEduPersonNetID'][0];
                
                $replace_values = array(
                    'firstname'     => $directory['givenName'][0],
                    'lastname'      => $directory['sn'][0],
                    'email'         => $directory['mail'][0],
                    'phone'         => preg_replace('/[^\d]/', '', $directory['telephoneNumber'][0]),
                );
                
                foreach($replace_values as $replace_local => $replace_value)
                {
                    $replace_current = $this->{$replace_local};
                    if (empty($replace_current) && !empty($replace_value))
                        $this->{$replace_local} = $replace_value;
                }
            }
        }

        return $this;
    }
    public function isProfileMissingRequiredFields()
    {
        return (empty($this->firstname) || empty($this->lastname) || empty($this->email) || empty($this->phone) || empty($this->addr1));
    }
    
    /**
     * Static Functions
     */

    public static function search($q, $return_as_array=FALSE)
    {
        $em = \Zend_Registry::get('em');

        $results = $em->createQuery('SELECT u FROM '.__CLASS__.' u WHERE u.uin LIKE :q OR u.username LIKE :q OR CONCAT(u.firstname, CONCAT(\' \', u.lastname)) LIKE :q ORDER BY u.lastname ASC')
            ->setParameter('q', '%'.$q.'%');
        
        if ($return_as_array)
            return $results->getArrayResult();
        else
            return $results->getResult();
    }
    
    // Get or create a user by UIN.
    public static function getOrCreate($uin, $username = NULL)
    {
        $record = self::getRepository()->findOneByUin($uin);
        
        if (!($record instanceof self))
        {
            $record = new self();
            $record->username = ($username) ? $username : $uin;
            $record->uin = $uin;
            $record->password = ' ';
            $record->assignFromDirectory();
            $record->save();
        }
        
        return $record;
    }
    
    public static function lookUp($user_text)
    {
        $user_text = trim($user_text);
        
        if (strpos($user_text, ';') !== FALSE)
        {
            $api = new \DF\Service\DoitApi();
            $user_text = $api->getUinFromCard($user_text);
        }
        
        if (strlen($user_text) == 9 && is_numeric($user_text))
        {
            return self::getOrCreate($user_text);
        }
        
        try
        {
            $em = \Zend_Registry::get('em');
            $user_row = $em->createQuery('SELECT u FROM '.__CLASS__.' u WHERE u.id != 0 AND (u.username LIKE :q OR u.id = :q)')
                ->setParameter('q', $user_text)
                ->getSingleResult();
            
            return $user_row;
        }
        catch(\Exception $e)
        {
            return NULL;
        }
    }
}