<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="forms_cirt", indexes={@index(name="search_idx", columns={"app_password"})})
 * @Entity
 * @HasLifecycleCallbacks
 */
class TravelForm extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->sessions = new ArrayCollection;

        $this->timestamp = time();
        $this->is_archived = FALSE;
    }

    /**
     * @Column(name="app_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /** @Column(name="org_id", type="integer", nullable=true) */
    protected $org_id;

    /** @Column(name="user_id", type="integer", nullable=true) */
    protected $user_id;

    /** @Column(name="app_password", type="string", length=32, nullable=true) */
    protected $password;

    /** @Column(name="app_timestamp", type="integer", length=4) */
    protected $timestamp;

    /** @Column(name="app_submittertype", type="string", length=255, nullable=true) */
    protected $submitter_type;

    /** @Column(name="app_data", type="json", nullable=true) */
    protected $data;

    public function getData()
    {
        // Handle conversion from previous single traveler file to new multiple.
        $data = (array)$this->data;

        if (empty($data['travelers_files']) && !empty($data['travelers_filename']))
        {
            $data['travelers_files'] = array($data['travelers_filename']);
        }

        return $data;
    }

    public function addData($new_data)
    {
        $data = (array)$this->data;

        foreach((array)$new_data as $key => $val)
            $data[$key] = $val;

        $this->data = $data;
    }

    /** @Column(name="app_name", type="string", length=255, nullable=true) */
    protected $index_name;

    /** @Column(name="app_purpose", type="text", nullable=true) */
    protected $index_purpose;

    /** @Column(name="app_startdate", type="integer", length=4, nullable=true) */
    protected $index_startdate;

    /** @Column(name="app_enddate", type="integer", length=4, nullable=true) */
    protected $index_enddate;

    /** @Column(name="is_archived", type="boolean", nullable=true) */
    protected $is_archived;

    /**
     * @ManyToOne(targetEntity="Entity\Organization")
     * @JoinColumn(name="org_id", referencedColumnName="org_id")
     */
    protected $organization;

    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    /** @OneToMany(targetEntity="Entity\TravelFormSession", mappedBy="form") */
    protected $sessions;

    /** @PostPersist */
    public function inserted()
    {
        $id = $this->id;
        $password = str_pad($id, 5, '0', STR_PAD_LEFT).'-'.substr(strtoupper(sha1('StuActTravelForm'.$id)), 0, 5);
        
        $this->password = $password;
        $this->save();
    }

    /** @PreUpdate */
    public function updated()
    {
        $this->reloadIndex();
    }

    public function reloadIndex()
    {
        $data = (array)$this->data;

        // Unserialize certain data to make admin pages significantly faster.
        $this->index_name = $data['org_name'];
        $this->index_purpose = $data['purpose_of_travel'];

        if ($this->sessions)
        {
            $dates = array();

            foreach($this->sessions as $session)
            {
                if ($session->outbound_departure)
                    $dates[] = (int)$session->outbound_departure;
                if ($session->inbound_arrival)
                    $dates[] = (int)$session->inbound_arrival;
            }

            $this->index_startdate = min($dates);
            $this->index_enddate = max($dates);
        }
        else
        {
            $this->index_startdate = (int)$data['outbound_departure'];
            $this->index_enddate = (int)$data['inbound_arrival'];
        }
    }
}