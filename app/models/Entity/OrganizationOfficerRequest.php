<?php
namespace Entity;


/**
 * OrganizationOfficerRequest
 *
 * @Table(name="organization_officer_requests")
 * @Entity
 */
class OrganizationOfficerRequest extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="request_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="org_id", type="integer") */
    protected $org_id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="off_position", type="integer", length=5) */
    protected $position;

    public function getName()
    {
        $settings = Organization::loadSettings();
        return $settings['officer_codes'][$this->position];
    }

    /**
     * @ManyToOne(targetEntity="Entity\Organization", inversedBy="officer_requests")
     * @JoinColumn(name="org_id", referencedColumnName="org_id")
     */
    protected $organization;

    /**
     * @ManyToOne(targetEntity="Entity\User", inversedBy="officer_requests")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;
}