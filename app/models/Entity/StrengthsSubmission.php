<?php
namespace Entity;


/**
 * @Table(name="strengths_submissions")
 * @Entity
 */
class StrengthsSubmission extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="seminar_id", type="integer", nullable=true) */
    protected $seminar_id;

    /** @Column(name="first_name", type="string", length=255, nullable=true) */
    protected $firstname;

    /** @Column(name="last_name", type="string", length=255, nullable=true) */
    protected $lastname;

    /** @Column(name="status", type="string", length=255, nullable=true) */
    protected $status;

    /** @Column(name="email", type="string", length=255, nullable=true) */
    protected $email;

    /** @Column(name="strength_1", type="string", length=255, nullable=true) */
    protected $strength1;

    /** @Column(name="strength_2", type="string", length=255, nullable=true) */
    protected $strength2;

    /** @Column(name="strength_3", type="string", length=255, nullable=true) */
    protected $strength3;

    /** @Column(name="strength_4", type="string", length=255, nullable=true) */
    protected $strength4;

    /** @Column(name="strength_5", type="string", length=255, nullable=true) */
    protected $strength5;

    /**
     * @ManyToOne(targetEntity="Entity\StrengthsSeminar", inversedBy="submissions")
     * @JoinColumn(name="seminar_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $seminar;
}