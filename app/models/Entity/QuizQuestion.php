<?php
namespace Entity;


/**
 * QuizQuestion
 *
 * @Table(name="quiz_question")
 * @Entity
 */
class QuizQuestion extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->options = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="quiz_id", type="integer") */
    protected $quiz_id;

    /** @Column(name="question", type="text") */
    protected $question;
    
    /** @Column(name="url", type="string", length=255) */
    protected $url;

    /** @Column(name="correct_answer_id", type="integer", nullable=true) */
    protected $correct_answer_id;
    
    /** @Column(name="weight", type="integer", nullable=true) */
    protected $weight;

    /**
     * @ManyToOne(targetEntity="Entity\Quiz", inversedBy="questions")
     * @JoinColumn(name="quiz_id", referencedColumnName="id")
     */
    protected $quiz;
    
    /**
     * @OneToMany(targetEntity="Entity\QuizOption", mappedBy="question", cascade={"remove"})
     */
    protected $options;
}