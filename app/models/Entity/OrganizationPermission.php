<?php
namespace Entity;

/**
 * OrganizationPermission
 *
 * @Table(name="organization_permissions")
 * @Entity
 */
class OrganizationPermission extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="permission_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="org_id", type="integer") */
    protected $org_id;

    /** @Column(name="position", type="integer", length=5) */
    protected $position;

    /** @Column(name="permission_name", type="string", length=50) */
    protected $permission;

    /**
     * @ManyToOne(targetEntity="Entity\Organization")
     * @JoinColumn(name="org_id", referencedColumnName="org_id")
     */
    protected $organization;
}