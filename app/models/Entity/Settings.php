<?php
namespace Entity;

/**
 * Settings
 *
 * @Table(name="settings")
 * @Entity
 */
class Settings extends \DF\Doctrine\Entity
{
    /**
     * @var string $setting_key
     *
     * @Column(name="setting_key", type="string", length=64)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    protected $setting_key;

    /** @Column(name="setting_value", type="json") */
    protected $setting_value;

	/**
     * Static Functions
     */
    
    public static function setSetting($key, $value)
    {
        $record = self::getRepository()->findOneBy(array('setting_key' => $key));
        
        if (!($record instanceof self))
        {
            $record = new self();
            $record->setSettingKey($key);
        }
        
        $record->setSettingValue($value);
        $record->save();
    }
    
    public static function getSetting($key, $default_value = NULL)
    {
        static $settings;

        if ($settings === null)
            $settings = array();

        if (isset($settings[$key]))
        {
            $value = $settings[$key];
        }
        else
        {
            $record = self::getRepository()->findOneBy(array('setting_key' => $key));
        
            if ($record instanceof self)
                $value = $record->getSettingValue();
            else
                $value = $default_value;

            $settings[$key] = $value;
        }

        return $value;
    }
    
    public static function fetchAll()
    {
        $all_records_raw = self::getRepository()->findAll();
        
        $all_records = array();
        foreach($all_records_raw as $record)
        {
            $all_records[$record['setting_key']] = $record['setting_value'];
        }
        return $all_records;
    }
}