<?php
namespace Entity;

/**
 * Message
 *
 * @Table(name="messages")
 * @Entity
 * @HasLifecycleCallbacks
 */
class Message extends \DF\Doctrine\Entity
{
	public function __construct()
    {
        $this->created_at = $this->updated_at = new \DateTime("now");
    }
    
	/** @PreUpdate */
    public function updated()
    {
        $this->updated_at = new \DateTime("now");
    }
	
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="is_read", type="integer", length=1, nullable=true) */
    protected $is_read;

    /** @Column(name="subject", type="string", length=255, nullable=true) */
    protected $subject;

    /** @Column(name="body", type="text", nullable=true) */
    protected $body;
    
    /** @Column(name="created_at", type="datetime") */
    protected $created_at;
    
    /** @Column(name="updated_at", type="datetime") */
    protected $updated_at;
    
    /* Get one or more messages associated with a user. */
    public static function getForCurrentUser($message_id)
    {
        $user = \DF\Auth::getLoggedInUser();
        $em = \Zend_Registry::get('em');
        
        $message_query = $em->createQueryBuilder()
            ->select('m')
            ->from('\Entity\Message', 'm')
            ->where('m.user_id = :user_id')
            ->setParameter('user_id', $user->id);
        
        if (is_array($message_id))
        {
            $message_query->andWhere('m.id IN (:message_ids)')->setParameter('message_ids', $message_id);
            return $message_query->getQuery()->getResult();
        }
        else
        {
            $message_query->andWhere('m.id = :message_id')->setParameter('message_id', $message_id);
            return $message_query->getQuery()->getSingleResult();
        }
    }
    
    public static function getUnreadForUser($user_id = NULL)
    {
        if (null === $user_id)
        {
            $auth = \Zend_Registry::get('auth');
            $user = $auth->getLoggedInUser();
            $user_id = $user->id;
        }
        
        $em = \Zend_Registry::get('em');
        $num_unread = $em->createQuery('SELECT COUNT(m.id) FROM '.__CLASS__.' m WHERE m.user_id = :user_id AND (m.is_read = 0 OR m.is_read IS NULL)')
            ->setParameter('user_id', $user_id)
            ->getSingleScalarResult();
        
        return $num_unread;
    }
}