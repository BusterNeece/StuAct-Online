<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="forms_camps_programs")
 * @Entity
 * @HasLifecycleCallbacks
 */
class CampsFormProgram extends \DF\Doctrine\Entity
{
    const UPDATE_THRESHOLD = 15552000;

	public function __construct()
	{
		$this->users = new ArrayCollection;
        $this->forms = new ArrayCollection;

        $this->created_at = $this->updated_at = new \DateTime('NOW');
	}

    /** @PreUpdate **/
    public function updated()
    {
        $this->updated_at = new \DateTime('NOW');
    }
	
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="org_id", type="integer", nullable=true) */
    protected $org_id;

    /** @Column(name="name", type="string", length=300, nullable=true) */
    protected $name;

    /** @Column(name="description", type="text", nullable=true) */
    protected $description;

    /** @Column(name="web", type="string", length=300, nullable=true) */
    protected $web;

    /** @Column(name="sponsor", type="string", length=20, nullable=true) */
    protected $sponsor;

    /** @Column(name="sponsoring_dept", type="string", length=100, nullable=true) */
    protected $sponsoring_dept;

    /** @Column(name="cpm_sponsor_name", type="string", length=150, nullable=true) */
    protected $cpm_sponsor_name;

    /** @Column(name="cpm_sponsor_email", type="string", length=100, nullable=true) */
    protected $cpm_sponsor_email;

    /** @Column(name="cpm_sponsor_phone", type="string", length=20, nullable=true) */
    protected $cpm_sponsor_phone;

    /** @Column(name="cpm_sponsor_cell", type="string", length=20, nullable=true) */
    protected $cpm_sponsor_cell;

    /** @Column(name="cpm_contact_name", type="string", length=150, nullable=true) */
    protected $cpm_contact_name;

    /** @Column(name="cpm_contact_email", type="string", length=100, nullable=true) */
    protected $cpm_contact_email;

    /** @Column(name="cpm_contact_phone", type="string", length=20, nullable=true) */
    protected $cpm_contact_phone;

    /** @Column(name="cpm_contact_cell", type="string", length=20, nullable=true) */
    protected $cpm_contact_cell;

    /** @Column(name="created_at", type="datetime", nullable=true) */
    protected $created_at;

    /** @Column(name="updated_at", type="datetime", nullable=true) */
    protected $updated_at;

    public function isUpToDate()
    {
        if (!$this->updated_at)
            return false;

        $timestamp = $this->updated_at->getTimestamp();
        $threshold = time() - self::UPDATE_THRESHOLD;
        return ($timestamp >= $threshold);
    }

    /**
     * @ManyToMany(targetEntity="CampsFormUser", mappedBy="programs")
     */
    protected $users;

    /**
     * @OneToMany(targetEntity="CampsForm", mappedBy="program")
     * @OrderBy({"time_created" = "DESC"})
     */
    protected $forms;

    /**
     * @ManyToOne(targetEntity="Organization")
     * @JoinColumns({
     *   @JoinColumn(name="org_id", referencedColumnName="org_id", onDelete="CASCADE")
     * })
     */
    protected $organization;
}