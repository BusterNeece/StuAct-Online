<?php
namespace Entity;

/**
 * LeadershipLibraryCategory
 *
 * @Table(name="leadership_library_category")
 * @Entity
 */
class LeadershipLibraryCategory extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->books = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=255, nullable=true) */
    protected $name;

    /**
     * @ManyToMany(targetEntity="Entity\LeadershipLibraryBook", mappedBy="categories")
     */
    protected $books;
    
    /**
     * Static Functions
     */
    
    public static function fetchSelect()
    {
        $em = \Zend_Registry::get('em');
        $all_categories = $em->createQuery('SELECT llc FROM '.__CLASS__.' llc ORDER BY llc.name ASC')
            ->getArrayResult();
        
        $cat_select = array();
        foreach((array)$all_categories as $cat)
        {
            $cat_select[$cat['id']] = $cat['name'];
        }
        
        return $cat_select;
    }
}