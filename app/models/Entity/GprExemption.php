<?php
namespace Entity;

/**
 * @Table(name="gpr_exemptions")
 * @Entity
 */
class GprExemption extends \DF\Doctrine\Entity
{
    const TYPE_TEMPORARY = 0;
    const TYPE_PERMANENT = 1;

    public function __construct()
    {
        $this->type = self::TYPE_TEMPORARY;
        $this->timestamp = time();
    }

    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="org_id", type="integer") */
    protected $org_id;

    /** @Column(name="uin", type="integer") */
    protected $uin;

    /** @Column(name="user_id", type="integer", nullable=true) */
    protected $user_id;

    /** @Column(name="type", type="smallint") */
    protected $type;

    /** @Column(name="timestamp", type="integer") */
    protected $timestamp;

    /**
     * @ManyToOne(targetEntity="Entity\Organization")
     * @JoinColumn(name="org_id", referencedColumnName="org_id")
     */
    protected $organization;

    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;
}