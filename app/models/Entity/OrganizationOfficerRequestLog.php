<?php
namespace Entity;


/**
 * OrganizationOfficerRequestLog
 *
 * @Table(name="organization_officer_requests_log")
 * @Entity
 */
class OrganizationOfficerRequestLog extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="request_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    protected $request_id;

    /** @Column(name="org_id", type="integer") */
    protected $org_id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="off_position", type="integer", length=5) */
    protected $off_position;

    /** @Column(name="is_approved", type="integer", length=1) */
    protected $is_approved;

    /** @Column(name="processing_user_id", type="integer", length=4) */
    protected $processing_user_id;

    /** @Column(name="processing_timestamp", type="integer", length=4) */
    protected $processing_timestamp;

    /**
     * @ManyToOne(targetEntity="Entity\Organization")
     * @JoinColumn(name="org_id", referencedColumnName="org_id")
     */
    protected $organization;
}