<?php
namespace Entity;


/**
 * AdvisorDirectory
 *
 * @Table(name="advisor_directory")
 * @Entity
 * @HasLifecycleCallbacks
 */
class AdvisorDirectory extends \DF\Doctrine\Entity
{
	public function __construct()
    {
        $this->created_at = $this->updated_at = new \DateTime("now");
    }
    
    /** @PreUpdate */
    public function updated()
    {
        $this->updated_at = new \DateTime("now");
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="types", type="json", nullable=true) */
    protected $types;

    /** @Column(name="summary", type="text", nullable=true) */
    protected $summary;

    /** @Column(name="interests", type="text", nullable=true) */
    protected $interests;

    /** @Column(name="availability", type="text", nullable=true) */
    protected $availability;

    /** @Column(name="location", type="text", nullable=true) */
    protected $location;

    /** @Column(name="role", type="text", nullable=true) */
    protected $role;

    /** @Column(name="advising_style", type="text", nullable=true) */
    protected $advising_style;

    /** @Column(name="comments", type="text", nullable=true) */
    protected $comments;

    /** @Column(name="is_private", type="integer", length=1, nullable=true) */
    protected $is_private;
    
    /** @Column(name="created_at", type="datetime") */
    protected $created_at;
    
    /** @Column(name="updated_at", type="datetime") */
    protected $updated_at;
	
    /**
     * @OneToOne(targetEntity="Entity\User", mappedBy="AdvisorDirectory")
     * @JoinColumn(name="user_id", referencedColumnName="user_id", unique=true)
     */
    protected $user;
}