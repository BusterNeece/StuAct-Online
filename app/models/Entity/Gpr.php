<?php
namespace Entity;


/**
 * Gpr
 *
 * @Table(name="gprs")
 * @Entity
 */
class Gpr extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="uin_hash", type="string", length=64)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    protected $uin_hash;

    /** @Column(name="semester1", type="float") */
    protected $semester1;

    /** @Column(name="semester2", type="float") */
    protected $semester2;

    /** @Column(name="cumulative", type="float") */
    protected $cumulative;

    /** @Column(name="status", type="string", length=3) */
    protected $status;
}