<?php
namespace Entity;

use \Entity\Event;
use \Entity\EventType;
use \Entity\User;
use \Entity\Training;

/**
 * EventAttendee
 *
 * @Table(name="event_attendees")
 * @Entity
 */
class EventAttendee extends \DF\Doctrine\Entity
{
    /** @Column(name="event_id", type="integer") */
    protected $event_id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="confirmed", type="integer", length=1, nullable=true) */
    protected $confirmed;

    /** @Column(name="is_processed", type="integer", length=1, nullable=true) */
    protected $is_processed;
    
    /** @Column(name="training_id", type="integer", nullable=true) */
    protected $training_id;
    
    /**
     * @ManyToOne(targetEntity="Entity\Event", inversedBy="attendees")
     * @Id
     * @JoinColumn(name="event_id", referencedColumnName="event_id")
     */
    protected $event;
    
    /**
     * @ManyToOne(targetEntity="Entity\User", inversedBy="event_attendance")
     * @Id
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;
    
    /**
     * @OneToOne(targetEntity="Entity\Training", orphanRemoval=true)
     * @JoinColumn(name="training_id", referencedColumnName="id")
     */
    protected $training;
    
    /**
     * Static Functions
     */
    
    public static function addAttendee($event, $user, $confirmed)
    {
        $ea = self::getRepository()->findOneBy(array(
            'event_id' => $event['id'],
            'user_id' => $user['id']
        ));
        
        if (!($ea instanceof self))
        {
            $ea = new self;
            $ea->user = $user;
            $ea->event = $event;
        }
        
        $ea->confirmed = ($confirmed) ? 1 : 0;
        
        $event_type = $event['type'];
        if ($ea->confirmed == 1 && $event_type['training_credit'] && !($ea->training instanceof Training))
        {
            $training_obj = Training::assignCredit($user, $event_type['training_credit']);
            $ea->training = $training_obj;
        }
        else if ($ea->confirmed == 0 && $ea->training instanceof Training)
        {
            $ea->training = NULL;
        }

        $message = $event_type->confirmation_message;
        if (!empty($message) && $ea->confirmed == 1)
        {
            \DF\Messenger::send(array(
                'to'        => $user['email'],
                'subject'   => 'Event Attendance Confirmed',
                'module'    => 'events',
                'template'  => 'confirmation',
                'vars'      => array(
                    'message' => $message,
                    'event' => $event->type->name,
                ),
            ));
        }
        
        $ea->save();
    }
    
    public static function deleteAttendee($event, $user)
    {
        $ea = self::getRepository()->findOneBy(array(
            'event_id' => $event['id'],
            'user_id' => $user['id']
        ));
        
        if ($ea instanceof self)
        {
            $ea->delete();
        }
    }
}