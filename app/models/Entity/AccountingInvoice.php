<?php
namespace Entity;


/**
 * AccountingInvoice
 *
 * @Table(name="accounting_invoices")
 * @Entity
 */
class AccountingInvoice extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="invoice_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $invoice_id;

    /** @Column(name="invoice_num", type="string", length=50, nullable=true) */
    protected $invoice_num;

    /** @Column(name="invoice_timestamp", type="integer", length=4, nullable=true) */
    protected $invoice_timestamp;

    /** @Column(name="invoice_type", type="string", length=50, nullable=true) */
    protected $invoice_type;

    /** @Column(name="invoice_data", type="text", nullable=true) */
    protected $invoice_data;
}