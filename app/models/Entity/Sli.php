<?php
namespace Entity;

/**
 * Sli
 *
 * @Table(name="sli")
 * @Entity
 */
class Sli extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="entry_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $entry_id;

    /** @Column(name="user_id", type="integer", length=4) */
    protected $user_id;

    /** @Column(name="entry_timestamp", type="integer", length=4) */
    protected $entry_timestamp;

    /** @Column(name="entry_data", type="text", nullable=true) */
    protected $entry_data;
    
    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;
}