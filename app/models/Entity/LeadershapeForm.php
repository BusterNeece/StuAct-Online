<?php
namespace Entity;


/**
 * LeadershapeForm
 *
 * @Table(name="forms_leadershape")
 * @Entity
 */
class LeadershapeForm extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="form_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $form_id;

    /** @Column(name="form_timestamp", type="integer", length=4) */
    protected $form_timestamp;

    /** @Column(name="student_name", type="string", length=255, nullable=true) */
    protected $student_name;

    /** @Column(name="student_email", type="string", length=255, nullable=true) */
    protected $student_email;

    /** @Column(name="student_phone", type="string", length=255, nullable=true) */
    protected $student_phone;

    /** @Column(name="nominator_name", type="string", length=255, nullable=true) */
    protected $nominator_name;

    /** @Column(name="nominator_department", type="string", length=255, nullable=true) */
    protected $nominator_department;

    /** @Column(name="nominator_email", type="string", length=255, nullable=true) */
    protected $nominator_email;

    /** @Column(name="nominator_phone", type="string", length=255, nullable=true) */
    protected $nominator_phone;
}