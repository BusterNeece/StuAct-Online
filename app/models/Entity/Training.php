<?php
namespace Entity;


/**
 * Training
 *
 * @Table(name="training")
 * @Entity
 * @HasLifecycleCallbacks
 */
class Training extends \DF\Doctrine\Entity
{
	public function __construct()
    {
        $this->created_at = $this->updated_at = new \DateTime("now");
        $this->is_current = 1;
    }
    
    /** @PreUpdate */
    public function updated()
    {
        $this->updated_at = new \DateTime("now");
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="custom_item_id", type="integer", nullable=true) */
    protected $custom_item_id;

    /** @Column(name="type", type="string", length=50, nullable=true) */
    protected $type;

    /** @Column(name="name", type="string", length=255, nullable=true) */
    protected $name;

    /** @Column(name="credits", type="integer", length=2) */
    protected $credits;
    
    /** @Column(name="is_current", type="integer", length=2) */
    protected $is_current;
    
    /** @Column(name="created_at", type="datetime") */
    protected $created_at;
    
    /** @Column(name="updated_at", type="datetime") */
    protected $updated_at;
    
    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    /**
     * @ManyToOne(targetEntity="Entity\TrainingCustomItem")
     * @JoinColumn(name="custom_item_id", referencedColumnName="id")
     */
    protected $custom_item;

    public function getTypeText()
    {
        $config = \Zend_Registry::get('module_config');
        $all_modules = $config['training']->modules->toArray();

        return $all_modules[$this->name]['name'];
    }

    /**
     * Static Functions
     */
    
    public static function updateUserStatus($user)
    {
        $training_components = self::getUserTrainingComponents($user);
        $is_user_trained = ($training_components['training_met']);
        
        if ($is_user_trained)
        {
            // Update the user's training flag.
            $user->flag_training = 1;
            $user->flag_training_timestamp = time();
            $user->save();
            
            // Mark all training "not current".
            $em = \Zend_Registry::get('em');
            $update_query = $em->createQuery('UPDATE \Entity\Training t SET t.is_current = 0 WHERE t.user_id = :user_id')
                ->setParameter('user_id', $user->id)
                ->execute();
        }
    }
    
    public static function getUserTrainingComponents($user)
    {
        $settings = self::loadSettings();

        $leader_status = $user->getLeaderStatus();
        $type = $leader_status['type'];
        
        $training_threshold = time() - 86400 * 400;
        
        $em = \Zend_Registry::get('em');
        $training_items_raw = $em->createQuery('SELECT t FROM '.__CLASS__.' t WHERE t.user_id = :user_id AND t.type = :type AND t.created_at >= :threshold AND t.is_current = 1 GROUP BY t.name ORDER BY t.created_at DESC')
            ->setParameter('user_id', $user->id)
            ->setParameter('type', $type)
            ->setParameter('threshold', date('Y-m-d h:i:s', $training_threshold))
            ->getArrayResult();
        
        $training_items = array();
        foreach($training_items_raw as $training_item)
        {
            $training_name = $training_item['name'];
            
            if (isset($settings['modules'][$training_name]))
            {
                $training_items[$training_name] = array_merge($training_item, $settings['modules'][$training_name]);
            }
        }
        
        $training_data = array(
            'leader_type'       => $type,
            'leader_status'     => $leader_status,
            'items'             => $training_items,
            
            'required_needed'   => 0,
            'required_current'  => 0,
            'required_met'      => FALSE,
            
            'elective_needed'   => 0,
            'elective_current'  => 0,
            'elective_met'      => FALSE,
            
            'batch_needed'      => 0,
            'batch_current'     => 0,
            'batch_met'         => FALSE,
            
            'training_met'      => FALSE,
        );
        
        // Calculate number of required items.
        foreach($settings['modules'] as $module_name => $module_info)
        {
            if ($module_info['type'] == $type && $module_info['is_required'])
            {
                if (!$module_info['batch'])
                    $training_data['required_needed'] += $module_info['credits'];
                else if (in_array($module_info['batch'], $leader_status['batch_groups']))
                    $training_data['batch_needed'] += $module_info['credits'];
            }
        }
        
        // Calculate number of items the user currently has.
        foreach($training_items as $item)
        {
            if ($item['batch'])
                $training_data['batch_current'] += $item['credits'];
            else if ($item['is_required'])
                $training_data['required_current'] += $item['credits'];
            else
                $training_data['elective_current'] += $item['credits'];
        }
        
        // Determine if user meets requirements.
        $training_data['required_met'] = ($training_data['required_current'] >= $training_data['required_needed']);
        
        $training_data['elective_needed'] = $settings['requirements'][$type][$leader_status['category']];
        $training_data['elective_met'] = ($training_data['elective_current'] >= $training_data['elective_needed']);
        
        $training_data['batch_met'] = ($training_data['batch_current'] >= $training_data['batch_needed']);
        
        $training_data['training_met'] = $training_data['required_met'] && $training_data['elective_met'] && $training_data['batch_met'];
        
        return $training_data;
    }
    
    public static function getVisibleTraining($user)
    {
        $training_components = self::getUserTrainingComponents($user);
        $leader_type = $training_components['leader_type'];
        $leader_status = $training_components['leader_status'];
        
        $module_config = \Zend_Registry::get('module_config');
        $training_config_raw = $module_config['training']->modules->toArray();
        
        $visible_modules = array();
        
        // Calculate number of required items.
        foreach($training_config_raw as $module_name => $module_info)
        {
            $module_info['is_taken'] = (isset($training_components['items'][$module_name]));
            $module_info['taken_timestamp'] = ($module_info['is_taken']) ? $training_components['items'][$module_name]['created_at']->getTimestamp() : NULL;
            
            if ($module_info['type'] == $leader_type)
            {
                if (!$module_info['batch'] || in_array($module_info['batch'], $leader_status['batch_groups']))
                    $visible_modules[$module_name] = $module_info;
            }
        }
        
        return $visible_modules;
    }
    
    public static function getVisibleTrainingByCategory($user)
    {
        $training = self::getVisibleTraining($user);
        $training_by_category = array(
            'all' => $training,
        );
        
        foreach((array)$training as $module_name => $module_info)
        {
            if ($module_info['batch'])
                $training_by_category['batch'][$module_name] = $module_info;
            else if ($module_info['is_required'])
                $training_by_category['required'][$module_name] = $module_info;
            else
                $training_by_category['elective'][$module_name] = $module_info;
        }
        
        return $training_by_category;
    }
    
    public static function assignCredit(User $user, $module_name)
    {
        $module_config = \Zend_Registry::get('module_config');
        $training_config_raw = $module_config['training']->modules->toArray();
        $training_info = $training_config_raw[$module_name];
        
        $training = new self();
        $training->user = $user;
        $training->type = $training_info['type'];
        $training->name = $module_name;
        $training->credits = ($training_info['credits']) ? (int)$training_info['credits'] : 1;
        $training->save();
        
        self::updateUserStatus($user);
        
        return $training;
    }

    public static function assignCustomCredit(TrainingCustomItem $item)
    {
        $training = new self();
        $training->user = $item->user;
        $training->custom_item = $item;
        $training->type = 'custom';
        $training->credits = $item->credits;
        $training->save();
        
        self::updateUserStatus($item->user);
        
        return $training;
    }

    public static function loadSettings()
    {
        static $settings;

        if (!$settings)
        {
            $module_config = \Zend_Registry::get('module_config');
            $training_module = $module_config['training'];

            $settings = $training_module->general->toArray();
            $settings['modules'] = $training_module->modules->toArray();
            $settings['requirements'] = $training_module->requirements->toArray();
        }

        return $settings;
    }
}