<?php
namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="forms_pep", indexes={@index(name="search_idx", columns={"app_id_hash","event_name","event_starttime","event_endtime"})})
 * @Entity
 * @HasLifecycleCallbacks
 */

class PreEventForm extends \DF\Doctrine\Entity
{
    const PHASE_INITIAL         = 1;
    const PHASE_MODIFY          = 2;
    const PHASE_ADVISOR         = 4;
    const PHASE_DEPT_ACTIVE     = 6;
    const PHASE_DEPT_ARCHIVED   = 7;
    const PHASE_DELETED         = 10;

    const VERSION_LEGACY        = 0;
    const VERSION_DFFORM        = 1;

    public function __construct()
    {
        $this->logs = new ArrayCollection;
        $this->phase = self::PHASE_INITIAL;
        $this->timestamp = time();
        $this->time_read = 0;
        $this->version = self::VERSION_DFFORM;
    }

    /** @PostPersist */
    public function initialize()
    {
        // Generate access code.
        $this->access_code = 'PEP-'.substr(strtoupper(sha1('PreEventForm_'.$this->id)), 0, 5).'-'.str_pad($this->id, 5, '0', STR_PAD_LEFT);

        $user_position = \Entity\OrganizationOfficer::getUserPosition($this->user, $this->organization);

        $this->data = array(
            'Logistics' => array(
                'Org_name'          => $this->organization->name,
                'Submitter_name'    => $this->user->firstname.' '.$this->user->lastname,
                'Submitter_position' => ($user_position) ? $user_position->name : '',
                'Submitter_phone'   => $this->user->phone,
                'Email_address'     => $this->user->email,
            ),
        );

        $this->save();
    }
    
    /**
     * @Column(name="app_id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="app_id_hash", type="string", length=255, nullable=true) */
    protected $access_code;

    /** @Column(name="app_org_id", type="integer", nullable=true) */
    protected $org_id;

    /** @Column(name="app_user_id", type="integer", nullable=true) */
    protected $user_id;

    /** @Column(name="version", type="smallint", nullable=true) */
    protected $version;

    public function getVersion()
    {
        return (int)$this->version;
    }

    /** @Column(name="app_data", type="array", nullable=true) */
    protected $data;

    public function setData($data)
    {
        $this->data = $data;
        $this->reloadIndex();
    }

    public function replaceData($section, $data)
    {
        return $this->_updateSection($section, $data, FALSE);
    }
    public function appendData($section, $data)
    {
        return $this->_updateSection($section, $data, TRUE);
    }
    
    protected function _updateSection($section, $data, $append = TRUE)
    {
        $all_data = (array)$this->data;
        if (!$append || !isset($all_data[$section]))
            $all_data[$section] = array();

        if (!empty($data))
            $all_data[$section] = array_merge((array)$all_data[$section], (array)$data);

        $this->setData($all_data);

        return $this;
    }

    /** @Column(name="app_notes", type="text", nullable=true) */
    protected $notes;

    /** @Column(name="app_claimant_user_id", type="integer", length=4, nullable=true) */
    protected $claimant_user_id;

    /** @Column(name="app_timestamp", type="integer", length=4, nullable=true) */
    protected $timestamp;

    /** @Column(name="app_time_read", type="integer", length=4, nullable=true) */
    protected $time_read;

    /** @Column(name="app_time_phase_changed", type="integer", length=4, nullable=true) */
    protected $time_phase_changed;

    /** @Column(name="app_is_completed", type="boolean", nullable=true) */
    protected $is_completed;

    /** @Column(name="app_is_archived", type="boolean", nullable=true) */
    protected $is_archived;

    /** @Column(name="app_phase", type="smallint", nullable=true) */
    protected $phase;

    public function setPhase($phase)
    {
        $old_phase = $this->phase;
        $this->phase = $phase;

        if ($old_phase != $phase)
        {
            $this->notify($phase);

            $phase_names = self::getPhaseNames();
            $old_phase_name = $phase_names[$old_phase];
            $new_phase_name = $phase_names[$phase];

            $log = new PreEventFormLog;
            $log->form = $this;
            $log->note = 'Routing step changed from "'.$old_phase_name.'" to "'.$new_phase_name.'".';
            $log->save();
        }
    }

    public function getPhaseName()
    {
        $phase_names = self::getPhaseNames();
        return $phase_names[$this->phase];
    }

    /** @Column(name="app_reminder_time", type="integer", length=4, nullable=true) */
    protected $reminder_time;

    /** @Column(name="app_is_reminder_sent", type="boolean", nullable=true) */
    protected $is_reminder_sent;

    /** @Column(name="app_is_open_event", type="boolean", nullable=true) */
    protected $is_open_event;

    /** @Column(name="app_insurance", type="boolean", nullable=true) */
    protected $insurance;
    
    /** @Column(name="event_name", type="string", length=300, nullable=true) */
    protected $index_name;
    
    /** @Column(name="event_starttime", type="integer", length=4, nullable=true) */
    protected $index_starttime;
    
    /** @Column(name="event_endtime", type="integer", length=4, nullable=true) */
    protected $index_endtime;

    public function reloadIndex()
    {
        if ($this->getVersion() == self::VERSION_DFFORM)
        {
            $data = (array)$this->data;
            
            $this->index_name = $data['Logistics']['Event_name'];
            $this->index_starttime = $data['Logistics']['Date_Event_start'];
            $this->index_endtime = $data['Logistics']['Date_Event_end'];
        }
    }
    
    /** @OneToMany(targetEntity="Entity\PreEventFormLog", mappedBy="form", cascade={"remove"}) */
    protected $logs;

    /**
     * @ManyToOne(targetEntity="Entity\Organization")
     * @JoinColumn(name="app_org_id", referencedColumnName="org_id")
     */
    protected $organization;

    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="app_user_id", referencedColumnName="user_id")
     */
    protected $user;

    public function renderView()
    {
        $sections = $this->getViewData();

        $output = '';
        foreach((array)$sections as $section_name => $section_html)
        {
            $output .= '<h2>'.$section_name.'</h2>'.$section_html;
        }

        return $output;
    }

    public function getViewData()
    {
        $output = array();

        if ($this->getVersion() == self::VERSION_DFFORM)
        {
            $module_config = \Zend_Registry::get('module_config');
            $settings = self::loadSettings();
            $steps = $this->getSteps();

            $data = (array)$this->data;
            $output = array();

            foreach($data as $step_name => $step_data)
            {
                if (!empty($step_data))
                {
                    if (in_array($step_name, $steps))
                    {
                        $config_file = 'step_'.strtolower($step_name);
                        $form_config = $module_config['form_preevent']->forms->{$config_file}->toArray();
                        unset($form_config['groups']['submit_group']);

                        $form = new \DF\Form($form_config);
                        $form->setDefaults($step_data);
                        $output[$step_name] = $form->renderView();
                    }
                    else
                    {
                        $output[$step_name] = \StuAct\Form\Legacy::renderView($step_data, TRUE, TRUE);
                    }
                }
            }

            return $output;
        }
        else
        {
            return \StuAct\Form\Legacy::renderView((array)$this->data, TRUE);
        }
    }

    public function getSteps()
    {
        $settings = self::loadSettings();
        $steps = (array)$settings['steps'];

        if ($this->organization->batch_group)
        {
            $batch = $this->organization->batch_group;
            $short_name = $batch->short_name;

            if (isset($settings['batch'][$short_name]))
                $steps = array_merge($steps, (array)$settings['batch'][$short_name]);
        }

        return $steps;
    }

    public function hasErrors()
    {
        $output = array();

        if ($this->getVersion() == self::VERSION_DFFORM)
        {
            $module_config = \Zend_Registry::get('module_config');
            $settings = self::loadSettings();
            $steps = $this->getSteps();

            $errors = array();

            foreach($steps as $step_name)
            {
                $step_data = (array)$this->data[$step_name];

                $config_file = 'step_'.strtolower($step_name);
                $form_config = $module_config['form_preevent']->forms->{$config_file}->toArray();

                $form = new \DF\Form($form_config);

                if (!$form->isValid($step_data))
                {
                    $form_errors = $form->getMessages(null, true);

                    if ($form_errors)
                        $errors[$step_name] = $form_errors;
                }
            }

            if ($errors)
                return $errors;
        }

        return FALSE;
    }

    public function notify($phase = NULL)
    {
        if ($phase === NULL)
            $phase = $this->phase;

        $settings = self::loadSettings();

        switch($phase)
        {
            // Notify the group's advisor that the form requires review.
            case self::PHASE_ADVISOR:
                $this->organization->notify('preeventplanning_advisor', array(
                    'id'        => $this->access_code,
                    'name'      => $this->index_name,
                ));
            break;

            // Notify the submitter that their form requires modification.
            case self::PHASE_MODIFY:
                $this->organization->notify('preeventplanning_decline', array(
                    'id'                    => $this->access_code,
                    'modifications_needed'  => $this->data['Advisor_Notes']['Advisor_Notes'],
                    'fields'                => $this->renderView(),
                    'email'                 => $this->user->email,
                ));
            break;

            case self::PHASE_DEPT_ACTIVE:
                if ($this->organization->batch_group)
                    $batch_type = $this->organization->batch_group->short_name;

                if (isset($settings['notify_admin'][$batch_type]))
                    $email_to = (array)$settings['notify_admin'][$batch_type];
                else
                    $email_to = (array)$settings['notify_admin']['none'];

                $email_to = array_merge(array($this->user->email), $email_to);

                // Send a message containing the truncated data.
                \DF\Messenger::send(array(
                    'to'        => $email_to,
                    'subject'   => 'Pre-Event Planning Form Submission',
                    'module'    => 'form_preevent',
                    'template'  => 'form',
                    'vars'      => array(
                        'record'    => $this,
                        'html'      => $this->renderView(),
                    ),
                ));
            break;

            default:
            break;
        }
    }

    /**
     * Static Functions
     */
    
    public static function loadSettings()
    {
        static $settings;

        if ($settings === NULL)
        {
            $module_config = \Zend_Registry::get('module_config');
            $settings = $module_config['form_preevent']->general->toArray();
        }

        return $settings;
    }
    public static function getPhaseNames()
    {
        return array(
            self::PHASE_INITIAL          => 'Incomplete',
            self::PHASE_MODIFY           => 'Modificiations Needed',
            self::PHASE_ADVISOR          => 'Advisor Approval Needed',
            self::PHASE_DEPT_ACTIVE      => 'Department Review (Active)',
            self::PHASE_DEPT_ARCHIVED    => 'Department Review (Archived)',
            self::PHASE_DELETED          => 'Deleted',
        );
    }
}