<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="strengths_seminars")
 * @Entity
 */
class StrengthsSeminar extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->submissions = new ArrayCollection;
    }

    /**
     * @Column(name="id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=255, nullable=true) */
    protected $name;

    /** @Column(name="event_time", type="integer") */
    protected $event_time;

    /** @Column(name="is_open", type="boolean") */
    protected $is_open;

    /** @OneToMany(targetEntity="Entity\StrengthsSubmission", mappedBy="seminar") */
    protected $submissions;

    /**
     * Static Functions
     */
    
    public static function fetchSelect($add_blank = false, $open_only = true)
    {
        $em = \Zend_Registry::get('em');

        if ($open_only)
            $records = $em->createQuery('SELECT ss FROM '.__CLASS__.' ss WHERE ss.is_open = 1 ORDER BY ss.event_time ASC')->getArrayResult();
        else
            $records = self::fetchArray('event_time');
        
        $select = array();
        if ($add_blank)
            $select[''] = 'N/A (No Seminar)';
        
        foreach($records as $record)
        {
            $select[$record['id']] = $record['name'].' ('.date('F j, Y', $record['event_time']).')';
        }
        
        return $select;
    }
}