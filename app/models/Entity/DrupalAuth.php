<?php
namespace Entity;

/**
 * DrupalAuth
 *
 * @Table(name="drupal_auth")
 * @Entity
 */
class DrupalAuth extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="string", length=200)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    protected $id;

    /** @Column(name="account_id", type="string", length=200) */
    protected $account_id;

    /** @Column(name="timestamp", type="integer", length=4) */
    protected $timestamp;
}