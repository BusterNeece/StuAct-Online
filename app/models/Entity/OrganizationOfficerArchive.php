<?php
namespace Entity;

/**
 * OrganizationOfficerArchive
 *
 * @Table(name="organization_officers_archive", indexes={@index(name="search_idx", columns={"uin","position"})})
 * @Entity
 */
class OrganizationOfficerArchive extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->date_start = time();
        $this->date_end = 0;
    }

    /**
     * @Column(name="assoc_id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $assoc_id;

    /** @Column(name="uin", type="integer") */
    protected $uin;

    /** @Column(name="org_id", type="integer") */
    protected $org_id;

    /** @Column(name="position", type="integer") */
    protected $position;

    /** @Column(name="name", type="string", length=255, nullable=true) */
    protected $name;

    /** @Column(name="email", type="string", length=255, nullable=true) */
    protected $email;

    /** @Column(name="date_start", type="integer") */
    protected $date_start;

    /** @Column(name="date_end", type="integer") */
    protected $date_end;

    /**
     * @ManyToOne(targetEntity="Entity\Organization")
     * @JoinColumn(name="org_id", referencedColumnName="org_id")
     */
    protected $organization;
}