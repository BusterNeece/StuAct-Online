<?php
namespace Entity;

/**
 * @Table(name="cname_requests")
 * @Entity
 */
class CnameRequest extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="request_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="domain", type="string", length=255) */
    protected $domain;

    /** @Column(name="old_location", type="integer", length=2, nullable=true) */
    protected $old_location;

    /** @Column(name="new_location", type="integer", length=2, nullable=true) */
    protected $new_location;
}