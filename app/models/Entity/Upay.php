<?php
namespace Entity;


/**
 * Upay
 *
 * @Table(name="upay")
 * @Entity
 */
class Upay extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="record_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $record_id;

    /** @Column(name="trans_id", type="string", length=200, nullable=true) */
    protected $trans_id;

    /** @Column(name="user_id", type="string", length=50, nullable=true) */
    protected $user_id;

    /** @Column(name="time_created", type="integer", length=4, nullable=true) */
    protected $time_created;

    /** @Column(name="time_updated", type="integer", length=4, nullable=true) */
    protected $time_updated;

    /** @Column(name="payment_status", type="string", length=200, nullable=true) */
    protected $payment_status;

    /** @Column(name="payment_amount", type="float", nullable=true) */
    protected $payment_amount;

    /** @Column(name="payment_card_type", type="string", length=200, nullable=true) */
    protected $payment_card_type;

    /** @Column(name="payment_name", type="string", length=255, nullable=true) */
    protected $payment_name;

    /** @Column(name="payment_order_id", type="integer", length=8, nullable=true) */
    protected $payment_order_id;

    /** @Column(name="payment_internal_trans_id", type="string", length=50, nullable=true) */
    protected $payment_internal_trans_id;
}