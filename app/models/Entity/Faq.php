<?php
namespace Entity;


/**
 * Faq
 *
 * @Table(name="faqs")
 * @Entity
 */
class Faq extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="faq_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="faq_type", type="string", length=255, nullable=true) */
    protected $type;

    /** @Column(name="faq_question", type="string", nullable=true) */
    protected $question;

    /** @Column(name="faq_answer", type="text", nullable=true) */
    protected $answer;
}