<?php
namespace Entity;


/**
 * SponsAuth
 *
 * @Table(name="sponsauth")
 * @Entity
 * @HasLifecycleCallbacks
 */
class SponsAuth extends \DF\Doctrine\Entity
{
    /** @PostPersist */
    public function inserted()
    {
        $this->access_code = self::generateAccessCode($this->id);
        $this->save();
    }
    
    // Generate an access code for this form.
    public static function generateAccessCode($id)
    {
        $code = array();
        $code[] = strtoupper(substr(sha1('SponsAuthForm'.$id), 0, 5));
        $code[] = str_pad($id, 5, '0', STR_PAD_LEFT);
        
        return implode('-', $code);
    }
    
    /**
     * @Column(name="event_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="access_code", type="string", length=32, nullable=true) */
    protected $access_code;

    /** @Column(name="event_type", type="string", length=255, nullable=true) */
    protected $event_type;

    /** @Column(name="event_name", type="string", length=255, nullable=true) */
    protected $event_name;

    /** @Column(name="event_desc", type="text", nullable=true) */
    protected $event_desc;

    /** @Column(name="event_location", type="text", nullable=true) */
    protected $event_location;

    /** @Column(name="event_starttime", type="integer") */
    protected $event_starttime;

    /** @Column(name="event_endtime", type="integer") */
    protected $event_endtime;

    /** @Column(name="dept_name", type="string", length=255, nullable=true) */
    protected $dept_name;

    /** @Column(name="contact_name", type="string", length=255, nullable=true) */
    protected $contact_name;

    /** @Column(name="contact_dept", type="string", length=255, nullable=true) */
    protected $contact_dept;

    /** @Column(name="contact_email", type="string", length=255, nullable=true) */
    protected $contact_email;

    /** @Column(name="contact_phone", type="string", length=50, nullable=true) */
    protected $contact_phone;

    /** @Column(name="course_name", type="string", length=255, nullable=true) */
    protected $course_name;

    /** @Column(name="is_approved", type="integer", length=1, nullable=true) */
    protected $is_approved = 0;
    
    /** @Column(name="approved_by", type="integer", nullable=true) */
    protected $approved_by;
    
    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="approved_by", referencedColumnName="user_id")
     */
    protected $approver;
}