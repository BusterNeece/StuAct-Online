<?php
namespace Entity;


/**
 * CpanelStatistics
 *
 * @Table(name="cpanel_statistics")
 * @Entity
 */
class CpanelStatistics extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="username", type="string", length=255)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    protected $username;

    /**
     * @Column(name="statistic_month", type="string", length=6)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    protected $statistic_month;

    /** @Column(name="statistic_timestamp", type="integer", length=4) */
    protected $statistic_timestamp;

    /** @Column(name="statistic_data", type="text") */
    protected $statistic_data;


}