<?php
namespace Entity;


/**
 * Quiz
 *
 * @Table(name="quiz")
 * @Entity
 */
class Quiz extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->results = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="short_name", type="string", length=50) */
    protected $short_name;

    /** @Column(name="title", type="string", length=255) */
    protected $title;

    /** @Column(name="description", type="text") */
    protected $description;
    
    /**
     * @OneToMany(targetEntity="Entity\QuizQuestion", mappedBy="quiz", cascade={"remove"})
     * @OrderBy({"weight" = "ASC"})
     */
    protected $questions;
    
    /**
     * @OneToMany(targetEntity="Entity\QuizResult", mappedBy="quiz", cascade={"remove"})
     */
    protected $results;
    
    public function registerCredit($user = NULL)
    {
        $user = (!is_null($user)) ? $user : \DF\Auth::getLoggedInUser();
        
        if ($user instanceof \Entity\User)
        {
            $quiz_result = new QuizResult();
            $quiz_result->quiz = $this;
            $quiz_result->user = $user;
            $quiz_result->save();
            
            return $quiz_result;
        }
        else
        {
            return NULL;
        }
    }
    
    public function hasPreviousCredit($user = NULL)
    {
        $user = (!is_null($user)) ? $user : \DF\Auth::getLoggedInUser();
        
        if (!($user instanceof User))
            return 0;
        
        try
        {
            $em = \Zend_Registry::get('em');
            $latest_quiz = $em->createQuery('SELECT qr FROM Entity\QuizResult qr WHERE qr.quiz = :quiz AND qr.user = :user ORDER BY qr.created_at DESC')
                ->setParameter('quiz', $this)
                ->setParameter('user', $user)
                ->setMaxResults(1)
                ->getSingleResult();
            
            return $latest_quiz->created_at->getTimestamp();
        }
        catch(\Doctrine\ORM\NoResultException $e)
        {
            return 0;
        }
    }
    
    public function getForm()
    {
        $form_config = array(
            'method'		=> 'post',
            'elements'		=> array(),
        );
        
        $questions = $this->_getQuestions();
        foreach($questions as $question_num => $question_info)
        {
            $field_config = array('radio', array(
                'label' => '<small>Question '.$question_num.':</small><br />'.$question_info['question'],
                'required' => true,
                'multiOptions' => $question_info['options'],
                'validators' => array(
                    array('QuizAnswer', FALSE, $question_info['correct']),
                ),
            ));
            
            if (!empty($question_info['url']))
            {
                $url = trim($question_info['url']);
                $field_config[1]['description'] = 'Additional information: <a href="'.$url.'" target="_blank">'.$url.'</a>';
            }
            
            $form_config['elements'][$question_info['field_name']] = $field_config;
        }
        
        $form_config['elements']['submit'] = array('submit', array(
            'type'	=> 'submit',
            'label'	=> 'Submit Answers',
            'helper' => 'formButton',
            'class' => 'ui-button',
        ));
        
        return new \DF\Form($form_config);
    }
    
    protected function _getQuestions()
    {
        $quiz_questions = array();
        
        if (count($this->questions) > 0)
        {
            $j = 1;
            
            foreach($this->questions as $question)
            {
                $quiz_question = array(
                    'field_name' => 'question_'.$j,
                    'question'	=> $question->question,
                    'url'       => $question->url,
                    'options'	=> array(),
                );
                
                $option_ids = array();
                $i = 1;
                foreach($question->options as $option)
                {
                    $option_letter = chr(64+$i);
                    $quiz_question['options'][$option_letter] = $option_letter.': '.$option->option_text;
                    
                    $option_ids[$option->id] = $option_letter;
                    $i++;
                }
                
                if ($question->correct_answer_id)
                {
                    $quiz_question['correct'] = $option_ids[$question->correct_answer_id];
                }
                
                $quiz_questions[$j] = $quiz_question;
                $j++;
            }
        }
        
        return $quiz_questions;
    }
    
    /**
     * Static functions
     */
    public static function fetchById($id)
    {
        return self::find($id);
    }
    
    public static function fetchByName($name)
    {
        return self::getRepository()->findOneBy(array('short_name' => $name));
    }
    
    public static function fetchSelect()
    {
        static $all_records;
        if( !isset($all_records) )
        {
            $em = \Zend_Registry::get('em');
            $all_records_raw = $em->createQuery('SELECT q FROM '.__CLASS__.' q ORDER BY q.title ASC')
                ->getArrayResult();
            
            $all_records = array();
            foreach($all_records_raw as $record)
            {
                $all_records[$record['id']] = $record['title'];
            }
        }
        
        return $all_records;
    }
	
    public static function fetchAll()
    {
        static $all_records;
        
        if( !isset($all_records) )
        {
            $em = \Zend_Registry::get('em');
            $all_records = $em->createQuery('SELECT q FROM '.__CLASS__.' q ORDER BY q.title ASC')
                ->execute();
        }
        
        return $all_records;
    }
}