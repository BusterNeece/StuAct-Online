<?php
namespace Entity;

/**
 * GprCache
 *
 * @Table(name="gpr_cache")
 * @Entity
 */
class GprCache extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="uin_hash", type="string", length=50)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    protected $uin_hash;

    /**
     * @Column(name="term", type="string", length=10)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    protected $term;

    /** @Column(name="gpa", type="float") */
    protected $gpa;

    /** @Column(name="cumul", type="float") */
    protected $cumul;

    /** @Column(name="classification", type="string", length=5) */
    protected $classification;
    
    public function setUinHash($uin)
    {
        $this->uin_hash = self::encodeUin($uin);
    }
    
    /**
     * Static Functions
     */
    
    public static function fetchEntry($uin, $term)
    {
        return self::getRepository()->findOneBy(array(
            'uin_hash'  => self::encodeUin($uin),
            'term'      => $term,
        ));
    }
    
    public static function getOrCreate($uin, $term)
    {
        $record = self::fetchEntry($uin, $term);
        
        if (!($record instanceof self))
        {
            $record = new self();
            $record->setUinHash($uin);
            $record->term = $term;
        }
        
        return $record;
    }
    
    public function clear($uin = NULL)
    {
        $em = \Zend_Registry::get('em');
        if ($uin === NULL)
        {
            $em->createQuery('DELETE FROM '.__CLASS__.' gc')
                ->execute();
        }
        else
        {
            $em->createQuery('DELETE FROM '.__CLASS__.' gc WHERE gc.uin_hash = :uin_hash')
                ->setParameter('uin_hash', self::encodeUin($uin))
                ->execute();
        }
    }
    
    public static function encodeUin($uin)
    {
        return \DF\Encryption::digest($uin);
    }
}