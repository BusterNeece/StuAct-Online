<?php

namespace Entity;

/**
 * Entity\Role
 *
 * @Table(name="role")
 * @Entity
 */
class Role extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->actions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @Column(name="name", type="string", length=100) */
    protected $name;

    /**
     * @ManyToMany(targetEntity="Entity\User", mappedBy="roles")
     */
    protected $users;
    
    /**
     * @ManyToMany(targetEntity="Entity\Action", inversedBy="roles")
     * @JoinTable(name="role_has_action",
     *      joinColumns={@JoinColumn(name="role_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="action_id", referencedColumnName="id")}
     * )
     */
    protected $actions;
    
    public static function fetchAll()
    {
        $em = \Zend_Registry::get('em');
        return $em->createQuery('SELECT r FROM '.__CLASS__.' r ORDER BY r.name ASC')
            ->execute();
    }
    
    public static function fetchSelect($add_blank = FALSE)
    {
        $roles_raw = self::fetchAll();
        $roles = array();
		
		if ($add_blank)
			$roles[0] = 'N/A';
		
        foreach((array)$roles_raw as $role)
        {
            $roles[$role['id']] = $role['name'];
        }
        return $roles;
    }
}