<?php
namespace Entity;

/**
 * @Table(name="camps")
 * @Entity
 */
class Camp extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="camp_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="camp_form_id", type="string", length=50) */
    protected $form_id;

    /** @Column(name="camp_name", type="string", length=255, nullable=true) */
    protected $name;

    /** @Column(name="contact_name", type="string", length=200, nullable=true) */
    protected $contact_name;

    /** @Column(name="contact_email", type="string", length=150, nullable=true) */
    protected $contact_email;

    /** @Column(name="contact_web", type="string", length=255, nullable=true) */
    protected $contact_web;

    /** @Column(name="date_start", type="integer", nullable=true) */
    protected $date_start;

    /** @Column(name="date_end", type="integer", nullable=true) */
    protected $date_end;
}