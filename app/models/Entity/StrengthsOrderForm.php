<?php
namespace Entity;

/**
 * StrengthsOrderForm
 *
 * @Table(name="forms_strengths_orders")
 * @Entity
 */
class StrengthsOrderForm extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="order_timestamp", type="integer", length=4, nullable=true) */
    protected $order_timestamp;

    /** @Column(name="order_type", type="string", length=50, nullable=true) */
    protected $order_type;

    /** @Column(name="contact_name", type="string", length=255, nullable=true) */
    protected $contact_name;

    /** @Column(name="contact_email", type="string", length=255, nullable=true) */
    protected $contact_email;

    /** @Column(name="contact_department", type="string", length=255, nullable=true) */
    protected $contact_department;

    /** @Column(name="contact_mailstop", type="string", length=255, nullable=true) */
    protected $contact_mailstop;

    /** @Column(name="contact_phone", type="string", length=255, nullable=true) */
    protected $contact_phone;

    /** @Column(name="contact_fax", type="string", length=255, nullable=true) */
    protected $contact_fax;

    /** @Column(name="reason_for_purchase", type="string", length=255, nullable=true) */
    protected $reason_for_purchase;

    /** @Column(name="num_codes", type="integer", length=4, nullable=true) */
    protected $num_codes;

    /** @Column(name="num_books", type="integer", length=4, nullable=true) */
    protected $num_books;

    /** @Column(name="price_books", type="float", nullable=true) */
    protected $price_books;

    /** @Column(name="price_codes", type="float", nullable=true) */
    protected $price_codes;

    /** @Column(name="is_tax_exempt", type="integer", length=1, nullable=true) */
    protected $is_tax_exempt;

    /** @Column(name="payment_method", type="string", length=255, nullable=true) */
    protected $payment_method;

    /** @Column(name="payment_check_num", type="string", length=255, nullable=true) */
    protected $payment_check_num;

    /** @Column(name="payment_dept_acct_num", type="string", length=255, nullable=true) */
    protected $payment_dept_acct_num;

    /** @Column(name="payment_sofc_acct_num", type="string", length=255, nullable=true) */
    protected $payment_sofc_acct_num;

    /** @Column(name="event_date", type="text", nullable=true) */
    protected $event_date;

    /** @Column(name="event_location", type="text", nullable=true) */
    protected $event_location;

    /** @Column(name="event_group_type", type="string", length=255, nullable=true) */
    protected $event_group_type;

    /** @Column(name="event_num_participants", type="string", length=255, nullable=true) */
    protected $event_num_participants;

    /** @Column(name="event_is_followup", type="integer", length=1, nullable=true) */
    protected $event_is_followup;

    /** @Column(name="event_expectations_individual", type="text", nullable=true) */
    protected $event_expectations_individual;

    /** @Column(name="event_expectations_group", type="text", nullable=true) */
    protected $event_expectations_group;

    /** @Column(name="event_strengths_reasons", type="text", nullable=true) */
    protected $event_strengths_reasons;

    /** @Column(name="event_scope", type="text", nullable=true) */
    protected $event_scope;

    /** @Column(name="event_group", type="text", nullable=true) */
    protected $event_group;

    /** @Column(name="event_participant_relationship", type="text", nullable=true) */
    protected $event_participant_relationship;

    /** @Column(name="event_additional_info", type="text", nullable=true) */
    protected $event_additional_info;

    /** @Column(name="event_possible_followups", type="text", nullable=true) */
    protected $event_possible_followups;

    /** @Column(name="admin_deposit_slip_number", type="string", length=255, nullable=true) */
    protected $admin_deposit_slip_number;

    /** @Column(name="admin_ipayment_number", type="string", length=255, nullable=true) */
    protected $admin_ipayment_number;

    /** @Column(name="admin_famis_posted_date", type="string", length=255, nullable=true) */
    protected $admin_famis_posted_date;

    /** @Column(name="admin_famis_posted_datestamp", type="integer", length=4, nullable=true) */
    protected $admin_famis_posted_datestamp;

    /** @Column(name="admin_famis_posted_fy", type="integer", length=2, nullable=true) */
    protected $admin_famis_posted_fy;

    /** @Column(name="admin_ipayment_posted_date", type="string", length=255, nullable=true) */
    protected $admin_ipayment_posted_date;

    /** @Column(name="admin_accounts", type="text", nullable=true) */
    protected $admin_accounts;

    /** @Column(name="admin_invoice_number", type="string", length=255, nullable=true) */
    protected $admin_invoice_number;

    /** @Column(name="admin_is_payment_received", type="integer", length=1, nullable=true) */
    protected $admin_is_payment_received;

    /** @Column(name="admin_is_posted_to_famis", type="integer", length=1, nullable=true) */
    protected $admin_is_posted_to_famis;

    /** @Column(name="admin_is_released", type="integer", length=1, nullable=true) */
    protected $admin_is_released;

    /** @Column(name="admin_are_codes_allocated", type="integer", length=1, nullable=true) */
    protected $admin_are_codes_allocated;

    /** @Column(name="admin_are_books_allocated", type="integer", length=1, nullable=true) */
    protected $admin_are_books_allocated;
    
    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * Static Functions
     */

    // Generate an invoice number from an order ID.
    public static function generateInvoiceNumber($order_id)
    {
        return 'SACT-SQ-'.str_pad($order_id, 5, '0', STR_PAD_LEFT);
    }
    
    // Determine the order ID when provided the invoice number.
    public static function generateOrderId($invoice_number)
    {
        return intval(str_replace('SACT-SQ-', '', $invoice_number));
    }
    
    // Generate an access code from an order ID.
    public static function generateAccessCode($order_id)
    {
        return substr(strtoupper(md5(self::generateInvoiceNumber($order_id))), 0, 5);
    }
}