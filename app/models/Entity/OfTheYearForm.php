<?php
namespace Entity;

/**
 * OfTheYearForm
 *
 * @Table(name="forms_oftheyear")
 * @Entity
 */
class OfTheYearForm extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->timestamp = time();
    }

    /**
     * @Column(name="app_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="app_data", type="json", nullable=true) */
    protected $data;

    /** @Column(name="app_type", type="string", length=200, nullable=true) */
    protected $type;

    /** @Column(name="app_timestamp", type="integer", length=4, nullable=true) */
    protected $timestamp;
}