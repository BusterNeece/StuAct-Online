<?php
namespace Entity;

/**
 * Conduct
 *
 * @Table(name="conduct")
 * @Entity
 */
class Conduct extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="uin_hash", type="string", length=64)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    protected $uin_hash;

    /** @Column(name="effective_start", type="integer", length=4) */
    protected $effective_start;

    /** @Column(name="effective_end", type="integer", length=4) */
    protected $effective_end;
    
    public function setUinHash($uin)
    {
        $this->_set('uin_hash', self::encodeUin($uin));
    }
    
    /**
     * Static Functions
     */
    
    public static function check($uin)
    {
        $em = self::getEntityManager();

        try
        {
            $result = $em->createQuery('SELECT c FROM '.__CLASS__.' c WHERE c.uin = :uin AND c.effective_start <= :time AND c.effective_end >= :time')
                ->setParameter('uin', $uin)
                ->setParameter('time', time())
                ->setMaxResults(1)
                ->getSingleResult();

            if ($result instanceof self)
                return false;
            else
                return true;
        }
        catch(\Exception $e)
        {
            return true;
        }
    }

    public static function fetchByUin($uin)
    {
        return self::getRepository()->findOneBy(array(
            'uin_hash'      => self::encodeUin($uin),
        ));
    }
    
    public static function getOrCreate($uin)
    {
        $record = self::fetchByUin($uin);
        
        if (!($record instanceof self))
        {
            $record = new self();
            $record->setUinHash($uin);
        }
        
        return $record;
    }
    
    public function clear($uin = NULL)
    {
        $em = \Zend_Registry::get('em');
        return $em->createQuery('DELETE FROM '.__CLASS__.' c WHERE c.uin_hash = :uin')
            ->setParameter('uin', self::encodeUin($uin))
            ->execute();
    }
    
    public static function encodeUin($uin)
    {
        return \DF\Encryption::digest($uin);
    }
}