<?php
namespace Entity;


/**
 * PowerSearch
 *
 * @Table(name="power_searches")
 * @Entity
 */
class PowerSearch extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="search_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $search_id;

    /** @Column(name="user_id", type="integer", length=4) */
    protected $user_id;

    /** @Column(name="search_name", type="string", length=255, nullable=true) */
    protected $search_name;

    /** @Column(name="search_data", type="text", nullable=true) */
    protected $search_data;


}