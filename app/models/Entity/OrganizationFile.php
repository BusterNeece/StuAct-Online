<?php
namespace Entity;


/**
 * OrganizationFile
 *
 * @Table(name="organization_files")
 * @Entity
 */
class OrganizationFile extends \DF\Doctrine\Entity
{
    const PRIVACY_NONE = 0;
    const PRIVACY_ADVISORS = 1;
    const PRIVACY_LEADERS = 2;

    public function __construct()
    {
        $this->privacy = self::PRIVACY_NONE;
        $this->timestamp = time();
    }

    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="org_id", type="integer") */
    protected $org_id;

    /** @Column(name="user_id", type="integer", nullable=true) */
    protected $user_id;

    /** @Column(name="file_name", type="string", length=255) */
    protected $name;

    /** @Column(name="file_title", type="string", length=255, nullable=true) */
    protected $title;

    /** @Column(name="file_description", type="text", nullable=true) */
    protected $description;

    /** @Column(name="file_type", type="string", length=200, nullable=true) */
    protected $type;

    /** @Column(name="file_privacy", type="integer", length=1, nullable=true) */
    protected $privacy;

    /** @Column(name="file_timestamp", type="integer", length=4, nullable=true) */
    protected $timestamp;

    /**
     * @ManyToOne(targetEntity="Entity\Organization", inversedBy="files")
     * @JoinColumn(name="org_id", referencedColumnName="org_id")
     */
    protected $organization;

    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    public function generateFilename($file_name_original)
    {
        $file_name_base = basename($file_name_original);
        $file_name_clean = preg_replace('#[^a-zA-Z0-9\_\.]#', '', $file_name_base);
        
        $file_name_new = $this->organization->id.'_'.date('Ymd_His').'_'.$file_name_clean;
        return $file_name_new;
    }

    public function getUrl()
    {
        if (defined('DF_UPLOAD_URL') && DF_UPLOAD_URL != '')
            return DF_UPLOAD_URL.'/'.$this->name;
        else
            return \DF\Url::content($this->name);
    }

    /**
     * Static Functions
     */

    public static function getPrivacyTypes()
    {
        return array(
            self::PRIVACY_NONE      => 'Full Access (All Advisors and Officers)',
            self::PRIVACY_ADVISORS  => 'Advisors Only',
            self::PRIVACY_LEADERS   => 'Advisors and Student Leaders Only',
        );
    }
    
}