<?php
namespace Entity;


/**
 * Training Assessments
 *
 * @Table(name="training_assessments")
 * @Entity
 */
class TrainingAssessment extends \DF\Doctrine\Entity
{
	public function __construct()
    {
        $this->created_at = new \DateTime("now");
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="user_id", type="integer", nullable=true) */
    protected $user_id;

    /** @Column(name="type", type="string", length=50, nullable=true) */
    protected $type;

    /** @Column(name="data", type="json") */
    protected $data;
    
    /** @Column(name="created_at", type="datetime") */
    protected $created_at;
    
    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;
}