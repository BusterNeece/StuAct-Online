<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="forms_camps_users")
 * @Entity
 */
class CampsFormUser extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->password_needs_reset = false;

        $this->programs = new ArrayCollection;
    }

    /**
     * @Column(name="user_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="type", type="string", length=50, nullable=true) */
    protected $type;

    /** @Column(name="uin", type="string", length=9, nullable=true) */
    protected $uin;

    /** @Column(name="email", type="string", length=300, nullable=true) */
    protected $email;

    /** @Column(name="firstname", type="string", length=100, nullable=true) */
    protected $firstname;

    /** @Column(name="lastname", type="string", length=100, nullable=true) */
    protected $lastname;

    /** @Column(name="name", type="string", length=300, nullable=true) */
    protected $name;

    public function getName()
    {
        if ($this->lastname)
            return $this->firstname.' '.$this->lastname;
        else
            return $this->name;
    }

    /** @Column(name="timestamp", type="integer", length=4, nullable=true) */
    protected $timestamp;

    /** @Column(name="password", type="string", length=40, nullable=true) */
    protected $password;

    public function getPassword()
    {
        return '';
    }
    public function comparePassword($pass)
    {
        $hashed_password = hash_hmac('sha1', $pass, $this->password_hash);
        return (strcasecmp($hashed_password, $this->password) == 0);
    }
    public function setPassword($pass)
    {
        if (!empty($pass))
        {
            $this->password_hash = sha1(mt_rand(0, time()));
            $this->password = hash_hmac('sha1', $pass, $this->password_hash);
            $this->password_set_timestamp = time();
            $this->password_needs_reset = false;
        }
    }

    /** @Column(name="password_hash", type="string", length=40, nullable=true) */
    protected $password_hash;

    /** @Column(name="password_set_timestamp", type="integer", nullable=true) */
    protected $password_set_timestamp;

    /** @Column(name="password_needs_reset", type="boolean", nullable=true) */
    protected $password_needs_reset;

    public function getPasswordNeedsReset()
    {
        if ($this->type == "netid")
        {
            return false;
        }
        else
        {
            $day_limit = 180;
            $threshold = time() - 86400*$day_limit;
            
            if ($this->password_set_timestamp < $day_limit)
                return true;
            else
                return $this->password_needs_reset;
        }
    }

    /** @Column(name="password_reset_code", type="string", length=40, nullable=true) */
    protected $password_reset_code;

    /**
     * @ManyToMany(targetEntity="CampsFormProgram", inversedBy="users")
     * @JoinTable(name="forms_camps_program_users",
     *      joinColumns={@JoinColumn(name="user_id", referencedColumnName="user_id", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="program_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $programs;

    /**
     * Static Function
     */

    public static function authenticate($username, $password)
    {
        $login_info = self::getRepository()->findOneBy(array('email' => $username));

        if (!($login_info instanceof self))
            return FALSE;

        if ($login_info->comparePassword($password))
            return $login_info;
        else
            return FALSE;
    }

    public static function fetchSelect($add_blank = FALSE)
    {
        $select = array();
        if ($add_blank)
            $select[''] = 'Select User...';

        $em = self::getEntityManager();
        $users = $em->createQuery('SELECT cfu FROM '.__CLASS__.' cfu ORDER BY cfu.lastname ASC, cfu.name ASC')
            ->getArrayResult();

        foreach($users as $record)
        {
            $name = ($record['firstname']) ? $record['lastname'].', '.$record['firstname'] : $record['name'];
            $select[$record['id']] = $name.' - '.$record['type'].' - '.$record['email'];
        }

        return $select;
    }
}