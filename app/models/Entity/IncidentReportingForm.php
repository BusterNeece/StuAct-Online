<?php
namespace Entity;


/**
 * IncidentReportingForm
 *
 * @Table(name="forms_incident_reporting")
 * @Entity
 */
class IncidentReportingForm extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="form_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $form_id;

    /** @Column(name="form_id_hash", type="string", length=25, nullable=true) */
    protected $form_id_hash;

    /** @Column(name="org_id", type="integer", nullable=true) */
    protected $org_id;

    /** @Column(name="user_id", type="integer", length=4, nullable=true) */
    protected $user_id;

    /** @Column(name="form_timestamp", type="integer", length=4, nullable=true) */
    protected $form_timestamp;

    /** @Column(name="form_data", type="text", nullable=true, nullable=true) */
    protected $form_data;

    /** @Column(name="form_notes", type="text", nullable=true, nullable=true) */
    protected $form_notes;

    /** @Column(name="form_phase", type="integer", length=1, nullable=true, nullable=true) */
    protected $form_phase;

    /**
     * @ManyToOne(targetEntity="Entity\Organization")
     * @JoinColumn(name="org_id", referencedColumnName="org_id")
     */
    protected $organization;
}