<?php
namespace Entity;


/**
 * LeadershipLibraryBook
 *
 * @Table(name="leadership_library_book")
 * @Entity
 */
class LeadershipLibraryBook extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="isbn", type="string", length=25, nullable=true) */
    protected $isbn;

    /** @Column(name="title", type="string", length=255, nullable=true) */
    protected $title;

    /** @Column(name="author", type="string", length=255, nullable=true) */
    protected $author;

    /** @Column(name="description", type="text", nullable=true) */
    protected $description;

    /** @Column(name="thumbnail", type="string", length=255, nullable=true) */
    protected $thumbnail;

    /** @Column(name="is_lost", type="integer", length=1, nullable=true) */
    protected $is_lost;

    /** @Column(name="user_id", type="integer", nullable=true) */
    protected $user_id;

    /** @Column(name="due_date", type="integer", nullable=true) */
    protected $due_date;

    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    /**
     * @ManyToMany(targetEntity="Entity\LeadershipLibraryCategory", inversedBy="books")
     * @JoinTable(name="leadership_library_book_has_category",
     *      joinColumns={@JoinColumn(name="book_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     */
    protected $categories;
}