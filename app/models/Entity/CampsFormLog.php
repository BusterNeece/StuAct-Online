<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="forms_camps_log")
 * @Entity
 */
class CampsFormLog extends \DF\Doctrine\Entity
{   
    public function __construct()
    {
        $this->timestamp = time();
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="app_id", type="integer") */
    protected $app_id;

    /** @Column(name="timestamp", type="integer") */
    protected $timestamp;

    /** @Column(name="note", type="text", nullable=true) */
    protected $note;

    /**
     * @ManyToOne(targetEntity="CampsForm", inversedBy="logs")
     * @JoinColumns({
     *   @JoinColumn(name="app_id", referencedColumnName="app_id", onDelete="CASCADE")
     * })
     */
    protected $form;
}