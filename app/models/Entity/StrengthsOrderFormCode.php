<?php
namespace Entity;

/**
 * StrengthsOrderFormCode
 *
 * @Table(name="forms_strengths_orders_codes")
 * @Entity
 */
class StrengthsOrderFormCode extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="code_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $code_id;

    /** @Column(name="code", type="string", length=15, nullable=true) */
    protected $code;

    /** @Column(name="is_assigned", type="integer", length=1) */
    protected $is_assigned;

    /** @Column(name="order_id", type="integer", length=4, nullable=true) */
    protected $order_id;

    /** @Column(name="code_timestamp", type="integer", length=4, nullable=true) */
    protected $code_timestamp;
}