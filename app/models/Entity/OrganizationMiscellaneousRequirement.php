<?php
namespace Entity;


/**
 * OrganizationMiscellaneousRequirement
 *
 * @Table(name="organization_miscellaneous_requirements")
 * @Entity
 */
class OrganizationMiscellaneousRequirement extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->timestamp = time();
    }

    /**
     * @Column(name="requirement_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="org_id", type="integer") */
    protected $org_id;

    /** @Column(name="requirement_creator", type="integer") */
    protected $creator;

    /** @Column(name="requirement_timestamp", type="integer") */
    protected $timestamp;

    /** @Column(name="requirement_type", type="string", length=200, nullable=true) */
    protected $type;

    public function getTypeName()
    {
        $types = self::getTypes();
        return $types[$this->type];
    }

    /** @Column(name="requirement_text", type="text", nullable=true) */
    protected $text;

    /** @Column(name="requirement_status", type="smallint", nullable=true) */
    protected $status;

    /** @Column(name="requirement_renews_annually", type="boolean", nullable=true) */
    protected $renews_annually;

    /**
     * @ManyToOne(targetEntity="Entity\Organization")
     * @JoinColumn(name="org_id", referencedColumnName="org_id")
     */
    protected $organization;

    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="requirement_creator", referencedColumnName="user_id")
     */
    protected $user;

    /**
     * Static Functions
     */

    public static function getTypes()
    {
        $settings = Organization::loadSettings();
        return $settings['miscellaneous_requirements'];
    }
}