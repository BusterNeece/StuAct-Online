<?php
namespace Entity;

/**
 * QuizOption
 *
 * @Table(name="quiz_option")
 * @Entity
 */
class QuizOption extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="quiz_question_id", type="integer") */
    protected $quiz_question_id;

    /** @Column(name="option_text", type="string", length=255) */
    protected $option_text;

    /**
     * @ManyToOne(targetEntity="Entity\QuizQuestion", inversedBy="options")
     * @JoinColumn(name="quiz_question_id", referencedColumnName="id")
     */
    protected $question;    
}