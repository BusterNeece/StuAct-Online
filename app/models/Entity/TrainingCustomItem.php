<?php
namespace Entity;

/**
 * @Table(name="training_custom_item")
 * @Entity
 * @HasLifecycleCallbacks
 */
class TrainingCustomItem extends \DF\Doctrine\Entity
{
    const PHASE_INCOMPLETE          = 0;
    const PHASE_DECLINED            = 1;
    const PHASE_PENDING_ADVISOR     = 2;
    const PHASE_PENDING_DEPT        = 4;
    const PHASE_APPROVED            = 10;

	public function __construct()
    {
        $this->phase = self::PHASE_INCOMPLETE;
        $this->created_at = $this->updated_at = new \DateTime("now");
    }
    
    /** @PreUpdate */
    public function updated()
    {
        $this->updated_at = new \DateTime("now");
    }

    /** @PostPersist **/
    public function inserted()
    {
        $this->access_code = str_pad($this->id, 6, '0', STR_PAD_LEFT).'-'.substr(strtoupper(sha1($this->id)), 0, 6);
        $this->save();
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="access_code", type="string", length=12) */
    protected $access_code;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="organization_id", type="integer") */
    protected $organization_id;

    /** @Column(name="phase", type="smallint") */
    protected $phase;

    public function setPhase($new_phase)
    {
        if ($this->phase != $new_phase)
        {
            $this->phase = $new_phase;
            $this->notify($new_phase);

            if ($new_phase == self::PHASE_APPROVED)
                Training::assignCustomCredit($this);
        }
    }

    public function isPending()
    {
        return (in_array($this->phase, array(self::PHASE_PENDING_ADVISOR, self::PHASE_PENDING_DEPT)));
    }

    public function getPhaseName($phase = NULL)
    {
        $phases = array(
            self::PHASE_DECLINED        => 'Declined',
            self::PHASE_APPROVED        => 'Approved',
            self::PHASE_PENDING_ADVISOR => 'Pending Advisor Review',
            self::PHASE_PENDING_DEPT    => 'Pending Student Activities Review',
        );

        if ($phase === null)
            $phase = $this->phase;

        return $phases[$phase];
    }

    public function notify($phase = NULL)
    {
        if ($phase === null)
            $phase = $this->phase;

        $settings = Training::loadSettings();

        switch($phase)
        {
            case self::PHASE_PENDING_ADVISOR:
                Organization::loadSettings();

                $advisor = $this->organization->getOfficerByPosition(ORG_OFFICER_PRIMARY_ADVISOR);
                if ($advisor instanceof OrganizationOfficerPosition)
                {
                    \DF\Messenger::send(array(
                        'to'        => $advisor->user->email,
                        'subject'   => 'Training Item Requires Review',
                        'module'    => 'training',
                        'template'  => 'custom_review',
                        'vars'      => array('record' => $this),
                    ));
                }
            break;

            case self::PHASE_PENDING_DEPT:
                \DF\Messenger::send(array(
                    'to'        => $settings['custom_email'],
                    'subject'   => 'Training Item Requires Review',
                    'module'    => 'training',
                    'template'  => 'custom_review',
                    'vars'      => array('record' => $this),
                ));
            break;

            case self::PHASE_APPROVED:
            case self::PHASE_DECLINED:
            default:
                \DF\Messenger::send(array(
                    'to'        => $this->user->email,
                    'subject'   => 'Training Item Status Changed',
                    'module'    => 'training',
                    'template'  => 'custom_status',
                    'vars'      => array('record' => $this),
                ));
            break;
        }
    }

    /** @Column(name="data", type="json") */
    protected $data;

    /** @Column(name="credits", type="smallint") */
    protected $credits;
    
    /** @Column(name="created_at", type="datetime") */
    protected $created_at;
    
    /** @Column(name="updated_at", type="datetime") */
    protected $updated_at;
    
    /**
     * @ManyToOne(targetEntity="Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    /**
     * @ManyToOne(targetEntity="Entity\Organization")
     * @JoinColumn(name="org_id", referencedColumnName="org_id")
     */
    protected $organization;
}