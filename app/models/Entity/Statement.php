<?php
namespace Entity;

/**
 * @Table(name="statements")
 * @Entity
 */
class Statement extends \DF\Doctrine\Entity
{
    const BASE_DIR = "E:/SOFC Statements";
    const UPLOAD_DIR = "E:/SOFC Statements Awaiting Processing";

    public function __construct()
    {
        $this->type = 'sofc';
    }

    /**
     * @Column(name="statement_id", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="statement_type", type="string", length=20, nullable=true) */
    protected $type;

    /** @Column(name="year", type="integer", length=2) */
    protected $year;

    /** @Column(name="month", type="integer", length=1) */
    protected $month;

    public function getMonthName()
    {
        $config = \Zend_Registry::get('config');
        $months = $config->general->months->toArray();

        return $months[$this->month];
    }

    /** @Column(name="account", type="integer", length=4) */
    protected $account;

    /** @Column(name="subaccount", type="integer", length=4) */
    protected $subaccount;

    public function getPath()
    {
        $folders = array(
            self::BASE_DIR,
            str_pad($this->year, 4, '0', STR_PAD_LEFT).'-'.str_pad($this->month, 2, '0', STR_PAD_LEFT),
            str_pad($this->account, 6, '0', STR_PAD_LEFT).'-'.str_pad($this->subaccount, 5, '0', STR_PAD_LEFT).'.pdf',
        );
        return implode(DIRECTORY_SEPARATOR, $folders);
    }
}