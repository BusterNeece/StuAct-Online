<?php
use \Entity\Organization;
use \Entity\User;

use \StuAct\Service\GoogleSearch;

class Search_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        return TRUE;
    }
    
    public function indexAction()
    {
        if ($this->_hasParam('q'))
        {
            $this->_forward('results');
            return;
        }
    }

    public function resultsAction()
    {
        Organization::loadSettings();

        $q = trim($this->_getParam('q', ''));
        $this->view->q = $q;

        $search_type = $this->_getParam('search', 'name');
        $this->view->type = $search_type;

        $results = array();

        if ($search_type == "name")
        {
            // User search.
            if ($this->acl->isAllowed(array('staff_findusers','admin_all')))
                $results['users'] = User::search($q);
            
            // URL for AJAX-driven Web Search.
            $results['web_url'] = GoogleSearch::getSearchUrl($q);
            
            // $results['web'] = GoogleSearch::search($q);
            $results['web'] = array();
        }
        
        // Organization Search
        $results['orgs'] = Organization::search($q, $search_type);

        $this->view->results = $results;
    }
}