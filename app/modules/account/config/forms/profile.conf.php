<?php
/**
 * Profile Information Settings
 */

$config = \Zend_Registry::get('config');
$strengths = $config->strengths->types->toArray();

return array(
	'form' => array(
		'method' => 'post',
		'groups' => array(

			'contact_info' => array(
				'legend' => 'Contact Information',
				'elements' => array(

					'firstname' => array('text', array(
						'label' => 'First Name',
						'class' => 'half-width',
						'required' => true,
					)),
					'lastname' => array('text', array(
						'label' => 'Last Name',
						'class' => 'half-width',
						'required' => true,
					)),

					'title' => array('text', array(
						'label' => 'Title',
						'class' => 'half-width',
					)),

					'email' => array('text', array(
						'label' => 'Preferred E-mail Address',
						'description' => 'This e-mail address is used to send you any automated notifications from the StuAct Online system. If you change this address, you will be sent a confirmation e-mail to ensure messages can be delivered to the new address.',
						'class' => 'half-width',
						'filters' => array('StringTrim'),
						'validators' => array('EmailAddress'),
						'required' => true,
					)),

					'email2' => array('text', array(
						'label' => 'Secondary E-mail Address',
						'class' => 'half-width',
						'validators' => array('EmailAddress'),
					)),

					'phone' => array('text', array(
						'label' => 'Phone Number',
						'class' => 'half-width',
						'required' => true,
					)),

					'addr1' => array('text', array(
						'label' => 'Address',
						'description' => 'Students: Enter current local address.<br>Staff/Faculty: Enter departmental address/mail stop.',
						'class' => 'full-width',
					)),

					'addr2' => array('text', array(
						'label' => 'Address 2',
						'class' => 'full-width',
					)),

					'addrcity' => array('text', array(
						'label' => 'City',
					)),

					'addrstate' => array('select', array(
						'label' => 'State',
						'multiOptions' => $config->general->states->toArray(),
						'default' => 'TX',
					)),

					'addrzip' => array('text', array(
						'label' => 'Zip Code',
					)),
				
				),
			),

			'personalize' => array(
				'legend' => 'Personalize StuAct Online',
				'elements' => array(

					'config_skin' => array('radio', array(
						'label' => 'Site Theme',
						'multiOptions' => $config->themes->names->toArray(),
					)),

				),
			),

			'strength' => array(
				'legend' => 'My Strengths',

				'elements' => array(

					'strength1' => array('select', array(
						'label' => 'Strength 1',
						'multiOptions' => $strengths,
					)),
					'strength2' => array('select', array(
						'label' => 'Strength 2',
						'multiOptions' => $strengths,
					)),
					'strength3' => array('select', array(
						'label' => 'Strength 3',
						'multiOptions' => $strengths,
					)),
					'strength4' => array('select', array(
						'label' => 'Strength 4',
						'multiOptions' => $strengths,
					)),
					'strength5' => array('select', array(
						'label' => 'Strength 5',
						'multiOptions' => $strengths,
					)),
				
				),
			),

			'staff' => array(
				'legend' => 'Staff Profile Details',
				'description' => 'As a member of the Student Activities staff, you can provide additional information about yourself in the fields below. This will make it easier for students to contact you and for the department to generate all-staff reports.',
				'elements' => array(

					'staff_phone' => array('text', array(
						'label' => 'Internal Phone Number',
						'belongsTo' => 'staff_profile',
					)),
					'staff_phone_public' => array('text', array(
						'label' => 'Public Phone Number',
						'belongsTo' => 'staff_profile',
					)),
					'staff_intercom' => array('text', array(
						'label' => 'Intercom Number',
						'style' => 'width: 40px',
						'belongsTo' => 'staff_profile',
					)),
					'staff_office' => array('text', array(
						'label' => 'Office Number',
						'style' => 'width: 40px',
						'belongsTo' => 'staff_profile',
					)),
					'staff_significant_other' => array('text', array(
						'label' => 'Significant Other',
						'class' => 'full-width',
						'belongsTo' => 'staff_profile',
					)),
					'staff_birthday' => array('unixdate', array(
						'label' => 'Birthday',
						'description' => 'Year will not be shown on your staff profile.',
						'blank' => true,
						'start_year' => 1930,
						'end_year' => date('Y')-10,
						'belongsTo' => 'staff_profile',
					)),
					'staff_emergency_name' => array('text', array(
						'label' => 'Emergency Contact Name',
						'class' => 'half-width',
						'belongsTo' => 'staff_profile',
					)),
					'staff_emergency_phone' => array('text', array(
						'label' => 'Emergency Contact Phone',
						'class' => 'half-width',
						'belongsTo' => 'staff_profile',
					)),
					'staff_shirt_size' => array('radio', array(
						'label' => 'Shirt Size',
						'multiOptions' => array(
							'YL' => 'Youth Large',
							'S' => 'Small',
							'M' => 'Medium',
							'L' => 'Large',
							'XL' => 'X-Large',
							'2XL' => '2X-Large',
							'3XL' => '3X-Large',
							'4XL' => '4X-Large',
						),
						'belongsTo' => 'staff_profile',
					)),

				),
			),

			'admin' => array(
				'legend' => 'Administrator Details',

				'elements' => array(

					'flag_training' => array('radio', array(
						'label' => 'Training Flag',
						'multiOptions' => array(
							0 => 'Does Not Meet Requirement',
							1 => 'Meets Requirement',
						),
					)),

					'roles' => array('multiCheckbox', array(
						'label' => 'Assigned Roles',
						'multiOptions' => \Entity\Role::fetchSelect(),
					)),
				
				),
			),

			'submit_group' => array(
				'elements' => array(
					'submit'		=> array('submit', array(
						'type'	=> 'submit',
						'label'	=> 'Save Changes',
						'helper' => 'formButton',
						'class' => 'ui-button',
					)),
				),
			),

		),
	),
);