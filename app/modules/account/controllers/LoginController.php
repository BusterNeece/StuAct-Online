<?php
use \Entity\User;

class Account_LoginController extends \DF\Controller\Action
{
	public function indexAction()
	{
        if (!isset($_REQUEST['ticket']))
		{
			$this->storeReferrer('login');
		}
		
		try
		{
			$this->auth->authenticate();
		}
		catch(Exception $e)
		{
			$this->flash($e->getMessage());
			$this->redirectToRoute(array('module' => 'default'));
		}
        
        // Check for successful login, then copy to NCE auth system.
		$user = \DF\Auth::getLoggedInUser();
        
		if ($user instanceof User)
		{
			$user_session = new \Zend_Session_Namespace('NCE_Auth');
			$user_session->profile = array(
				'username' => $user->username,
				'uin' => $user->uin,
				'session_sync' => 0, // Force reload when user visits NCE area
				'is_logged_in' => TRUE,
			);
		}
		
		$this->flash('<b>Logged in!</b><br>Be sure to log out after your session is completed.', \DF\Flash::SUCCESS);
		$this->redirectToStoredReferrer('login');
		$this->doNotRender();
		return;
    }
    
    public function impersonateAction()
    {
        $this->acl->checkPermission('administer all');
        
        // New user.
        $uin = $this->_getParam('uin');
        $user = User::getOrCreate($uin);
        
        if (!($user instanceof User))
            throw new \DF\Exception\DisplayOnly('User not found!');
        
        // Set new identity in Zend_Auth
        $this->auth->setUser($user);
        
        // Set new identity in NCE auth.
        $user_session = new \Zend_Session_Namespace('NCE_Auth');
        $user_session->profile = array(
            'username' => $user->username,
            'uin' => $user->uin,
            'session_sync' => 0, // Force reload when user visits NCE area
            'is_logged_in' => TRUE,
        );
        
        $this->redirectHome();
        return;
    }
}