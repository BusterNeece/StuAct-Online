<?php
use \Entity\User;

class Account_ProfileController extends \DF\Controller\Action
{
	protected $_user;
	protected $_admin_mode;
	
	public function preInit()
	{
		$current_user = $this->auth->getLoggedInUser();

		// Legacy username redirection.
		if ($this->_hasParam('username'))
		{
			$username = $this->_getParam('username');

			if ($username == "me" || $username == $current_user->username)
				$this->redirectFromHere(array('username' => NULL));

			$user = User::getRepository()->findOneByUsername($username);
			$this->redirectFromHere(array('username' => NULL, 'id' => $user->id));
			return;
		}

		// Look up user.
		if ($this->_hasParam('id'))
		{
			$user_id = (int)$this->_getParam('id');

			if ($user_id == $current_user->id)
				$this->redirectFromHere(array('id' => NULL));

			$this->_user = User::find($user_id);
			$this->_admin_mode = TRUE;
		}
		else
		{
			$this->_user = $this->auth->getLoggedInUser();
			$this->_admin_mode = FALSE;
		}

		// "User Not Found" and "Permission Denied" are the same error for security purposes.
		if (!($this->_user instanceof User))
		{
			if ($this->auth->isLoggedIn())
				throw new \DF\Exception\PermissionDenied;
			else
				throw new \DF\Exception\NotLoggedIn;
		}
		
		$this->view->record = $this->_user;
		$this->view->admin_mode = $this->_admin_mode;
		if ($this->_admin_mode)
			$this->view->headTitle($this->_user->getName());
		else
			$this->view->headTitle('My Profile');
	}
	
	public function permissions()
	{
		if ($this->_admin_mode)
			return $this->acl->isAllowed('admin_users');
		else
			return $this->acl->isAllowed('is logged in');
	}
	
	public function indexAction()
	{
		// Set up Strengths panel.
		$strengths = array();
		if ($this->_user->strength1)
		{
			$strengths_config = $this->config->strengths->toArray();
			for($i = 1; $i <= 5; $i++)
			{
				$name = $strengths_config['types'][$this->_user['strength'.$i]];
				$desc = $strengths_config['descriptions'][$name];
				$strengths[$name] = $desc;
			}
		}
		$this->view->strengths = $strengths;

		// Set up eligibility panel.
		$current_term = \StuAct\Grades::getCurrentActiveTerm();
		$alternate_term = \StuAct\Grades::getCurrentAlternateTerm();
		
		$this->view->current_term_name = \StuAct\Grades::getTermName($current_term);
		$this->view->alternate_term_name = \StuAct\Grades::getTermName($alternate_term);

		$is_eligible_active = \StuAct\Grades::checkGrade($this->_user);
		$is_eligible_alternate = \StuAct\Grades::checkGradeUsingAlternateTerm($this->_user);
		$is_conduct_eligible = \StuAct\Conduct::checkConduct($this->_user);

		$this->view->is_eligible_active = ($is_eligible_active && $is_conduct_eligible);
		$this->view->is_eligible_alternate = ($is_eligible_alternate && $is_conduct_eligible);

		// Role list.
		$roles = array();
		if (count($this->_user->roles) > 0)
		{
			foreach($this->_user->roles as $role)
				$roles[] = $role->name;
		}
		$this->view->roles = $roles;
	}

	public function editAction()
	{
		$form_config = $this->current_module_config->forms->profile->form->toArray();

		if (!$this->acl->isAllowed('admin_users'))
			unset($form_config['groups']['admin']);

		if (!$this->acl->userAllowed('staff_directory', $this->_user))
			unset($form_config['groups']['staff']);

		$form = new \DF\Form($form_config);
		$form->setDefaults($this->_user->toArray(TRUE, TRUE));

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();
			$this->_user->fromArray($data);
			$this->_user->save();

			$this->alert('<b>Profile updated!</b>', 'green');
			$this->redirectFromHere(array('action' => 'index'));
			return;
		}

		$this->renderForm($form, 'edit', 'Edit Profile');
	}

	public function historyAction()
	{
	}

	public function compassAction()
	{
		$this->acl->checkPermission('admin_sims');
		$uin = $this->_user->uin;

		// Pull conduct data.
		$this->view->conduct = \StuAct\Conduct::fetchConduct($uin);

		// Check general system API availability.
		$api = new \DF\Service\DoitApi;

		if (!$api->checkAvailability())
			throw new \DF\Exception\DisplayOnly('Compass Currently Unavailable: This page cannot be displayed because the Compass service is not currently operating. Please check back again later.');

		// Pull GPR data.
		$grades = array();
		$grades_raw = \StuAct\Grades::fetchGradesForAllTerms($uin);

		if ($grades_raw)
		{
			foreach($grades_raw as $term => $gpa)
			{
				$grades[] = array(
					'term'		=> $term,
					'gpa'		=> number_format($gpa['gpa'], 2),
					'term_name'	=> \StuAct\Grades::getTermName($term),
				);
			}
		}

		$this->view->grades = $grades;
		$this->view->compass = $api->getStudentRecord($uin);
	}

	public function reloadeligibilityAction()
	{
		\StuAct\Grades::clearCache($this->_user->uin);

		$this->alert('<b>Eligibility status reloaded successfully.</b>', 'green');
		$this->redirectFromHere(array('action' => 'index'));
		return;
	}
}