<?php
class IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        return true;
    }
    
    public function indexAction()
    {
    	$this->view->rotator_items = $this->current_module_config->rotator->items->toArray();
    }
}