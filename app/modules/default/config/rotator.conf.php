<?php
/** 
 * Homepage Rotator Contents
 */

return array(
	'items' => array(

		/*
		array(
			'image' => \DF\Url::content('rotator/bootstrap/mscopenhouse.jpg'),
			'title'	=> 'MSC Fall Open House registration is open!!',
			'description' => 'The date is set for September 1st, so register online at <a href="http://mscopenhouse.tamu.edu" target="_blank">mscopenhouse.tamu.edu</a> and head down to the MSC Box Office to reserve your table today. Open House reaches thousands of students every fall, so don\'t miss your chance to recruit our best Aggies! Hurry now and get the early bird price before May 10th!',
		),
		*/

		array(
			'image' => \DF\Url::content('rotator/bootstrap/StrengthsQuest.jpg'),
			'title'	=> 'Everyone has Strengths. What are Yours?',
			'description' => 'Discover your Strengths by taking a quick assessment, then learn about how to utilize your Strengths for academic, personal, and career development! Check out <a href="http://studentactivities.tamu.edu/leadandserve/opportunities/programs/strengthsquest" target="_blank">the website</a> for more details.',
		),

		array(
			'image' => \DF\Url::content('rotator/bootstrap/fsl_recruitment.jpg'),
			'title'	=> 'Fraternity and Sorority Recruitment is Underway!',
			'description' => 'Fraternities and sororities have been part of the fabric of Texas A&M since the early 1970s. Texas A&M is home to approximately 60 nationally affiliated Greek-letter organizations, which collectively constitute the largest membership-based and multi-faceted community on campus. <a href="http://greeklife.tamu.edu" target="_blank">Click here</a> for more information!',
		),

		/*
		array(
			'image' => \DF\Url::content('rotator/bootstrap/livechat.jpg'),
			'title'	=> 'Chat with Us!',
			'description' => 'Contacting the Student Activities team has never been easier. Now you can chat live with a member of our team directly from the bottom corner of any page on StuAct Online. <a href="http://studentactivities.tamu.edu/webim/client.php?group=12" target="_blank">Try it out today</a>.',
		),
		*/

		array(
			'image' => \DF\Url::content('rotator/bootstrap/aggieserve.jpg'),
			'title'	=> 'Get Hooked on Service at AggieServe',
			'description' => 'Browse our volunteer opportunities database, and find a great way to support your favorite cause and make a difference in the community. <a href="http://aggieserve.tamu.edu/">Visit AggieServe</a> today!',
		),

		array(
			'image' => \DF\Url::content('rotator/bootstrap/facebook.jpg'),
			'title'	=> 'Follow Us on Facebook',
			'description' => 'Student Activities now has a Facebook page, complete with department information, news and updates, and photos of our staff members. <a href="http://stuact.tamu.edu/fb">Visit our fan page today.</a>',
		),

	),
);