<?php
/**
 * Sponsored/Authorized Event Request Form
 */

$module_config = Zend_Registry::get('module_config');
$routing_raw = $module_config['sponsauth']->routing->departments->toArray();

$departments_raw = array_keys($routing_raw);
sort($departments_raw);
$departments = array('' => 'Select Department...') + array_combine($departments_raw, $departments_raw);

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		
		'groups'		=> array(
			
			'event_info' => array(
				'legend' => 'Event Details',
				'elements' => array(
					
					'event_name' => array('text', array(
						'label' => 'Event Name',
						'class' => 'full-width',
						'required' => true,
					)),
					
					'event_location' => array('text', array(
						'label' => 'Event Location',
						'class' => 'full-width',
						'required' => true,
					)),
					
					'dept_name' => array('select', array(
						'label' => 'Department or College Conducting Event',
						'multiOptions' => $departments,
						'required' => true,
						'description' => 'If you do not see your department listed, contact Student Activities at <a href="mailto:info@stuact.tamu.edu">info@stuact.tamu.edu</a>.',
					)),
					
					'event_desc' => array('textarea', array(
						'label' => 'Extra Event Information',
						'class' => 'full-width full-height',
						'description' => 'This information should include the class(es) attending the event (if applicable) and optionally a brief description of the event\'s purpose.',
					)),
					
					'event_starttime' => array('unixdate', array(
						'label' => 'Event Start Date',
						'required' => true,
					)),
					
					'event_endtime' => array('unixdate', array(
						'label' => 'Event End Date',
						'required' => true,
					)),
                    
                    'course_name' => array('text', array(
                        'label' => 'Course(s) Requiring This Event',
                        'required' => true,
                    )),
				),
			),
		
			'contact' => array(
				'legend' => 'Faculty/Staff Contact Information',
				'elements' => array(
					
					'contact_name' => array('text', array(
						'label' => 'Name',
						'class' => 'full-width',
						'required' => true,
					)),
					
					'contact_dept' => array('text', array(
						'label' => 'Department',
						'class' => 'full-width',
					)),
					
					'contact_phone' => array('text', array(
						'label' => 'Phone Number',
						'class' => 'full-width',
					)),
					
					'contact_email' => array('text', array(
						'label' => 'E-mail Address',
						'filters' => array('StringTrim'),
						'validators' => array('EmailAddress'),
						'class' => 'full-width',
						'required' => true,
					)),
				
				),
			),
			
			'submit_group' => array(
				'elements' => array(
					'submit_btn' => array('submit', array(
						'type'	=> 'submit',
						'label'	=> 'Submit Form',
						'helper' => 'formButton',
						'class' => 'ui-button',
					)),
				
				),
			),
		),
	),
);