<?php
/**
 * Sponsored/Authorized Routing
 */

return array(

	// Recipient for internal review process.
	'sponsored'		=> 'tsmith@stuact.tamu.edu',
	
	// List of departments and the routing recipients.
	'departments' 	=> array(
		'Student Organization' => array(
			'tsmith@stuact.tamu.edu',
		),
		
		'College of Agriculture and Life Sciences' => array(
            'k-dooley@tamu.edu',
			'cskaggs@tamu.edu',
		),
		'College of Architecture' => array(
			'jvanegas@tamu.edu',
		),
		'Bush School of Government and Public Service' => array(
			'skirkpatrick@tamu.edu',
		),
		'Mays Business School' => array(
			'm-loudder@tamu.edu',
			'drysdale@tamu.edu',
			'b-shetty@tamu.edu',
		),
		'College of Education and Human Development' => array(
			'byrd99@tamu.edu',
		),
		'Dwight Look College of Engineering' => array(
			'r-autenreith@tamu.edu',
			'r-james@tamu.edu',
		),
		'College of Geosciences' => array(
			's-bednarz@tamu.edu',
		),
		'College of Liberal Arts' => array(
			'b-crouch@tamu.edu',
			'Pat-hurley@tamu.edu',
			'dcurtis@tamu.edu',
			'mstephenson@tamu.edu',
		),
		'College of Science' => array(
			't-scott@tamu.edu',
		),
		'College of Veterinary Medicine and Biomedical Sciences' => array(
			'engreen@tamu.edu',
		),
		
		'Admissions and Records' => array(
			'jpp2@tamu.edu',
		),
		'Aggie Honor System Office' => array(
			'tim_powers@tamu.edu',
		),
		'Athletics'	=> array(
			'pking@tamu.edu',
		),
		'Career Center' => array(
			'mmarberry@tamu.edu',
			'leigh@tamu.edu',
		),
		'Corps of Cadets' => array(
			'bstebbins@tamu.edu',
		),
		'Financial Aid'	=> array(
			'jmgunn@tamu.edu',
		),
		'General Academic Programs' => array(
			'k_boyd@tamu.edu',
		),
		'Greek Life' => array(
			'cjwoods@tamu.edu',
		),
		'Honors Program' => array(
			'ed-funkhouser@tamu.edu',
		),
		'MSC Programs' => array(
			'treber@tamu.edu',
		),
		'Multicultural Services' => array(
			'cjwoods@tamu.edu',
		),
        'Office of Sustainability' => array(
			'cjwoods@tamu.edu',
		),
		'Professional and Graduate School Advising' => array(	
			'ed-funkhouser@tamu.edu',
		),
		'Recreational Sports' => array(
			'cjwoods@tamu.edu',
		),
		'Student Activities' => array(
			'cjwoods@tamu.edu',
		),
		'Student Life' => array(
			'tsmith@stuact.tamu.edu',
		),
		'Study Abroad Programs' => array(
			'jflaherty@ipo.tamu.edu',
		),
	),
	
);