<?php
return array(
    'default' => array(
		'sponsauth'	=> array(
			'label'		=> 'Sponsored/Authorized Events',
			'module'	=> 'sponsauth',
			
			'pages'		=> array(
				
				'sponsauth_submit' => array(
					'module'			=> 'sponsauth',
					'controller'		=> 'index',
					'action'			=> 'submit',
				),
				'sponsauth_sponsored' => array(
					'module'			=> 'sponsauth',
					'controller'		=> 'index',
					'action'			=> 'sponsored',
					'label'				=> 'Sponsored Event Form',
				),
				'sponsauth_authorized' => array(
					'module'			=> 'sponsauth',
					'controller'		=> 'index',
					'action'			=> 'authorized',
					'label'				=> 'Authorized Event Form',
				),
				'sponsauth_manage'		=> array(
					'module' => 'sponsauth',
					'controller' => 'manage',
					'action' => 'index',
					'label' => 'Manage Events',
					'permission' => 'admin_sponsauth',
					'pages' => array(
					
						'sponsauth_manage_edit' => array(
							'module' => 'sponsauth',
							'controller' => 'manage',
							'action' => 'edit',
						),
						
					),
				),
			),
		),
    ),
);
