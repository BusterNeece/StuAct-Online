<?php
/**
 * Sponsored Event management page.
 */

use \Entity\SponsAuth;

class Sponsauth_ManageController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::isAllowed('admin_sponsauth');
	}
	
	public function indexAction()
	{
        $query = $this->em->createQueryBuilder()
            ->select('sa')
            ->from('\Entity\SponsAuth', 'sa')
            ->orderBy('sa.event_starttime', 'DESC');
        
		$page = ($this->_hasParam('page')) ? $this->_getParam('page') : 1;
		$paginator = new \DF\Paginator\Doctrine($query, $page);
		$this->view->pager = $paginator;
	}

	public function routingAction()
	{
		$routing = $this->current_module_config->routing->toArray();
		$this->view->routing = $routing;
	}
	
	public function editAction()
	{
		if ($this->_hasParam('id'))
		{
			$record = SponsAuth::find($this->_getParam('id'));
			$form_type = NULL;
		}
		else
		{
			$record = NULL;
			$form_type = $this->_getParam('type');
		}
		
		$form = new \StuAct\Form\SponsAuth($record, $form_type, TRUE);
		
		if( !empty($_POST) && $form->isValid($_POST) )
        {
			$record = $form->save();
			
			$this->alert('Record changes saved!');
			$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
			return;
		}
		
		$this->view->form = $form;
	}
	
	public function deleteAction()
	{
		$record = SponsAuth::find($this->_getParam('id'));
		
		if ($record)
			$record->delete();
		
		$this->alert('Record deleted!');
		$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
	}
}