<?php
use \Entity\Organization;
use \Entity\User;

use \DF\Cache;

class Getinvolved_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        return TRUE;
    }

    public function init()
    {
    	parent::init();
		\Zend_Layout::getMvcInstance()->setLayout('getinvolved');
    }

    public function indexAction()
    {
    	// Pull Get Involved news items.
		$getinvolved_articles = \StuAct\Utilities::getNewsFeed('http://studentactivities.tamu.edu/getinvolved/news.xml', 'getinvolved_news', 900);
		$this->view->getinvolved_articles = $getinvolved_articles;

		// Pull All-U Calendar events.
		$calendar_events = Cache::get('getinvolved_events');

		if (!$calendar_events)
		{
			$calendar_feed_url = 'http://allucalendar.tamu.edu/MasterCalendar/RSSFeeds.aspx?data=%2be%2fgTmcOGekla7000jJwX1cVrKfNt%2flvoYJ1SVuBP8M%3d';
			
			try
			{
				$calendar_feed = \Zend_Feed::import($calendar_feed_url);
			}
			catch(\Exception $e)
			{
				$calendar_feed = NULL;
			}
			
			if (!is_null($calendar_feed))
			{
				$calendar_events = array();
				$row_num = 0;
				
				foreach($calendar_feed as $item)
				{
					$pub_date = strtotime($item->pubDate());
					
					if ($pub_date < (time() + (86400 * 4)) && $row_num < 10)
					{
						$event_day = date('Y-m-d', $pub_date);
						
						if (!isset($calendar_events[$event_day]))
						{
							$calendar_events[$event_day] = array(
								'text'		=> date('l, F j', $pub_date),
								'events'	=> array(),
							);
						}
						
						$event = array(
							'title'		=> str_replace('All Day', '', $item->title()),
							'time'		=> date('g:ia', $pub_date),
							'all_day'	=> (strpos($item->title(), 'All Day') !== FALSE),
							'link'		=> $item->link(),
						);
						$event_hash = md5($event['time'].$event['title']);
						
						$calendar_events[$event_day]['events'][] = $event;
						$row_num++;
					}
				}

				Cache::set($calendar_events, 'getinvolved_events', array('getinvolved'));
			}
		}

		$this->view->calendar_events =$calendar_events;
    }
}