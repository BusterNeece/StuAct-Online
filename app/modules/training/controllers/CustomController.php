<?php
/**
 * Training Center - Submit for Sponsored Groups
 */

use \Entity\Training;
use \Entity\TrainingCustomItem;
use \Entity\Organization;

class Training_CustomController extends \DF\Controller\Action
{
	public function permissions()
	{
        return $this->acl->isAllowed('is logged in');
	}

	public function indexAction()
	{
		$user = $this->auth->getLoggedInUser();
		$orgs = array();

		if ($this->acl->isAllowed('manage training'))
		{
			$orgs_raw = $this->em->createQuery('SELECT o.id, o.name FROM Entity\Organization o WHERE o.category = :cat ORDER BY o.name ASC')
				->setParameter('cat', 'Sponsored')
				->getArrayResult();

			foreach($orgs_raw as $org)
				$orgs[$org['id']] = $org['name'];
		}
		else
		{
			$orgs_raw = $user->officer_positions->filter(function($record) {
				return ($record->organization->category == "Sponsored");
			});

			foreach($orgs_raw as $off_pos)
			{
				$org = $off_pos->organization;
				$orgs[$org->id] = $org->name;
			}
		}

		if (count($orgs) == 0)
			throw new \DF\Exception\DisplayOnly('You are not eligible to submit training items for any organizations.');

		if ($this->_hasParam('org_id'))
		{
			$org_id = (int)$this->_getParam('org_id');
			$org = Organization::find($org_id);

			$form = new \DF\Form($this->current_module_config->forms->custom);

			if ($_POST && $form->isValid($_POST))
			{
				$data = $form->getValues();

				$record = new TrainingCustomItem;
				$record->user = $user;
				$record->organization = $organization;
				$record->data = $data;
				$record->credits = min(count($data['modules']) + count($data['values']), 4);
				$record->save();

				$position = $org->getUserPosition($user);
				if ($position && $position->isInGroup('advisors'))
					$record->setPhase(TrainingCustomItem::PHASE_PENDING_DEPT);
				else
					$record->setPhase(TrainingCustomItem::PHASE_PENDING_ADVISOR);

				$this->alert('<b>Custom training item submitted!</b><br>You will be updated via e-mail after the item is reviewed.', 'green');
				$this->redirectFromHere(array('controller' => 'index', 'action' => 'index', 'org_id' => NULL));
				return;
			}

			$this->view->headTitle('Submit Custom Training Item');
			$this->renderForm($form);
		}
		else
		{
			$this->view->orgs = $orgs;
			$this->render();
		}
	}

	public function reviewAction()
	{
		$code = $this->_getParam('code');
		$record = TrainingCustomItem::getRepository()->findOneBy(array('access_code' => $code));

		if (!($record instanceof TrainingCustomItem))
			throw new \DF\Exception\DisplayOnly('Training item not found!');

		if (!$record->isPending())
			throw new \DF\Exception\DisplayOnly('This training item has already been reviewed.');

		$this->view->record = $record;

		$form = new \DF\Form($this->current_module_config->forms->custom);
		$form->populate($record->data);
		$this->view->form = $form;

		if ($this->_hasParam('decision'))
		{
			$decision = $this->_getParam('decision');

			if ($decision == "approve")
				$record->setPhase(TrainingCustomItem::PHASE_APPROVED);
			else
				$record->setPhase(TrainingCustomItem::PHASE_DECLINED);

			$this->alert('<b>Training item reviewed.</b>');
			$this->redirectToRoute(array('module' => 'training'));
			return;
		}
	}
}