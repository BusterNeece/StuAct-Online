<?php
/**
 * Training Center - Advisors Area
 */

use \Entity\User;
use \Entity\Training;

class Training_ManageController extends \DF\Controller\Action
{
	public function permissions()
	{
        return $this->acl->isAllowed('manage training');
	}
    
	public function indexAction()
	{
        $all_modules = $this->current_module_config->modules->toArray();

        $training_credits = \Entity\Training::fetchArray();
        
        foreach($training_credits as $training)
        {
            $module_key = $training['name'];

            if (isset($all_modules[$module_key]))
                $all_modules[$module_key]['users']++;
        }

        $assessments = \Entity\TrainingAssessment::fetchArray();

        foreach($assessments as $assessment)
        {
            $module_key = $assessment['type'];

            if (isset($all_modules[$module_key]))
                $all_modules[$module_key]['assessments'][] = $assessment;
        }

        foreach($all_modules as &$module_info)
        {
            $criteria = array('quality_of_content', 'length', 'module_format_and_design', 'comprehensiveness_of_quiz');
            $scores_raw = array();

            if ($module_info['assessments'])
            {
                foreach($module_info['assessments'] as $assessment)
                {
                    foreach($criteria as $criterion)
                    {
                        if (isset($assessment['data'][$criterion]))
                        {
                            $scores_raw[$criterion]['votes']++;
                            $scores_raw[$criterion]['total'] += $assessment['data'][$criterion];
                        }
                    }
                }
            }

            $scores = array();

            foreach($criteria as $criterion)
            {
                $score_raw = $scores_raw[$criterion];

                if ($score_raw['votes'] != 0)
                {
                    $score = round($score_raw['total'] / $score_raw['votes'], 1);
                    $scores[$criterion] = $score;
                }
                else
                {
                    $scores[$criterion] = 'N/A';
                }
            }

            $module_info['scores'] = $scores;
        }

        $modules_by_type = array();

        foreach($all_modules as $module_key => $module)
        {
            $modules_by_type[$module['type']][$module_key] = $module;
        }

        $this->view->modules = $modules_by_type;
    }

    public function userAction()
    {
        $user_id = (int)$this->_getParam('id');
        $user = User::find($user_id);

        if (!($user instanceof User))
            throw new \DF\Exception\DisplayOnly('User not found!');
        
        $this->view->user = $user;

        $this->view->training_modules = $this->current_module_config->modules->toArray();
        $this->view->training = $this->em->createQuery('SELECT t FROM Entity\Training t WHERE t.user_id = :user_id ORDER BY t.updated_at DESC')
            ->setParameter('user_id', $user->id)
            ->getArrayResult();
    }

    public function resyncuserAction()
    {
        $user_id = (int)$this->_getParam('id');
        $user = User::find($user_id);

        if (!($user instanceof User))
            throw new \DF\Exception\DisplayOnly('User not found!');
        
        Training::updateUserStatus($user);

        $this->alert('User training status and credit resynchronized.', 'green');

        $this->redirectFromHere(array('action' => 'user'));
        return;
    }

    public function assessmentsAction()
    {
        $all_modules = $this->current_module_config->modules->toArray();
        $module_name = $this->_getParam('course', 'all');

        if ($module_name == 'all')
        {
            $assessments_raw = $this->em->createQuery('SELECT ta FROM Entity\TrainingAssessment ta ORDER BY ta.created_at DESC')
                ->getArrayResult();
        }
        else
        {
            $assessments_raw = $this->em->createQuery('SELECT ta FROM Entity\TrainingAssessment ta WHERE ta.type = :training_type ORDER BY ta.created_at DESC')
                ->setParameter('training_type', $module_name)
                ->getArrayResult();
        }

        $export_keys = array('date' => 'date', 'module' => 'module');
        $assessments = array();

        if ($assessments_raw)
        {
            foreach($assessments_raw as $assessment)
            {
                $data = $assessment['data'];
                if (!empty($data))
                {
                    unset($data['module_info']);

                    $export_keys += array_combine(array_keys($data),array_keys($data));

                    $module_key = $assessment['type'];
                    $module_info = $all_modules[$module_key];

                    $data['date'] = $assessment['created_at']->format('m/d/Y g:ia');
                    $data['module'] = $module_info['name'];

                    $assessments[] = $data;
                }
            }
        }

        // Force comments to the end.
        unset($export_keys['additional_comments']);
        $export_keys['additional_comments'] = 'additional_comments';
        
        $export_data = array($export_keys);

        foreach($assessments as $data)
        {
            $export_row = array();

            foreach($export_keys as $key_name)
            {
                if (isset($data[$key_name]))
                {
                    $export_row[] = $data[$key_name];
                }
                else
                {
                    $export_row[] = 'N/A';
                }
            }

            $export_data[] = $export_row;
        }

        $this->doNotRender();
        \DF\Export::csv($export_data);
        return;
    }
    
    public function attendeesAction()
    {
		$attendance = $this->em->createQuery('SELECT t, u FROM Entity\Training t JOIN t.user u ORDER BY t.name ASC, t.created_at DESC')
			->getResult();
		
		$export_data = array(array(
			'Training Type',
			'Date Taken',
			'NetID',
			'UIN',
			'Name',
			'E-mail Address',
			'Leader Type',
			'Leader Category',
		));
		
		foreach($attendance as $training)
		{
			$leader_status = $training->user->getLeaderStatus();
			
			$export_data[] = array(
				$training->getTypeText(),
				$training['created_at']->format('m/d/Y g:ia'),
				$training->user->username,
				$training->user->uin,
				$training->user->firstname.' '.$training->user->lastname,
				$training->user->email,
				ucfirst($leader_status['type']),
				ucfirst($leader_status['category']),
			);
		}

        $this->doNotRender();
        \DF\Export::csv($export_data);
        return;
    }

    public function advisorAction()
    {
        $form_config = $this->current_module_config->forms->advisor->form->toArray();
        $form = new \StuAct\Form($form_config);

        $this->view->form = $form;
    }
}