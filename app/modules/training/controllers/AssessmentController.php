<?php
/**
 * Training Center - Advisors Area
 */

use \Entity\Training;
use \Entity\TrainingAssessment;

class Training_AssessmentController extends \DF\Controller\Action
{
	public function permissions()
	{
        return true;
	}
    
    public function indexAction()
    {
        $courses = $this->current_module_config->modules->toArray();
        $course = $this->_getParam('course');
        
        if (!isset($courses[$course]))
            throw new \DF\Exception\DisplayOnly('Course Not Found!');
        
        $course_info = $courses[$course];
        
        $form_config = $this->current_module_config->forms->assessment->form->toArray();
        
        if ($course_info['type'] != "officer")
            unset($form_config['groups']['officers']);
        else if ($course_info['type'] != "advisor")
            unset($form_config['groups']['advisors']);
        
        $form_config['groups']['module']['elements']['module_info'][1] = array(
            'label' => $course_info['name'],
            'markup' => $course_info['description'],
        );
        
        $form = new \StuAct\Form($form_config);
        
        if (!empty($_POST) && $form->isValid($_POST))
        {
            $record = new TrainingAssessment;
            
            if ($this->auth->isLoggedIn())
                $record->user = $this->auth->getLoggedInUser();
            
            $record->type = $course;
            $record->data = $form->getValues();
            $record->save();
            
            $this->render('success');
            return;
        }
        
        $this->view->form = $form;
    }
}