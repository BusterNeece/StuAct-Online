<?php
return array(
    'default' => array(
        'training' => array(
            'label' => 'Training',
            'module' => 'training',
            'controller' => 'index',
            'action' => 'index',
            
            'pages' => array(
                'module' => array(
                    'module' => 'training',
                    'controller' => 'index',
                    'action' => 'module',
                ),
            ),
        ),
    ),
);