<?php
/**
 * Custom Training Item
 */

$settings = \Entity\Training::loadSettings();

$modules = array();
$modules_raw = $settings['modules'];
foreach($modules_raw as $item_code => $item_info)
{
    if (!$item_info['is_required'] && !$item_info['batch'])
    {
        $code = str_replace(array('advisor_', 'officer_'), array('', ''), $item_code);
        $name = trim(str_replace(array('Advisor ', 'Officer '), array('', ''), $item_info['name']));
        $modules[$code] = $name;
    }
}

return array(
    'method' => 'post',
    'elements' => array(

        'name' => array('text', array(
            'label' => 'Training Item Name/Summary',
            'description' => 'Provide a brief summary of the training, or the name of the associated course.',
            'class' => 'full-width',
            'required' => true,
        )),

        'description' => array('textarea', array(
            'label' => 'Detailed Description',
            'description' => 'Include additional details about the training you completed, including the subject matters covered and details of the training\'s location and date.',
            'class' => 'full-width half-height',
            'required' => true,
        )),

        'modules' => array('multiCheckbox', array(
            'label' => 'Corresponding Training Modules',
            'description' => 'If this training item corresponds to an existing training inside the StuAct Online system, select it from the list below.',
            'multiOptions' => $modules,
        )),

        'values' => array('multiCheckbox', array(
            'label' => 'Corresponding Core Values',
            'description' => 'If this training included concepts that specifically relate to core Aggie values, select them from the list below.',
            'multiOptions' => array(
                'excellence'    => 'Excellence',
                'integrity'     => 'Integrity',
                'leadership'    => 'Leadership',
                'loyalty'       => 'Loyalty',
                'respect'       => 'Respect',
            ),
        )),

        'submit' => array('submit', array(
            'type'	=> 'submit',
            'label'	=> 'Submit Training Item',
            'helper' => 'formButton',
            'class' => 'ui-button',
        )),
	),
);