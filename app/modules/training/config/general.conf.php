<?php
/**
 * Training General Configuration
 */

return array(

    // E-mail address for custom training item review.
    'custom_email' => 'recognition@stuact.tamu.edu',
    
);