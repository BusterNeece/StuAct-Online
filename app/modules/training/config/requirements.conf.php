<?php
/**
 * Training Requirements (not including required core)
 */

return array(
    
    'advisor' => array(
        'Registered'    => 0,
        'Affiliated'    => 2,
        'Sponsored'     => 4,
    ),
    
    'officer' => array(
        'Registered'    => 0,
        'Affiliated'    => 2,
        'Sponsored'     => 4,
    ),
    
);