<?php
$org_settings = \Entity\Organization::loadSettings();

return array(
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements' 		=> array(

			'type' => array('radio', array(
				'label' => 'Requirement Type',
				'multiOptions' => $org_settings['miscellaneous_requirements'],
				'required' => true,
			)),

			'status' => array('radio', array(
				'label' => 'Status',
				'multiOptions' => $org_settings['miscellaneous_flag_codes'],
				'required' => true,
			)),

			'text' => array('textarea', array(
				'label' => 'Notes (Optional)',
				'class' => 'full-width half-height',
			)),

			'renews_annually' => array('radio', array(
				'label' => 'Requirement Renews Annually',
				'multiOptions' => array(0 => 'No', 1 => 'Yes'),
				'default' => 0,
				'required' => true,
			)),

			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),

		),
	),
);