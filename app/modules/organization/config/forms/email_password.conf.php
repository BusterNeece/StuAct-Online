<?php
return array(
	'form' => array(
		'method' => 'post',
		'elements' => array(

			'password' => array('password', array(
				'label' => 'New Account Password',
				'required' => true,
			)),

			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Set New Password',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),

		),
	),
);