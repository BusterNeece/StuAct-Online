<?php
return array(
	'form' => array(
		'method' => 'post',
		'elements' => array(

			'account' => array('text', array(
				'label' => 'Mailing List Username (E-mail Address Prefix)',
				'description' => 'Only enter the part of the e-mail address before the @ symbol, i.e. for president@mysite.tamu.edu, enter "president".',
				'required' => true,
			)),

			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Create Mailing List',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),

		),
	),
);