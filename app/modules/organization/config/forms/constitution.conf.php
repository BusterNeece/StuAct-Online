<?php

$eligibility_clause = 'The officers of this organization must meet the following requirements:
(a) Have a minimum cumulative and semester grade point ratio (GPR) as stated below and meet that minimum cumulative and semester GPR in the semester immediately prior to the election/appointment, the semester of election/appointment and semesters during the term of office.
1.  For undergraduate students, the minimum cumulative and semester GPR is 2.00.  In order for this provision to be met, at least six hours (half-time credits) must have been taken for the semester under consideration. In one limited circumstance, summer semester hours may be applied to this provision. In order for summer coursework to qualify toward a grade point ratio prior to election/appointment, at least six credit hours must have been taken during the course of either the full or two summer session(s).
2.  For graduate level students the minimum cumulative and semester GPR is a 3.00.  In order for this provision to be met, at least four hours (half-time credits) must have been taken for the semester under consideration.  In one limited circumstance, summer semester hours may be applied to this provision. In order for summer coursework to qualify toward a grade point ratio prior to election/appointment, at least four credit hours must have been taken during the course of either the full or two summer session(s) unless fewer credits are required as they complete the final stages of their degree.
(b) Be in good standing (see 27.1.4) with the university and enrolled:
1.  at least half time (six or more credit hours), if an undergraduate student (unless fewer credits are required to graduate in the spring and fall semesters) during the term of office. Students enrolled in the Blinn TEAM program are also eligible to hold an office, as long as the student is meeting all applicable Blinn TEAM requirements and is in good standing with the program.
2.  at least half time (four or more credits), if a graduate level student (unless fewer credits are required in the final stages of their degree as defined by the Continuous Registration Requirement) during their term of office.
(c) Be ineligible to hold an office should the student fail to maintain the requirements as prescribed in (a) and (b).';

$finances_clause = 'All monies belonging to this organization shall be deposited and disbursed through a bank account established for this organization at the Student Organization Finance Center and/or the Fiscal Office. All funds must be deposited within 24 hours after collection. The advisor to this organization must approve and sign each expenditure before payment.';

return array(
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'enctype'		=> 'multipart/form-data',
		
		'groups'		=> array(

			'document' => array(
				'legend' => 'Upload Constitution Document',
				'elements' => array(
					'constitution' => array('file', array(
						'label' => 'Select File',
						'validators' => array(
							array('Extension', false, array('doc', 'docx', 'txt', 'rtf', 'pdf')),
						),
						'required' => true,
					)),
				),
			),

			'summary' => array(
				'legend' => 'Constitution Summary',
				'elements' => array(

					'about' => array('markup', array(
						'label' => 'About the Constitution Summary',
						'markup' => '
							<p>Recognized Student Organizations at Texas A&amp;M University are required to write and maintain an up-to-date constitution.  Each year, the Department of Student Activities is given the opportunity to review and suggest revisions to this constitution.  While every organization is unique, there are required elements that must be included in each constitution.</p>
							<p>Using the text fields below, <b>copy and paste the exact clauses from your constitution</b> that correspond to the requirement listed.</p>
						',
					)),

					'name' => array('textarea', array(
						'label' => 'Name of organization stated clearly and any acronym/abbreviation is noted',
						'class' => 'full-width full-height',
						'required' => true,
					)),

					'purpose' => array('textarea', array(
						'label' => 'Purpose and goals: general purpose, type of activities, affiliation with other groups or national organizations',
						'class' => 'full-width full-height',
						'required' => true,
					)),

					'membership' => array('textarea', array(
						'label' => 'Membership eligibility, standards and requirements',
						'class' => 'full-width full-height',
						'required' => true,
					)),

					'leadership' => array('textarea', array(
						'label' => 'Leadership eligibility, standards and requirements (excluding grade requirements)',
						'class' => 'full-width full-height',
						'required' => true,
					)),

					'is_open' => array('radio', array(
						'label' => 'Is your organization\'s leadership or membership contingent on any of the following (outlined in the <a href="http://student-rules.tamu.edu/statement" target="_blank">university\'s statement on harassment and discrimination</a>)',
						'description' => 'Race, Color, Age, Religion, Sex, Disability, National or Ethnic Origin, Sexual Orientation, Veteran Status',
						'multiOptions' => array(
							0 => 'Yes',
							1 => 'No',
						),
						'required' => true,
					)),

					'discipline_officers' => array('textarea', array(
						'label' => 'Procedures for disciplining and/or removing an officer',
						'description' => 'This item must include:<br />
							<ul>
								<li>A notice to the officer of the issue</li>
								<li>A process whereby the officer is given an opportunity to share his/her perspective</li>
							</ul>',
						'class' => 'full-width full-height',
						'required' => true,
					)),

					'discipline_members' => array('textarea', array(
						'label' => 'Procedures for disciplining and/or removing a member',
						'description' => 'This item must include:<br />
							<ul>
								<li>A notice to the member of the issue</li>
								<li>A process whereby the member is given an opportunity to share his/her perspective</li>
							</ul>',
						'class' => 'full-width full-height',
						'required' => true,
					)),

					'officer_selection' => array('textarea', array(
						'label' => 'Officer selection processes',
						'description' => 'This item must include:<br />
							<ul>
								<li>The timing of the selection</li>
								<li>A description of the selection method (voting, appointment, etc)</li>
							</ul>',
						'class' => 'full-width full-height',
						'required' => true,
					)),

					'officer_duties' => array('textarea', array(
						'label' => 'Specific officer duties listed for any elected, selected or appointed leadership position',
						'class' => 'full-width full-height',
						'required' => true,
					)),

					'eligibility' => array('textarea', array(
						'label' => 'Eligibility requirements',
						'description' => 'The GPR requirement found in the University Student Rules must be used as the minimum standard. This statement is listed below. If your organization is using a more stringent standard than the one listed, update the text field accordingly.',
						'class' => 'full-width full-height',
						'required' => true,
						'default' => $eligibility_clause,
					)),

					'financial' => array('textarea', array(
						'label' => 'Financial procedures',
						'description' => 'The process below is required for use by all student organizations, as described in University Student Rules.',
						'class' => 'full-width full-height',
						'required' => true,
						'default' => $finances_clause,
					)),

					'advisor' => array('textarea', array(
						'label' => 'Advisor expectations',
						'description' => 'These can be adapted from the acknowledgement of advisor responsibilities that is a part of advisor training.',
						'class' => 'full-width full-height',
						'required' => true,
					)),

					'amendment' => array('textarea', array(
						'label' => 'Constitutional amentment procedure',
						'description' => 'If amendments are voted and approved by membership, include the number of votes needed from membership for approval.',
						'class' => 'full-width full-height',
						'required' => true,
					)),
				),
			),
			
			'understanding' => array(
				'legend' => 'Statement of Understanding',
				'elements' => array(
					'statement' => array('markup', array(
						'markup' => '
							<p>Upon initialing this Statement of Understanding:</p>
							<ul>
								<li>I am certifying that all of the required elements are contained within the constitution for this Recognized Student Organization.</li>
								<li>I understand that the Department of Student Activities will be reviewing the constitution and should any elements not be found within the constitution I will be contacted in order to facilitate any necessary changes to the constitution.</li>
								<li>I also understand that in the event that requested changes are not submitted in a timely manner to the Department of Student Activities, the recognition status of the organization may be affected.</li>
								<li><b>I also understand that if there is a discrepancy between this constitution summary and the full constitution on file, this summary will take precedence.</b></li>
							</ul>',
					)),

					'initials' => array('text', array(
						'label' => 'Your Initials',
						'class' => 'input-small',
						'required' => true,
						'maxlength' => 5,
					)),
				),
			),
			
			'submit'		=> array(
				'elements'		=> array(
					'submit'		=> array('submit', array(
						'type'	=> 'submit',
						'label'	=> 'Save and Submit',
						'helper' => 'formButton',
						'class' => 'ui-button',
					)),
				),
			),
		),
	),
);