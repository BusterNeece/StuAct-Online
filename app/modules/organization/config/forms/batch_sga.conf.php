<?php
return array(
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'enctype'		=> 'multipart/form-data',
		'elements'		=> array(
		
			'certification' => array('markup', array(
				'markup' => 'I hereby certify that I have reviewed the SGA Constitution and understand that as a committee/organization within SGA, the SGA Constitution is the primary governing document for my committee/organization.',
			)),

			'CSLName' => array('text', array(
				'label' => 'Chief Student Leader Name',
				'class' => 'half-width',
				'required' => true,
			)),

			'CSLInitials' => array('text', array(
				'label' => 'Chief Student Leader Initials',
				'required' => true,
			)),

			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Generate and Submit Constitution',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);