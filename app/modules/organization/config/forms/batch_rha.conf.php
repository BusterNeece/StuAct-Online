<?php
return array(
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'enctype'		=> 'multipart/form-data',
		'elements'		=> array(
		
			'certification' => array('markup', array(
				'markup' => 'I hereby certify that I have reviewed the current Residence Hall Association Constitution and understand that as a hall council within RHA, the RHA Constitution is the primary governing document for my hall council. I further certify that a current copy of our hall council bylaws will be kept on file with the Department of Residence Life/Residence Hall Association.',
			)),

			'CSLName' => array('text', array(
				'label' => 'Chief Student Leader Name',
				'class' => 'half-width',
				'required' => true,
			)),

			'CSLInitials' => array('text', array(
				'label' => 'Chief Student Leader Initials',
				'required' => true,
			)),

			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Generate and Submit Constitution',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);