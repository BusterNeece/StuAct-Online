<?php
return array(
	'form' => array(
		'method' => 'post',
		'elements' => array(

			'account' => array('text', array(
				'label' => 'Account Username (E-mail Address Prefix)',
				'description' => 'Only enter the part of the e-mail address before the @ symbol, i.e. for president@mysite.tamu.edu, enter "president".',
				'required' => true,
			)),

			'destination' => array('textarea', array(
				'label' => 'Destination Address(es)',
				'description' => 'To forward to more than one address, enter each one on a new line.',
				'required' => true,
			)),

			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Create Forwarder',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),

		),
	),
);