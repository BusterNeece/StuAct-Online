<?php
return array(
	'form' => array(
		'method' => 'post',
		'elements' => array(

			'about' => array('markup', array(
				'label' => 'About Student Organization Web Hosting',
				'markup' => '<p>The Department of Student Activities provides free web hosting to all active student organizations at Texas A&M University. As long as your organization maintains its active recognition status, your group will be able to use an official ".tamu.edu" domain name to promote its activities and events, send and receive e-mails, and more.</p>

					<p>All student organization web sites use a powerful content management system called <a href="http://www.drupal.org/" target="_blank">Drupal</a>, which makes it very easy to set up the basic content of your web site, while offering thousands of modules and themes to add more advanced functionality if needed.</p>',
			)),

			'it_web_addr' => array('text', array(
				'label' => 'Desired Web Address',
				'class' => 'half-width',
				'required' => true,
			)),

			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Request Web Site',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),

		),
	),
);