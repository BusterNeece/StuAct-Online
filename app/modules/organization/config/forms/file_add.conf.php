<?php

return array(
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'enctype'		=> 'multipart/form-data',
		'elements' 		=> array(

			'file' => array('file', array(
				'label' => 'Select File',
				'required' => true,
			)),

			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Upload New File',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),

		),
	),
);