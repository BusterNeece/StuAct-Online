<?php
return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method' => 'post',
		'elements' => array(

			'current_date' => array('markup', array(
				'label' => 'Current Status Change Date',
			)),

			'new_date' => array('unixdate', array(
				'label' => 'New Status Change Date',
				'required' => true,
			)),

			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),

		),
	),
);