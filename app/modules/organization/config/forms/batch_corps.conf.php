<?php
return array(
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'enctype'		=> 'multipart/form-data',
		'elements'		=> array(

			'about' => array('markup', array(
				'label' => 'Statement of Understanding',
				'markup' => '
					<p>I hereby certify that I have reviewed a current copy of The Standard and Policy and Procedures Guide of the Corps of Cadets and understand that as a staff or unit within the Corps, The Standard and PPG are the primary governing documents for my staff or unit.  In addition, my staff or unit agrees to abide by student leader eligibility requirements and on-campus banking requirements as defined by Student Rule 41.</p>
				',
			)),

			'CSLName' => array('text', array(
				'label' => 'Commanding Officer/Chief Student Leader Name',
				'class' => 'half-width',
				'required' => true,
			)),

			'CSLInitials' => array('text', array(
				'label' => 'Commanding Officer/Chief Student Leader Initials',
				'required' => true,
			)),

			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Generate and Submit Constitution',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);