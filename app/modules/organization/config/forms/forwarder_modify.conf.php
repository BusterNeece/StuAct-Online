<?php
return array(
	'form' => array(
		'method' => 'post',
		'elements' => array(
		
			'destination' => array('textarea', array(
				'label' => 'Destination Address(es)',
				'description' => 'To forward to more than one address, enter each one on a new line.',
				'required' => true,
			)),

			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),

		),
	),
);