<?php
/**
 * IsAllowed - Organization-Specific ACL Hook
 */

namespace DF\View\Helper;
class IsAllowed extends \Zend_View_Helper_Abstract
{
	public function isAllowed($action)
	{
		$org_acl = \StuAct\Acl\Organization::getInstance();
    	return $org_acl->isAllowed($action, $this->view->organization);
	}
}