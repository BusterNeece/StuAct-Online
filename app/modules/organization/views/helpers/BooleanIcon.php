<?php
namespace DF\View\Helper;

class BooleanIcon extends \Zend_View_Helper_Abstract
{
	public function booleanIcon($status, $size = 'large')
	{	
		return $this->view->icon(array(
			'type'		=> ($size == "large") ? 'png32' :'png',
			'image'		=> ($status) ? 'tick' : 'cross',
			'alt'		=> '(Status)',
			'title'		=> ($status) ? 'Yes' : 'No',
		));
	}
}