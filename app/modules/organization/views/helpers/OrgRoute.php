<?php
/**
 * OrgRoute - Organization-Specific Router Assistant
 */

namespace DF\View\Helper;
class OrgRoute extends \Zend_View_Helper_Abstract
{
	public function orgRoute($path = array())
	{
		if (!is_array($path))
			$path = array('controller' => $path);

		$path['module'] = 'organization';
		$path['id'] = $this->view->organization->id;

		return $this->view->route($path);
	}
}