<?php
/**
 * Organization Homepage
 */

use \Entity\Organization;
use \Entity\User;

class Organization_IndexController extends \StuAct\Controller\Action\Organization
{
	public function permissions()
	{
		return TRUE; // No login required (auto-redirect)
	}

	public function init()
	{
		$this->_allow_no_org = TRUE;
		parent::init();
	}

	public function preDispatch()
	{
		parent::preDispatch();
		$this->view->active_tab = 'home';
	}

	public function indexAction()
	{
		// Generic Organization portal view.
		if (!$this->_org_id)
		{
			$this->render();
			return;
		}

		// Organization "home" tab.
		if (!$this->_isAllowed('home'))
		{
			$this->redirectFromHere(array('controller' => 'profile', 'action' => 'index'));
			return;
		}

		$this->view->logs = \Entity\Log::fetchByOrganization($this->_org_id, 5);
		$this->render('home');
	}
}