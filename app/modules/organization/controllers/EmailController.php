
<?php
use \Entity\Organization;
use \Entity\User;

class Organization_EmailController extends \StuAct\Controller\Action\Organization
{
	public function permissions()
	{
		return $this->_isAllowed('it_view');
	}

	protected $_cpanel;
	protected $_accounts;

	public function preDispatch()
	{
		parent::preDispatch();
		$this->view->active_tab = 'web';

		$this->_cpanel = new \StuAct\WebHostManager\CPanel($this->_organization);
		$this->_accounts = $this->_loadAccounts();
	}

	protected function _loadAccounts($force_reload = FALSE)
	{
		$cache_name = 'org_'.$this->_organization->id.'_cpanel';
		$cpanel_info = \DF\Cache::get($cache_name);

		if ((!$cpanel_info || $force_reload) && $this->_cpanel->checkStatus())
		{
			\DF\Session::suspend();

			// Quick check to verify passwords are in sync.
			$login_check = $this->_cpanel->checkLogin();
			if ($login_check === FALSE)
				$this->_cpanel->resetWebPassword();

			// Pull accounts.
			$cpanel_info = array(
				'email_accounts'		=> $this->_cpanel->listMailAccounts(),
				'forwarder_accounts'	=> $this->_cpanel->listForwarders(),
				'listserv_accounts'		=> $this->_cpanel->listMailingLists(),
			);

			\DF\Cache::set($cpanel_info, $cache_name);
			\DF\Session::resume();
		}

		return $cpanel_info;
	}
	protected function _clearCache()
	{
		$cache_name = 'org_'.$this->_organization->id.'_cpanel';
		\DF\Cache::delete($cache_name);
	}

	public function indexAction()
	{
		$this->view->listserv = new StuAct\WebHostManager\Listserv($this->_organization);
		$this->view->cpanel_info = $this->_loadAccounts();
	}

	/**
	 * Main Account
	 */

	public function checkmainaccountAction()
	{
		$uri = 'http://stuorg-webmail.tamu.edu/login/';
		$uri_params = array(
			'user'			=> $this->_organization->it_web_username,
			'pass'			=> $this->_organization->it_web_password,
			'login_theme'	=> 'cpanel',
			'submit'		=> 'Login',
		);

		$this->redirect($uri.'?'.http_build_query($uri_params));
		return;
	}

	/**
	 * E-mail Accounts
	 */

	public function emailaddAction()
	{
		$form = new \DF\Form($this->current_module_config->forms->email_add->form);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			if ($this->_cpanel->createEmail($data['account'], $data['password']))
				$this->alert('<b>E-mail address successfully added.</b><br />The newly created e-mail account can be accessed through the webmail link below.', 'green');
			else
				$this->alert('<b>An error occurred while creating this e-mail account.</b><br />Make sure the address does not already exist.', 'red');

			$this->_loadAccounts(TRUE);
			$this->redirectFromHere(array('action' => 'index', 'account' => NULL));
			return;
		}

		$this->view->headTitle('Add E-mail Account');
		$this->renderForm($form);
	}

	public function emailpasswordAction()
	{
		if (!$this->_hasParam('account'))
			throw new \DF\Exception\DisplayOnly('Account not specified.');

		$form = new \DF\Form($this->current_module_config->forms->email_password->form);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();
			$account = $this->_getParam('account');

			if ($this->_cpanel->changeEmailPassword($account, $data['password']))
				$this->alert('<b>E-mail account password successfully changed.</b>', 'green');
			else
				$this->alert('<b>An error occurred while attempting to change the e-mail account password.</b><br />No changes have been made.', 'red');

			$this->redirectFromHere(array('action' => 'index', 'account' => NULL));
			return;
		}

		$this->view->headTitle('Set E-mail Account Password');
		$this->renderForm($form);
	}

	public function emaildeleteAction()
	{
		if (!$this->_hasParam('account'))
			throw new \DF\Exception\DisplayOnly('Account not specified.');

		$account = $this->_getParam('account');

		if ($this->_cpanel->deleteEmail($account))
			$this->alert('<b>E-mail account successfully deleted.</b><br />All messages associated with this account have been deleted as well.', 'green');
		else
			$this->alert('<b>An error occurred while deleting the e-mail account specified.</b><br />No changes have been made.', 'green');

		$this->_loadAccounts(TRUE);
		$this->redirectFromHere(array('action' => 'index', 'account' => NULL));
		return;
	}

	/**
	 * E-mail Forwarders
	 */

	public function forwarderaddAction()
	{
		$form = new \DF\Form($this->current_module_config->forms->forwarder_add->form);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			if ($this->_cpanel->addForwarder($data['account'], $data['destination']))
				$this->alert('<b>E-mail forwarder successfully deleted.</b><br />Messages will begin forwarding from this address immediately.', 'green');
			else
				$this->alert('<b>An error occurred while creating the e-mail forwarder.</b><br />Check to make sure the address provided is not already taken.', 'red');

			$this->_loadAccounts(TRUE);
			$this->redirectFromHere(array('action' => 'index', 'account' => NULL));
			return;
		}

		$this->view->headTitle('Add Forwarder');
		$this->renderForm($form);
	}

	public function forwardermodifyAction()
	{
		if (!$this->_hasParam('account'))
			throw new \DF\Exception\DisplayOnly('Account not specified.');

		$form = new \DF\Form($this->current_module_config->forms->forwarder_modify->form);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();
			$account = $this->_getParam('account');

			if ($this->_cpanel->changeForwarderDestination($account, $data['destination']))
				$this->alert('<b>E-mail forwarder destination successfully changed.</b><br />All new messages will begin forwarding to the new address.', 'green');
			else
				$this->alert('<b>An error occurred while modifying the forwarder destination.</b>', 'red');

			$this->_loadAccounts(TRUE);
			$this->redirectFromHere(array('action' => 'index', 'account' => NULL));
			return;
		}

		$this->view->headTitle('Set Forwarder Destination');
		$this->renderForm($form);
	}

	public function forwarderdeleteAction()
	{
		if (!$this->_hasParam('account'))
			throw new \DF\Exception\DisplayOnly('Account not specified.');

		$account = $this->_getParam('account');

		if ($this->_cpanel->deleteForwarder($account))
			$this->alert('<b>E-mail forwarder successfully deleted.</b>', 'green');
		else
			$this->alert('<b>An error occurred while deleting the e-mail forwarder specified.</b><br />No changes have been made.', 'green');

		$this->_loadAccounts(TRUE);
		$this->redirectFromHere(array('action' => 'index', 'account' => NULL));
		return;
	}

	/**
	 * Mailing Lists
	 */

	public function listservaddAction()
	{
		$form = new \DF\Form($this->current_module_config->forms->listserv_add->form);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			if ($this->_cpanel->addMailingList($data['account']))
			{
				$this->alert('<b>Mailing list successfully created.</b><br />Add members to the new mailing list below.', 'green');
				$this->_loadAccounts(TRUE);
				$this->redirectFromHere(array('action' => 'listservmembers', 'account' => $data['account']));
			}
			else
			{
				$this->alert('<b>An error occurred while creating the mailing list specified.</b><br />No changes have been made.', 'red');
				$this->redirectFromHere(array('action' => 'index', 'account' => NULL));
			}
			return;
		}

		$this->view->headTitle('Add Mailing List');
		$this->renderForm($form);
	}

	public function listservmembersAction()
	{
		if (!$this->_hasParam('account'))
			throw new \DF\Exception\DisplayOnly('Account not specified.');

		$listserv = new \StuAct\WebHostManager\Listserv($this->_organization);
		$account = $this->_cpanel->filterEmailAddress($this->_getParam('account'));

		$this->view->account = $account;

		switch(strtolower($this->_getParam('action2', 'view')))
		{
			case 'delete_all':
				$listserv->removeAllMembers($account);

				$this->alert('<b>All members removed.</b>', 'green');
				$this->redirectFromHere(array('action2' => NULL));
				return;
			break;
				
			case 'delete':
				$members = (is_array($_REQUEST['member'])) ? $_REQUEST['member'] : array($_REQUEST['member']);
				$members_removed = 0;
				
				foreach($members as $member)
				{
					if ($listserv->removeMember($account, $member))
						$members_removed++;
				}
				
				$this->alert('<b>'.$members_removed.' member(s) were removed from the mailing list.</b>', 'green');
				$this->redirectFromHere(array('action2' => NULL));
				return;
			break;
			
			case 'add':
				$addresses = explode("\n", $_REQUEST['addresses']);

				$new_status = intval($_REQUEST['status']);
				$num_members_added = $listserv->addMembers($account, $addresses, $new_status);
				
				$this->alert('<b>'.$num_members_added.' member(s) added to mailing list.</b>', 'green');
				$this->redirectFromHere(array('action2' => NULL));
				return;
			break;
				
			case 'export':
				$listserv_members = $listserv->listMembers($account);
				
				$export_data = array();
				$export_data[] = array('E-mail Address', 'Permissions');
				
				foreach($listserv_members as $member)
				{
					$export_data[] = array($member['address'], $member['status_name']);
				}

				$this->doNotRender();
				\DF\Export::csv($export_data);
				return;
			break;
			
			default:
				$this->view->listserv_members = $listserv->listMembers($account);
			break;
		}
	}

	public function listservdeleteAction()
	{
		$account = $this->_getParam('account');

		if ($account)
		{
			if ($this->_cpanel->deleteMailingList($account))
			{
				$this->alert('<b>Mailing list successfully deleted.</b>', 'green');
			}
			else
			{
				$this->alert('<b>An error occurred while deleting the mailing list specified.</b><br />No changes have been made.', 'green');
			}
		}
		
		$this->_loadAccounts(TRUE);
		$this->redirectFromHere(array('action' => 'index', 'account' => NULL));
	}
}