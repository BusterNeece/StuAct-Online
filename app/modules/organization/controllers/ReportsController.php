<?php
/**
 * Organization Reports
 */

use \Entity\Organization;
use \Entity\User;

class Organization_ReportsController extends \DF\Controller\Action
{
	public function permissions()
	{
		return $this->acl->isAllowed('admin_orgs');
	}

	public function indexAction()
	{}

	public function restrictedAction()
	{
		$org_settings = Organization::loadSettings();
		$this->view->org_settings = $org_settings;

		$restricted_orgs = $this->em->createQuery('SELECT o FROM Entity\Organization o WHERE o.status = :status ORDER BY o.date_status_changed ASC')
			->setParameter('status', ORG_STATUS_RESTRICTED)
			->execute();

		$this->view->orgs = $restricted_orgs;
	}
}