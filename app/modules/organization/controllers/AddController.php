<?php
/**
 * Organization Homepage
 */

use \Entity\Organization;

class Organization_AddController extends \StuAct\Controller\Action\Organization
{
	public function permissions()
	{
		return TRUE; // No login required
	}

	public function init()
	{
		$this->_allow_no_org = TRUE;
		parent::init();
	}

	public function indexAction()
	{
		$q = trim($this->_getParam('q', ''));

		if ($q)
		{
			$this->view->q = $q;
			$this->view->results = Organization::search($q);
		}
	}
}