<?php
use \Entity\Organization;
use \Entity\User;

class Organization_WebController extends \StuAct\Controller\Action\Organization
{
	public function permissions()
	{
		return $this->_isAllowed('it_view');
	}

	public function preDispatch()
	{
		parent::preDispatch();
		$this->view->active_tab = 'web';
	}

	public function indexAction()
	{}

	// Request Web Account Form
	public function requestAction()
	{
		if ($this->_organization->it_web_pending)
			throw new \DF\Exception\DisplayOnly('The Department of Student Activities has received your request for web account space. Requests are typically processed within a week of submission. You will be automatically notified once your account has been set up.');

		if ($this->_organization->it_web_type != 0)
		{
			$this->redirectFromHere(array('action' => 'index'));
			return;
		}

		$form_config = $this->current_module_config->forms->web_request->form;
		$form = new \DF\Form($form_config);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();
			$addr = str_replace(array('http://', 'www.', '.tamu.edu'), array('', '', ''), $data['it_web_addr']).'.tamu.edu';

			if (strcmp($this->_organization->it_web_addr, $addr) !== 0)
			{
				$domain_result = \StuAct\Domain::lookup($addr);
				if (!$domain_result['available'])
					throw new \DF\Exception\DisplayOnly('The domain name you have requested is not available for registration. It is currently hosted by "'.$domain_result['current'].'". Please submit a new domain name or contact Student Activities for assistance.');
			}

			$this->_organization->it_web_addr = $addr;
			$this->_organization->it_web_pending = 1;
			$this->_organization->it_web_drupal_pending = 1;
			$this->_organization->it_web_pending_date = time();
			$this->_organization->save();

			$this->_organization->notify('it_new_account', array(
				'email'		=> 'help@dsa.tamu.edu',
				'address'	=> $addr,
			));

			$this->alert('<b>Web site request submitted successfully.</b><br>Requests are typically processed within the week they are submitted. You will be notified via e-mail with any updates.');
			$this->redirectFromHere(array('controller' => 'index', 'action' => 'index'));
			return;
		}

		$this->view->headTitle('Request New Web/E-mail Account');
		$this->renderForm($form);
	}

	/**
	 * User-side Functional Pages
	 */

	// Drupal Auto-login
	public function loginAction()
	{
		$ticket = new \Entity\DrupalLoginTicket;
		$ticket->id = md5(mt_rand(1, 100).time().$this->_organization->it_web_username);
		$ticket->organization = $this->_organization;
		$ticket->save();

		$autologin_params = array(
			'nce_auth_ticket'		=> $ticket->id,
			'org_id'				=> $this->_org_id,
		);

		$ping_result = \StuAct\Domain::lookup($this->_organization->it_web_addr);

		if ($ping_result['current'] == "stuorg-sites.tamu.edu")
			$base_url = 'http://'.$this->_organization->it_web_addr;
		else
			$base_url = 'http://stuorg-sites.tamu.edu/~'.$this->_organization->it_web_username;

		$autologin_url = $base_url.'/autologin?'.http_build_query($autologin_params);

		$this->redirect($autologin_url);
		return;
	}

	public function drupalinstallcontentAction()
	{
		$drupal = new \StuAct\Drupal($this->_organization);

		$module = $this->_getParam('project');
		$result = $drupal->installProject($module);

		if ($result)
			$this->alert('<b>The Drupal item you requested was successfully installed.</b><br>You can now visit your web site to enable it and set it up.', 'green');
		else
			$this->alert('<b>The Drupal item you requested could not be installed.</b><br>This can be because the module name was mistyped or there is no available compatible version.', 'red');

		$this->redirectFromHere(array('action' => 'index'));
		return;
	}

	// cPanel Auto-Login
	public function cpanelloginAction()
	{
		$uri = 'http://stuorg-cpanel.tamu.edu/login/';
		$uri_params = array(
			'user'			=> $this->_organization->it_web_username,
			'pass'			=> $this->_organization->it_web_password,
			'login_theme'	=> 'cpanel',
			'submit'		=> 'Login',
		);

		$this->redirect($uri.'?'.http_build_query($uri_params));
		return;
	}

	// Reset Account Password
	public function resetpasswordAction()
	{
		$cpanel = new \StuAct\WebHostManager\CPanel($this->_organization);
		$cpanel->resetWebPassword();

		$this->alert('<b>Master password reset.</b>', 'green');
		$this->redirectFromHere(array('action' => 'index'));
	}
}