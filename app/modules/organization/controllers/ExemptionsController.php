<?php
use \Entity\Organization;
use \Entity\User;
use \Entity\GprExemption;

class Organization_ExemptionsController extends \StuAct\Controller\Action\Organization
{
	public function permissions()
	{
		return $this->_isAllowed('gpr_exemptions');
	}

	public function preDispatch()
	{
		parent::preDispatch();
		$this->view->active_tab = 'roster';
	}

	/**
	 * Actions
	 */

	public function indexAction()
	{
		$exemption_types = array(
			GprExemption::TYPE_TEMPORARY => 'Temporary',
			GprExemption::TYPE_PERMANENT => 'Permanent',
		);

		$exemptions_by_type = array();
		$exemptions = $this->em->createQuery('SELECT ge, u FROM Entity\GprExemption ge LEFT JOIN ge.user u WHERE ge.org_id = :org_id ORDER BY ge.timestamp DESC')
			->setParameter('org_id', $this->_org_id)
			->getArrayResult();

		foreach($exemptions as $exemption)
		{
			$exemptions_by_type[$exemption['type']][] = $exemption;
		}

		$this->view->exemption_types = $exemption_types;
		$this->view->exemptions = $exemptions_by_type;
	}

	public function addAction()
	{
		$uin = (int)$this->_getParam('uin');
		$user = User::getOrCreate($uin);

		if (!($user instanceof User))
			throw new \DF\Exception\DisplayOnly('User not found!');

		$exemption = new GprExemption;
		$exemption->organization = $this->_organization;
		$exemption->uin = $user->uin;
		$exemption->user = $user;
		$exemption->type = $this->_getParam('type');
		$exemption->save();

		$this->alert('<b>Exemption added.</b>', 'green');
		$this->redirectFromHere(array('action' => 'index', 'type' => NULL, 'uin' => NULL));
		return;
	}

	public function removeAction()
	{
		$id = (int)$this->_getParam('exemption_id');
		$exemption = GprExemption::find($id);

		if ($exemption)
			$exemption->delete();

		$this->alert('<b>Exemption removed.</b>', 'green');
		$this->redirectFromHere(array('action' => 'index', 'exemption_id' => NULL));
		return;
	}
}