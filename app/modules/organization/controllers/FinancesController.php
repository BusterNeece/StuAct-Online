<?php
use \Entity\Organization;
use \Entity\User;
use \Entity\Statement;

class Organization_FinancesController extends \StuAct\Controller\Action\Organization
{
	public function permissions()
	{
		return $this->_isAllowed('sofc_view');
	}

	public function preDispatch()
	{
		parent::preDispatch();
		$this->view->active_tab = 'finances';
	}

	public function indexAction()
	{
		$this->redirectFromHere(array('action' => 'statements'));
	}

	public function statementsAction()
	{
		if (!$this->_organization->account_number)
			throw new \DF\Exception\DisplayOnly('This organization does not have an SOFC account number. Contact Student Activities for more information.');
		
		$statements_raw = $this->em->createQuery('SELECT s FROM Entity\Statement s WHERE s.account = :acct ORDER BY s.year DESC, s.month DESC')
			->setParameter('acct', $this->_organization->account_number)
			->getArrayResult();

		if (empty($statements_raw))
			throw new \DF\Exception\DisplayOnly('This organization does not have any SOFC statements from previous months.');
		
		$statements = array();
		$months = $this->config->general->months->toArray();

		foreach($statements_raw as $file)
		{
			$year = (int)$file['year'];
			$month = $months[$file['month']];

			$statements[$year][$month][] = $file;
		}

		$this->view->statements = $statements;
	}

	public function viewstatementAction()
	{
		$statement_id = (int)$this->_getParam('statement_id');
		$statement = Statement::find($statement_id);

		if (!$statement || $statement->account != $this->_organization->account_number)
			throw new \DF\Exception\PermissionDenied;

		$file_fullname = $statement->path;

		if (!file_exists($file_fullname))
			throw new \DF\Exception\DisplayOnly('The statement you requested could not be downloaded.');
		
		if ($file_fullname && file_exists($file_fullname))
		{
			$this->doNotRender();

			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private", FALSE);
			header("Content-Type: application/pdf");
			header("Content-Disposition: inline; filename=".basename($file_fullname));
			header("Content-Length: ".filesize($file_fullname));
			
			echo readfile($file_fullname);
			return;
			exit;
		}
		break;
	}

	public function sigcardAction()
	{
		$sigcard = \Zend_Pdf::load(DF_INCLUDE_STATIC.'/documents/sofc_sigcard_2010.pdf');

		$color_black = new \Zend_Pdf_Color_Html('#000000');
		$color_lightgray = new \Zend_Pdf_Color_Html('#BBBBBB');

		$sigcard_page1 = $sigcard->pages[0];

		$font_courier = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_COURIER_BOLD);
		$font_trebuchet = \Zend_Pdf_Font::fontWithPath(DF_INCLUDE_STATIC.'/fonts/trebuc.ttf');
		$font_trebuchet_bold = \Zend_Pdf_Font::fontWithPath(DF_INCLUDE_STATIC.'/fonts/trebucbd.ttf');
		$font_barcode = \Zend_Pdf_Font::fontWithPath(DF_INCLUDE_STATIC.'/fonts/FRE3OF9X.TTF');

		$x_offset = 0;
		$y_offset = 782;

		// Usage of drawText:
		// $sigcard_page1->drawText('text', x, -y);

		// Writing barcode
		$sigcard_page1->setFont($font_barcode, 45);
		$sigcard_page1->drawText('*OAP005*', $x_offset+50, $y_offset-104);

		// Writing Account ID
		$sigcard_page1->setFont($font_courier, 16);

		for($i = 0; $i < 6; $i++)
		{
			$org_id_num = substr($this->_organization->account_number, $i, 1);
			$sigcard_page1->drawText($org_id_num, $x_offset+237+(18 * $i), $y_offset-100);
		}

		// Writing current date
		$date_string = date('mdY');

		for($i = 0; $i < 8; $i++)
		{
			$date_item = substr($date_string, $i, 1);
			$sigcard_page1->drawText($date_item, $x_offset+406+(18 * $i), $y_offset-100);
		}

		// Writing organization name
		$org_name = htmlspecialchars_decode($org_info['org_name']);
		$font_size = 18 - (round(strlen($org_name) / 8) * 1);

		$sigcard_page1->setFont($font_trebuchet, $font_size);
		$sigcard_page1->drawText($this->_organization->name, $x_offset+22, $y_offset-135);

		// Writing cycle month
		$sigcard_page1->setFont($font_trebuchet, 14);
		$sigcard_page1->drawText($this->config->general->months->{$this->_organization->cycle}, $x_offset+400, $y_offset-135);

		// Writing officer information
		$print_officers = array();
		$print_advisors = array();

		$sigcard_arrangement = $this->_organization->getPositionsOnSigcard();
		foreach($sigcard_arrangement as $position_id => $user_id)
		{
			$position = $this->_organization->getOfficerByPosition($position_id);

			$position->user->assignFromDirectory();
			$this->em->persist($position->user);

			if ($position->isInGroup('advisors'))
				$print_advisors[] = $position;
			else
				$print_officers[] = $position;
		}

		$this->em->flush();

		$current_row = 0;
		$increase_per_row = 40;
		$this_y = $y_offset - 284;

		foreach($print_officers as $position)
		{
			$current_row++;
			$this_y -= $increase_per_row;

			$officer = $position->user;
			$print_uin = $this->_getPrintableUIN($officer->uin);
			
			// First column
			$sigcard_page1->setFont($font_trebuchet_bold, 8);
			$sigcard_page1->drawText(strtoupper($position->name), $x_offset+20, $this_y+14);
			$sigcard_page1->setFont($font_trebuchet_bold, 14);
			$sigcard_page1->drawText($officer['firstname'].' '.$officer['lastname'], $x_offset+20, $this_y);
			$sigcard_page1->setFont($font_trebuchet, 8);
			$sigcard_page1->drawText($print_uin, $x_offset+20, $this_y-10);
			
			// Second column
			$sigcard_page1->setFont($font_trebuchet_bold, 8);
			$sigcard_page1->drawText($officer['email'], $x_offset+180, $this_y+5);
			$sigcard_page1->setFont($font_trebuchet, 8);
			$sigcard_page1->drawText($officer['phone'], $x_offset+180, $this_y-5);
			
			// Third column
			$sigcard_page1->drawText($officer['addr1'], $x_offset+320, $this_y+5);
			$sigcard_page1->drawText($officer['addrcity'].', '.$officer['addrstate'].' '.$officer['addrzip'], $x_offset+320, $this_y-5);
		}

		$current_row = 0;
		$increase_per_row = 40;
		$this_y = $y_offset - 170;

		foreach($print_advisors as $position)
		{
			$current_row++;
			$this_y -= $increase_per_row;

			$advisor = $position->user;
			$print_uin = $this->_getPrintableUIN($advisor->uin);
				
			// First column
			$sigcard_page1->setFont($font_trebuchet_bold, 8);
			$sigcard_page1->drawText(strtoupper($position->name), $x_offset+20, $this_y+14);
			$sigcard_page1->setFont($font_trebuchet_bold, 14);
			$sigcard_page1->drawText($advisor['firstname'].' '.$advisor['lastname'], $x_offset+20, $this_y);
			$sigcard_page1->setFont($font_trebuchet, 8);
			$sigcard_page1->drawText($print_uin, $x_offset+20, $this_y-10);
			
			// Second column
			$sigcard_page1->setFont($font_trebuchet_bold, 8);
			$sigcard_page1->drawText($advisor['email'], $x_offset+180, $this_y+5);
			$sigcard_page1->setFont($font_trebuchet, 8);
			$sigcard_page1->drawText($advisor['phone'], $x_offset+180, $this_y-5);
			
			// Third column
			$sigcard_page1->drawText($advisor['addr1'], $x_offset+320, $this_y+5);
			$sigcard_page1->drawText($advisor['addr2'], $x_offset+320, $this_y-5);

			if ($current_row == 2)
				break;
		}

		$this->doNotRender();

		// Printing the results to the screen.
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", FALSE);
		header("Content-Type: application/pdf");
		header("Content-Disposition: inline; filename=SOFCSignatureCard.pdf");

		echo $sigcard->render();
		return;
	}

	protected function _getPrintableUIN($uin)
	{
		$filtered_uin = str_pad($uin, 9, '0', STR_PAD_LEFT);
		$print_uin = 'XXX-XX-'.substr($filtered_uin, 5, 4);
		
		return $print_uin;
	}
}