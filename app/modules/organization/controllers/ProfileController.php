<?php
/**
 * Organization Homepage
 */

use \Entity\Organization;
use \Entity\User;

class Organization_ProfileController extends \StuAct\Controller\Action\Organization
{
	public function permissions()
	{
		return TRUE;
	}

	public function preDispatch()
	{
		parent::preDispatch();
		$this->view->active_tab = 'profile';
	}

	public function indexAction()
	{
		$this->redirectFromHere(array('action' => 'public'));
		return;
	}

	public function publicAction()
	{
		$this->_checkPermission('profile_public_view');

		// Set up public profile.
		$public_form_config = $this->current_module_config->forms->profile_public->form->toArray();
		unset($public_form_config['groups']['survey_questions']);

		$public_form = new \StuAct\Form($public_form_config);
		$public_form->populate($this->_organization->toArray(TRUE, TRUE));
		$this->view->public_form = $public_form;
	}

	public function staffAction()
	{
		$this->_checkPermission('profile_staff_view');

		// Set up staff profile.
		$staff_form_config = $this->current_module_config->forms->profile_staff->form->toArray();
		$staff_form = new \StuAct\Form($staff_form_config);
		$staff_form->populate($this->_organization->toArray(TRUE, TRUE));
		$this->view->staff_form = $staff_form;
	}

	public function editpublicAction()
	{
		$this->_checkPermission('profile_public_edit');

		$form_config = $this->current_module_config->forms->profile_public->form->toArray();
		$form = new \StuAct\Form($form_config);
		$form->populate($this->_organization->toArray(TRUE, TRUE));

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			try
			{
				$this->_organization->fromArray($data);
				$this->_organization->date_last_updated = time();
				$this->_organization->save();

				\Entity\Log::post(array(
					'organization'	=> $this->_organization,
					'user'			=> $this->auth->getLoggedInUser(),
					'type'			=> 'profile',
					'message' 		=> 'Edited public profile.',
				));

				if (stristr($data['info_banking_usage'], 'offcampus') !== FALSE)
				{
					$email_config = $this->config->email->toArray();
					$this->_organization->notify('sofc_profile_answers');
				}

				$this->alert('<b>Public profile saved.</b>', 'green');
				$this->redirectFromHere(array('action' => 'index'));
				return;
			}
			catch(\Exception $e)
			{
				$this->alert('An error occurred: '.$e->getMessage(), 'red');
			}
		}

		$this->renderForm($form, 'edit', 'Edit Public Profile');
	}

	public function editstaffAction()
	{
		$this->_checkPermission('profile_staff_edit');

		// Set up staff profile.
		$form_config = $this->current_module_config->forms->profile_staff->form->toArray();
		$form = new \StuAct\Form($form_config);
		$form->populate($this->_organization->toArray(TRUE, TRUE));

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			try
			{
				$this->_organization->fromArray($data);
				$this->_organization->save();

				\Entity\Log::post(array(
					'organization'	=> $this->_organization,
					'user'			=> $this->auth->getLoggedInUser(),
					'type'			=> 'profile',
					'message' 		=> 'Edited staff profile.',
				));

				$this->alert('<b>Staff profile saved.</b>', 'green');
				$this->redirectFromHere(array('action' => 'index'));
				return;
			}
			catch(\Exception $e)
			{
				$this->alert('An error occurred: '.$e->getMessage(), 'red');
			}
		}

		$this->renderForm($form, 'edit', 'Edit Staff Profile');
	}
}