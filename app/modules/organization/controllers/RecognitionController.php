<?php
use \Entity\Organization;
use \Entity\OrganizationMiscellaneousRequirement;
use \Entity\User;

class Organization_RecognitionController extends \StuAct\Controller\Action\Organization
{
	public function permissions()
	{
		return $this->_isAllowed('home');
	}

	public function preDispatch()
	{
		parent::preDispatch();
		$this->view->active_tab = 'recognition';
	}

	// Recognition checklist
	public function indexAction()
	{}

	public function resyncAction()
	{
		$this->_checkPermission('profile_staff_edit');

		$this->_organization->synchronize();
		$this->_organization->save();

		$this->alert('<b>Organization recognition status resynchronized.</b>', 'green');
		$this->redirectFromHere(array('action' => 'index'));
		return;
	}

	public function statuschangedateAction()
	{
		$this->_checkPermission('profile_staff_edit');

		$form_config = $this->current_module_config->forms->status_change_date->form->toArray();

		$next_status_change = $this->_organization->next_status_change;
		if ($next_status_change != 0 && $this->_organization->status != ORG_STATUS_RECOGNIZED)
			$form_config['elements']['current_date'][1]['markup'] = date('F j, Y', $next_status_change);
		else
			throw new \DF\Exception\DisplayOnly('This organization currently is ineligible for a status date change due to its recognition status.');

		$form = new \StuAct\Form($form_config);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();
			$diff = $data['new_date'] - $next_status_change;

			$this->_organization->date_status_changed += $diff;
			$this->_organization->save();

			$this->alert('<b>Organization status change date saved.</b>', 'green');
			$this->redirectFromHere(array('action' => 'index'));
			return;
		}

		$this->renderForm($form, 'edit', 'Set Status Change Date');
	}

	// Miscellaneous requirements.
	public function miscAction()
	{
		$this->_checkPermission('profile_staff_edit');
	}

	public function editmiscAction()
	{
		$this->_checkPermission('profile_staff_edit');

		$form_config = $this->current_module_config->forms->misc->form;
		$form = new \DF\Form($form_config);

		$id = (int)$this->_getParam('misc_id');
		if ($id != 0)
		{
			$record = OrganizationMiscellaneousRequirement::find($id);
			$form->setDefaults($record->toArray(TRUE, TRUE));
		}

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			if (!$record instanceof OrganizationMiscellaneousRequirement)
			{
				$record = new OrganizationMiscellaneousRequirement;
				$record->organization = $this->_organization;
				$record->user = $this->auth->getLoggedInUser();
			}

			$record->fromArray($data);
			$record->save();

			$this->alert('<b>Miscellaneous requirement updated!</b>', 'green');
			$this->redirectFromHere(array('action' => 'misc', 'misc_id' => NULL));
			return;
		}

		$this->view->headTitle('Add/Update Miscellaneous Requirement');
		$this->renderForm($form);
	}

	public function deletemiscAction()
	{
		$this->_checkPermission('profile_staff_edit');
		
		$id = (int)$this->_getParam('misc_id');
		$record = OrganizationMiscellaneousRequirement::find($id);

		if ($record instanceof OrganizationMiscellaneousRequirement)
			$record->delete();

		$this->alert('<b>Miscellaneous requirement deleted!</b>', 'green');
		$this->redirectFromHere(array('action' => 'misc', 'misc_id' => NULL));
		return;
	}
}