<?php
use \Entity\Organization;
use \Entity\User;
use \Entity\Log;

class Organization_HistoryController extends \StuAct\Controller\Action\Organization
{
	public function permissions()
	{
		return $this->_isAllowed('logs_view');
	}

	public function preDispatch()
	{
		parent::preDispatch();
		$this->view->active_tab = 'resources';
	}

	/**
	 * Actions
	 */

	public function indexAction()
	{
		$qb = $this->em->createQueryBuilder()
			->select('l')
			->from('Entity\Log', 'l')
			->where('l.org_id = :org_id')
			->setParameter('org_id', $this->_organization->id)
			->orderBy('l.timestamp', 'DESC');

		$category = $this->_getParam('category', 'all');
		$this->view->category = $category;

		if ($category != "all")
		{
			$qb->andWhere('l.type = :type_id')->setParameter('type_id', $category);
		}

		$pager = new \DF\Paginator\Doctrine($qb, (int)$this->_getParam('page', 1));
		$this->view->pager = $pager;
	}
}