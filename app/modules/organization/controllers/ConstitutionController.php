<?php
use \Entity\Organization;
use \Entity\OrganizationFile;
use \Entity\User;

class Organization_ConstitutionController extends \StuAct\Controller\Action\Organization
{
	public function permissions()
	{
		return $this->_isAllowed('home');
	}

	public function preDispatch()
	{
		parent::preDispatch();
		$this->view->active_tab = 'recognition';
	}

	public function indexAction()
	{
		$user = $this->auth->getLoggedInUser();

		// Redirect batch groups who shouldn't use this page.
		if (!$this->_organization->ignore_batch_generator && $this->_organization->batch_group)
		{
			$bg = $this->_organization->batch_group;
			if ($bg->use_generator && $bg->short_name)
				$this->redirectFromHere(array('action' => 'batch'));
		}

		$const_data = (array)$this->_organization->constitution_data;

		if ($const_data)
		{
			$flag = $this->_organization->flag_constitution;

			if ($flag == ORG_FLAG_ORG_ACTION_REQ)
			{
				$const_message = '<b>Your previously submitted constitution was declined for the following reasons:</b>';
				$const_message .= '<ul><li>'.implode('</li><li>', (array)$this->_organization->constitution_declinereasons).'</li></ul>';

				if ($this->_organization->constitution_comments)
					$const_message .= '<blockquote>'.nl2br($this->_organization->constitution_comments).'</blockquote>';

				$const_message .= 'Please resubmit an updated constitution using the form below.';

				$this->alert($const_message, 'red');
			}
			elseif (!in_array($flag, array(ORG_FLAG_SACT_ACTION_REQ, ORG_FLAG_ORG_ACTION_REQ)))
		    {
		    	$this->alert('<b>Your organization has a previous constitution on file.</b><br>To download this file, <a href="'.\DF\Url::file($this->_organization->constitution_filename).'">click here</a>.', 'info');

		    	if (!$this->_hasParam('renew'))
		    	{
		    		// Show the "Renewal" page instead of the standard constitution summary page.
		    		$this->render('renew');
		    		return;
		    	}
		    	else if ($this->_getParam('renew') == "none")
		    	{
		    		$this->_organization->constitution_submitted_by = $user->id;
		    		$this->_organization->setFlag('constitution', ORG_FLAG_MEETS_REQ);
		    		$this->_organization->synchronize();
		    		$this->_organization->save();

		    		$this->_organization->log('Constitution renewed for another year.', 'profile', $user);

		    		$this->alert('<b>Constitution renewed successfully!</b><br />This organization\'s constitution has been renewed for the coming year.', 'green');
		    		$this->redirectFromHere(array('controller' => 'recognition', 'action' => 'index', 'renew' => NULL));
		    		return;
		        }
		    }
	    }

	    $form = new \DF\Form($this->current_module_config->forms->constitution->form);
	    $form->setDefaults($const_data);

	    if ($_POST && $form->isValid($_POST))
	    {
	    	$data = (array)$form->getValues();

	    	$uploaded_files = (array)$form->processFiles('organization', $this->_org_id);
	    	$data = array_merge($data, $uploaded_files);

	    	$this->_organization->constitution_filename = $data['constitution'][1];
	    	unset($data['constitution']);

	    	$this->_organization->constitution_data = $data;
	    	$this->_organization->constitution_pages = array('is_batch' => FALSE);
			
			// Remove old constitution claimant if previous constitution was completely processed.
			if (in_array($this->_organization->flag_constitution, array(ORG_FLAG_DOES_NOT_MEET_REQ, ORG_FLAG_MEETS_REQ)))
				$this->_organization->constitution_claimant = '';

			$this->_organization->constitution_submitted_by = $user->id;
			$this->_organization->setFlag('constitution', ORG_FLAG_SACT_ACTION_REQ);
			$this->_organization->save();

			// Attach to organization's filesystem.
			$file = new OrganizationFile;
			$file->organization = $this->_organization;
			$file->name = $this->_organization->constitution_filename;
			$file->title = 'Organization Constitution';
			$file->type = 'constitution';
			$file->save();
			
			// Notify staff.
			$this->_organization->notify('new_constitution');
			
			// Write to the log.
			\Entity\Log::post(array(
				'user'		=> $user,
				'organization' => $this->_organization,
				'message' 	=> 'New constitution uploaded.',
			));

			$this->alert('<b>Constitution uploaded successfully!</b><br />This organization\'s constitution has been uploaded and will be reviewed by a member of the Department of Student Activities staff.', 'green');

			if ($data['is_open'] == 0)
				$this->redirect('http://studentactivities.tamu.edu/recognition/openmemberexemption');
			else
				$this->redirectFromHere(array('controller' => 'recognition', 'action' => 'index', 'renew' => NULL));
			return;
		}

		$this->view->headTitle('Submit Constitution');
		$this->renderForm($form);
	}

	/**
	 * Batch Constitutions
	 */

	public function batchAction()
	{
		$batch_group = $this->_organization->batch_group;

		if (!$batch_group)
			throw new \DF\Exception\DisplayOnly('This organization is not a part of a batch group.');

		$batch_type = $batch_group->short_name;
		$form_config = $this->current_module_config->forms->{'batch_'.$batch_type}->form;
		$form = new \DF\Form($form_config);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();
			
			$const_extension = ($batch_type == "greek") ? 'htm' : 'pdf';
			$const_file_name = 'organization/'.date('Ymdhis').'-BatchConstitution.'.$const_extension;
			$const_file_path = \DF\File::getFilePath($const_file_name);

			switch($batch_type)
			{
				case "sga":
					$constitution = \Zend_Pdf::load(DF_INCLUDE_STATIC.'/documents/sga_agreement.pdf');
					$constitution_page1 = $constitution->pages[0];
					$font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
				
					$constitution_page1->setFont($font, 10);
					
					if ($this->_organization->profile_purpose)
					{
						$mission = wordwrap($this->_organization->profile_purpose, 90, "\n");
						$mission_parts = explode("\n", $mission);
						
						if (count($mission_parts) > 8)
						{
							$mission_parts = array_slice($mission_parts, 0, 8);
							$mission_parts[count($mission_parts)-1] .= '...';
						}
						
						$mission_y = 435;
						foreach($mission_parts as $mission_part)
						{
							$constitution_page1->drawText($mission_part, 74, $mission_y);
							$mission_y -= 10;
						}
					}

					$constitution_page1->setFont($font, 13);
					$constitution_page1->drawText($this->_organization->name, 74, 510);
					
					$constitution_page1->drawText($data['CSLName'], 74, 314);
					$constitution_page1->drawText($data['CSLInitials'], 74, 224);
					$constitution_page1->drawText(date('F j, Y'), 400, 224);
						
					// Upload result directly into filesystem rather than displaying.
					$constitution->save($const_file_path);
				break;
				
				case "greek":
					$form->populate($_POST);
					
					$constitution_file = $form->renderView();
					file_put_contents($const_file_path, $constitution_file);
				break;
				
				case "rha":
				case "corps":
					if ($batch_type == "rha")
						$base_document = DF_INCLUDE_STATIC.'/documents/RHA_Batch_Constitution.pdf';
					else if ($batch_type == "corps")
						$base_document = DF_INCLUDE_STATIC.'/documents/Corps_Batch_Constitution.pdf';
					
					$constitution = \Zend_Pdf::load($base_document);
					$constitution_page1 = $constitution->pages[0];
					$font = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
					
					$constitution_page1->setFont($font, 14);
					$constitution_page1->drawText($this->_organization->name, 72, 695);
					$constitution_page1->drawText($data['CSLName'], 72, 621);
					$constitution_page1->drawText($data['CSLInitials'], 72, 340);
					$constitution_page1->drawText(date('F j, Y'), 480, 30);
					
					// Upload result directly into filesystem rather than displaying.
					$constitution->save($const_file_path);
				break;
			}

			$this->_organization->constitution_filename = $const_file_name;
			$this->_organization->constitution_pages = array('is_batch' => TRUE);

			if (in_array($this->_organization->flag_constitution, array(ORG_FLAG_DOES_NOT_MEET_REQ, ORG_FLAG_MEETS_REQ)))
				$this->_organization->constitution_claimant = '';

			$this->_organization->setFlag('constitution', ORG_FLAG_SACT_ACTION_REQ);
			$this->_organization->save();

			// Attach to organization's filesystem.
			$file = new OrganizationFile;
			$file->organization = $this->_organization;
			$file->name = $const_file_name;
			$file->title = 'Organization Constitution';
			$file->description = 'Constitution automatically generated by StuAct Online using the batch organization constitution generator.';
			$file->type = 'constitution';
			$file->save();

			// Notify staff.
			$this->_organization->notify('new_constitution');

			\Entity\Log::post(array(
				'user'		=> $this->auth->getLoggedInUser(),
				'organization' => $this->_organization,
				'message' 	=> 'Batch constitution generated.',
			));

			$this->alert('<b>Constitution uploaded successfully!</b><br />This organization\'s constitution has been uploaded and will be reviewed by a member of the Department of Student Activities staff.', 'green');

			$this->redirectFromHere(array('controller' => 'recognition', 'action' => 'index'));
			return;
		}

		$this->view->headTitle('Generate Batch Constitution');
		$this->renderForm($form);
	}
}