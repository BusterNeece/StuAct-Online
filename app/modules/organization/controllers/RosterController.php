<?php
use \Entity\Organization;
use \Entity\OrganizationOfficer;
use \Entity\OrganizationOfficerRequest;
use \Entity\OrganizationPermission;
use \Entity\User;

class Organization_RosterController extends \StuAct\Controller\Action\Organization
{
	public function permissions()
	{
		if ($this->_getActionName() == "newrequest")
			return $this->acl->isAllowed('is logged in');
		else if ($this->_getActionName() == "add")
			return $this->_isAllowed('members_add');
		else
			return $this->_isAllowed('members_view');
	}

	public function preDispatch()
	{
		parent::preDispatch();
		$this->view->active_tab = 'roster';
	}

	public function indexAction()
	{
		// Process and sort regular roster entries.
		$officers = $this->_organization->officer_positions;

		$groups = array(
			'advisors' => array(),
			'leaders' => array(),
			'officers' => array(),
			'members' => array(),
		);

		foreach($officers as $position)
		{
			if ($position->isInGroup('advisors'))
				$groups['advisors'][] = $position;
			elseif ($position->isInGroup('student_leaders'))
				$groups['leaders'][] = $position;
			elseif ($position->isInGroup('officers'))
				$groups['officers'][] = $position;
			else
				$groups['members'][] = $position;
		}

		$this->view->groups = $groups;

		// Show editable officer requests (if applicable)
		$requests = array();
		$requests_raw = $this->_organization->officer_requests;

		if (count($requests_raw) > 0)
		{
			foreach($requests_raw as $request)
			{
				if ($this->_org_acl->canModifyPosition($request->position, $this->_organization))
					$requests[] = $request;
			}
		}

		$this->view->requests = $requests;
	}

	public function togglepermissionAction()
	{
		$user_id = $this->_getParam('user_id');
		$user = User::find($user_id);

		if (!($user instanceof User))
			throw new \DF\Exception\DisplayOnly('User not found!');

		$position = $this->_organization->getUserPosition($user);

		if (!($position instanceof OrganizationOfficer))
			throw new \DF\Exception\DisplayOnly('Position not found!');
		else if (!$this->_org_acl->canModifyPosition($position))
			throw new \DF\Exception\PermissionDenied;

		$priv_name = $this->_getParam('permission');
		if ($position->hasPermission($priv_name))
		{
			// Remove permission.
			$perm = OrganizationPermission::getRepository()->findOneBy(array('org_id' => $this->_org_id, 'position' => $position->position, 'permission' => $priv_name));

			if ($perm instanceof OrganizationPermission)
				$perm->delete();
		}
		else
		{
			// Create new permission.
			$perm = new OrganizationPermission;
			$perm->organization = $this->_organization;
			$perm->position = $position->position;
			$perm->permission = $priv_name;
			$perm->save();
		}

		if ($this->isAjax())
		{
			$this->renderJson(array('status' => 'success'));
		}
		else
		{
			$this->alert('<b>Roster position special permission toggled.</b>', 'green');
			$this->redirectFromHere(array('action' => 'index', 'user_id' => NULL, 'permission' => NULL));
		}
		return;
	}

	public function processrequestAction()
	{
		$request_id = (int)$this->_getParam('request_id');
		$request = OrganizationOfficerRequest::getRepository()->findOneBy(array('id' => $request_id, 'org_id' => $this->_org_id));

		if (!($request instanceof OrganizationOfficerRequest))
			throw new \DF\Exception\DisplayOnly('The request you listed could not be found. It may have already been processed.');

		if (!$this->_org_acl->canModifyPosition($request->position, $this->_organization))
			throw new \DF\Exception\PermissionDenied;

		if ($this->_getParam('decision') == "approve")
		{
			$existing_position = $this->_organization->getUserPosition($request->user);
			if ($existing_position)
				$existing_position->delete();

			$officer = new OrganizationOfficer;
			$officer->organization = $this->_organization;
			$officer->user = $request->user;
			$officer->position = $request->position;
			$officer->save();

			$this->alert('<b>Roster request approved.</b>', 'green');
		}
		else
		{
			$request->delete();

			$this->alert('<b>Roster request declined.</b>', 'green');
		}

		$this->redirectFromHere(array('action' => 'index', 'request_id' => NULL, 'decision' => NULL));
		return;
	}

	public function addAction()
	{
		$form_config = $this->current_module_config->forms->roster_add->form->toArray();

		$positions = $this->_getAvailablePositions(FALSE);
		if (!$positions)
			throw new \DF\Exception\DisplayOnly('You are not eligible to assign a user to any positions.');

		$form_config['elements']['position'][1]['multiOptions'] = $positions;
		$form = new \DF\Form($form_config);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			$user = User::lookUp($data['user']);
			if (!$user)
				throw new \DF\Exception\DisplayOnly('User not found!');

			$position_num = $data['position'];
			if (!isset($positions[$position_num]))
				throw new \DF\Exception\PermissionDenied;

			$existing_position = $this->_organization->getUserPosition($user);
			if ($existing_position)
				$existing_position->delete();

			$officer = new OrganizationOfficer;
			$officer->organization = $this->_organization;
			$officer->user = $user;
			$officer->position = $position_num;
			$officer->save();

			$this->alert('<b>User added to roster.</b>', 'green');
			$this->redirectFromHere(array('action' => 'index'));
			return;
		}

		$this->view->headTitle('Roster');
		$this->renderForm($form, 'edit', 'Add User to Roster');
	}

	public function removeAction()
	{
		$user_id = $this->_getParam('user_id');
		$user = User::find($user_id);

		if (!($user instanceof User))
			throw new \DF\Exception\DisplayOnly('User not found!');

		$position = $this->_organization->getUserPosition($user);

		if (!($position instanceof OrganizationOfficer))
			throw new \DF\Exception\DisplayOnly('Position not found!');
		else if (!$this->_org_acl->canModifyPosition($position))
			throw new \DF\Exception\PermissionDenied;

		// All internal management functions are handled by the OrganizationOfficer class.
		$position_name = $position->name;
		$position->delete();

		$this->_organization->synchronize();

		$this->alert('<b>'.$position_name.' removed from roster.</b>', 'green');
		$this->redirectFromHere(array('action' => 'index', 'user_id' => NULL, 'permission' => NULL));
		return;
	}

	public function renewAction()
	{
		$positions = $this->_getAvailablePositions(TRUE);
		$this->view->positions = $positions;
	}

	public function newrequestAction()
	{
		$positions = $this->_getAvailablePositions(TRUE);

		if (!$positions)
			throw new \DF\Exception\DisplayOnly('You are not eligible to select any positions.');

		if ($this->_hasParam('position'))
		{
			$position_num = $this->_getParam('position');

			if (!isset($positions[$position_num]))
				throw new \DF\Exception\PermissionDenied;

			$user = $this->auth->getLoggedInUser();

			$request = new OrganizationOfficerRequest;
			$request->user = $user;
			$request->organization = $this->_organization;
			$request->position = $position_num;
			$request->save();

			$current_officer = $this->_organization->getOfficerByPosition($position_num);

			$additional_info = array(
				'request'		=> $request,
				'position_name'	=> $positions[$position_num],
				'current'		=> $current_officer,
			);
			
			// Notify the necessary people.
			if (isset($this->_settings['officer_groups']['advisors'][$position_num]))
				$this->_organization->notify('new_advisor', $additional_info);
			else if (isset($this->_settings['officer_groups']['leaders'][$position_num]))
				$this->_organization->notify('new_leader', $additional_info);
			else
				$this->_organization->notify('new_member', $additional_info);

			$this->alert('<b>Request submitted!</b><br>You will be contacted via e-mail when your request is processed by the organization.', 'green');
			$this->redirectFromHere(array('controller' => 'index', 'action' => 'index', 'position' => NULL));
			return;
		}

		$this->view->positions = $positions;
	}

	public function trainingAction()
	{
		// Process and sort regular roster entries.
		$officers = $this->_organization->officer_positions;
		$training = array();

		foreach($officers as $position)
		{
			if ($this->_org_acl->canModifyPosition($position->position, $this->_organization))
			{
				$user_training = $this->em->createQuery('SELECT t FROM Entity\Training t WHERE t.user_id = :user_id AND t.updated_at >= :start_date ORDER BY t.updated_at DESC')
					->setParameter('start_date', date('Y-m-d', strtotime('-2 Years')))
					->setParameter('user_id', $position->user->id)
					->getArrayResult();

				$training[] = array(
					'position' 	=> $position,
					'user'		=> $position->user,
					'training' 	=> $user_training,
				);
			}
		}

		$this->view->training_modules = $this->module_config['training']->modules->toArray();
		$this->view->training = $training;
	}

	protected function _getAvailablePositions($for_self = TRUE)
	{
		$positions = $this->_settings['officer_codes'];

		// Clear excessive advisors.
		$num_advisors = $this->_organization->roster_num_advisors;
		for($i = $num_advisors+1; $i <= 10; $i++)
		{
			unset($positions[100+$i]);
		}

		// Clear excessive officers.
		$num_officers = $this->_organization->roster_num_officers;
		for($i = $num_officers+1; $i <= 30; $i++)
		{
			unset($positions[200+$i]);
		}

		// Remove members.
		unset($positions[ORG_MEMBER]);

		// Denote all currently filled positions.
		foreach($this->_organization->officer_positions as $position)
		{
			$pos = $position->position;
			$positions[$pos] .= ' (Currently Filled)';
		}

		$user = $this->auth->getLoggedInUser();

		if ($for_self)
		{
			// Clear ineligible positions (if applicable)
			$is_eligible = \StuAct\Grades::checkGrade($user);

			if (!$is_eligible)
			{
				$this->alert($this->view->renderHere('ineligible'), 'red');

				foreach($this->_settings['officer_groups']['gpr'] as $position_num)
					unset($positions[$position_num]);
			}
		}
		else
		{
			foreach($positions as $position_num => $position_name)
			{
				if (!$this->_org_acl->canModifyPosition($position_num, $this->_organization, $user))
					unset($positions[$position_num]);
			}
		}

		return $positions;
	}
}