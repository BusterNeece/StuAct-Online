<h2>Web/E-mail Account Request Rejected: Address Taken or Unusable</h2>

<?=$this->org->email_header ?>

<p>The web address you requested through the StuAct Online system, <b>{$org_info.it_web_addr}</b>, cannot be assigned to you for one of the following reasons:</p>

<ul>
	<li>The domain is currently taken and cannot be registered by the Department of Student Activities. If you know who currently holds the web address you requested, you must first contact them and request that they release the address before it can be used to host your Student Activities web site. If you do not know who currently holds the address, you must choose a different one.</li>
    <li>The domain requested is invalid and cannot be registered as requested. Domain names cannot begin with numbers, and may only contain dashes and alphanumeric characters.</li>
</ul>

<p>
	Your web/e-mail account request has been reset. To submit it again, click the link below:<br />
    <a href="{$config_data.url_base}/organization/{$org_info.org_id_encoded}/resources/it/view">{$config_data.url_base}/organization/{$org_info.org_id_encoded}/resources/it/view</a>
</p>

<p>For questions regarding your organization's web/e-mail account, reply to this message to contact the Student Activities Information Technology team.</p>