<h2>Organization Not Recognized</h2>

<?=$this->org->email_header ?>

<p>The organization above is now listed as "Not Recognized" in the StuAct Online system. The group's SOFC mail slot can be reassigned. The number currently assigned to the organization is <b>#{$org_info.profile_mail_stop}</b>.</p>

<p>The progress of the organization on the recognition checklist at the time of its status change is included below for quick reference.</p>

{include file="messages/organization/recognition/status_table.tpl"}