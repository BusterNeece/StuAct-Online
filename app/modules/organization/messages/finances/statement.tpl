<h2>New SOFC Statements Available</h2>

<?=$this->org->email_header ?>

<p>The Student Organization Finance Center (SOFC) has posted statements for the most recent month to StuAct Online. To view these statements and download them in PDF form, visit this page:</p>
<p><a href="{$config_data.url_base}/organization/{$org_info.org_id_encoded}/resources/sofc/statements">{$config_data.url_base}/organization/{$org_info.org_id_encoded}/resources/sofc/statements</a></p>

<p>Please check that you are receiving a statement for each account that you are responsible for, including sub accounts. If your account did not process any transactions during the month, statements will not generate. You may call the SOFC to verify at (979) 845-1114.</p>

<p>Advisors: Please review this statement to monitor the activity in the account and also recognize any inconsistencies or problem areas. Please ensure that the organizations responsible officer is keeping a documented reconciliation of the account. This will assist you in maintaining the integrity of your organizations funds.</p>

<p>Treasurers: Please file a written reconciliation of this account statement in your organizations business files.  Account Reconciliation is a method of reconciling the organizations financial accounts to the  monthly statements produced by the Student Organization Finance Center.  It is the process of comparing organizational account records to the reports generated from FAMIS (the accounting system used by the SOFC) to verify the accuracy of each.  Outstanding items should be listed and your balance should match the balance of the SOFC statement.  If this statement does not reconcile, the error should be found and corrected.  Prompt reconciliation (within 4 weeks of receipt) helps in resolving discrepancies.</p>

<p>If you need additional help, training, or the most up-to-date forms, please review the SOFC website at <a href="http://sofc.tamu.edu">http://sofc.tamu.edu</a>.  You may also contact the SOFC 845-1114 for assistance.</p>