<h2>SOFC Signature Card Received</h2>

<?=$this->org->email_header ?>

<p>The Student Organization Finance Center (SOFC) has received your organization's latest signature card, and they have updated their records.  No additional action is required by your organization.</p>

<p>If you have any questions about the annual recognition process, please contact the Department of Student Activities at 979-862-2953.</p>