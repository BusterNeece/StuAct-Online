<?php
return array(
    'default' => array(
        'leadandserve' => array(
            'label'     => 'Leadership & Service Center',
            'uri'       => '/leadandserve',
            
            'pages'     => array(
                
                'leadlib'	=> array(
                    'label'		=> 'Leadership Library',
                    'module'	=> 'leadership_library',
                    
                    'pages'		=> array(
                        
                        'leadlib_special' => array(
                            'label'             => 'Unlisted Books',
                            'module'            => 'leadership_library',
                            'controller'		=> 'index',
                            'action'			=> 'special',
                            'permission'        => 'admin_leadlib',
                        ),
                        
                        'leadlib_edit' => array(
                            'module'            => 'leadership_library',
                            'controller'        => 'index',
                            'action'            => 'edit',
                            'permission'        => 'admin_leadlib',
                        ),
                        
                    ),
                ),
                
            ),
        ),
		
    ),
);
