<?php
/**
 * Check Out Book
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
            
            'id' => array('hidden'),
            
            'user' => array('text', array(
                'label' => 'User NetID/UIN',
                'class' => 'half-width',
                'description' => 'Enter the UIN or NetID of the person checking out the book, or swipe their student ID.',
                'required' => true,
            )),
            
            'due_date' => array('unixdate', array(
                'label' => 'Due Date',
                'required' => true,
            )),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Check Out Book',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);