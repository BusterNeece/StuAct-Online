<?php
use \Entity\LeadershipLibraryBook;
use \Entity\LeadershipLibraryCategory;

class Leadership_Library_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        if ($this->_getParam('action') != "index")
            return $this->acl->isAllowed('admin_leadlib');
        else
            return true;
    }
    
    /**
     * Main display.
     */
    public function indexAction()
    {
        if (isset($_GET['q']))
			$this->redirectFromHere(array('q' => $_GET['q']));
        
        $this->view->categories = $categories = LeadershipLibraryCategory::fetchSelect();
        
        // Base display query.
        $query = $this->em->createQueryBuilder()
            ->select('llb, llc')
            ->from('\Entity\LeadershipLibraryBook', 'llb')
            ->leftJoin('llb.categories', 'llc')
            ->andWhere('llb.user_id IS NULL')
            ->andWhere('llb.is_lost = 0')
            ->orderBy('llb.title', 'ASC');
        
        $show_results = FALSE;
        
        if ($this->_hasParam('q'))
        {
            $show_results = TRUE;
            
            $this->view->searchterms = $search_terms = $this->_getParam('q');
            $this->view->headTitle('Search Results: '.$search_terms);
            $q = '%'.$search_terms.'%';
            
            $query->andWhere('llb.title LIKE :query OR llb.isbn LIKE :query OR llb.author LIKE :query OR llb.description LIKE :query')
                ->setParameter('query', $q);
            
        }
        else if ($this->_hasParam('category'))
        {
            $show_results = TRUE;
            $category_id = (int)$this->_getParam('category', 0);
            $this->view->category_id = $category_id;
            
            if ($category_id != 0)
            {
                $query->andWhere('llc.id = :category_id')
                    ->setParameter('category_id', $category_id);
                
                $category_name = $categories[$category_id];
                $this->view->subtitle = 'Books in Category: '.$category_name;
            }
            else
            {
                $this->view->subtitle = 'Books in All Categories';
            }
        }
        else
        {
            $this->view->subtitle = 'Leadership Library';
        }
        
        if ($show_results)
        {
            $page_num = $this->_getParam('page', 1);
            $paginator = new \DF\Paginator\Doctrine($query, $page_num, 30);
            
            $this->view->pager = $paginator;
        }
        
        // Administrator access.
        $this->view->can_edit = $can_edit = $this->acl->isAllowed('admin_leadlib');
        
        // Checkout dialog.
        if ($can_edit)
        {
            $checkout_form_config = $this->current_module_config->forms->checkout->form->toArray();
            $checkout_form_config['action'] = \DF\Url::route(array('action' => 'checkout'), NULL, FALSE);
            $this->view->checkout_form = new \StuAct\Form($checkout_form_config);
        }
    }
    
    /**
     * Administrator functions.
     */
    
    /* Add or edit a book. */
    public function editAction()
    {
        $form = new \StuAct\Form($this->current_module_config->forms->book->form);
        
        if ($this->_hasParam('id'))
        {
            $id = (int)$this->_getParam('id');
            $record = LeadershipLibraryBook::find($id);
            
            $form->populate($record->toArray(TRUE, TRUE));
        }
        
        if( !empty($_POST) && $form->isValid($_POST) )
        {
            if (!($record instanceof LeadershipLibraryBook))
                $record = new LeadershipLibraryBook;
            
            $data = $form->getValues();
            $record->fromArray($data);
            
            if ($data['isbn'])
            {
                $isbn = str_replace('-', '', $data['isbn']);
            
                // Look up the book from the Google Books API.
                $google_books_uri = 'https://www.googleapis.com/books/v1/volumes';
                $google_books_params = array(
                    'key'       => $this->config->services->google->api_key,
                    'q'         => 'isbn:'.$isbn,
                );
                
                $request_uri = $google_books_uri.'?'.http_build_query($google_books_params);
                $response_raw = file_get_contents($request_uri);
                
                $response = \Zend_Json::decode($response_raw);
                
                if (count($response['items']) > 0)
                {
                    $book = $response['items'][0]['volumeInfo'];
                    $record_info = $this->_processFromGoogle($book);
                    $record->fromArray($record_info);
                }
            }
            
            $record->save();
            
            if ($is_new)
                $this->alert('Book "'.$record['title'].'" added!');
            else
                $this->alert('Book "'.$record['title'].'" changes saved!');
            
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
        }
		
        $this->view->form = $form;
    }
    
    /* Delete a record. */
    public function deleteAction()
    {
        $id = (int)$this->_getParam('id');
        $record = LeadershipLibraryBook::find($id);
        
        if ($record instanceof LeadershipLibraryBook)
            $record->delete();
        
        $this->alert('Book deleted!');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
    }
    
    /* View special-case books (unavailable, lost, etc). */
    public function specialAction()
    {
        $lost_books = $this->em->createQuery('SELECT llb FROM \Entity\LeadershipLibraryBook llb WHERE llb.is_lost = 1 ORDER BY llb.title ASC')
            ->getArrayResult();
        
        $this->view->lost_books = $lost_books;
        
        $checked_out_books = $this->em->createQuery('SELECT llb FROM \Entity\LeadershipLibraryBook llb WHERE llb.user_id IS NOT NULL ORDER BY llb.title ASC')
            ->getArrayResult();
        
        $this->view->checked_out_books = $checked_out_books;
    }
    
    public function checkoutAction()
    {
        $id = (int)$this->_getParam('id');
        $record = LeadershipLibraryBook::fetchById($id);
        
        $form = new \StuAct\Form($this->current_module_config->forms->checkout->form);
        
        if($record instanceof LeadershipLibraryBook && !empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();
            $user = User::lookUp($data['user']);
            
            if ($user instanceof User)
            {
                $record->user_id = $user->id;
                $record->due_date = $data['due_date'];
                $record->save();
            }
        }
        
        $this->alert('Book checked out!');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
    }
    
    public function checkinAction()
    {
        $id = (int)$this->_getParam('id');
        $record = LeadershipLibraryBook::fetchById($id);
        
        if ($record instanceof LeadershipLibraryBook)
        {
            $record->User = NULL;
            $record->due_date = 0;
            
            $record->save();
        }
        
        $this->alert('Book checked in!');
        $this->redirectFromHere(array('action' => 'special', 'id' => NULL));
    }
    
    /*
    public function importAction()
    {
        $books_raw = Doctrine_Query::create()
            ->from('LeadLibBook llb')
            ->orderBy('llb.title')
            ->fetchArray();
        
        // Look up the book from the Google Books API.
        $google_books_uri = 'https://www.googleapis.com/books/v1/volumes';
        $google_books_params = array(
            'key'       => $this->config->services->google->api_key,
        );
        
        foreach($books_raw as $book)
        {
            set_time_limit(15);
            
            $query = 'intitle:"'.$book['title'].'" inauthor:"'.$book['a_firstname'].' '.$book['a_lastname'].'"';
            $google_books_params['q'] = $query;
            
            $request_uri = $google_books_uri.'?'.http_build_query($google_books_params);
            $response_raw = file_get_contents($request_uri);
            $response = Zend_Json::decode($response_raw);
            
            $record = new LeadershipLibraryBook();
            
            if (count($response['items']) > 0)
            {
                $google_book = $response['items'][0]['volumeInfo'];
                $record_info = $this->_processFromGoogle($google_book);
            }
            else
            {
                $record_info = array(
                    'title' => $book['title'],
                    'author' => $book['a_firstname'].' '.$book['a_lastname'],
                );
            }
            
            $record_info['is_available'] = ($book['available'] == 0-1) ? 1 : 0;
            $record_info['is_lost'] = $book['is_lost'];
            
            \DF\Utilities::print_r($query);
            \DF\Utilities::print_r($record_info);
            
            $record->fromArray($record_info);
            $record->save();
        }
        
        echo 'Done!';
        exit;
    }
    */
    
    /**
     * Internal Functions
     */
    
    protected function _processFromGoogle($book)
    {
        $record_info = array();
        
        $record_info['title'] = $book['title'];
        if ($book['subtitle'])
            $record_info['title'] .= ': '.$book['subtitle'];
        
        $record_info['author'] = implode(", ", (array)$book['authors']);
        
        if ($book['industryIdentifiers'])
        {
            foreach($book['industryIdentifiers'] as $ident)
            {
                if ($ident['type'] == "ISBN_13")
                    $record_info['isbn'] = $ident['identifier'];
            }
        }
        
        $record_info['description'] = $book['description'];
        
        if ($book['categories'])
            $record_info['categories'] = implode(", ", (array)$book['categories']);
        
        if ($book['pageCount'])
            $record_info['description'] = "(".$book['pageCount']." Pages) ".$record_info['description'];
        
        if ($book['imageLinks'])
            $record_info['thumbnail'] = $book['imageLinks']['smallThumbnail'];
        
        return $record_info;
    }
}