<?php
/**
 * Test page
 */

use \Entity\Organization;

class Util_TestController extends \DF\Controller\Action
{
	public function permissions()
	{
		return true;
	}
	
	public function preDispatch()
	{
		parent::preDispatch();
		
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
	}
	
	public function indexAction()
	{
		$this->doNotRender();

		\StuAct\Synchronization\Manager::run('hourly');
		exit;



		$sync = new \StuAct\Synchronization\Manager;
		$sync->runTask(new \StuAct\Synchronization\Listservs);

		echo 'Done';
		return;
		
		$db = new \Zend_Db_Adapter_Pdo_Mysql(array(
			'host'		=> 'localhost',
			'username' 	=> 'root',
			'password'	=> '',
			'dbname'	=> 'test_stuactonline',
		));
		$db->getConnection();
		$db->query('SET NAMES utf8');
		$db->query('SET CHARACTER SET utf8');
		
		$new_db = new \Zend_Db_Adapter_Pdo_Mysql(array(
			'host'		=> 'mysql2.dsa.reldom.tamu.edu',
			'username' 	=> 'app_dsanet',
			'password'	=> 'juDEhUpr4neCUCuqeXew',
			'dbname'	=> 'app_dsanet',
		));
		$new_db->getConnection();
		$new_db->query('SET NAMES utf8');
		$new_db->query('SET CHARACTER SET utf8');
		
		$raw = $db->fetchAll('SELECT u.* FROM users AS u WHERE u.staff_phone IS NOT NULL');
		$results = array();
		
		foreach($raw as $row)
		{
			$uin = $row['uin'];
			
			$item = array();
			foreach($row as $col_key => $col_val)
			{
				if (substr($col_key, 0, 5) == "staff")
					$item[$col_key] = $col_val;
			}
			
			$updated_row = array('staff_profile' => json_encode($item));
			
			$new_db->update('user', $updated_row, 'uin = '.$uin);
		}
		
		echo 'Done';
		return;
		$sync = new \StuAct\Synchronization\Manager;
		$sync->runTask(new \StuAct\Synchronization\Chat);
		return;

		\DF\Service\Twilio::sms('9794508100', 'Test Message from StuAct Online');
		return;

		$directory = \DF\Service\TamuRest::getByUin(420007765);
		\DF\Utilities::print_r($directory);
		return;

		StuAct\Synchronization\Manager::run('hourly');
		exit;
		
		$org_settings = Organization::loadSettings();

		$orgs = $this->em->createQuery('SELECT o, oo, u FROM Entity\Organization o LEFT JOIN o.officer_positions oo LEFT JOIN oo.user u WHERE o.status IN (:status) AND o.flag_officers_eligible = :meets ORDER BY o.name ASC')
			->setParameter('status', array(ORG_STATUS_RECOGNIZED, ORG_STATUS_RESTRICTED, ORG_STATUS_IN_TRANSITION))
			->setParameter('meets', ORG_FLAG_MEETS_REQ)
			->getResult();

		$ineligible_now = array();
		foreach($orgs as $org)
		{
			$ineligible_users = array();

			foreach($org->officer_positions as $position)
			{
				if ($position->isInGroup('gpr'))
				{
					$user = $position->user;
					$user_eligible = \StuAct\Grades::checkGrade($user, '201211');

					if (!$user_eligible)
						$ineligible_users[] = $user->firstname.' '.$user->lastname.' ('.$user->uin.')';
				}
			}

			if ($ineligible_users)
			{
				$ineligible_now[] = array(
					'org'		=> $org->name,
					'users'		=> $ineligible_users,
				);
			}
		}

		echo count($orgs).' groups checked.<br>';
		echo count($ineligible_now).' groups becoming ineligible from new system:<br>';
		echo \DF\Utilities::print_r($ineligible_now);
		exit;

		$export_data = array();
		$export_data[] = array('Test Col 1', 'Test Col 2', 'Test Col 3');
		$export_data[] = array('Data 1', 'Data 2', 'Data 3');
		$export_data[] = array('Data B1', 'Data B2', 'Data B3');
		
		\DF\Export::excel($export_data, 'html');
		exit;
		
		/* Signature Card Conversion
		$orgs = $this->em->createQuery('SELECT o FROM Entity\Organization o WHERE o.positions_on_sigcard IS NOT NULL AND o.positions_on_sigcard != :empty AND o.positions_on_sigcard != :empty_array ORDER BY o.id ASC')
			->setParameter('empty', '')
			->setParameter('empty_array', 'a:0:{}')
			->getResult();

		foreach($orgs as $org)
		{
			$old_positions = $org->getPositionsOnSigcard();
			$new_positions = $org->getPositionsOnSigcardNew();

			if ($old_positions != $new_positions)
			{
				foreach($old_positions as $position_num => $position_name)
				{
					$position = $org->getOfficerByPosition($position_num);
					if ($position && !$position->hasPermission('sigcard'))
					{
						// Create new permission.
						$perm = new OrganizationPermission;
						$perm->organization = $org;
						$perm->position = $position_num;
						$perm->permission = 'sigcard';
						$perm->save();
					}
				}
			}
		}

		echo 'Done';
		exit;
		 */

		$org = \Entity\Organization::find(1);
		\DF\Utilities::print_r($org->getPositionsOnSigcard());
		exit;

		$user = \Entity\User::getOrCreate(901009093);
		
		$messages = \StuAct\ImportantMessages::fetchForUser($user);
		exit;
		
		throw new \Exception('Test');
		
		// $this->em->getConfiguration()->setSQLLogger(new \Doctrine\DBAL\Logging\EchoSQLLogger());

		$results = $this->em->createQuery('SELECT o, oo, oou, mr 
			FROM Entity\Organization o 
			LEFT JOIN o.officer_positions oo 
			LEFT JOIN oo.user oou
			LEFT JOIN o.miscellaneous_requirements mr
			WHERE o.id = :id')
			->setParameter('id', 1)
			->execute();

		$org = $results[0];
		$org->synchronize();

		$uow = $this->em->getUnitOfWork();
		$uow->computeChangeSets($org);

		\DF\Utilities::print_r($uow->getEntityChangeSet($org));

		$this->em->flush();

		echo 'Done!';
		return;

		/*
		$card_id = '%6016426591287830?;6016426591287830?+6016426591287830?';

		$service = new \DF\Service\DoitApi;
		\DF\Utilities::print_r($service->getUinFromCard($card_id));
		return;
		*/


		/*
        $record = \Entity\ClassCenterBox::find('TESTBOX123');

        $test_timestamp = strtotime('March 1, 2011 11:00:00');
        // $record->created_at = new \DateTime('NOW');
        // $record->save();
        */
    }
}