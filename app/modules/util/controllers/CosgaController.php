<?php
class Util_CosgaController extends \DF\Controller\Action
{
	public function permissions()
	{
		return true;
	}

	public function indexAction()
	{
		if ($_FILES)
		{
			$upload_path = \DF\File::moveUploadedFile($_FILES['csv'], 'cosgatemp.csv');
			$csv = \DF\File::getCleanCsv($upload_path);
			\DF\File::deleteFile($upload_path);

			$schools = array();
			$fields = array('name', 'type', 'email', 'cellphone', 'emergencycontact', 'vegetarianmeal', 'tshirtsize', 'classification');

			foreach($csv as $school)
			{
				$name = $school['universityname'];
				if (!isset($schools[$name]))
					$schools[$name] = array();
				
				for($i = 1; $i <= 6; $i++)
				{
					if (!empty($school['participant'.$i.'name']))
					{
						$student = array();
						foreach($fields as $field_name)
							$student[$field_name] = $school['participant'.$i.$field_name];
						
						$schools[$name][] = $student;
					}
				}
			}

			ksort($schools);

			$headers = array_merge(array('school'), $fields);
			$export_data = array($headers);

			foreach($schools as $school_name => $participants)
			{
				foreach((array)$participants as $participant)
				{
					$participant['school'] = $school_name;

					$export_row = array();
					foreach($headers as $header)
						$export_row[] = $participant[$header];

					$export_data[] = $export_row;
				}
			}

			$this->doNotRender();
			\DF\Export::csv($export_data);
			return;
		}
	}
}