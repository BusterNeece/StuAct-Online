<?php
use \Entity\User;

class Util_ChatController extends \DF\Controller\Action
{
	public function permissions()
	{
		return $this->acl->isAllowed('is logged in');
	}

	public function indexAction()
	{
		$user = $this->auth->getLoggedInUser();

		if (!$user->chat_username)
			throw new \DF\Exception\DisplayOnly('No chat username found!');
	}
}