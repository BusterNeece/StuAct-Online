<?php
use \Entity\Organization;
use \Entity\Settings;

class Util_SofcController extends \DF\Controller\Action
{
	public function permissions()
	{
        return $this->acl->isAllowed('admin_sofc');
	}
	
	public function indexAction()
	{}

	public function auditAction()
	{
		Organization::loadSettings();
		
		$form = new \DF\Form($this->current_module_config->forms->sofcaudit);

		$org_query = $this->em->createQuery('SELECT o FROM Entity\Organization o WHERE o.status NOT IN (:hide_statuses) AND o.sofc_audit = 1 ORDER BY o.name ASC')
			->setParameter('hide_statuses', array(ORG_STATUS_NOT_RECOGNIZED, ORG_STATUS_EXEMPT));

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();
			Settings::setSetting('sofc_audit_text', $data['template']);

			$orgs_to_notify = $org_query->getResult();
			$orgs_processed = array();
			foreach($orgs_to_notify as $org)
			{
				$org->notify('sofc_audit', array('audit_message' => $data['template']));
				$orgs_processed[] = $org->name;
			}

			$this->alert('<b>Notifications Successful</b><br>The following groups were sent notifications:<br>'.implode('<br>', $orgs_processed), 'green');

			$this->redirectToRoute(array('module' => 'admin'));
			return;
		}
		else
		{
			$org_list = $org_query->getArrayResult();
			$orgs_to_notify = array();

			foreach($org_list as $org)
				$orgs_to_notify[] = $org['name'];

			$this->view->form = $form;
			$this->view->orgs_to_notify = $orgs_to_notify;
		}
	}
}