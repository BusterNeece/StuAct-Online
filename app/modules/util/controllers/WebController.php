<?php
use \Entity\Organization;

class Util_WebController extends \DF\Controller\Action
{
	public function permissions()
	{
		return true;
	}

	public function preDispatch()
	{
		$this->doNotRender();
	}

	public function indexAction()
	{
		echo json_encode(array('status' => 'OK'));
		return;
	}

	public function pingAction()
	{
		$url = $this->_getParam('url');
		$domain_result = \StuAct\Domain::lookup($url);

		echo json_encode($domain_result);
		return;
	}

	public function usernameAction()
	{
		$username = $this->_getParam('username');
		$org_id = (int)$this->_getParam('org_id');

		$record = $this->em->createQuery('SELECT o.name FROM Entity\Organization o WHERE o.id != :org_id AND o.it_web_username = :username AND o.it_web_type != 0')
			->setParameter('org_id', $org_id)
			->setParameter('username', $username)
			->getArrayResult();

		if (count($record) > 0)
		{
			echo json_encode(array(
				'available' => false,
				'current' => $record[0]['name'],
			));
		}
		else
		{
			echo json_encode(array(
				'available' => true,
				'current' => '',
			));
		}
	}
}