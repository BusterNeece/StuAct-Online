<?php
/**
 * Remote application data feeds.
 */

class Util_FeedsController extends \DF\Controller\Action
{
	public function permissions()
	{
        $key_req = $this->_getParam('key');
        $key_sys = $this->config->feeds->access_key;

        return (strcmp($key_req, $key_sys) == 0);
	}

    public function preDispatch() 
    {
        $this->doNotRender();
        header("Content-Type: application/json");
    }
	
	public function indexAction()
	{}

    public function organizationsAction()
    {
        $all_orgs = \Entity\Organization::fetchArray();
        $result = array();

        foreach($all_orgs as $org)
        {
            $result[] = array(
                'id'        => $org['id'],
                'account_number' => $org['account_number'],
                'name'      => $org['name'],
                'status'    => $org['status_text'],
                'category'  => $org['category'],
                'classification' => $org['classification'],
                'profile_purpose'   => $org['profile_purpose'],
                'profile_contact_name' => $org['profile_contact_name'],
                'profile_contact_phone' => $org['profile_contact_phone'],
                'profile_contact_email' => $org['profile_contact_email'],
            );
        }

        echo \Zend_Json::encode($result);
        return;
    }

    public function officersAction()
    {
        $officer_codes = $this->config->organizations->officer_codes->toArray();

        $records_raw = $this->em->createQuery('SELECT u, oo, o FROM Entity\OrganizationOfficer oo JOIN oo.user u JOIN oo.organization o ORDER BY o.name ASC')
            ->getArrayResult();
        
        $records = array();
        foreach($records_raw as $record)
        {
            $records[] = array(
                'username'      => $record['user']['username'],
                'uin'           => $record['user']['uin'],
                'name'          => $record['user']['firstname'].' '.$record['user']['lastname'],
                'position_id'   => $record['position'],
                'position_name' => $officer_codes[$record['position']],
                'org_id'        => $record['organization']['id'],
                'org_name'      => $record['organization']['name'],
            );
        }

        echo \Zend_Json::encode($records);
        return;
    }

    public function conductAction()
    {
        $all_conduct = \Entity\Conduct::fetchArray();

        echo \Zend_Json::encode($all_conduct);
        return;
    }
}