<?php
return array(
	'method'		=> 'post',
	'elements'		=> array(

		'template' => array('textarea', array(
			'label' => 'Message to Send to Organizations',
			'default' => \Entity\Settings::getSetting('sofc_audit_text'),
			'class' => 'full-width full-height',
			'style' => 'height: 700px;',
		)),

		'submit_btn' => array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Send SOFC Audit Notifications',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),

	),
);