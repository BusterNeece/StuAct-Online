<?php

use \DF\Utilities;

return array(
	'id' => 'pep_form',
	'method' => 'post',

	'groups' => array(

		'Emotional_section' => array(
			'legend'		=> 'Emotional Impact and Planning',
			'elements'		=> array(

				/*
				'Activities_description' => array('textarea', array(
					'label' => 'Please describe the activities you are planning for this event',
					'class' => 'full-width full-height',
					'required' => true,
				)),
				*/

				'File_Activities' => array('file', array(
					'label' => 'Upload any documents relevant to the actviities of this event, i.e. itinerary',
					'multiFile' => 3,
				)),
				'Emotional_risks' => array('textarea', array(
					'label' => 'What emotional risks are involved in the event, and how are you mitigating these risks?',
					'class' => 'full-width full-height',
					'required' => true,
				)),
				'Alternative_activities' => array('textarea', array(
					'label' => 'What activities would be available if participants did not choose to participate in any of the scheduled primary activities?',
					'class' => 'full-width half-height',
					'required' => true,
				)),
				'Alcohol_present' => array('radio', array(
					'label' => 'Will alcohol be present at the event (alcohol will be served during the event, participants can bring their own alcohol, the event takes place in a location where alcohol is available, etc.)?',
					'description' => 'If Yes: You must review and adhere to all rules about alcohol at student organization events. Please read the policies and procedures about alcoholic beverages at recognized student organization events, and visit <a href="http://studentactivities.tamu.edu/risk/eventplanning/alcohol" target="_blank">the Risk Management page for events with alcohol</a> for the answers to common questions about alcohol and events.',
					'multiOptions' => Utilities::pairs(array('Yes', 'No')),
				)),
				'Event_for_new_members' => array('radio', array(
					'label' => 'Does this event pertain specifically to new members of your organization (orientation, training, rite of passage, ceremony, etc)?',
					'description' => 'If Yes: Familiarize yourself with the <a href="http://stophazing.tamu.edu/rules-and-laws/student-rules" target="_blank">student rule regarding hazing</a>. If you witness or have knowledge of possible hazing, please visit the <a href="http://stophazing.tamu.edu/report" target="_blank">Stop Hazing</a> site or contact Student Conflict Resolution Services at 979-847-7272 or <a href="mailto:scrs@tamu.edu">scrs@tamu.edu</a>.',
					'multiOptions' => Utilities::pairs(array('Yes', 'No')),
				)),
				'Event_new_member_involvement' => array('textarea', array(
					'label' => 'Explain how this event pertains to new members, if applicable', 
					'class' => 'full-width half-height',
				)),
				'Crisis_response' => array('textarea', array(
					'label' => 'What is your crisis response plan for this event?',
					'description' => 'Include the roles of the following, if applicable: UPD, Medical Responders, Counseling Center, Advisor, Media Response',
					'class' => 'full-width full-height',
					'required' => true,
				)),

			),
		),

		'submit_group' => array(
			'elements' => array(
				'btn_submit' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save and Continue',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);