<?php

return array(
	'id' => 'pep_form',
	'method' => 'post',

	'groups' => array(

		'General_Org_Info' => array(
			'legend'		=> 'General Organization Information',
			'elements'		=> array(

				'Org_name' => array('text', array(
					'label' => 'Name of Organization',
					'class' => 'full-width',
					'required' => true,
				)),

				'Submitter_name' => array('text', array(
					'label' => 'Submitter Name',
					'class' => 'full-width',
					'required' => true,
				)),

				'Submitter_position' => array('text', array(
					'label' => 'Position in Organization',
					'class' => 'full-width',
					'required' => true,
				)),

				'Submitter_phone' => array('text', array(
					'label' => 'Phone Number',
					'class' => 'half-width',
					'required' => true,
				)),

				'Email_address' => array('text', array(
					'label' => 'E-mail Address',
					'class' => 'half-width',
					'required' => true,
					'validators' => array('EmailAddress'),
				)),

				'Onsite_contact_name' => array('text', array(
					'label' => 'On-Site Contact Name',
					'class' => 'full-width',
				)),

				'Onsite_contact_phone' => array('text', array(
					'label' => 'On-Site Contact Phone Number',
					'class' => 'half-width',
				)),

				'Onsite_Code_Maroon_contact' => array('text', array(
					'label' => 'On-Site Code Maroon Contact',
					'class' => 'half-width',
				)),

				'Advisor_present_during_event' => array('radio', array(
					'label' => 'Advisor Present During Event?',
					'multiOptions' => array('Yes' => 'Yes', 'No' => 'No'),
				)),

			),
		),

		'Event_Specific_Info' => array(
			'legend' => 'Event-Specific Information',
			'elements' => array(
				
				'Event_name' => array('text', array(
					'label' => 'Name of Event',
					'class' => 'full-width',
					'required' => true,
				)),
				'Date_Event_start' => array('unixdatetime', array(
					'label' => 'Event Start Date',
					'blank' => true,
					'required' => true,
				)),
				'Date_Event_end' => array('unixdatetime', array(
					'label' => 'Event End Date',
					'blank' => true,
					'required' => true,
				)),
				'Event_description' => array('textarea', array(
					'label' => 'Brief Description of Event/Activity',
					'class' => 'full-width half-height',
					'required' => true,
				)),
				
				'Event_location' => array('textarea', array(
					'label' => 'Location(s) of Event',
					'class' => 'full-width half-height',
				)),
				'Production_time' => array('text', array(
					'label' => 'Event Production Time',
					'description' => 'How many days has it/will it take for the organization to produce this event?',
					'class' => 'full-width',
				)),

				'Target_audience' => array('text', array(
					'label' => 'Target Audience of Event',
					'class' => 'full-width',
				)),

				'Event_involves_minors' => array('radio', array(
					'label' => 'Does this event involve participants under the age of 18?',
					'multiOptions' => \DF\Utilities::pairs(array('Yes', 'No')),
					'required' => true,
					'description' => 'If yes: Programs for participants under the age of 18 may qualify as a camp or program for minors, as defined by <a href="http://rules-saps.tamu.edu/PDFs/11.99.99.M1.pdf" target="_blank">university rules</a>. Please visit <a href="http://studentactivities.tamu.edu/camps" target="_blank">studentactivities.tamu.edu/camps</a> for more information about the camps process. Camps and programs for minors have a separate application and planning form that must be approved.',
				)),

				'Goals_of_event' => array('textarea', array(
					'label' => 'Goals of the Event',
					'class' => 'full-width full-height',
				)),
				'Contribution_to_mission' => array('textarea', array(
					'label' => 'Contribution to Organization Mission',
					'description' => 'How does this event help to achieve the mission of your organization?',
					'class' => 'full-width half-height',
				)),
				'Event_is_recurring' => array('radio', array(
					'label' => 'Is Event Recurring?',
					'multiOptions' => \DF\Utilities::pairs(array('Yes', 'No')),
				)),
				'Event_recurring_dates' => array('textarea', array(
					'label' => 'If Yes, Additional Locations and Event Date(s)',
					'class' => 'full-width half-height',
				)),

				'TAMU_values' => array('textarea', array(
					'label' => 'Representation of TAMU Core Value(s)',
					'description' => 'How does this event represent one or more of the Texas A&M University core values of Leadership, Respect, Selfless Service, Excellence, and Loyalty?',
					'class' => 'full-width half-height',
				)),
				
			),
		),

		'submit_group' => array(
			'elements' => array(
				'btn_submit' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save and Continue',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);