<?php

return array(
	'id' => 'pep_form',
	'method' => 'post',

	'groups' => array(

		'Greek_Life_Questions' => array(
			'legend'		=> 'Greek Life Questions',
			'elements'		=> array(

				'Type_of_event' => array('radio', array(
					'label' => 'Type of event',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Fundraiser',
						'Retreat',
						'New Member Event',
						'Conference',
						'Service Project',
						'Educational Programs',
						'Recruitment/Informational',
						'Recreation/Sporting',
						'Social Event',
						'Philanthropy',
						'Other',
					)),
				)),

				'Open_to_public' => array('radio', array(
					'label' => 'Is this event open to the public?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Yes',
						'No',
						'N/A',
					)),
				)),

				'Closed_event' => array('radio', array(
					'label' => 'Is this event closed (invitation/guest list only)?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Yes',
						'No',
						'N/A',
					)),
				)),

				'Estimated_members_attending' => array('text', array(
					'label' => 'Estimated members attending',
					'class' => 'half-width',
				)),

				'Estimated_nonmembers_attending' => array('text', array(
					'label' => 'Estimated non-members attending',
					'class' => 'half-width',
				)),

				'Resources_used' => array('multiCheckbox', array(
					'label' => 'What resources have you used in planning?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Faculty/Staff Advisor',
						'Organization Resources',
						'Headquarters Staff',
						'Chapter Advisor',
						'Organization Policies',
						'Greek Life Staff',
						'Student Activities Staff',
						'Other',
					)),
				)),

				'Travel_to_event' => array('multiCheckbox', array(
					'label' => 'How will attendees be traveling to the event?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Personal Vehicle',
						'University Vehicle',
						'Commercial Plane',
						'Rental Vehicle',
						'University Bus',
						'Charter Bus',
						'Other',
					)),
				)),

				'Travel_from_event' => array('multiCheckbox', array(
					'label' => 'How will attendees be traveling from the event?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Personal Vehicle',
						'University Vehicle',
						'Commercial Plane',
						'Rental Vehicle',
						'University Bus',
						'Charter Bus',
						'CARPOOL',
						'Designated Drivers',
						'Other',
					)),
				)),

				'Is_security_present' => array('radio', array(
					'label' => 'Will your event have security present?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Yes',
						'No',
						'N/A',
					)),
				)),

				'Security_company' => array('text', array(
					'label' => 'If so, what security company will you be using?',
					'class' => 'half-width',
				)),

				'Security_officers_present' => array('text', array(
					'label' => 'How many security officers will be present?',
					'class' => 'half-width',
				)),

				'Guest_list' => array('radio', array(
					'label' => 'Will you have a guest list for this event?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Yes',
						'No',
						'N/A',
					)),
				)),

				'Guest_list_monitor' => array('text', array(
					'label' => 'Who will be monitoring the guest list?',
					'class' => 'half-width',
				)),

				'Collecting_money' => array('radio', array(
					'label' => 'Will a representative of your organization be collecting any monies at the event?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Yes',
						'No',
						'N/A',
					)),
				)),

				'Provided_for_money_storage' => array('radio', array(
					'label' => 'Have you made provisions for the late night security and controls specific to monies being deposited or stored in a locked/secure location?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Yes',
						'No',
						'N/A',
					)),
				)),
			
			),
		),

		'Alcohol_management' => array(
			'legend'		=> 'Alcohol Management',
			'elements'		=> array(

				'Alcohol_present' => array('radio', array(
					'label' => 'Will alcohol be present at your event?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Yes',
						'No',
						'N/A',
					)),
				)),

				'Alcohol_management_method' => array('radio', array(
					'label' => 'How will alcohol be managed at the event?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'BYOB',
						'Third Party Vendor',
						'N/A',
					)),
				)),

				'BYOB__Using_ticket_system' => array('radio', array(
					'label' => 'BYOB Events: Will you be utilizing a ticket system to distribute alcohol?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Yes',
						'No',
						'N/A',
					)),
				)),

				'BYOB__Quantity_guidelines' => array('radio', array(
					'label' => 'BYOB Events: Have you established guidelines for how much alcohol each individual can bring to the event?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Yes',
						'No',
						'N/A',
					)),
				)),

				'BYOB__Storage_facility' => array('radio', array(
					'label' => 'BYOB Events: Will you have a central storage facility for the alcohol?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Yes',
						'No',
						'N/A',
					)),
				)),

				'Third_party_vendor_name' => array('text', array(
					'label' => 'Third Party Events: What third party vendor are you utilizing?',
					'class' => 'half-width',
				)),

				'Third_party_vendor_consulted' => array('radio', array(
					'label' => 'Third Party Events: Have you met with the third party vendor to discuss the event?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Yes',
						'No',
						'N/A',
					)),
				)),

				'Age_check_method' => array('radio', array(
					'label' => 'How will identification cards be checked to identify those of legal drinking age?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Chapter members will verify at entrance of event',
						'Licensed bartender will verify before each purchase',
						'Security guard at entrance of event',
						'N/A',
					)),
				)),

				'Age_identification_method' => array('radio', array(
					'label' => 'How will guests who are of age be identified?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Wristband',
						'Hand Stamp',
						'ID checked before each purchase',
						'Other',
						'N/A',
					)),
				)),

				'Wristbands_needed' => array('radio', array(
					'label' => 'Do you need wristbands from the Department of Greek Life?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Yes',
						'No',
						'N/A',
					)),
				)),

				'Number_of_wristbands_needed' => array('text', array(
					'label' => 'If so, how many wristbands are needed?',
					'class' => 'half-width',
				)),

				'How_are_wristbands_used' => array('text', array(
					'label' => 'How will wristbands be used?',
					'class' => 'half-width',
				)),

				'Monitors_utilized' => array('radio', array(
					'label' => 'Will sober event/party monitors be utilized?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Yes',
						'No',
						'N/A',
					)),
				)),

				'Protocol_for_underage_drinking' => array('text', array(
					'label' => 'What is the protocol you have established for handling underage drinking?',
					'class' => 'full-width',
				)),

				'Protocol_for_excessive_intoxication' => array('text', array(
					'label' => 'What is the protocol you have established for handling excessive intoxication?',
					'class' => 'full-width',
				)),

				'Nonalcoholic_beverages_available' => array('radio', array(
					'label' => 'Will non-alcoholic beverages be available?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Yes',
						'No',
						'N/A',
					)),
				)),

				'Food_available' => array('radio', array(
					'label' => 'Will food be available?',
					'multiOptions' => \DF\Utilities::pairs(array(
						'Yes',
						'No',
						'N/A',
					)),
				)),
			
			),
		),


		'submit_group' => array(
			'elements' => array(
				'btn_submit' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save and Continue',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);