<?php
use \DF\Utilities;

return array(
	'id' => 'pep_form',
	'method' => 'post',

	'groups' => array(

		'Activities_group' => array(
			'legend'		=> 'Activities',
			'elements'		=> array(

				'Activities' => array('multiCheckbox', array(
					'label' => 'Does this event involve any type of physical activity? Check all that apply', 
					'multiOptions' => Utilities::pairs(array(
						'Water Sports (sailing, tubing, wakeboarding, swimming, surfing, diving, water aerobics, jet skiing, blob, etc.)',
						'Snow Sports (skiing, snowboarding, snowshoeing, sledding, etc.)',
						'Motor Sports (bumper cars, go carts, four wheeling, off roading, drag racing, etc.)',
						'Sports Activities (basketball, volleyball, softball, kickball, dodgeball, flag football, capture the flag, ultimate frisbee, soccer, frolf, golf, Quidditch, whirly ball, paintball, laser tag, broomball, roller/ice skating, etc.)',
						'Climbing (indoor/outdoor, bouldering, rappelling, etc.)',
						'Running (Parkour, 5K, Fun Run, marathons, triathlons, etc.)',
						'Dancing/Cheerleading (Competitive, non-competitive, practices, dances, etc.)',
						'Other',
					)),
				)),

				'Activities_if_yes' => array('markup', array(
					'label' => 'If you selected any of the activities above',
					'markup' => '<ul>
						<li>It would be wise to have all participants sign a liability waiver. The Department of Student Activities has a standard waiver. Participants can sign as an assumption of risk, medical release, and emergency contact form.</li>
						<li>Consider contacting the TAMU <a href="http://tamect.tamu.edu" target="_blank">Emergency Care Team</a> to ensure medical personnel at the event.</li>
					</ul>',
				)),

				'Activities__detailed_description' => array('textarea', array(
					'label' => 'Describe the activities, locations, equipment and supervision involved',
					'class' => 'full-width full-height',
				)),
				'Activities__preventing_harm' => array('textarea', array(
					'label' => 'How are you preventing physical harm to your participants and organization members?',
					'class' => 'full-width full-height',
					'required' => true,
				)),
				'Activities__waiver_required' => array('radio', array(
					'label' => 'Will participants/attendees be required to sign <a href="http://studentactivities.tamu.edu/site_files/riskrelease.pdf" target="_blank">waiver forms</a>?',
					'description' => 'If yes: According to the University retention schedule, waivers must be kept for three years after the event. Waivers should be kept on site at all times and kept in a locked or secured location. Hard copy waivers may be scanned and kept in a password-protected or encrypted electronic format.',
					'multiOptions' => Utilities::pairs(array('Yes', 'No')),
				)),
				'File_waiver' => array('file', array(
					'label' => 'If you are using a different form or have developed your own waiver, please upload it',
				)),

			),
		),

		'Travel' => array(
			'legend' => 'Travel',
			'elements' => array(

				'Travel__25_miles_or_more' => array('radio', array(
					'label' => 'Does this event involve traveling 25 miles or more outside the Bryan/College Station area?',
					'multiOptions' => Utilities::pairs(array('Yes', 'No')),
				)),
				'Travel__if_yes' => array('markup', array(
					'label' => 'If you selected "Yes" above',
					'markup' => '<ul>
						<li>The organization must submit a <a href="http://studentactivities.tamu.edu/app/form_travel" target="_blank">Travel Information Form</a>.</li>
						<li>All traveling participants must sign a waiver form.</li>
						<li>If your organization is traveling internationally, you are required to meet with a staff member in the <a href="http://studyabroad.tamu.edu" target="_blank">Study Abroad Programs Office</a> and fill out an application form.</li>
						<li>Complete the questions below.</li>
					</ul>',
				)),

				'Travel__Mode_of_transportation' => array('multiCheckbox', array(
					'label' => 'Please select your mode(s) of transportation',
					'description' => 'If renting a van: Schedule a van training with Student Organization Development and Administration in Student Activities.',
					'multiOptions' => Utilities::pairs(array(
						'Personal Vehicle',
						'Rental Vehicle',
						'Charter Bus',
						'Commercial Plane',
						'Other',
					)),
				)),
				'Travel__Mode_other' => array('textarea', array(
					'label' => 'If other, please specify',
					'class' => 'full-width half-height',
				)),

			),
		),

		'submit_group' => array(
			'elements' => array(
				'btn_submit' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save and Continue',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);