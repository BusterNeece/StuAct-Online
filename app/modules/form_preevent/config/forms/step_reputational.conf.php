<?php
use \DF\Utilities;

return array(
	'id' => 'pep_form',
	'method' => 'post',

	'groups' => array(

		'Reputational_section' => array(
			'legend'		=> 'Reputational Impact and Planning',
			'elements'		=> array(

				'Other_groups_involved' => array('textarea', array(
					'label' => 'Please list any other student organizations, affiliated organizations (local, national, international), and/or departments involved in this event',
					'class' => 'full-width half-height',
				)),
				'Marketing' => array('textarea', array(
					'label' => 'How are you marketing this event?',
					'class' => 'full-width half-height',
				)),
				'Liability_insurance' => array('radio', array(
					'label' => 'Are you required to, or have you considered, purchasing liability insurance for your organization or this event?',
					'description' => 'If Yes: Contact Student Organization Development and Administration (979-845-0692) for detailed information and cost of liability insurance.',
					'multiOptions' => Utilities::pairs(array('Yes', 'No')),
					'required' => true,
				)),
				'Branding' => array('textarea', array(
					'label' => 'How are you utilizing the student organization\'s name, University name, logo, and/or trademarks (t-shirts, flyers, etc)?',
					'description' => 'Ensure that your design is approved by Collegiate Licensing.',
					'class' => 'full-width half-height',
				)),
				'Event_is_controversial' => array('radio', array(
					'label' => 'Could this event be perceived as controversial (demonstrations, protests, political issues, sensitive topics, etc)?',
					'description' => 'If Yes: Review the university rules for expressive activity and explain below.',
					'multiOptions' => Utilities::pairs(array('Yes', 'No')),
				)),
				'Event_controversy_detail' => array('textarea', array(
					'label' => 'How can this event be perceived as controversial if applicable?',
					'description' => 'Include any plans to mitigate this perception.',
					'class' => 'full-width half-height',
					'required' => true,
				)),
				'Reputational_impact' => array('textarea', array(
					'label' => 'How could this event affect the reputation of your organization, and how are you planning accordingly?',
					'class' => 'full-width full-height',
					'required' => true,
				)),

			),
		),

		'submit_group' => array(
			'elements' => array(
				'btn_submit' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save and Continue',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);