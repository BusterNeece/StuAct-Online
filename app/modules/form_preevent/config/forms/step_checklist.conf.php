<?php

return array(
	'id' => 'pep_form',
	'method' => 'post',

	'groups' => array(

		'general_info' => array(
			'legend'		=> 'Pre-Form Checklist',
			'elements'		=> array(

				'requirements' => array('markup', array(
					'markup' => \Entity\Block::render('pep_introduction'),
				)),

			),
		),

		'submit_group' => array(
			'elements' => array(
				'btn_submit' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save and Continue',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);