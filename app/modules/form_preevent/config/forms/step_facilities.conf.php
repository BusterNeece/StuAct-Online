<?php

use \DF\Utilities;

return array(
	'id' => 'pep_form',
	'method' => 'post',

	'groups' => array(

		'Inclement_Weather_Plan' => array(
			'legend'		=> 'Inclement Weather Plan',
			'elements'		=> array(

				'Event_takes_place_outdoors' => array('radio', array(
					'label'	=> 'Does this event take place outdoors?',
					'description' => 'If yes, please answer the questions below.',
					'multiOptions' => array('Yes' => 'Yes', 'No' => 'No'),
				)),
				'Indoor_backup_location' => array('text', array(
					'label' => 'What is your indoor back-up location?',
					'class' => 'full-width',
				)),
				'Parking_accommodations_for_backup_location' => array('text', array(
					'label' => 'What parking accommodations will need to be made considering the back-up location?',
					'class' => 'full-width',
				)),

				'Event_cancelled_upon_weather' => array('radio', array(
					'label' => 'In the event of inclement weather, will this event be cancelled?',
					'multiOptions' => array('Yes' => 'Yes', 'No' => 'No'),
				)),
				'Date_Event_relocation_or_cancellation' => array('unixdatetime', array(
					'label' => 'What time will you decide to move or cancel the event in case of inclement weather?',
					'blank' => true,
				)),
				'Event_inclement_action_decider' => array('text', array(
					'label' => 'Who will make that decision?',
					'class' => 'full-width',
				)),
				'Event_inclement_notification' => array('textarea', array(
					'label' => 'How will you notify participants?',
					'class' => 'full-width half-height',
				)),

			),
		),

		'Production_Elements' => array(
			'legend' => 'Production Elements',
			'elements' => array(
				
				'Supplies_required' => array('textarea', array(
					'label' => 'What supplies or equipment is needed for this event?',
					'class' => 'full-width half-height',
				)),
				'Parking_control_needed' => array('radio', array(
					'label' => 'Will this event require assistance for parking or traffic control?',
					'description' => 'If Yes: Contact Transportation Services for assistance with charter buses, parking, large sports events, camps, conferences, etc.',
					'multiOptions' => array('Yes' => 'Yes', 'No' => 'No'),
				)),
				'Maintaining_organization_presence' => array('textarea', array(
					'label' => 'How will your organization maintain an effective presence during the event? What staffing will the organization provide during the event?',
					'class' => 'full-width full-height',
				)),
				'Where_will_staffing_be_located' => array('text', array(
					'label' => 'Where will staffing be located during the event?',
					'class' => 'full-width',
				)),
				'Estimated_attendees' => array('radio', array(
					'label' => 'What is the estimated number of attendees for the event?',
					'description' => 'If 250+, contact the University Police Department to obtain security at the event or crowd control.',
					'multiOptions' => array(
						'0-20'		=> '0-20',
						'21-50'		=> '20-50',
						'51-100'	=> '50-100',
						'101-200'	=> '100-200',
						'201-249'	=> '200-250',
						'250-500'	=> '250-500',
						'500-1000'	=> '500-1000',
						'1000+'		=> '1000+',
					),
				)),

			),
		),

		'Food_and_Other_Services' => array(
			'legend' => 'Food & Other Services',
			'elements' => array(
				
				'Providing_or_catering_food' => array('radio', array(
					'label' => 'Are you providing or catering food at this event?',
					'multiOptions' => array(
						'No' => 'No',
						'Yes Company' => 'Yes - A company or third party group is providing food.',
						'Yes Organization' => 'Yes - The organization or a member of the organization is providing food.',
					),
				)),
				'Organization_preparing_food' => array('multiCheckbox', array(
					'label' => 'If your organization is providing food, will the organization be preparing any of the following items for people outside the general membership?',
					'multiOptions' => Utilities::pairs(array(
						'Meat (beef, pork, lamb)',
						'Poultry (chicken, turkey, duck)',
						'Sausages/Hotdogs',
						'Fish',
						'Shellfish and Crustaceans',
						'Eggs',
						'Milk and Dairy Products',
						'Heat-Treated Plant Food (cooked rice, beans, or vegetables)',
						'Baked Potatoes',
						'Mushrooms',
						'Raw Sprouts',
						'Tofu or Soy-Protein Foods',
						'Untreated Garlic and Oil Mixtures',
						'Submarine Sandwiches',
						'Chef Salads',
					)),
				)),
				'Food_Instructions' => array('markup', array(
					'markup' => '<p>If you selected any of the items in the question above: You must complete the following forms:</p>
						<ul>
							<li><a href="http://www.brazoshealth.org/EHS/tempevent.php">Brazos Valley Health Permit Form</a></li>
							<li><a href="http://ehsd.tamu.edu/FoodSafetyFoodDistributionForms.aspx">Environmental Health and Safety Form</a></li>
						</ul>',
				)),

				'Overnight_accommodations' => array('radio', array(
					'label' => 'Will this event require overnight accommodations?',
					'multiOptions' => Utilities::pairs(array('Yes', 'No')),
				)),
				'Overnight_housing' => array('textarea', array(
					'label' => 'If yes, where are the participants being housed or suggested to stay?',
					'class' => 'full-width half-height',
				)),

				'Facility_safety' => array('textarea', array(
					'label' => 'How will your organization ensure the safety of the facilities you are using for this event, and how will you properly maintain the facilities during and after the event?', 
					'description' => 'For each facilities risk listed, explain how you will manage the risk.',
					'class' => 'full-width full-height',
				)),

				'Contract_involved' => array('radio', array(
					'label' => 'Does this event involve a contract or agreement with a non-university vendor involving the exchange of money?',
					'multiOptions' => Utilities::pairs(array('Yes', 'No')),
				)),
				'Contract_parties' => array('multiCheckbox', array(
					'label' => 'If yes, what entities are you in contract negotiations with?',
					'description' => 'Contracts must be reviewed 6 weeks in advance before the funds will be released for your event.',
					'multiOptions' => Utilities::pairs(array(
						'Speaker',
						'Performer/Entertainer',
						'DJ',
						'Facility',
						'Caterer',
						'Other',
					)),
				)),
				'Contract_parties_other' => array('text', array(
					'label' => 'If other, please specify',
					'class' => 'full-width',
				)),

			),
		),

		'submit_group' => array(
			'elements' => array(
				'btn_submit' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save and Continue',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);