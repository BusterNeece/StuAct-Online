<?php
/**
 * General Pre-Event Planning Form Configuration
 */

return array(

	// Steps in the form.
	'steps'	=> array(
		'Checklist',
		'Logistics',
		'Facilities',
		'Financial',
		'Physical',
		'Emotional',
		'Reputational',
	),

	// Batch-specific steps.
	'batch' => array(
		'greek' => array(
			'Greek',
		),
	),

	// Notifications for administrators.
    'notify_admin'  => array(
        // Default routing.
        'none' => array(
            'tsmith@stuact.tamu.edu',
        ),
        
        // Greek Life
        'greek' => array(
            'greeklife@tamu.edu',
        ),
        
        // SGA
        'sga' => array(
            'sedwards@stuact.tamu.edu',
            'tsmith@stuact.tamu.edu',
        ),
    ),

);