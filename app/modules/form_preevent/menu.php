<?php
return array(
    'default' => array(

        'form_preevent' => array(
            'label' => 'Pre-Event Planning Form',
            'module' => 'form_preevent',

            'pages' => array(
                'form_preevent_form' => array(
                    'module' => 'form_preevent',
                    'controller' => 'form',
                ),
            ),
        ),

    ),
);