<?php
/**
 * Forms Center
 */

use \Entity\Organization;
use \Entity\User;
use \Entity\PreEventForm;

// Force mail delivery even on development.
define('DF_FORCE_MAIL', TRUE);

class Form_Preevent_IndexController extends \DF\Controller\Action
{
	public function permissions()
	{
		return $this->acl->isAllowed('is logged in');
	}

	public function indexAction()
	{
		$user = $this->auth->getLoggedInUser();

		if ($this->_hasParam('org_id'))
		{
			$this->view->org = $org = $this->_getOrganization();

			// Get organization forms.
			$forms_q = $this->em->createQuery('SELECT pef FROM Entity\PreEventForm pef WHERE pef.org_id = :org_id ORDER BY pef.id DESC')
				->setParameter('org_id', $org);
			
			$page = (int)$this->_getParam('page', 1);
			$pager = new \DF\Paginator\Doctrine($forms_q, $page);
			$this->view->pager = $pager;

			$this->render('index_org');
			return;
		}
	}

	public function createAction()
	{
		$this->view->org = $org = $this->_getOrganization();

		// Create new form.
		$record = new PreEventForm;
		$record->organization = $org;
		$record->user = $this->auth->getLoggedInUser();
		$record->save();

		$code = $record->access_code;
		$this->redirectFromHere(array('controller' => 'form', 'action' => 'begin', 'code' => $code));
		return;
	}

	public function formAction()
	{
		$id = $this->_getParam('id');
		$this->redirectFromHere(array('controller' => 'form', 'action' => 'index', 'code' => $id));
		return;
	}

	/**
	 * Internal Functions
	 */

	protected function _getOrganization()
	{
		$org_id = (int)$this->_getParam('org_id');
		$org = Organization::find($org_id);

		if (!($org instanceof Organization))
			throw new \DF\Exception\DisplayOnly('Organization not found!');

		return $org;
	}
}