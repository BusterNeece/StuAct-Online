<?php
/**
 * Forms Center
 */

use \Entity\Organization;
use \Entity\OrganizationOfficer;
use \Entity\User;
use \Entity\PreEventForm;

// Force mail delivery even on development.
define('DF_FORCE_EMAIL', TRUE);

class Form_Preevent_FormController extends \DF\Controller\Action
{
	public function permissions()
	{
		return true;
	}

	protected $_code;
	protected $_record;
	protected $_settings;
	protected $_is_advisor;

	public function preDispatch()
	{
		$code = $this->_getParam('code');
		$form = PreEventForm::getRepository()->findOneBy(array('access_code' => $code));

		if (!($form instanceof PreEventForm))
			throw new \DF\Exception\DisplayOnly('A form with the ID you specified could not be found!');
		
		$this->_record = $form;
		$this->_code = $form->access_code;
		$this->_settings = PreEventForm::loadSettings();

		$position = $form->organization->getUserPosition();

		if ($this->acl->isAllowed('admin_forms_pep'))
			$this->_is_advisor = true;
		else if ($position)
			$this->_is_advisor = $position->isInGroup('advisors');
		else if (!$this->auth->isLoggedIn())
			throw new \DF\Exception\NotLoggedIn;
		else
			throw new \DF\Exception\PermissionDenied;

		$this->view->assign(array(
			'record'	=> $form,
			'org'		=> $form->organization,
			'code'		=> $form->access_code,
			'form_settings' => $this->_settings,
			'is_advisor' => $this->_is_advisor,
		));
	}

	public function indexAction()
	{
		if (in_array($this->_record->phase, array(PreEventForm::PHASE_INITIAL, PreEventForm::PHASE_MODIFY)))
			$this->redirectFromHere(array('action' => 'begin'));
		elseif ($this->record->phase == PreEventForm::PHASE_ADVISOR)
			$this->redirectFromHere(array('action' => 'review'));
		else
			$this->redirectFromHere(array('action' => 'view'));
	}

	/**
	 * Form Completion Steps
	 */

	public function beginAction()
	{
		$first_step = $this->_settings['steps'][0];
		$this->redirectFromHere(array('action' => 'form', 'step' => $first_step));
		return;
	}

	public function formAction()
	{
		// Special handling for "legacy" forms.
		if ($this->_record->version != PreEventForm::VERSION_DFFORM)
		{
			$id = $this->_record->access_code;
			$this->redirect('/online/forms/preeventplanning/'.$id.'/step1');
			return;
		}

		// Verify form status.
		if (!in_array($this->_record->phase, array(PreEventForm::PHASE_INITIAL, PreEventForm::PHASE_MODIFY))) 
		{
			$is_advisor_phase = ($this->_record->phase == PreEventForm::PHASE_ADVISOR && $this->_is_advisor);
			if (!$is_advisor_phase)
			{
				$this->redirectFromHere(array('action' => 'view'));
				return;
			}
		}

		// Verify step.
		$step = $this->_getParam('step');
		$this->view->step = $step;

		$steps = $this->_record->getSteps();
		$this->view->steps = $steps;

		$step_offset = array_search($step, $steps);

		if ($step_offset === FALSE)
			throw new \DF\Exception\DisplayOnly('The step you specified could not be found.');
		
		$next_step = (isset($steps[$step_offset+1])) ? $steps[$step_offset+1] : 'final';
		
		// Load form.
		$form_name = strtolower($step);
		$form_config = $this->current_module_config->forms->{'step_'.$form_name}->toArray();

		$form = new \StuAct\Form($form_config);

		$goto_step = $this->_getParam('goto_step', 'next');
		if ($goto_step != "next")
		{
			foreach($form->getElements() as $element_name => $element)
			{
				$element->setRequired(FALSE);
			}
		}
		else
		{
			$goto_step = $next_step;
		}

		$all_data = $this->_record->data;
		$form->setDefaults((array)$all_data[$step]);

		// Handle incoming submissions.
		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();
			$uploaded_files = $form->processFiles('preevent', $this->_code);
			$data = array_merge($data, (array)$uploaded_files);

			if ($step != "Checklist")
				$this->_record->replaceData($step, $data);
			
			$this->_record->save();

			if ($goto_step == 'final')
				$this->redirectFromHere(array('action' => 'confirm', 'step' => NULL));
			else
				$this->redirectFromHere(array('step' => $goto_step));
			return;
		}

		// Render view.
		$this->view->form = $form;
	}

	public function confirmAction()
	{
		if ($this->_record->phase == PreEventForm::PHASE_ADVISOR && !$this->_is_advisor)
			throw new \DF\Exception\DisplayOnly('This form is currently pending advisor review. You are not authorized to review it.');

		if ($this->_hasParam('decision'))
		{
			$decision = $this->_getParam('decision');

			if ($this->_is_advisor)
			{
				$this->_record->appendData('Advisor_Notes', $_POST);

				if ($decision == "approve")
				{
					$this->_record->phase = PreEventform::PHASE_DEPT_ACTIVE;
					$this->_record->save();

					$this->alert('<b>Form successfully sent to Student Activities for review.</b>', 'green');
				}
				else
				{
					$this->_record->phase = PreEventForm::PHASE_MODIFY;
					$this->_record->save();

					$this->alert('<b>Form successfully sent to organization submitter for editing.</b>', 'green');
				}
			}
			else
			{
				$this->_record->appendDate('Submitter_Questions', $_POST);

				$this->_record->phase = PreEventForm::PHASE_ADVISOR;
				$this->_record->save();

				$this->alert('<b>Form successfully sent to organization advisor for review and approval.</b>', 'green');
			}

			$this->redirectToRoute(array('module' => 'form_preevent'));
			return;
		}
		else
		{
			$errors = $this->_record->hasErrors();
			if ($errors)
			{
				$this->view->errors = $errors;
				$this->render('confirm_errors');
				return;
			}
		}
	}

	public function deleteAction()
	{
		$this->_record->phase = PreEventForm::PHASE_DELETED;
		$this->_record->save();

		$this->alert('<b>Form successfully marked as deleted.</b>', 'green');
		$this->redirectFromHere(array('controller' => 'index', 'action' => 'index', 'code' => NULL));
		return;
	}
	
	public function viewAction()
	{}

	public function printAction()
	{}
}