<?php
return array(
    'default' => array(
        
        'view_events' => array(
            'label' => 'Events',
            'module' => 'events',
            'controller' => 'index',
            'action' => 'index',
            
            'pages' => array(
                
                'calendar' => array(
                    'module' => 'events',
                    'controller' => 'index',
                    'action' => 'calendar',
                ),
                'registered' => array(
                    'label' => 'My Registered Events',
                    'module' => 'events',
                    'controller' => 'index',
                    'action' => 'registered',
                    'permission' => 'is logged in',
                ),
                
            ),
        ),
        
        'manage_events' => array(
            'label' => 'Manage Events',
            'module' => 'events',
            'controller' => 'manage',
            'action' => 'index',
            'permission' => 'admin_events',
            
            'pages' => array(
                
                'category' => array(
                    'module' => 'events',
                    'controller' => 'manage',
                    'action' => 'category',
                ),
                'edit' => array(
                    'module' => 'events',
                    'controller' => 'manage',
                    'action' => 'edit',
                ),
                'attendees' => array(
                    'module' => 'events',
                    'controller' => 'manage',
                    'action' => 'attendees',
                ),
                
            ),
        ),
    ),
);