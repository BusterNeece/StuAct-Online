<?php
use \Entity\Event;
use \Entity\EventType;
use \Entity\EventAttendee;
use \Entity\User;

class Events_ManageController extends \DF\Controller\Action
{
    public function permissions()
    {
		return $this->acl->isAllowed(array('admin_events', 'admin_events_checkin'));
    }
    
    public function indexAction()
    {
        // Event types.
        $this->view->event_types = EventType::fetchSelect();
        
        // Get today's events.
        $today_events = $this->em->createQuery('SELECT e FROM \Entity\Event e WHERE e.starttime <= :end AND e.endtime >= :start ORDER BY e.starttime ASC')
            ->setParameter('start', time()-86400)
            ->setParameter('end', time()+86400)
            ->getResult();
        
        $this->view->today_events = $today_events;

        // Event report form.
        $report_form = new \DF\Form($this->current_module_config->forms->report->form);
        $this->view->report_form = $report_form;
    }
    
    public function categoryAction()
    {
        $category_name = $this->_getParam('category');
        $event_types = EventType::fetchSelect();
        $this->view->category = $event_types[$category_name];
        
        $items_per_page = 30;
        
        // Determine first page that contains an event from today.
        $first_page = 1;
        
        $items_before_today = $this->em->createQuery('SELECT COUNT(e.id) FROM \Entity\Event e WHERE e.type_id = :category AND e.starttime < :time')
            ->setParameter('category', $category_name)
            ->setParameter('time', time())
            ->getSingleScalarResult();
        
        if ($items_before_today != 0)
            $first_page = ceil($items_before_today / $items_per_page);
        
        // Paginate events.
        $query = $this->em->createQueryBuilder()
            ->select('e, et')
            ->from('\Entity\Event', 'e')
            ->join('e.type', 'et')
            ->andWhere('e.type_id = :category')
            ->setParameter('category', $category_name)
            ->orderBy('e.starttime', 'ASC');
        
        $page = $this->_getParam('page', $first_page);
        $pager = new \DF\Paginator\Doctrine($query, $page, $items_per_page);
        $this->view->pager = $pager;
    }
    
    public function edittypeAction()
    {
        $this->acl->checkPermission('admin_events');

        $category = $this->_getParam('category');

        $form_config = $this->current_module_config->forms->eventtype->form->toArray();
        $form = new \StuAct\Form($form_config);

        if ($category)
        {
            $record = EventType::find($category);
            if (!($record instanceof EventType))
                throw new \DF\Exception\DisplayOnly('Event type not found!');
            
            $form->setDefaults($record->toArray(TRUE, TRUE));
            $form->removeElement('id');
        }
        
        if (!empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();

            if (!($record instanceof EventType))
                $record = new EventType;
            
            $record->fromArray($data);
            $record->save();
            
            $this->alert('Event type updated.');
            $this->redirectFromHere(array('action' => 'category', 'category' => $record->id));
            return;
        }

        $this->view->headTitle('Add/Edit Event Type');
        $this->renderForm($form);
        return;
    }
    
    public function editAction()
    {
        $this->acl->checkPermission('admin_events');

        $id = (int)$this->_getParam('id');
        
        $form_config = $this->current_module_config->forms->event->form->toArray();
        $form = new \StuAct\Form($form_config);
        
        if ($id != 0)
        {
            $record = Event::find($id);
            $form->setDefaults($record->toArray());
        }
        else if ($this->_hasParam('category'))
        {
            $form->setDefaults(array('type_id' => $this->_getParam('category')));
        }
        
        if (!empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();
            
            if (!($record instanceof Event))
                $record = new Event;
            
            $record->fromArray($data);
            $record->save();
            
            $this->alert('Event updated.', 'green');
            $this->redirectFromHere(array('action' => 'category', 'id' => NULL));
            return;
        }

        $this->view->headTitle('Add/Edit Event');
        $this->renderForm($form);
        return;
	}
	
	public function deleteAction()
	{
        $this->acl->checkPermission('admin_events');
        
        $id = (int)$this->_getParam('id');
        $event = Event::find($id);
        $event->delete();
        
        $this->alert('Event successfully deleted.', 'green');
        $this->redirectFromHere(array('action' => 'category'));
        return;
	}
    
    /**
     * Event Attendee Functions
     */
    
    public function attendeesAction()
	{
        $id = (int)$this->_getParam('id');
        $event = Event::find($id);
        
        if (!($event instanceof Event))
            throw new \DF\Exception\DisplayOnly('Event not found!');
        
        $event_attendees = $event->attendees;
		$unconfirmed_attendees = 0;
		$confirmed_attendees = 0;
		
		if ($event_attendees)
		{
			foreach($event_attendees as $attendee)
			{
				if ($attendee['confirmed'] == NCE_OPT_NO)
					$unconfirmed_attendees++;
				else
					$confirmed_attendees++;
			}
		}
		
		if ($event['event_capacity'] != 0 && $confirmed_attendees > $event['event_capacity'])
			$this->view->event_over_capacity = TRUE;
		
		$this->view->event = $event;
		$this->view->event_attendees = $event_attendees;
		$this->view->confirmed_attendees = $confirmed_attendees;
		$this->view->unconfirmed_attendees = $unconfirmed_attendees;
	}
	
	public function addattendeeAction()
	{
        $this->doNotRender();
        
        $id = (int)$this->_getParam('id');
        $event = Event::find($id);
        
        if (!($event instanceof Event))
            throw new \DF\Exception\DisplayOnly('Event not found!');
        
        $user = User::lookUp($_REQUEST['username']);
        
        if ($user instanceof User)
        {
            // Check for existing record.
            EventAttendee::addAttendee($event, $user, $_REQUEST['confirmed']);
            
            $result = array(
                'success'   => TRUE,
                'title'     => 'Successfully added event attendee '.$user['username'],
                'text'      => 'You can continue to check in event attendees.',
                'color'     => 'green',
            );
        }
        else
        {
            $result = array(
                'success'   => FALSE,
                'title'     => 'User Not Found',
                'text'      => 'The username you provided could not be found in the StuAct Online system. If you instead register the UIN of the user, they will receive credit the first time they log in.',
                'color'     => 'red',
            );
        }
        
        if ($this->isAjax())
        {
            $this->doNotRender();
            echo json_encode($result);
            return;
        }
        else
        {
            $this->alert('<b>'.$result['title'].'</b><br>'.$result['text'], $result['color']);
            $this->redirectFromHere(array('action' => 'attendees'));
            return;
        }
	}
	
	public function deleteattendeeAction()
	{
        $this->doNotRender();
        
        $id = (int)$this->_getParam('id');
        $event = Event::find($id);
        
        if (!($event instanceof Event))
            throw new \DF\Exception\DisplayOnly('Event not found!');
        
        $user_id = (int)$this->_getParam('user_id');
        $user = User::find($user_id);
        
        if ($user instanceof User)
        {
            // Check for existing record.
            EventAttendee::deleteAttendee($event, $user);
            
            $result = array(
                'success'   => TRUE,
                'title'     => 'Attendee Removed',
                'text'      => 'Successfully removed event attendee '.$user['username'].'.',
                'color'     => 'green',
            );
        }
        else
        {
            $result = array(
                'success'   => FALSE,
                'title'     => 'User Not Found',
                'text'      => 'The username you provided could not be found in the StuAct Online system. If you instead register the UIN of the user, they will receive credit the first time they log in.',
                'color'     => 'red',
            );
        }
        
        if ($this->isAjax())
        {
            $this->doNotRender();
            echo json_encode($result);
            return;
        }
        else
        {
            $this->alert($result['text'], $result['color']);
            $this->redirectFromHere(array('action' => 'attendees', 'user_id' => NULL));
            return;
        }
	}
	
	public function exportattendeesAction()
	{
        $id = (int)$this->_getParam('id');
        $event = Event::find($id);
        
        if (!($event instanceof Event))
            throw new \DF\Exception\DisplayOnly('Event not found!');
        
        $event_attendees = $event->attendees;
        
		$exported_data = array(
			array(
				'Name',
				'UIN',
				'Username',
				'Phone',
				'E-mail 1',
				'E-mail 2',
			),
		);
		
		foreach($event_attendees as $event_attendee)
		{
			$exported_data[] = array(
				$event_attendee['user']['firstname'].' '.$event_attendee['user']['lastname'],
				$event_attendee['user']['uin'],
				$event_attendee['user']['username'],
				$event_attendee['user']['phone'],
				$event_attendee['user']['email'],
				$event_attendee['user']['email2'],
			);
		}
        
        $this->doNotRender();
        \DF\Export::csv($exported_data);
		return;
	}
	
    /**
     * Reports
     */
    
	public function reportAction()
	{
        $form = new \DF\Form($this->current_module_config->forms->report->form);

        if ($_POST && $form->isValid($_POST))
        {
            $report = $form->getValues();
        
            $report['events'] = $this->em->createQuery('SELECT e, ea, u FROM \Entity\Event e LEFT JOIN e.attendees ea LEFT JOIN ea.user u WHERE e.type_id = :event_type AND e.starttime BETWEEN :start AND :end ORDER BY e.starttime ASC')
                ->setParameter('event_type', $report['event_type'])
                ->setParameter('start', $report['range_start'])
                ->setParameter('end', $report['range_end'])
                ->getArrayResult();
    		
    		$report['num_events'] = count($report['events']);
    		$num_attendeees = array();
    		
    		if ($report['events'])
    		{
    			foreach($report['events'] as $event)
    			{
    				$event_id = $event['id'];
                    $attendees = $event['attendees'];
    				$num_attendees['total'] += count($attendees);
    				
    				foreach($attendees as $attendee)
    				{
                        $attendee = array_merge($attendee, $attendee['user']);
    					$attendee['event'] = $event;
    					$report['attendees'][] = $attendee;
    					
    					if ($attendee['confirmed'] == NCE_OPT_YES)
    					{
    						$num_attendees['confirmed']++;
    					}
    					else
    					{
    						$num_attendees['unconfirmed']++;
    					}
    				}
    			}
    		}
    		$report['num_attendees'] = $num_attendees;

            $type = EventType::find($report['event_type']);

            // Generate report.
            $export = array();
            $export[] = array('Event Range Report');
            $export[] = array('');

            $export[] = array('General Details');
            $export[] = array('Event Type:', $type->name);
            $export[] = array('Start Date:', date('m/d/Y g:ia', $report['range_start']));
            $export[] = array('End Date:', date('m/d/Y g:ia', $report['range_end']));
            $export[] = array('Total Events:', $report['num_events']);
            $export[] = array('Total Attendance:', $report['num_attendees']['total'].' ('.$report['num_attendees']['confirmed'].' Confirmed, '.$report['num_attendees']['unconfirmed'].')');
            $export[] = array('');

            $export[] = array('Events in Date Range');
            $export[] = array('Event Time', 'Event Subtype', 'Location', 'Current', 'Capacity');

            foreach((array)$report['events'] as $event)
            {
                $export[] = array(
                    date('m/d/Y g:ia', $event['starttime']),
                    $event['subtype'],
                    $event['location'],
                    count($event['attendees']),
                    ($event['capacity'] == 0) ? 'None' : $event['capacity'],
                );
            }

            $export[] = array('');
            $export[] = array('Full Attendance List');
            $export[] = array('Event Time', 'Confirmed', 'Name', 'UIN', 'E-mail Address', 'Phone');

            foreach((array)$report['attendees'] as $event_attendee)
            {
                $export[] = array(
                    date('m/d/Y g:ia', $event_attendee['event']['starttime']),
                    ($event_attendee['confirmed'] == 0) ? 'No' : 'Yes',
                    $event_attendee['user']['firstname'].' '.$event_attendee['user']['lastname'],
                    $event_attendee['user']['uin'],
                    $event_attendee['user']['email'],
                    $event_attendee['user']['phone'],
                );
            }

             $this->doNotRender();

            \DF\Export::csv($export);
            return;
        }

        $this->view->headTitle('Generate Report');
        $this->renderForm($form);
	}
}