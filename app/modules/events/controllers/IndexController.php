<?php
use \Entity\Event;
use \Entity\EventType;
use \Entity\EventAttendee;

class Events_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        return true;
    }
    
    /**
     * Main display.
     */
    public function indexAction()
    {
        $visible_types_raw = $this->em->createQuery('SELECT et FROM \Entity\EventType et WHERE et.is_hidden = 0 ORDER BY et.name ASC')
            ->getArrayResult();
        
        $visible_types = array();
        foreach((array)$visible_types_raw as $type)
        {
            $visible_types[$type['id']] = $type['name'];
        }
        
        $this->view->types = $visible_types;
    }
    
    public function calendarAction()
    {
        if ($this->_getParam('view', 'standard') == "compact")
            \Zend_Layout::getMvcInstance()->setLayout('compact');

        $this->view->category = $category = $this->_getParam('category');

        if ($category)
        {
            $event_type = EventType::find($category);
            $this->view->category_name = $event_type->name;
        }
        else
        {
            $this->view->category_name = 'All Events';
        }
        
        $calendar = new \DF\Calendar($this->_getParam('month'));
		
		// Fetch a list of events.
		$timestamps = $calendar->getTimestamps();

        $records = $this->em->createQueryBuilder()
            ->select('e, et')
            ->from('Entity\Event', 'e')
            ->join('e.type', 'et')
            ->where('e.starttime <= :end')->setParameter('end', $timestamps['end'])
            ->andWhere('e.endtime >= :start')->setParameter('start', $timestamps['start'])
            ->orderBy('e.starttime', 'ASC');

        if ($category)
            $records->andWhere('e.type_id = :category')->setParameter('category', $category);

        $records = $records->getQuery()->getArrayResult();
		
		foreach($records as &$record)
		{
			$record['start_timestamp'] = $record['starttime'];
			$record['end_timestamp'] = $record['endtime'];
		}
		
		$this->view->records = $records;
		$this->view->calendar = $calendar->fetch($records);
    }
    
    public function detailAction()
    {
        $id = (int)$this->_getParam('id');
        $event = Event::find($id);
        
        $this->view->event = $event;
    }
    
    /**
     * User registration
     */
    
    public function registeredAction()
    {
        $this->acl->checkPermission('is logged in');
        $user = $this->auth->getLoggedInUser();
        
        $events = $this->em->createQuery('SELECT e, et, ea FROM \Entity\Event e JOIN e.type et JOIN e.attendees ea WHERE ea.user_id = :user_id ORDER BY e.starttime ASC')
            ->setParameter('user_id', $user->id)
            ->execute();
        
        $this->view->events = $events;
    }
    
    public function registerAction()
    {
        $this->acl->checkPermission('is logged in');
        $user = $this->auth->getLoggedInUser();
		
        $id = (int)$this->_getParam('id');
        $event = Event::find($id);
        
        if ($event->canAttend())
        {
            EventAttendee::addAttendee($event, $user, FALSE);
            $this->alert('Successfully registered for event.');
        }
        else
        {
            $this->alert('Unable to register for event.');
        }
        
        $this->redirectFromHere(array('action' => 'registered', 'id' => NULL));
        return;
    }
    
    public function unregisterAction()
    {
        $this->acl->checkPermission('is logged in');
        $user = $this->auth->getLoggedInUser();
		
        $id = (int)$this->_getParam('id');
        $event = Event::find($id);
        
        if ($event->getRelativeTime() == "future")
        {
            EventAttendee::deleteAttendee($event, $user);
            $this->alert('Successfully unregistered for event.');
        }
        else
        {
            $this->alert('Unable to unregister for event.');
        }
        
        $this->redirectFromHere(array('action' => 'registered', 'id' => NULL));
        return;
    }
}