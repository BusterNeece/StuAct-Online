<?php
/**
 * Add/Edit Event Form
 */

return array(
	'form' => array(
		'method'		=> 'post',
		'action'		=> \DF\Url::route(array('module' => 'events', 'controller' => 'manage', 'action' => 'report')),
        
		'elements'		=> array(
			
            'event_type' => array('select', array(
                'label' => 'Event Type',
                'required' => true,
                'multiOptions' => \Entity\EventType::fetchSelect(),
            )),

            'range_start' => array('unixdate', array(
                'label' => 'Start Date',
                'required' => true,
            )),

            'range_end' => array('unixdate', array(
                'label' => 'Start Date',
                'required' => true,
            )),
			
			'submit' => array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Generate Report',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);