<?php
use \Entity\Organization;

class Admin_WebsController extends \DF\Controller\Action
{
	public function permissions()
    {
        return $this->acl->isAllowed('administer web sites');
    }

    public function indexAction()
    {
    	$webs = $this->em->createQueryBuilder()
    		->select('o')
    		->from('Entity\Organization', 'o')
    		->where('(o.it_web_type != 0 OR o.it_web_pending = 1)')
    		->addOrderBy('o.it_web_pending', 'DESC')
    		->addOrderBy('o.name', 'ASC');

    	if ($this->_hasParam('q'))
    	{
    		$q = $this->_getParam('q');
    		$webs->andWhere('(o.it_web_addr LIKE :q OR o.name LIKE :q)')->setParameter('q', '%'.$q.'%');

    		$this->view->searchterms = $q;
    	}

    	$pager = new \DF\Paginator\Doctrine($webs, $this->_getParam('page', 1));
    	$this->view->pager = $pager;
    }

    public function editAction()
    {
    	$org_id = (int)$this->_getParam('id');
		$org = Organization::find($org_id);

		if (!($org instanceof Organization))
			throw new \DF\Exception\DisplayOnly('Organization not found!');

		$org_info = $org->toArray();
		if (!$org_info['it_web_password'])
			$org_info['it_web_password'] = \DF\Utilities::generatePassword();

    	$form = new \DF\Form($this->current_module_config->forms->web);
    	$form->setDefaults($org_info);

    	$whm = new \StuAct\WebHostManager;

    	if ($_POST && $form->isValid($_POST))
    	{
    		$data = $form->getValues();
    		$update_db_only = $data['update_db_only'];
    		$send_cname_email = $data['send_cname_email'];
    		unset($data['update_db_only'], $data['send_cname_email']);

    		$user_messages = array();
			
			/**
			 * cPanel Server Integration
			 */

			$cpanel_errors = false;
			$cpanel_messages = '';
			$created_account = false;

			$was_cpanel = ($org->it_web_type == 10);
			$is_cpanel = ($data['it_web_type'] == 10);
			
			if (!$update_db_only)
			{
				// Create a new cpanel account if shifting into cPanel from another hosting provider.
				if (!$was_cpanel && $is_cpanel)
				{
					$data['it_web_addr'] = str_replace('http://', '', $data['it_web_addr']);
					
					$cpanel_result = $whm->createAccount($data['it_web_addr'], $data['it_web_username'],$_REQUEST['it_web_password'],$cpanel_config['default_package']);
					if ($cpanel_result == true)
					{
						$created_account = true;
						$cpanel_messages .= 'Account creation successful!';
					}
					else
					{	
						$cpanel_messages .= 'Error creating account! Hosting information has been reverted to previous state. Error:'.$whm->getLastError();
						$data['it_web_type'] = $org->it_web_type;
						$cpanel_errors = true;
					}
				}
				
				// Actions only to be performed if the account was previously associated with cPanel and still is.
				if ($was_cpanel && $is_cpanel)
				{
					// Change cpanel account password if different from what is in the database.
					if ($org->it_web_password != $data['it_web_password'])
					{
						$temp_message = $whm->changePassword($org->it_web_username, $data['it_web_password'])."\n";
			
						if (strpos($temp_message, 'has been changed') !== FALSE)
						{
							$cpanel_messages .= "Password change successful.\n";
							$update_drupal_password = ($org->it_web_drupal == 1);
						}
						else
						{
							$data['it_web_password'] = $org->it_web_password;
							$cpanel_errors = true;
							
							if (stripos($temp_message, 'Sorry, the password may not contain the username for security reasons.') !== FALSE)
								$cpanel_messages .= "Password change failed (contains username). Reverting to old password listed in the database.\n";
							else if (stripos($temp_message, 'Sorry, passwords must be at least 5 characters for security reasons.') !== FALSE)
								$cpanel_messages .= "Password change failed (shorter than 5 characters). Reverting to old password listed in the database.\n";
							else
								$cpanel_messages .= "Password change failed (unknown error). Reverting to old password listed in the database.\n";
						}
					}

					// Change domain name if domain is different from what is in the database.
					if ($org->it_web_addr != $data['it_web_addr'])
					{
						$change_result = $whm->changeDomain($org->it_web_username, $data['it_web_addr'])."\n";
						
						if ($change_result)
						{
							$cpanel_messages .= "Domain change successful.\n";
						}
						else
						{
							$data['it_web_addr'] = $org->it_web_addr;
							$cpanel_errors = true;
							
							if (strpos($temp_message, 'is already setup!') !== FALSE)
								$cpanel_messages .= "Domain change failed (already setup on server). Reverting to old domain listed in database.\n";
							else
								$cpanel_messages .= "Could not change domain (unknown error). Reverting to old domain listed in database.\n";
						}
					}

					// Change username if username is different from what is in the database.
					if ($org->it_web_username != $data['it_web_username'])
					{
						$change_result = $whm->changeUsername($org->it_web_username, $data['it_web_username']);
			
						if ($change_result)
						{
							$cpanel_messages .= "Username change successful.\n";
							$update_drupal_username = ($org->it_web_drupal == 1);
						}
						else
						{
							$data['it_web_username'] = $org->it_web_username;
							$cpanel_errors = true;
							$temp_message = $whm->getLastError();
							
							if (strpos($temp_message, 'you must specify a valid user name!') !== FALSE)
								$cpanel_messages .= "Username change failed (invalid username). Reverting to old username listed in database.\n";
							else if (strpos($temp_message, 'is too long') !== FALSE)
								$cpanel_messages .= "Username change failed (too long). Reverting to old username listed in database.\n";
							else
								$cpanel_messages .= "Username change failed (unknown error). Reverting to old username listed in database.\n";
						}
					}
				}
				
				// Kill cpanel account if shifting out of cPanel to another hosting provider.
				if ($was_cpanel && !$is_cpanel)
				{
					$account_removed = $whm->killAccount($org_info['it_web_username']);
					
					if ($account_removed)
					{
						$cpanel_messages .= "Account successfully removed from cPanel.\n";
					}
					else
					{
						$data['it_web_type'] = 10;

						$cpanel_messages .= "Account removal failed. Reverting to old hosting type listed in database.\n";
						$cpanel_errors = true;
					}
				}
				
				// Present cPanel messages for display, if there are any.
				if ($cpanel_messages)
				{
					$user_messages[] = array(
						'message'	=> '<b>cPanel produced the following messages:</b><br /><blockquote>'.nl2br($cpanel_messages).'</blockquote>',
						'color'		=> 'blue',
					);
				}
			}

			$org->fromArray($data);
			$org->save();

			/**
			 * Drupal Username/Password Update
			 */
			$drupal = new \StuAct\Drupal($org);

			if ($update_drupal_password)
				$drupal->synchronizePassword();
			
			if ($update_drupal_username)
				$drupal->synchronizeUsername($org->it_web_username);
			
			/**
			 * Organization Notifications
			 */

			$fields_to_check = array('it_web_type', 'it_web_username', 'it_web_password', 'it_email_type', 'it_email_username', 'it_email_password');
			$any_fields_changed = false;
			foreach($fields_to_check as $field_to_check)
			{
				if (strcmp($data[$field_to_check], $org[$field_to_check]) != 0)
					$any_fields_changed = true;
			}
			
			/**
			 * CNAME Request
			 */

			if (!$cpanel_errors && ($send_cname_email == 1))
			{
				$record = new \Entity\CnameRequest;
				$record->domain = $data['it_web_addr'];
				$record->old_location = ($data['it_web_type'] == 10) ? 0 : 10;
				$record->new_location = ($data['it_web_type'] == 10) ? 10 : 0;
				$record->save();
			}
			
			/**
			 * Drupal Auto-Installer
			 */
			
			$user_messages[] = array(
				'message'	=> '<b>Organization web/e-mail account information updated.</b>',
				'color'		=> 'green',
			);

			foreach($user_messages as $user_message)
				$this->alert($user_message['message'], $user_message['color']);
			
			if (($org->it_web_drupal_pending == 1 || $created_account) && !$cpanel_errors)
			{
				$this->alert('<b>The organization requested the Drupal CMS.</b><br />Installation will automatically continue after this page.', 'blue');
				$this->redirectToRoute(array('module' => 'admin', 'controller' => 'drupal', 'action' => 'org', 'id' => $org->id, 'origin' => 'pending'));
			}
			else
			{
				if ($this->_getParam('origin') == "pending")
					$this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'origin' => NULL));
				else
					$this->redirect($org->route(array('controller' => 'web', 'origin' => NULL)));
			}
			return;
		}

		$this->view->form = $form;
		$this->view->org = $org;
    }
}