<?php
use \Entity\User;
use \Entity\Action;
use \Entity\Role;

class Admin_PermissionsController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed('administer all');
    }
    
    public function indexAction()
    {
        $this->view->actions = Action::fetchArray('name');
        $this->view->roles = Role::fetchArray('name');
    }
	
	public function editactionAction()
	{
        $form = new \StuAct\Form($this->current_module_config->forms->action->form);
		
		if ($this->_hasParam('id'))
		{
            $record = Action::find($this->_getParam('id'));
			$form->setDefaults($record->toArray());
		}

        if(!empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();
			
			if (!($record instanceof Action))
				$record = new Action();
			
			$record->fromArray($data);
			$record->save();
			
			$this->alert('Action updated.');
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
            return;
        }

        $this->view->headTitle('Add/Edit Action');
        $this->renderForm($form);
	}
	
	public function deleteactionAction()
	{
        $this->validateToken($this->_getParam('csrf'));
        
        $action = Action::find($this->_getParam('id'));
		if ($action instanceof Action)
			$action->delete();
			
		$this->alert('Action deleted!');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
	}

    public function editroleAction()
    {
		$form_config = $this->current_module_config->forms->role->form->toArray();
        $form = new \StuAct\Form($form_config);
		
		if ($this->_hasParam('id'))
		{
            $record = Role::find($this->_getParam('id'));
			$form->setDefaults($record->toArray(TRUE, TRUE));
		}

        if( !empty($_POST) && $form->isValid($_POST) )
        {
            $data = $form->getValues();
			
			if (!($record instanceof Role))
				$record = new Role;

			$record->fromArray($data);
            $record->save();

            $this->alert('<b>Role updated!</b>', 'green');
			$this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
            return;
        }

        $this->view->headTitle('Add/Edit Role');
        $this->renderForm($form);
    }

    public function deleteroleAction()
    {
        $this->validateToken($this->_getParam('csrf'));
		
        $record = Role::find($this->_getParam('id'));
		if ($record instanceof Role)
            $record->delete();
		
		$this->alert("Role deleted!");
		$this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
    }
    
    public function membersAction()
    {
        $role_id = (int)$this->_getParam('id');
        $role = Role::find($role_id);
        
        $this->view->role = $role;
    }
    public function deletememberAction()
    {
        $role_id = (int)$this->_getParam('id');
        $role = Role::find($role_id);
        
        $user_id = (int)$this->_getParam('user_id');
        $user = User::find($user_id);
        
        $user->roles->removeElement($role);
        $user->save();
        
        $this->alert('User(s) removed from role.', 'green');
        $this->redirectFromHere(array('action' => 'members', 'user_id' => NULL));
        return;
    }
    public function addmemberAction()
    {
        $role_id = (int)$this->_getParam('id');
        $role = Role::find($role_id);
        
        $uins = explode("\n", $this->_getParam('uins'));
        
        foreach($uins as $uin)
        {
            $uin = trim($uin);
            
            if (!empty($uin))
            {
                $user = User::lookUp($uin);
                $user->roles->add($role);
                $this->em->persist($user);
            }
        }

        $this->em->flush();
        
        $this->alert('User(s) added to role.', 'green');
        $this->redirectFromHere(array('action' => 'members'));
        return;
    }
}