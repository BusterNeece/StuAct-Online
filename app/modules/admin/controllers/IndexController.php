<?php
class Admin_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::getInstance()->isAllowed('admin_visible');
    }
	
    /**
     * Main display.
     */
    public function indexAction()
    {
        /* List of Administrative pages. */
        $admin_items = $this->current_module_config->portal->toArray();

        $admin_visible = array();
        $admin_visible_flat = array();

        foreach($admin_items as $category_name => $sections)
        {
            foreach($sections as $section_name => $items)
            {
                foreach($items as $item)
                {
                    $key = preg_replace("/[^a-zA-Z0-9]/", "", strtolower($item['text']));
                    $letter = strtoupper(substr($key, 0, 1));

                    if ($this->acl->isAllowed($item['priv']))
                    {
                        $admin_visible[$category_name][$section_name][$key] = $item;
                        $admin_visible_flat[$letter][$key] = $item;
                    }
                }
            }
        }

        $user = $this->auth->getLoggedInUser();
        $user_pref_view = $user->getPreference('admin_portal_view');

        $view = $this->_getParam('view', $user_pref_view);
        if (!$view)
            $view = 'full';
        
        $this->view->current_view = $view;
        if ($view != $user_pref_view)
            $user->setPreference('admin_portal_view', $view);
        
        switch($view)
        {
            case "alpha":
                ksort($admin_visible_flat);
                foreach($admin_visible_flat as &$items)
                {
                    ksort($items);
                }

                $this->view->admin_visible = $admin_visible_flat;
                
                $this->render('index_alpha');
            break;

            case "nodesc":
            default:
                $this->view->descriptions = ($view != "nodesc");
                $this->view->admin_visible = $admin_visible;
                $this->render();
            break;
        }
    }
}