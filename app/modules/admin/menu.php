<?php
return array(
    'default' => array(
        'admin' => array(
            'label' => 'System Administration',
            'module' => 'admin',
            'controller' => 'index',
            'show_only_with_subpages' => TRUE,
            'order' => 5,

            'pages' => array(

				'settings'	=> array(
					'label'	=> 'Manage Settings',
					'module' => 'admin',
					'controller' => 'settings',
					'action' => 'index',
					'permission' => 'administer all',
				),
				
                'users' => array(
                    'label' => 'Manage Users',
                    'module' => 'admin',
                    'controller' => 'users',
					'action' => 'index',
					'permission' => 'administer all',

					'pages' => array(
						'users_add' => array(	
							'module' => 'admin',
							'controller' => 'users',
							'action' => 'add',
						),
						'users_edit' => array(	
							'module' => 'admin',
							'controller' => 'users',
							'action' => 'edit',
						),
					),
				),

				'permissions' => array(
					'label' => 'Permissions',
					'module' => 'admin',
					'controller' => 'permissions',
					'action' => 'index',
					'permission' => 'administer all',

					'pages'	=> array(
						'permissions_action' => array(
							'module'	=> 'admin',
							'controller' => 'permissions',
							'action'	=> 'editaction',
						),
						'permissions_role' => array(
							'module'	=> 'admin',
							'controller' => 'permissions',
							'action'	=> 'editrole',
						),
						'permissions_members' => array(
							'module'	=> 'admin',
							'controller' => 'permissions',
							'action'	=> 'members',
						),
					),
				), 

				'webs' => array(
					'label' => 'Web Sites',
					'module' => 'admin',
					'controller' => 'webs',
					'permission' => 'administer web sites',

					'pages' => array(
						'web_view' => array(
							'module' => 'admin',
							'controller' => 'webs',
							'action' => 'view',
						),
					),
				),

            ),
        ),
    ),
);