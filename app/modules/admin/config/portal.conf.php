<?php
/**
 * Admin Home Items 
 */

return array(
	'organizations'		=> array(
		'User Submissions & Requests'	=> array(
			array(
				'url'			=> '/online/admin/manage_nsos.php',
				'public_url'	=> '/online/forms/nso/index',
				'text'			=> 'NSO Applications',
				'description'	=> 'Process applications for New Student Organizations.',
				'priv'			=> 'admin_orgs',
			),
			array(
				'url'			=> '/online/admin/view_pending_requests.php',
				'text'			=> 'Pending Requests',
				'description'	=> 'Organization roster requests that require departmental approval.',
				'priv'			=> 'admin_orgs',
			),
			array(
				'url'			=> '/online/admin/manage_constitutions.php',
				'text'			=> 'Constitutions',
				'description'	=> 'Review pending organization constitutions.',
				'priv'			=> array('admin_orgs', 'admin_const'),
			),
			array(
				'url'			=> '/online/admin/manage_sigcards.php',
				'text'			=> 'SOFC Signature Cards',
				'description'	=> 'Check whether a given organization has an up-to-date and approved signature card and register newly received cards.',
				'priv'			=> array('admin_orgs', 'admin_sofc'),
			),
			array(
				'url'			=> '/online/admin/sofc/status_lookup.php',
				'text'			=> 'SOFC Status Lookup Tool',
				'description'	=> 'View the overall recognition status and any SOFC-related miscellaneous requirements for any organization.',
				'priv'			=> array('admin_orgs', 'admin_sofc'),
			),
			array(
				'url'			=> '/online/admin/view_pending_it.php',
				'text'			=> 'Web/Drupal Requests',
				'description'	=> 'Review and fulfill requests for Web/E-mail accounts and Drupal CMS installation.',
				'priv'			=> 'admin_all',
			),
			array(
				'url'			=> '/online/util/set_status_change_date.php',
				'text'			=> 'Set Status Change Date',
				'description'	=> 'Move the date at which, if no recognition requirements change, an organization\'s status will automatically change.',
				'priv'			=> 'admin_orgs',
			),
			array(
				'url'			=> '/online/forms/incident_reporting/admin',
				'public_url'	=> '/online/forms/incident_reporting/index',
				'text'			=> 'Organization Incident Reports',
				'description'	=> 'View submitted, incomplete, and archived Incident Reports.',
				'priv'			=> 'admin_forms_incident_reporting',
			),
		),
		'Synchronization'			=> array(
			array(
				'url'			=> '/online/util/synchronize_orgs.php',
				'text'			=> 'Synchronize Organizations (Partial)',
				'description'	=> 'Manually run the automatic system synchronization process.',
				'priv'			=> 'admin_all',
			),
			array(
				'url'			=> '/online/util/synchronize_orgs.php?forceall=true',
				'text'			=> 'Synchronize Organizations (All)',
				'description'	=> 'Synchronize all organizations at once.',
				'priv'			=> 'admin_all',
			),
			array(
				'url'			=> '/online/util/synchronize_offcycle.php',
				'text'			=> 'Synchronize Organizations (Off-Cycle)',
				'description'	=> 'Manually run the off-cycle synchronization process, currently only responsible for sending queued messages.',
				'priv'			=> 'admin_all',
			),
		),
		'Reports'				=> array(
			array(
				'url'			=> \DF\Url::route(array('module' => 'organization', 'controller' => 'reports', 'action' => 'restricted')),
				'text'			=> 'Restricted Organizations',
				'description'	=> 'View organizations that are currently in the "Restricted" status, sorted by date of upcoming non-recognition.',
				'priv'			=> 'admin_orgs',
			),
			array(
				'url'			=> '/online/search/power',
				'text'			=> 'Power Search',
				'description'	=> 'Generate detailed reports on the leaders and organizations in the system.',
				'priv'			=> 'staff_powersearch',
			),
			array(
				'url'			=> '/online/admin/organizations/sli_report.php',
				'text'			=> 'SLI Attendance Report',
				'description'	=> 'View a report of all sponsored organizations listed in the system and the attendance records of their officers.',
				'priv'			=> array('admin_orgs', 'admin_sli_report'),
			),
			array(
				'url'			=> 'http://studentactivities.tamu.edu/online/admin/view_statistics.php',
				'text'			=> 'Statistics',
				'description'	=> 'View organization status and classification statistics.',
				'priv'			=> 'admin_visible',
			),
		),
		'Miscellaneous'				=> array(
			array(
				'url'			=> \DF\Url::route(array('module' => 'util', 'controller' => 'sofc', 'action' => 'audit')),
				'text'			=> 'Send SOFC Audit Notifications',
				'description'	=> 'Send notifications to all organizations that are flagged as "SOFC Audit".',
				'priv'			=> 'admin_sofc',
			),
			array(
				'url'			=> '/online/util/create_organization.php',
				'text'			=> 'Create New Organization',
				'description'	=> 'Manually enter a new organization into the StuAct Online. Should only be used for unofficial organizations, such as test groups.',
				'priv'			=> 'admin_orgs',
			),
			array(
				'url'			=> '/online/admin/manage_statements.php?type=sofc',
				'text'			=> 'Process Uploaded SOFC Statements',
				'description'	=> 'Add newly uploaded SOFC statements to organization listings and notify student leaders.',
				'priv'			=> 'admin_sofc',
			),
			array(
				'url'			=> '/online/admin/manage_statements.php?action=reload&type=sofc',
				'text'			=> 'Reload SOFC Statement Cache',
				'description'	=> 'Reload the system\'s statement cache without notifying organizations.',
				'priv'			=> 'admin_all',
			),
			array(
				'url'			=> \DF\Url::route(array('module' => 'training', 'controller' => 'manage')),
				'text'			=> 'Training Review Center',
				'description'	=> 'View the number of attendees for each training module and review the assessments submitted.',
				'priv'			=> 'manage training',
			),
			array(
				'url'			=> \DF\Url::route(array('module' => 'batch', 'controller' => 'manage')),
				'text'			=> 'Batch Group Management',
				'description'	=> 'Edit the description and members of the batch organization groups.',
				'priv'			=> 'admin_orgs',
			),
		),
	),
	'accounting'		=> array(
		'Administer Accounting Systems'	=> array(
			array(
				'url'			=> 'http://dsanet.tamu.edu/accounting_prodev',
				'text'			=> 'Professional Development Tracker',
				'description'	=> 'Administer the Professional Development funds tracker for all staff members.',
				'priv'			=> 'admin_prodev',
			),
			array(
				'url'			=> 'http://dsanet.tamu.edu/accounting_phones',
				'text'			=> 'Phone Charge Manager',
				'description'	=> 'View phone charge approval and upload new data for the current month.',
				'priv'			=> 'admin_accounting_phone',
			),
			array(
				'url'			=> '/online/accounting/invoices/index',
				'text'			=> 'Invoice Manager',
				'description'	=> 'Create and print general accounting invoices.',
				'priv'			=> 'admin_accounting_invoices',
			),
			array(
				'url'			=> '/online/accounting/purchase_orders/index',
				'text'			=> 'Purchase Order Manager',
				'description'	=> 'Track purchase orders.',
				'priv'			=> 'admin_po',
			),
			array(
				'url'			=> 'http://dsanet.tamu.edu/accounting_copiers',
				'text'			=> 'Copier Codes',
				'description'	=> 'Add and remove copier codes for student organizations.',
				'priv'			=> 'admin_accounting_copiers',
			),
            array(
                'url'           => 'http://dsanet.tamu.edu/hr_checklist/manage',
                'public_url'    => 'http://dsanet.tamu.edu/hr_checklist/index',
                'text'          => 'HR Checklists',
                'description'   => 'View the progress on currently submitted HR checklists and create new ones.',
                'priv'          => 'administer hr checklists',
            ),
		),
	),
	'events'			=> array(
		'StuAct Online Events'		=> array(
			array(
				'url'			=> \DF\Url::route(array('module' => 'events', 'controller' => 'manage')),
                'public_url'	=> \DF\Url::route(array('module' => 'events', 'controller' => 'index')),
				'text'			=> 'Event Manager',
				'description'	=> 'Add, edit, and remove events from the StuAct Online event tracking system. View attendance for any event, and confirm attendees at training-related events.',
				'priv'			=> array('admin_events', 'admin_events_checkin'),
			),
		),
		'Sponsored/Authorized Events' => array(
			array(
				'url'			=> \DF\Url::route(array('module' => 'sponsauth', 'controller' => 'manage')),
				'public_url'	=> \DF\Url::route(array('module' => 'sponsauth', 'controller' => 'index')),
				'text'			=> 'Sponsored/Authorized Calendar',
				'description'	=> 'Add events to the departmental calendar of sponsored and authorized events.',
				'priv'			=> 'admin_sponsauth',
			),
		),
	),
	'users'			=> array(
		'Organization Eligibility'	=> array(
			array(
				'url'			=> '/online/admin/manage_gprexemptions.php',
				'text'			=> 'GPR Exemptions',
				'description'	=> 'Exempt users temporarily or permanently from automated GPR checks associated with filling an officer position in an organization roster.',
				'priv'			=> 'admin_users',
			),
			array(
				'url'			=> '/online/admin/gpr/set_active_term.php',
				'text'			=> 'Set GPR Active Term',
				'description'	=> 'Change the GPR term that this system uses to check officer eligibility.',
				'priv'			=> 'admin_users',
			),
			array(
				'url'			=> '/online/admin/manage_conduct_reports.php',
				'text'			=> 'Conduct Probation',
				'description'	=> 'Add or remove users from the system\'s conduct probation list, which is used to prevent users on the list from requesting officer positions in organizations.',
				'priv'			=> 'admin_users',
			),
			array(
				'url'			=> \DF\Url::route(array('module' => 'util', 'controller' => 'gpr', 'action' => 'bulk')),
				'text'			=> 'Bulk GPR Lookup',
				'description'	=> 'Enter multiple UINs and generate an Excel file containing the corresponding GPR data stored in the StuAct Online system.',
				'priv'			=> 'admin_sims',
			),
			array(
				'url'			=> '/online/admin/gpr/batch_gpr_check.php',
				'text'			=> 'GPRs for Batch Groups',
				'description'	=> 'Retrieve the GPRs of all students listed on the rosters of all organizations associated with a particular batch group.',
				'priv'			=> 'admin_sims',
			),
			array(
				'url'			=> '/online/util/upload_gprs.php',
				'text'			=> 'Upload GPR Information',
				'description'	=> 'Input a standardized-format GPR data dump to be used for automated organization checks.',
				'priv'			=> 'admin_all',
			),
			array(
				'url'			=> '/online/forms/gpr_appeal/admin',
				'public_url'	=> '/online/forms/gpr_appeal/index',
				'text'			=> 'GPR Appeal Forms',
				'description'	=> 'Manage GPR appeal requests.',
				'priv'			=> 'admin_forms_gpr_manager',
			),
			array(
				'url'			=> '/online/forms/gpr_appeal/admin',
				'text'			=> 'GPR Appeal Forms (Information Gathering)',
				'description'	=> 'Manage GPR appeal requests that require additional information.',
				'priv'			=> 'admin_forms_gpr_info',
			),
			array(
				'url'			=> '/online/forms/gpr_appeal/stipulations',
				'text'			=> 'GPR Appeal Stipulation Report',
				'description'	=> 'View all existing approved appeals with stipulations along with the current records of the relevant student.',
				'priv'			=> 'admin_forms_gpr_stipulations',
			),
			array(
				'url'			=> '/online/forms/gpr_appeal/special',
				'text'			=> 'Pending GPR Appeals',
				'description'	=> 'View GPR appeal requests pending your review.',
				'priv'			=> array('admin_forms_gpr_committee', 'admin_forms_gpr_final'),
			),
			array(
				'url'			=> '/online/forms/gpr_appeal/previous',
				'text'			=> 'Previously Submitted GPR Appeals',
				'description'	=> 'View the details of previously submitted and processed GPR appeal requests.',
				'priv'			=> 'admin_forms_gpr_final',
			),
		),
		'System'						=> array(
			array(
				'url'			=> \DF\Url::route(array('module' => 'admin', 'controller' => 'users')),
				'text'			=> 'User Management',
				'description'	=> 'Create, edit and assign roles to users.',
				'priv'			=> 'administer all',
			),
			array(
				'url'			=> \DF\Url::route(array('module' => 'admin', 'controller' => 'permissions')),
				'text'			=> 'Permissions (Roles and Actions)',
				'description'	=> 'Manage the definition and permissions of system roles.',
				'priv'			=> 'administer all',
			),
		),
		'Utilities'					=> array(
			array(
				'url'			=> '/online/util/crosswalk.php',
				'text'			=> 'Crosswalk UIN Lookup',
				'description'	=> 'Look up a student\'s UIN from their TAMU ID.',
				'priv'			=> 'admin_visible',
			),
		),
	),
	'forms'				=> array(
		'Forms Center'				=> array(
			array(
				'url'			=> '/online/admin/forms/index.php',
				'text'			=> 'Forms Center',
				'description'	=> 'Assign and view all of the department forms in one location.',
				'priv'			=> 'admin_visible',
			),
		),
		'Risk Management Forms'		=> array(
			array(
				'url'			=> '/online/admin/manage_forms_pep.php',
				'public_url'	=> '/online/forms/preeventplanning/index',
				'text'			=> 'Pre-Event Planning Forms',
				'description'	=> 'View submitted, incomplete, and archived Pre-Event Planning Forms.',
				'priv'			=> 'admin_forms_pep',
			),
			array(
				'url'			=> '/online/admin/manage_camps.php',
				'public_url'	=> '/online/camps/list',
				'text'			=> 'Camps Directory',
				'description'	=> 'Add, edit and remove events from the directory used to build the camps calendar and list.',
				'priv'			=> 'admin_forms_camps',
			),
			/*
			array(
				'url'			=> '/online/admin/forms/camps.php',
				'public_url'	=> '/online/forms/camps/index',
				'text'			=> 'Camps Forms',
				'description'	=> 'View and manage all submitted Camp &amp; Enrichment Program forms.',
				'priv'			=> 'admin_forms_camps',
			),
			*/
			array(
				'url'			=> \DF\Url::route(array('module' => 'form_camps', 'controller' => 'manage')),
				'public_url'	=> \DF\Url::route(array('module' => 'form_camps', 'controller' => 'index')),
				'text'			=> 'Camps Forms v3',
				'description'	=> 'View and manage all submitted Camp &amp; Enrichment Program forms.',
				'priv'			=> 'admin_forms_camps',
			),
			/*
			array(
				'url'			=> '/online/forms/concessions/admin',
				'public_url'	=> '/online/forms/concessions/index',
				'text'			=> 'Concessions Forms',
				'description'	=> 'View submitted, incomplete, and archived Concessions forms.',
				'priv'			=> 'admin_forms_concessions',
			),
			*/
			array(
				'url'			=> \DF\Url::route(array('module' => 'form_travel', 'controller' => 'manage')),
				'public_url'	=> \DF\Url::route(array('module' => 'form_travel', 'controller')),
				'text'			=> 'Travel Information Form',
				'description'	=> 'View all Travel Information Forms.',
				'priv'			=> 'admin_forms_cirt',
			),
		),
		'StrengthsQuest'			=> array(
			array(
				'url'			=> '/online/admin/manage_strengths.php',
				'text'			=> 'StrengthsQuest',
				'description'	=> 'View and edit StrengthsQuest seminars, and view overall strength statistics from the StuAct Online system.',
				'priv'			=> 'admin_strengths',
			),
			array(
				'url'			=> '/online/admin/manage_strengths_orders.php',
				'public_url'	=> '/online/forms/strengths_order',
				'text'			=> 'StrengthsQuest Orders',
				'description'	=> 'View all pending and processed orders for StrengthsQuest books and codes.',
				'priv'			=> 'admin_strengths',
			),
		),
	),
	'misc'					=> array(
		'Student Org Web Hosting'	=> array(
			array(
				'url'			=> \DF\Url::route(array('module' => 'admin', 'controller' => 'webs')),
				'text'			=> 'Web Account Management Panel',
				'description'	=> 'View all existing cPanel accounts and process pending requests.',
				'priv'			=> 'administer web sites',
			),
			array(
				'url'			=> \DF\Url::route(array('module' => 'admin', 'controller' => 'drupal')),
				'text'			=> 'Drupal Auto-Installer System',
				'description'	=> 'View all current Drupal installations and run bulk updates.',
				'priv'			=> 'administer web sites',
			),
			array(
				'url'			=> '/online/util/suspend_unrecognized_org_sites.php',
				'text'			=> 'Suspend Unrecognized Organization Sites',
				'description'	=> 'Suspend the web accounts of organizations that are no longer recognized.',
				'priv'			=> 'admin_all',
			),
		),
		'StuAct Homepage'			=> array(
			array(
				'url'			=> '/node/add',
				'text'			=> 'Drupal: Create Content',
				'description'	=> 'Create a new story for the department homepage.',
				'priv'			=> 'drupal_newsposter',
			),
			array(
				'url'			=> '/admin',
				'text'			=> 'Drupal Administration',
				'description'	=> 'Administer the department Drupal site.',
				'priv'			=> 'admin_all',
			),
			array(
				'url'			=> '/online/admin/getinvolved/set_featured_org.php',
				'public_url'	=> '/online/getinvolved',
				'text'			=> 'GetInvolved: Set Featured Organization',
				'description'	=> 'Set the organization that is automatically profiled on the Get Involved homepage.',
				'priv'			=> 'drupal_newsposter',
			),
		),
		'StuAct Online Subsections' => array(
            array(
				'url'			=> \DF\Url::route(array('module' => 'quizzes', 'controller' => 'manage')),
				'text'			=> 'Quizzes',
				'description'	=> 'Manage the database of quizzes, questions, and options.',
				'priv'			=> 'manage quizzes',
			),
			array(
				'url'			=> 'http://aggieserve.tamu.edu/admin/',
				'public_url'	=> 'http://aggieserve.tamu.edu/',
				'text'			=> 'AggieServe Administration',
				'description'	=> 'Administer the AggieServe volunteer opportunities database.',
				'priv'			=> 'admin_aggieserve',
			),
			array(
				'url'			=> DF\Url::route(array('module' => 'help', 'controller' => 'faq')),
				'public_url'	=> DF\Url::route(array('module' => 'help', 'controller' => 'faq')),
				'text'			=> 'Frequently Asked Questions',
				'description'	=> 'View and edit questions listed on the StuAct Online FAQ page.',
				'priv'			=> 'admin_faqs',
			),
			array(
				'url'			=> \DF\Url::route(array('module' => 'leadership_library')),
				'public_url'	=> \DF\Url::route(array('module' => 'leadership_library')),
				'text'			=> 'Leadership Library',
				'description'	=> 'Manage the collection of books in the Leadership Library and track their current status.',
				'priv'			=> 'admin_leadlib',
			),
			array(
				'url'			=> 'https://dsanet.tamu.edu/directory/manage',
				'public_url'	=> 'https://dsanet.tamu.edu/directory',
				'text'			=> 'Staff Directory',
				'description'	=> 'Manage the departmental staff directory.',
				'priv'			=> 'admin_staff_directory',
			),
			/*
			array(
				'url'			=> '/online/admin/staff_directory/export.php',
				'text'			=> 'Staff Directory Export',
				'description'	=> 'Export the detailed profiles of all members of the department staff directory.',
				'priv'			=> 'admin_all',
			),
			*/
			array(
				'url'			=> \DF\Url::route(array('module' => 'classcenter', 'controller' => 'index')),
				'text'			=> 'Class Center Shirt Inventory System',
				'description'	=> 'Track and export data for shirt boxes for events like Maroon Out and Elephant Walk.',
				'priv'			=> array('admin_maroonout', 'admin_maroonout_view'),
			),
		),
	),
);