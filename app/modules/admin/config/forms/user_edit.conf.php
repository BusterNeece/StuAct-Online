<?php
/**
 * Edit User form 
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
			
			'firstname'		=> array('text', array(
				'label' => 'First Name',
				'required' => true,
	        )),
			
			'lastname'		=> array('text', array(
				'label' => 'Last Name',
				'required' => true,
			)),
			
			'email'			=> array('text', array(
				'label' => 'Email',
				'validators' => array(
					'EmailAddress'
				),
				'required' => true,
			)),
			
			'phone'			=> array('text', array(
				'label' => 'Phone',
			)),
			
			'roles'			=> array('multicheckbox', array(
				'label' => 'Roles',
				'multiOptions' => \Entity\Role::fetchSelect(),
			)),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);