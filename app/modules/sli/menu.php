<?php
return array(
    'default' => array(
		'sli'		=> array(
			// SLI
			'label'		=> 'SLI',
			'module'	=> 'sli',
			
			'pages'		=> array(
				'sli_submit'		=> array(
					'module'			=> 'sli',
					'controller'		=> 'index',
					'action'			=> 'submit',
					'label'				=> 'Submit Journal Entry',
				),
			),
		),
    ),
);
