<?php
use \Entity\ClassCenterBox;
use \Entity\ClassCenterTracking;

class Classcenter_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed(array('admin_maroonout', 'admin_maroonout_view'));
    }

    protected $_session;
    protected $_settings;
    protected $_category;

    public function preDispatch()
    {
    	$this->_session = \DF\Session::get('classcenter');
    	$this->_settings = ClassCenterBox::loadSettings();

    	$this->_category = $this->_getParam('category', NULL);

    	if ($this->_category)
    		$this->view->headTitle('Class Center Inventory: '.$this->_settings['categories'][$this->_category]);
    	
    	parent::preDispatch();
    }

    protected function _getBox($required = TRUE)
    {
    	if ($_GET)
    		$this->redirectFromHere($_GET);

    	$box_id = (string)$this->_getParam('box_id');
    	$box = ClassCenterBox::find($box_id, $this->_category);

    	if ($required && !($box instanceof ClassCenterBox))
    	{
    		$this->alert('<b>Box Not Found.</b><br />To create a new box with this number, complete the form below.', 'yellow');
			$this->redirectFromHere(array('box_id' => $box_id, 'action' => 'editbox'));
			return;
    	}

    	return $box;
    }
    
    public function indexAction()
    {
    	if (!$this->_category)
    	{
    		$this->view->categories = $this->_settings['categories'];
			$this->render('index_select');
			return;
    	}

    	// Show all available box dates.
		$box_dates_raw = $this->em->createQuery('SELECT ct.created_at FROM Entity\ClassCenterTracking ct JOIN ct.box cb WHERE cb.is_deleted = 0 AND cb.category = :category GROUP BY ct.created_at ORDER BY ct.created_at DESC')
			->setParameter('category', $this->_category)
			->getArrayResult();

		$box_dates = array();
		foreach($box_dates_raw as $box_date_raw)
		{
			$date_midnight = strtotime($box_date_raw['created_at']->format('Y-m-d').' 00:00:00');

			if (!isset($box_dates[$date_midnight]))
				$box_dates[$date_midnight] = $box_date_raw['created_at']->format('F j, Y (l)');
		}

		if ($this->_session->active_box_date)
			$this->view->active_box_date = $this->_session->active_box_date;
		
		$this->view->box_dates = $box_dates;

		// Handle report form.
		$report_form_config = $this->current_module_config->forms->report->form->toArray();
		$report_form_config['action'] = $this->view->routeFromHere(array('action' => 'report'));
		$report_form = new \DF\Form($report_form_config);
		$this->view->report_form = $report_form;
    }

    public function archiveAction()
    {
    	// Get threshold timestamp for archival.
    	$month = date('m');

    	if ($month >= 6)
    		$year = date('Y');
    	else
    		$year = date('Y')-1;

    	$timestamp = strtotime('June 1, '.$year.' 00:00:00');

    	$this->em->createQuery('UPDATE Entity\ClassCenterBox cb SET cb.is_deleted = 1 WHERE cb.created_at <= :timestamp')
    		->setParameter('timestamp', $timestamp)
    		->execute();

    	$this->alert('All old class center box entries from prior school years removed!', 'green');
    	$this->redirectFromHere(array('action' => 'index'));
    	return;
    }

    public function reportAction()
    {
    	$form = new \DF\Form($this->current_module_config->forms->report->form);

    	if ($_POST && $form->isValid($_POST))
    	{
    		$data = $form->getValues();

    		$this->redirectFromHere(array(
    			'action'		=> 'viewbydate',
    			'timestamp'		=> $data['start'],
    			'end_timestamp'	=> $data['end'],
	    	));
	    	return;
    	}
    
    	$this->renderForm($form, 'edit', 'Generate Report');
    }

    public function editboxAction()
    {
    	$form = new \DF\Form($this->current_module_config->forms->create->form);

    	$box = $this->_getBox(FALSE);
    	if ($box instanceof ClassCenterBox)
    		$form->setDefaults($box->toArray(TRUE, TRUE));

    	if ($_POST && $form->isValid($_POST))
    	{
    		if (!($box instanceof ClassCenterBox))
    		{
    			$box = new ClassCenterBox;
    			$box->box_code = $this->_getParam('box_id');
    			$box->category = $this->_category;
    		}

    		$box->fromArray($form->getValues());
			$box->save();

			$this->alert('<b>Box Created</b><br />You can now manage this box below.', 'green');

			$this->redirectFromHere(array('action' => 'managebox'));
			return;
		}

		$this->renderForm($form, 'edit', 'Box Details');
	}

	public function manageboxAction()
	{
		$box = $this->_getBox(TRUE);
		$this->view->box = $box;
	}

	public function exportboxAction()
	{
		$box = $this->_getBox(TRUE);
		$this->doNotRender();

		$export_data = array(
			array('Details for Box #'.$box->box_code),
			array('Shirt Size:', $box->size_name),
			array('Date Added:', $box->created_at->format('F j, Y')),
			array('Tracking Entries:', count($box->logs)),
			array(''),
			array('Tracking Entries for Box #'.$box->id),
			array(
				'Date',
				'Ending Quantity',
				'Starting Quantity',
				'Quantity Damaged',
				'Total Sold',
			),
		);
		
		foreach($box->logs as $entry)
		{
			$export_row = array(
				$entry['timestamp_text'],
				$entry['quantity_out'],
				$entry['quantity_in'],
				$entry['quantity_damaged'],
				$entry['total_sold'],
			);
			$export_data[] = $export_row;
		}

		\DF\Export::csv($export_data);
		return;
	}

	public function scanboxAction()
	{
		$box = $this->_getBox(TRUE);
		
		// Check for any tracking entries that are within the current date.
		$timestamp_start = strtotime('Midnight today');
		$timestamp_end = strtotime('Midnight tomorrow');
		$latest_timestamp = 0;

		$entry_today = FALSE;
		if ($box->logs)
		{
			foreach($box->logs as $log)
			{
				$timestamp = $log->created_at->getTimestamp();

				if ($timestamp > $latest_timestamp)
					$latest_timestamp = $timestamp;

				if ($timestamp >= $timestamp_start && $timestamp <= $timestamp_end && $log->type == MAROONOUT_TYPE_NORMAL && $log->is_pending_completion)
					$entry_today = $log;
			}
		}

		if ($latest_timestamp)
		{
			$this->alert('<b>This box was last scanned on '.date('l, F j, Y \a\t g:ia', $latest_timestamp).'.</b><br>Please verify that the box is not being incorrectly scanned twice before continuing.', 'red');
		}

		$entry_type = ($entry_today) ? 'scan_in' : 'scan_out';

		$form_config = $this->current_module_config->forms->{$entry_type}->form;
		$form = new \DF\Form($form_config);

		if ($entry_type == "scan_in")
		{
			$defaults = array(
				'quantity_in' => $entry_today->quantity_out,
			);
			$form->setDefaults($defaults);
		}

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			if ($entry_today)
			{
				$tracking = $entry_today;
				$tracking->is_pending_completion = FALSE;
			}
			else
			{
				$tracking = new ClassCenterTracking;
				$tracking->box = $box;
				$tracking->type = MAROONOUT_TYPE_NORMAL;
				$tracking->is_pending_completion = TRUE;
			}

			$tracking->fromArray($data);
			$tracking->save();

			if ($entry_type == 'scan_in')
				$this->alert('<b>Box #'.$box->box_code.' checked in.</b>', 'green');
			else
				$this->alert('<b>Box #'.$box->box_code.' checked out.</b>', 'green');
			
			$this->redirectFromHere(array('action' => 'index', 'box_id' => NULL, 'start_timestamp' => NULL, 'end_timestamp' => NULL));
			return;
		}

		$this->renderForm($form, 'edit', 'Enter Box Scan Details');
	}

	public function addboxlogAction()
	{
		$box = $this->_getBox(TRUE);
		$form = new \DF\Form($this->current_module_config->forms->tracking->form);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			$tracking = new ClassCenterTracking;
			$tracking->box = $box;
			$tracking->fromArray($data);
			$tracking->save();

			$this->alert('<b>Tracking entry added successfully.</b>', 'green');
			$this->redirectFromHere(array('action' => 'managebox'));
			return;
		}

		$this->renderForm($form, 'edit', 'Add Box Log Entry');
	}

	public function viewbydateAction()
	{
		if ($_GET)
			$this->redirectFromHere($_GET);

		$start_timestamp = strtotime(date('Y-m-d', (int)$this->_getParam('timestamp')).' 00:00:00');
		if ($this->_hasParam('end_timestamp'))
			$end_timestamp = strtotime(date('Y-m-d', (int)$this->_getParam('end_timestamp')).' 23:59:59');
		else
			$end_timestamp = $start_timestamp + 86399;

		if (date('Y-m-d', $start_timestamp) != date('Y-m-d', $end_timestamp))
			$timestamp_text = date('F j, Y', $start_timestamp).' to '.date('F j, Y', $end_timestamp);
		else
			$timestamp_text = date('l, F j, Y', $start_timestamp);
		
		$this->view->timestamp_text = $timestamp_text;
		$this->_session->active_box_date = $start_timestamp;

		$tracking_raw = $this->em->createQuery('SELECT ct, cb FROM Entity\ClassCenterTracking ct JOIN ct.box cb WHERE ct.created_at >= :start_timestamp AND ct.created_at <= :end_timestamp AND cb.is_deleted = 0 AND cb.category = :category ORDER BY ct.created_at ASC, cb.size ASC, cb.box_code DESC')
			->setParameters(array(
				'start_timestamp'	=> $start_timestamp,
				'end_timestamp'		=> $end_timestamp,
				'category'			=> $this->_category,
			))
			->getResult();
		
		// Get totals per size.
		$totals_per_size = array();
		
		foreach($this->_settings['types'] as $opt_type => $type_text)
		{
			$totals_per_size[$opt_type] = array(
				'type_text'		=> $type_text,
				'totals'		=> array(),
			);
			
			foreach($this->_settings['sizes'] as $shirt_size => $size_name)
			{
				$totals_per_size[$opt_type]['totals'][$shirt_size] = array(
					'size_name'		=> $size_name,
					'quantity_in'	=> 0,
					'quantity_out'	=> 0,
					'quantity_damaged' => 0,
					'total_sold'	=> 0,
				);
			}
			
			$totals_per_size[$opt_type]['totals']['all'] = array(
				'size_name'		=> 'Total',
				'quantity_in'	=> 0,
				'quantity_out'	=> 0,
				'quantity_damaged' => 0,
				'total_sold'	=> 0,
			);
		}
		
		$orders_by_box = array();
		
		foreach($tracking_raw as $tracking)
		{
			$box = $tracking->box;
			if ($box->isActive())
			{
				$shirt_size = $box->size;
				$tracking_type = $tracking->type;
				
				$items_to_sum = array('quantity_in', 'quantity_out', 'quantity_damaged', 'total_sold');
				
				foreach($items_to_sum as $item_to_sum)
				{
					$totals_per_size[$tracking_type]['totals'][$shirt_size][$item_to_sum] += $tracking[$item_to_sum];
					$totals_per_size[$tracking_type]['totals']['all'][$item_to_sum] += $tracking[$item_to_sum];
				}
			}

			$orders_by_box[$box->box_code][] = $tracking;
		}

		switch(strtolower($this->_getParam('format', 'html')))
		{
			case "csv":
				$export_data = array();
				
				$export_data[] = array($timestamp_text);
				$export_data[] = array(' ');
				
				foreach($totals_per_size as $type => $type_info)
				{
					$export_data[] = array(
						'Totals: '.$type_info['type_text'],
					);
					
					$export_data[] = array(
						'Shirt Size',
						'Quantity In',
						'Quantity Out',
						'Damaged',
						'Total Sold',
					);
				
					foreach($type_info['totals'] as $shirt_size => $size)
					{
						$export_row = array(
							$size['size_name'],
							$size['quantity_in'],
							$size['quantity_out'],
							$size['quantity_damaged'],
							$size['total_sold'],
						);
						$export_data[] = $export_row;
					}
				}
						
				$export_data[] = array(' ');
				$export_data[] = array('Tracking Entries for Date');
				$export_data[] = array(
					'Date',
					'Entry Type',
					'Is Accurate',
					'Box ID',
					'Shirt Size',
					'Quantity In',
					'Quantity Out',
					'Damaged',
					'Total Sold',
				);
				
				foreach($tracking_raw as $tracking_row)
				{
					$export_row = array(
						date('m/d/Y', $tracking_row['timestamp']),
						$tracking_row['tracking_type_text'],
						($tracking_row['is_inaccurate'] == NCE_OPT_NO) ? 'Yes' : 'No',
						$tracking_row['box_id'],
						$tracking_row['box_size'],
						$tracking_row['quantity_in'],
						$tracking_row['quantity_out'],
						$tracking_row['quantity_damaged'],
						$tracking_row['total_sold'],
					);
					$export_data[] = $export_row;
				}
				
				\DF\Export::csv($export_data);
			break;

			default:
				$this->view->totals_per_size = $totals_per_size;
				$this->view->orders_by_box = $orders_by_box;
			break;
		}
	}

	public function deleteboxlogAction()
	{
		$id = (int)$this->_getParam('tracking_id');
		$record = ClassCenterTracking::find($id);

		if ($record instanceof ClassCenterTracking)
		{
			$record->is_inaccurate = !$record->is_inaccurate;
			$record->save();
		}

		$this->alert('<b>The invalid flag of the tracking entry was toggled.</b>', 'green');
		$this->redirectFromHere(array('action' => 'managebox', 'tracking_id' => NULL));
		return;
	}

	public function deleteboxAction()
	{
		$box = $this->_getBox(TRUE);
		$box->is_deleted = !$box->is_deleted;
		$box->save();

		$this->alert('<b>Box marked as deleted.</b>', 'green');
		$this->redirectFromHere(array('action' => 'index', 'box_id' => NULL));
		return;
	}
}