<?php
return array(
    'default' => array(
		'classcenter' => array(
			'label'		=> 'Class Center Inventory',
			'module'	=> 'classcenter',
			'permission' => array('admin_maroonout', 'admin_maroonout_view'),
				
			'pages'		=> array(
			
				'cc_index' => array(
					'module' => 'classcenter',
					'controller' => 'index',
					'action' => 'index',
				),
				'cc_managebox' => array(
					'module' => 'classcenter',
					'controller' => 'index',
					'action' => 'managebox',
				),
				'cc_viewbydate' => array(
					'module' => 'classcenter',
					'controller' => 'index',
					'action' => 'viewbydate',
				),
				
			),
		),
    ),
);