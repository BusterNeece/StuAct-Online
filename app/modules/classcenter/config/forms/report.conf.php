<?php
return array(
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(

			'start' => array('unixdate', array(
				'label' => 'Start Date',
				'required' => true,
			)),
			'end' => array('unixdate', array(
				'label' => 'End Date',
				'required' => true,
			)),
			
			'submit_btn' => array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'View Report',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);