<?php
$settings = \Entity\ClassCenterBox::loadSettings();

return array(
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(

            'size' => array('radio', array(
                'label' => 'Shirt/Box Size',
                'multiOptions' => $settings['sizes'],
            )),
			
			'submit_btn' => array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);