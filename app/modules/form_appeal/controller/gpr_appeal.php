<?php
/**
 * Concessions form.
 */

define('NCE_FORCE_SSL', TRUE);

require '../framework/global.php';

define('PAGE_BASE_URL', NCE_URL_BASE.'/forms/gpr_appeal');

$template->setBreadcrumb('/', TRUE);

// Load settings ahead of time to generate constants needed for the system.
$form_settings = SAO_Form_GradeAppeal::loadSettings();
$template->form_settings = $form_settings;

// Routing information.
$routing = (isset($_REQUEST['routing'])) ? $_REQUEST['routing'] : 'normal';
if (!array_key_exists($routing, $form_settings['routing_types']))
    $routing = 'normal';

if ($routing == "normal")
    $template->title = 'GPR Exemption Application';
else
    $template->title = $form_settings['routing_types'][$routing].' GPR Exemption Application';

if (isset($_REQUEST['id']))
{
	/**
	 * Form-specific functions
	 */
	
	$form_id_hash = $_REQUEST['id'];
	$form = new SAO_Form_GradeAppeal($form_id_hash);
    
    $form_routing = $form->getRoutingType();
    
    if (strcmp($form_routing, $routing) !== 0)
    {
        NCE_Error::display('Form Not Submitted Using This Routing Method', 'The form you requested was not submitted using this routing process.');
        exit;
    }
	
	$template->form = $form_info = $form->getInfo();
	$template->form_data = $form_data = $form->getFormData();
	
	if (!$form->isValid())
	{
		NCE_Error::display('Form ID Not Found', 'The ID number you specified could not be found in the database.');
		exit;		
	}
	
	switch ($_REQUEST['action'])
	{
		// The actual form.
		case 'form':
			$form_phase = $form->getPhase();
			
			if ($form_phase != APPEAL_PHASE_INITIAL)
			{
				NCE_Error::display('Cannot access page', 'This page cannot be accessed when a form is in a later phase of review.');
				exit;
			}
			if (isset($_REQUEST['form']))
			{
				$form->setFormData($_REQUEST['form']);
				
				// If the submitting user is the CSL of the organization, don't request their endorsement.
				$position_in_org = $user->isOrgMember($form_info['org_id']);
				
				$form->setPhase(APPEAL_PHASE_ENDORSEMENT);
				
				if ($position_in_org == ORG_OFFICER_CHIEF_STUDENT_LEADER)
				{
					$form->setEndorsement('csl', APPEAL_ENDORSEMENT_BYPASSED);
					
					$form->setFormData(array(
						'csl_comments'		=> 'Submitter of request is the organization\'s Chief Student Leader.',
					));
				}
				
				$user->showOnNextPage('<b>The form has been sent to the organization for endorsement.</b><br />You will be notified via e-mail of any updates.', 'green');
				
				header("Location: ../index");
				exit;
			}
			else
			{
				echo renderPage('form');
				exit;
			}
		break;
		
		// Advisor and CSL Endorsement
		case 'endorsement':
			$position_in_org = $user->isOrgMember($form_info['org_id']);
			
			switch($position_in_org)
			{
				case ORG_OFFICER_CHIEF_STUDENT_LEADER:
					$endorsement_type = 'csl';
					break;
				
				case ORG_OFFICER_PRIMARY_ADVISOR:
					$endorsement_type = 'advisor';
					break;
					
				default:
					NCE_Error::display('Not Authorized to Access Feature', 'You are not in a position that is authorized to access this application.');
					exit;
					break;
			}
			
			if ($form_data['access'][$endorsement_type] == 1)
				$template->show_form_data = true;
			else
				$template->show_form_data = false;
			
			if (isset($_REQUEST['form']))
			{
				$decision = intval($_REQUEST['form']['decision']);
				$comments = $_REQUEST['form']['comments'];
				
				$new_endorsement = ($decision == NCE_OPT_YES) ? APPEAL_ENDORSEMENT_APPROVED : APPEAL_ENDORSEMENT_DECLINED;
				$form->setEndorsement($endorsement_type, $new_endorsement);
				
				$form_data = array(
					$endorsement_type.'_comments'		=> $comments,
				);
				$form->setFormData($form_data);
				
				echo renderPage('endorsement_success');
				exit;
			}
			else
			{
				$current_endorsement = $form->getEndorsement($endorsement_type);
				
				if ($current_endorsement != APPEAL_ENDORSEMENT_PENDING)
				{
					NCE_Error::display('Step Already Completed', 'Your portion of this appeal process has already been completed. This could be due to having visited this page previously or waiting too long to visit this page.');
					exit;
				}
				
				echo renderPage('endorsement');
				exit;
			}
		break;
		
		// View the results of the submitted application.
		case 'results':
			if ($form->getPhase() != APPEAL_PHASE_COMPLETE)
			{
				NCE_Error::display('Cannot View Form Results', 'This form is not currently open to viewing results.');
				exit;
			}
			
			$org = new NCE_Organization($form_info['org_id']);
			$org_info = $org->getInfo();
			
			$org_advising_department = $org_info['org_advising_department'];
			$template->is_sga = ($org_advising_department == "Student Government Association");
			
			echo renderPage('results');
			exit;
		break;
		
		// Administrative form overview.
		case 'view':
			$user->checkPermission(array(
                $form->permissionName('manager'),
                $form->permissionName('final'),
                $form->permissionName('info'),
            ),'OR');
			
			// Pre-populate some information from COMPASS before showing the page.
			if ($form->getPhase() == APPEAL_PHASE_INFO && !isset($form_data['info']))
			{
				$info = array();
				$uin = $form_info['uin'];
				
				$grade_data = NCE_Grades::fetchGradesForAllTerms($uin);
				$info['gpr_cumul'] = number_format($grade_data['grades'][NCE_Grades::CUMULATIVE_GPR], 2);
				
				$info['gpr_names'] = array();
				$info['gpr_values'] = array();
				foreach($grade_data['grades'] as $term_num => $term_gpr)
				{
					if ($term_num != NCE_Grades::CUMULATIVE_GPR)
					{
						$term_name = NCE_Grades::getTermName($term_num);
					
						$info['gpr_names'][] = $term_name;
						$info['gpr_values'][] = number_format($term_gpr, 2);
					}
				}
				
				$info['gpr_names'] = array_pad($info['gpr_names'], 10, '');
				$info['gpr_values'] = array_pad($info['gpr_values'], 10, '');
				
				$info['course_names'] = array_fill(0, 10, '');
				$info['course_values'] = array_fill(0, 10, '');
				
				$student_details = NCE_Grades::getStudentDetails($uin);
				
				$info['student_name'] = $student_details['firstname'].' '.$student_details['lastname'];
				$info['student_year'] = $student_details['classification'];
				$info['student_major'] = $student_details['major1_desc'];
				
				$updated_form_data = array(
					'info'		=> $info,
				);
				$form->setFormData($updated_form_data);
				
				$template->redirect(NCE_THIS_PAGE);
				exit;
			}
			
			if (isset($_REQUEST['form']))
			{
				switch($form->getPhase())
				{
					case APPEAL_PHASE_ENDORSEMENT:
						$form->setEndorsement('csl', $_REQUEST['form']['csl']);
						$form->setEndorsement('advisor', $_REQUEST['form']['advisor']);
						
						$user->showOnNextPage('<b>Endorsements saved.</b>', 'green');
					break;
						
					case APPEAL_PHASE_INFO:
						$form->setFormData($_REQUEST['form']);
						
						// Send to committee for review.
						if ($_REQUEST['send_to_next'] == NCE_OPT_YES)
						{
							$form->setPhase(APPEAL_PHASE_ROUTING);
							$user->showOnNextPage('<b>Form information submitted and sent to the committee for review.</b>', 'green');	
						}
						else
						{
							$user->showOnNextPage('<b>Form saved.</b>', 'green');
						}
					break;
					
					case APPEAL_PHASE_ROUTING:
						$new_phase = $_REQUEST['form']['goto'];
						$form->setPhase($new_phase);
						
						$user->showOnNextPage('<b>Form routed.</b>', 'green');
					break;
						
					case APPEAL_PHASE_DECISION:
						$form->setInfo(array(
							'app_review_final'		=> $_REQUEST['form']['decision'],
						));
						$form->setFormData($_REQUEST['form']);
						$form->setPhase(APPEAL_PHASE_FINAL);
						
						$user->showOnNextPage('<b>Form saved and sent to final review.</b>', 'green');
					break;
				}
				
				$template->redirect(NCE_THIS_PAGE);
				exit;
			}
			else
			{
				switch($form->getPhase())
				{
					case APPEAL_PHASE_COMMITTEE:
						$committee_members = NCE_Groups::getAllUsersWithPriv('admin_forms_gpr_committee', FALSE);
						foreach($committee_members as &$member)
						{
							$member['has_made_decision'] = (isset($form_data['decisions'][$member['user_id']]));
						}
						
						$template->committee_members = $committee_members;
					break;
				}
                
				echo renderPage('view');
				exit;
			}
		break;
		
		// Special page for committee review.
		case 'committee':
			$user->checkPermission($form->permissionName('committee'));
			
			if (isset($_REQUEST['form']))
			{
				$decision = array(
					'name'				=> $user->info['firstname'].' '.$user->info['lastname'],
					'decision'			=> $_REQUEST['form']['decision'],
					'decision_text'		=> $form_settings['committee_decisions'][$_REQUEST['form']['decision']],
					'comments'			=> $_REQUEST['form']['comments'],
				);
				
				$form_data['decisions'][$user->user_id] = $decision;
				$form->setFormData($form_data);
				
				$number_of_reviewers = count(NCE_Groups::getAllUsersWithPriv('admin_forms_gpr_committee', FALSE));
				$number_of_decisions = count($form_data['decisions']);
				
				if ($number_of_reviewers == $number_of_decisions)
				{
					$form->setPhase(APPEAL_PHASE_DECISION);
				}
				
				echo renderPage('committee_success');
				exit;
			}
			else
			{
				echo renderPage('committee');
				exit;
			}
			break;
		
		// Special page for final approval.
		case 'final':
			$user->checkPermission($form->permissionName('final'));
			
			if (isset($_REQUEST['decision']))
			{
				$form->setFormData(array(
					'final_comments'		=> $_REQUEST['form']['final_comments'],
				));
				
				if ($_REQUEST['decision'] == "approve")
				{
					$committee_decision = $form_info['app_review_final'];
					
					if ($committee_decision == APPEAL_DECISION_GRANT || $committee_decision == APPEAL_DECISION_GRANT_WITH_STIPULATIONS)
					{
						NCE_Grades::addOrgExemption($form_info['uin'], $form_info['org_id'], NCE_Grades::EXEMPT_TEMPORARY);
					}
					else if ($committee_decision == APPEAL_DECISION_DECLINE || $committee_decision == APPEAL_DECISION_DECLINE_EXTENUATING)
					{
						$form_user = new NCE_User($form_info['username']);
						$position_in_org = $form_user->isOrgMember($form_info['org_id']);
						
						if ($position_in_org)
						{
							$org = new NCE_Organization($form_info['org_id']);
							$org->deleteMember($form_info['user_id'], $position_in_org, FALSE, TRUE);
						}
					}
					
					$form->setFormData(array(
						'decisions'		=> NULL,
					));
					
					$form->setPhase(APPEAL_PHASE_COMPLETE);
				}
				else
				{
					$form->setPhase(APPEAL_PHASE_DECISION);
				}
				
				echo renderPage('final_complete');
				exit;
			}
			else
			{
				$decision = $form_info['app_review_final'];
				$template->decision_text = $form_settings['decisions'][$decision];	
				
				echo renderPage('final');
				exit;
			}
		break;
		
		// Archive a form.
		case 'archive':
			$form->setPhase(APPEAL_PHASE_ARCHIVED);
			
			$user->showOnNextPage('<b>Form #'.$form_id_hash.' has been archived.', 'green');
			
			if ($user->hasPriv('admin_forms_gpr_manager'))
			{
				header("Location: ../admin");
			}
			else
			{
				header("Location: ../index");
			}
			exit;
		break;
		
		// Re-send any notifications for the current phase
		case 'notify':
			$user->checkPermission($form->permissionName('manager'));
			
			$form->notify();
			
			$user->showOnNextPage('<b>Notifications re-sent for form #'.$form_id_hash.'.</b>', 'green');
			
			header("Location: ./view");
			exit;
		break;
	}
}
else
{
	/**
	 * General form management functions
	 */
	
	switch ($_REQUEST['action'])
	{
		case 'new':
			if (isset($_REQUEST['submitted']))
			{
				$org_id_field = ($_REQUEST['search_type'] == "select-org") ? 'select_org_id' : 'search_org_id';
				$org_id = $_REQUEST[$org_id_field];
				
				if (!$org_id)
				{
					NCE_Error::display('No Organization Specified', 'You did not specify an organization for this request. Please go back and correct this error.');
					exit;
				}
				
				if (strlen($org_id) == 6)
					$org_id = (int)NCE_Organization::findByAccountNumber($org_id);
				
				$org = new NCE_Organization($org_id);
				$org_info = $org->getInfo();
				
				if ($org_info['org_advising_department'] == 'Department of Greek Life')
				{
					NCE_Error::display('Cannot Use This Form for Greek Life Organizations', 'To submit a GPR exemption request for a Greek Life organization, contact the Department of Greek Life directly.');
					exit;
				}
				
				$form_id = SAO_Form_GradeAppeal::create($org_id, $routing);
				$form = new SAO_Form_GradeAppeal($form_id);
				
				// If the user chooses to bypass endorsement, indicate this in the form's records.
				if ($_REQUEST['submit_for_endorsement'] == NCE_OPT_NO)
					$form->setEndorsement('csl', APPEAL_ENDORSEMENT_BYPASSED);
				
				$form->setEndorsement('advisor', APPEAL_ENDORSEMENT_PENDING);
				
				header("Location: ./".$form_id."/form");
				exit;
			}
			else
			{
				echo renderPage('new');
				exit;
			}
			break;
			
		// Special pages for committee and final reviewers.
		case 'special':
			$forms_by_phase = getAllForms($routing);
            
			if ($user->hasPriv(SAO_Form_GradeAppeal::getPermissionName('manager', $routing)))
			{
				header("Location: ./admin");
				exit;
			}
			else if ($user->hasPriv(SAO_Form_GradeAppeal::getPermissionName('committee', $routing)))
			{ 
				$template->view_type = "committee";
				$forms = $forms_by_phase[APPEAL_PHASE_COMMITTEE];
				
				foreach($forms as &$form)
				{
					$form_obj = new SAO_Form_GradeAppeal($form['app_id_hash']);
					$form_data = $form_obj->getFormData();
					$form['is_processed'] = (isset($form_data['decisions'][$user->user_id]));
				}
				$template->forms = $forms;
				
				echo renderPage('special');
				exit;
			}
			else if (SAO_Form_GradeAppeal::getPermissionName('final', $routing))
			{
				$template->view_type = "final";
				$template->forms = $forms_by_phase[APPEAL_PHASE_FINAL];
				
				echo renderPage('special');
				exit;
			}
			else
			{
				header("Location: ./index");
				exit;
			}
			break;
			
		case 'admin':
			$user->checkPermission(array(
                SAO_Form_GradeAppeal::getPermissionName('manager', $routing),
                SAO_Form_GradeAppeal::getPermissionName('info', $routing),
            ), 'OR');
			
			// Retrieve forms in the current phase.
			if (!$user->hasPriv(SAO_Form_GradeAppeal::getPermissionName('manager', $routing)))
			{
				$template->view = $view = APPEAL_PHASE_INFO;
			}
			else
			{
				$view = (isset($_REQUEST['view'])) ? $_REQUEST['view'] : APPEAL_PHASE_INITIAL;
				$template->view = $view;
			}
			
			$forms = getAllForms($routing, $view);
			$paginator = Zend_Paginator::factory((array)$forms);
			
			if (isset($_REQUEST['page']))
			{
				$paginator->setCurrentPageNumber($_REQUEST['page']);
			}
	
			$template->forms = $paginator;
			$template->num_forms = count($forms);
			$template->pages = (array)$paginator->getPages();
			
			if ($user->hasPriv(SAO_Form_GradeAppeal::getPermissionName('manager', $routing)))
			{
				// Totals by phase (for tabs)
				$template->totals_by_phase = getTotals($routing);
				$template->setContentTabs('forms/gpr_appeal/normal/admin_tabs.tpl', $view);
			}
			
			echo renderPage('admin');
			exit;
		break;
		
		case 'stipulations':
			$user->checkPermission(SAO_Form_GradeAppeal::getPermissionName('stipulations', $routing));
			
			$finished_forms = array(APPEAL_PHASE_COMPLETE, APPEAL_PHASE_ARCHIVED);
			
			$forms = $db->fetchAll('SELECT fga.*, u.firstname, u.lastname, u.uin FROM forms_gpr_appeal AS fga LEFT JOIN users AS u ON fga.user_id = u.user_id WHERE fga.app_phase IN ('.$db->quote($finished_forms).') AND fga.app_routing_type = ? ORDER BY fga.app_timestamp_updated ASC, fga.app_timestamp ASC', array($routing));
			array_walk($forms, array('SAO_Form_GradeAppeal', 'process'));
			
			$stipulations = array();
			$uins_to_scan = array();
			
			foreach($forms as $form)
			{
				if ($form['app_data']['decision'] == APPEAL_DECISION_GRANT_WITH_STIPULATIONS && !empty($form['app_data']['stipulations']))
				{
					$stipulations[] = $form;
					$uins_to_scan[] = $form['uin'];
				}
			}
			
			$records = NCE_Grades::getBulkStudentRecords($uins_to_scan);
			
			foreach($stipulations as &$form)
			{
				$uin = $form['uin'];
				$form['record'] = $records[$uin];
			}
			
			$template->term_name = NCE_Grades::getTermName();
			$template->stipulations = $stipulations;
			
			echo renderPage('stipulations');
			exit;
		break;
		
		// Allows the final reviewer to look back at all forms that were reviewed previously.
		case 'previous':
			$user->checkPermission(array(
                SAO_Form_GradeAppeal::getPermissionName('manager', $routing),
                SAO_Form_GradeAppeal::getPermissionName('final', $routing),
            ), 'OR');
			
			$forms_by_phase = getAllForms($routing);
			$forms = array_merge($forms_by_phase[APPEAL_PHASE_COMPLETE], $forms_by_phase[APPEAL_PHASE_ARCHIVED]);
			usort($forms, 'sortByLastName');
			
			$paginator = Zend_Paginator::factory((array)$forms);
			
			if (isset($_REQUEST['page']))
				$paginator->setCurrentPageNumber($_REQUEST['page']);
	
			$template->forms = $paginator;
			$template->num_forms = count($forms);
			$template->pages = (array)$paginator->getPages();
			
			echo renderPage('previous');
			exit;
		break;
		
		case 'index':
		default:
			$user_forms = $db->fetchAll('SELECT fga.*, o.org_name FROM forms_gpr_appeal AS fga LEFT JOIN organizations AS o ON fga.org_id = o.org_id WHERE fga.user_id = ? AND fga.app_routing_type = ? ORDER BY fga.app_timestamp DESC', array($user->user_id, $routing));
			array_walk($user_forms, array('SAO_Form_GradeAppeal', 'process'));
			
			$template->forms = $user_forms;
			
			echo renderPage('index');
			exit;
		break;
	}
}

/**
 * Cache/Fetch All Forms
 */

function getTotals($routing)
{
	global $db;

	$form_totals = $db->fetchAll('SELECT fga.app_phase, COUNT(fga.app_id) AS num_apps FROM forms_gpr_appeal AS fga WHERE fga.app_routing_type = ? GROUP BY fga.app_phase', array($routing));

	$totals_by_phase = array();
	foreach($form_totals as $total)
	{
		$totals_by_phase[$total['app_phase']] = (int)$total['num_apps'];
	}

	return $totals_by_phase;
}

function getAllForms($routing, $phase = 'all')
{
	global $db;

	$phase_name = (is_array($phase)) ? implode('.', $phase) : $phase;
    $cache_name = 'forms_gpr_appeal_'.$routing.'_'.$phase_name;

    $settings = SAO_Form_GradeAppeal::loadSettings();

    $forms_by_phase = NCE_Cache::get($cache_name);
	
	if (!$forms_by_phase)
	{
		if ($phase !== "all")
			$forms_where = ' AND fga.app_phase IN ('.$db->quote((array)$phase).')';
		else
			$forms_where = '';
		
		$forms = $db->fetchAll('SELECT fga.app_id, fga.user_id, fga.org_id, fga.app_id_hash, fga.app_phase, fga.app_review_advisor, fga.app_review_csl, fga.app_review_final, fga.app_timestamp, fga.app_timestamp_updated, fga.app_routing_type, o.org_name, u.firstname, u.lastname FROM forms_gpr_appeal AS fga LEFT JOIN users AS u ON fga.user_id = u.user_id LEFT JOIN organizations AS o ON fga.org_id = o.org_id WHERE fga.app_routing_type = ? '.$forms_where.' ORDER BY fga.app_timestamp_updated ASC, fga.app_timestamp ASC', array($routing));
		
		// array_walk($forms, array('SAO_Form_GradeAppeal', 'process'));

		$forms_by_phase = array();
		foreach($forms as $form)
		{
			$form['app_phase_text'] = SAO_Form_GradeAppeal::getPhaseName($form['app_phase']);

			if ($form['app_review_final'] != 0)
			{
				$form['decision_text'] = $settings['decisions'][$form['app_review_final']];
			}

			if ($form['app_timestamp_updated'] == 0)
				$form['time_since_update'] = 'Never Updated';
			else
				$form['time_since_update'] = NCE_Utilities::timeDifferenceText(NCE_TIME, $form['app_timestamp_updated']).' ago';

			if ($phase === "all")
				$forms_by_phase[$form['app_phase']][] = $form;
			else
				$forms_by_phase[] = $form;
		}
		
		NCE_Cache::set($forms_by_phase, $cache_name, array('forms','gpr_appeal', $routing));
	}

	return $forms_by_phase;
}

function sortByLastName($a, $b)
{
	return strcmp($a['lastname'], $b['lastname']);
}

/**
 * Hierarchical rendering of templates.
 */

function renderPage($template_name)
{
    global $template, $routing;
    $template_path = SAO_Form_GradeAppeal::getTemplatePath('view', $routing, $template_name);
    return $template->renderPage($template_path);
}