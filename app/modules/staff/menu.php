<?php
return array(
    'default' => array(
        'staff' => array(
            'label' => 'Staff Portal',
            'module' => 'staff',
			'controller' => 'index',
			'action' => 'index',
			'permission' => 'staff_directory',
        ),
		
		'staff_directory' => array(
			'label' => 'Staff Directory',
			'module' => 'staff',
			'controller' => 'directory',
			'action' => 'index',
		),
    ),
);