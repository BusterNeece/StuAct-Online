<?php
/**
 * Staff portal
 */

class Staff_DirectoryController extends \DF\Controller\Action
{
	public function permissions()
	{
		return true;
	}
	
	public function indexAction()
	{
		$this->redirect('https://dsanet.tamu.edu/directory/index/index/id/13');
		return;
	}
}