<?php
class Hr_Checklist_ManageController extends \DF\Controller\Action
{
	public function permissions()
	{
		return true;
	}
	
	public function indexAction()
	{
		$this->redirect('http://dsanet.tamu.edu/hr_checklist/manage');
		return;
    }
}