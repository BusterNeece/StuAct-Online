<?php
use \Entity\OfTheYearForm;

// Force mail delivery even on development.
define('DF_FORCE_EMAIL', TRUE);

class Form_Oftheyear_IndexController extends \DF\Controller\Action
{
	public function permissions()
	{
		return true;
	}

	protected $_settings;
	public function preDispatch()
	{
		parent::preDispatch();
		$this->_settings = $this->current_module_config->general->toArray();
	}

	public function indexAction()
	{
		if ($this->_hasParam('type'))
			$this->redirectFromHere(array('action' => 'form', 'type' => $this->_getParam('type')));
	}

	public function formAction()
	{
		$deadline = strtotime($this->_settings['deadline']);
		if (time() > $deadline)
			throw new \DF\Exception\DisplayOnly('Deadline Passed: The deadline for this award has passed. Form submissions were due at '.$this->_settings['deadline'].'.');

		$type = $this->_getParam('type');
		$type_name = $this->_settings['types'][$type];

		$form_config = $this->current_module_config->forms->{$type}->toArray();
		$form = new \DF\Form($form_config);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			$record = new OfTheYearForm;
			$record->type = $type;
			$record->data = $data;
			$record->save();

			\DF\Messenger::send(array(
				'to'			=> $this->_settings['notify'],
				'subject'		=> $type_name.' Application',
				'message'		=> $form->populate($data)->renderMessage(),
			));

			$this->alert('<b>Your submission has been received!</b><br>Thank you for your submission.', 'green');
			$this->redirectHome();
		}

		$this->view->headTitle($type_name.' Application');
		$this->renderForm($form);
	}
}