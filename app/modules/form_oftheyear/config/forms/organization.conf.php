<?php
return array(	
	'method'		=> 'post',
	'groups'		=> array(

		'intro' => array(
			'legend' => 'Purpose and Eligibility',
			'elements' => array(

				'purpose' => array('markup', array(
					'label' => 'Award Purpose',
					'markup' => '
						<p>The purpose of this award is to recognize Texas A&amp;M University recognized student organizations that have significantly contributed to the Texas A&amp;M community through exemplary operations, programs, and services. Awards will be presented to one organization from each category (registered, affiliated, and sponsored).</p>

						<p>Recipients of the award will receive $100 for their SOFC bank accounts. The Recognized Student Organization of the Year Award will be presented at the All-University Awards Ceremony during Parents\' Weekend. Winners will be contacted beforehand and are highly encouraged to attend.</p>
					',
				)),

				'eligibility' => array('markup', array(
					'label' => 'Eligibility',
					'markup' => '
						<p>In order to be eligible to receive the Organization of the Year award, student organizations must be actively recognized through the Department of Student Activities (in the "In Transition", "Recognized", or "Restricted" statuses). MSC Student Programs are recognized through a separate process and are therefore not eligible for this award.</p>
					',
				)),
				
			),
		),

		'org' => array(
			'legend' => 'Organization Details',
			'elements' => array(

				'OrganizationNominated' => array('text', array(
					'label' => 'Name of Organization Nominated',
					'class' => 'full-width',
					'required' => true,
				)),

				'OrganizationCategorization' => array('radio', array(
					'label' => 'Category of Organization',
					'required' => true,
					'multiOptions' => \DF\Utilities::pairs(array('Registered', 'Affiliated', 'Sponsored')),
				)),

			),
		),

		'application_area' => array(
			'legend' => 'Application',
			'elements' => array(
				'criteria' => array('markup', array(
					'label' => 'Criteria',
					'markup' => '
						<p>Please describe how this organization has met 2 or more of the following criteria in the current academic year:</p>
            			<ol type="1">
            				<li>Demonstrated significant organizational innovation through the programs or services offered and the management of internal operations.</li>
                			<li>Exemplified organization\'s mission and purpose through specific events and activities. </li>
                			<li>Brought positive public recognition to Texas A&amp;M University.</li>
                			<li>Impacted a diverse range of university constituents.</li>
                			<li>Facilitated developmental opportunities for organizational leaders and members.</li>
            			</ol>
            			<p>Responses should not exceed 500 words. You are only required to demonstrate 2 of the criteria, but demonstrating more is encouraged.</p>
					',
				)),

				'Application' => array('textarea', array(
					'label' => 'Application',
					'class' => 'full-width full-height',
					'required' => true,
				)),

			),
		),

		'additional' => array(
			'legend' => 'Additional Information (Optional)',
			'elements' => array(
				'SubmitterName' => array('text', array(
					'label' => 'Your Name',
					'class' => 'half-width',
				)),
				'SubmitterOrganization' => array('text', array(
					'label' => 'Your Student Organization',
					'class' => 'half-width',
				)),
				'SubmitterPhone' => array('text', array(
					'label' => 'Your Phone Number',
				)),
				'SubmitterEmail' => array('text', array(
					'label' => 'Your E-mail Address',
					'class' => 'half-width',
				)),
			),
		),
		
		'submit_group' => array(
			'elements' => array(
				'submit_btn' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Submit Form',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			
			),
		),
	),
);