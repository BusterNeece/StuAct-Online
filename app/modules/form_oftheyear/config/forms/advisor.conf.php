<?php
$module_config = \DF\Registry::get('module_config');
$settings = $module_config['form_oftheyear']->general->toArray();

return array(	
	'method'		=> 'post',
	'groups'		=> array(

		'intro' => array(
			'legend' => 'Purpose and Eligibility',
			'elements' => array(

				'purpose' => array('markup', array(
					'label' => 'Award Purpose',
					'markup' => '
						<p>The purpose of this award is to recognize and reward individuals who have clearly distinguished themselves by providing exemplary guidance, support and dedication to the advisement of a recognized student organization at Texas A&amp;M University. Awards will be presented to one advisor from each of the three categories (registered, affiliated, and sponsored).</p>

						<p><b>To be eligible, nominees must have served as an advisor to the organization for a minimum of two years.</b> Advisors who have served for fewer than two years can be nominated for the New Advisor of the Year award.</p>

						<p>Although nominations of advisors by faculty or staff members is permitted, we highly encourage you to work with the students in the advised organization to produce a letter of support or a separate nomination from the students themselves. Letters of support associated with this nomination can be sent via e-mail to <a href="mailto:'.$settings['staff_contact_email'].'">'.$settings['staff_contact_email'].'</a>.</p>
					',
				)),

				'eligibility' => array('markup', array(
					'label' => 'Eligibility',
					'markup' => '
						<p>In order to be eligible to receive the Advisor of the Year award:</p>
					    <ul>
					    	<li>Advisors must have served as the organization\'s advisor for a minimum of two years.</li>
					        <li>The advised student organization must be actively recognized through the Department of Student Activities. MSC Student Programs are recognized through a separate process and are therefore not eligible for this award.</li>
					    </ul>
					',
				)),
				
			),
		),

		'details' => array(
			'legend' => 'Advisor Details',
			'elements' => array(

				'AdvisorNominated' => array('text', array(
					'label' => 'Advisor Nominated',
					'class' => 'full-width',
					'required' => true,
				)),
				'OrganizationAdvised' => array('text', array(
					'label' => 'Organization Advised',
					'class' => 'full-width',
					'required' => true,
				)),
				'YearsAdvising' => array('text', array(
					'label' => 'Years Advising This Organization',
					'maxlength' => 3,
					'required' => true,
				)),
				'OrganizationCategorization' => array('radio', array(
					'label' => 'Category of Organization',
					'required' => true,
					'multiOptions' => \DF\Utilities::pairs(array('Registered', 'Affiliated', 'Sponsored')),
				)),

			),
		),

		'application_area' => array(
			'legend' => 'Application',
			'elements' => array(
				'criteria' => array('markup', array(
					'label' => 'Criteria',
					'markup' => '
						<p>Please describe how the advisor meets two or more of the following criteria:</p>
			            <ol type="1">
			            	<li>Advisor clearly demonstrated initiative in working with student org. members</li>
			                <li>Advisor was available to his/her org\'s leadership/membership</li>
			                <li>Advisor effectively facilitated the org\'s efforts to accomplish its mission</li>
			                <li>Advisor was dedicated to the development of student learning</li>
			                <li>Advisor served as a role model for organization members</li>
			            </ol>
			            <p>Responses should not exceed 500 words. You are only required to demonstrate 2 of the criteria, but demonstrating more is encouraged.</p>
					',
				)),

				'Application' => array('textarea', array(
					'label' => 'Application',
					'class' => 'full-width full-height',
					'required' => true,
				)),

			),
		),

		'additional' => array(
			'legend' => 'Additional Information (Optional)',
			'elements' => array(
				'SubmitterName' => array('text', array(
					'label' => 'Your Name',
					'class' => 'half-width',
				)),
				'SubmitterOrganization' => array('text', array(
					'label' => 'Your Student Organization',
					'class' => 'half-width',
				)),
				'SubmitterPhone' => array('text', array(
					'label' => 'Your Phone Number',
				)),
				'SubmitterEmail' => array('text', array(
					'label' => 'Your E-mail Address',
					'class' => 'half-width',
				)),
			),
		),
		
		'submit_group' => array(
			'elements' => array(
				'submit_btn' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Submit Form',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			
			),
		),
	),
);