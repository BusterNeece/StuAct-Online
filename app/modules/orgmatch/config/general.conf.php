<?php
/**
 * OrgMatch configuration file.
 */

$orgmatch_config = array();

$orgmatch_config['nonacademic_categories'] = array(
	'none'			=> 'None',
	'corps'			=> 'Corps of Cadets',
	'recsports'		=> 'Recreational Sports',
	'greeklifeifc'	=> 'Greek Life - Interfraternity Council',
	'greeklifecpc'	=> 'Greek Life - Collegiate Panhellenic Council',
	'greeklifemgc'	=> 'Greek Life - Multicultural Greek Council',
	'greeklifenphc'	=> 'Greek Life - National Pan-Hellenic Council',
	'greeklife'		=> 'Greek Life - Other',
	'reslife'		=> 'Residence Life',
	'mcs'			=> 'Multicultural Services',
	'studentlife'	=> 'Department of Student Life',
	'sga'			=> 'Student Government Association',
	'other'			=> 'Other',
);

$orgmatch_config['nonacademic_options'] = array(
	'reslife'		=> 'On-campus Resident',
	'corps'			=> 'Corps of Cadets',
	'sga'			=> 'SGA Committee Member',
	'greeklife'		=> 'Greek Life Group',
	'recsports'		=> 'Rec Sports Group',
);

// Array of colleges.
$orgmatch_config['colleges'] = array(
	'none'				=> 'General (No College)',
	'interdiscipline'	=> 'Interdisciplinary Degree Programs',
	'aglifesciences'	=> 'College of Agriculture and Life Sciences',
	'architecture'		=> 'College of Architecture',
	'maysschool'		=> 'Mays Business School',
	'edhumandev'		=> 'College of Education and Human Development',
	'engineering'		=> 'Dwight Look College of Engineering',
	'geosciences'		=> 'College of Geosciences',
	'liberalarts'		=> 'College of Liberal Arts',
	'science'			=> 'College of Science',
	'vetmedicine'		=> 'College of Veterinary Medicine and Biomedical Sciences',
	'bushschool'		=> 'George Bush School of Government and Public Service',
	'galveston'			=> 'Texas A&M University at Galveston',
	'other'				=> 'Other',
);

$orgmatch_config['frequency_options'] = array(
	'never'					=> 'Never',
	'annual'				=> 'Once per year',
	'onesemester'			=> 'Once per semester',
	'twosemester'			=> 'Twice per semester',
	'threesemester'			=> 'Three or more times per semester',
);

$orgmatch_config['classification_options'] = array(
	'undergraduate'			=> 'Undergraduate student',
	'graduate'				=> 'Graduate student',
	'faculty'				=> 'Faculty member',
	'staff'					=> 'Staff member',
	'community'				=> 'Community member',
);

$orgmatch_config['public_options'] = array(
	'membership_small'		=> 'Organizations with small membership bodies',
	'membership_large'		=> 'Organizations with large membership bodies',
	'timerequirement_small' => 'A small weekly time requirement (0-5 hours)',
	'timerequirement_large' => 'A large weekly time requirement (5-20 hours)',
	'commserv'				=> 'Active participation in community service activities',
	'fundraising'			=> 'Active participation in fundraising efforts',
	'speakers'				=> 'Public or private speakers',
	'oncampussocial'		=> 'On-campus social activities',
	'offcampussocial'		=> 'Off-campus social activities',
	'conferences'			=> 'Conferences',
	'travel_instate'		=> 'In-state travel',
	'travel_outofstate'		=> 'Out-of-state travel',
	'travel_international'	=> 'International travel',
);

$orgmatch_config['options'] = array(
	'affiliation_nonacademic' => $orgmatch_config['nonacademic_categories'],
	'affiliation_academic'	=> $orgmatch_config['colleges'],
	
	'membership_intake'		=> array(
		'open'						=> 'Open',
		'appint'					=> 'Application/Interview',
		'apponly'					=> 'Application Only',
		'pledgeship'				=> 'Pledgeship',
		'invitation'				=> 'Invitation Only',
	),
	
	'membership_population'	=> array(
		'30'						=> '1-30',
		'50'						=> '31-50',
		'100'						=> '51-100',
		'300'						=> '100-300',
		'1000'						=> '300-1000',
		'5000'						=> '1000+',
	),
	
	'membership_timerequirement' => array(
		'3'							=> '0-3 Hours',
		'5'							=> '3-5 Hours',
		'10'						=> '5-10 Hours',
		'20'						=> '10-20 Hours',
		'30'						=> '20+ Hours',
	),

	'membership_policies'	=> array(
		'undergraduate'			=> 'Our organization accepts undergraduate students as members',
		'graduate'				=> 'Our organization accepts graduate students as members',
		'facultystaff'			=> 'Our organization accepts faculty/staff as members',
		'community'				=> 'Our organization accepts community members as members',
		'gpr'					=> 'Our organization membership will be required to meet specific GPR requirements',
		'otherorg'				=> 'Our organization membership will be contingent on membership in another organization (i.e. Corps of Cadets, etc.)',
		'titleix'				=> 'Our organization has a Title IX exemption (you must provide proof of a Title IX exemption for your organization)',
	),
	
	'commserv_frequency'	=> $orgmatch_config['frequency_options'],
	'commserv_types'		=> array(
		'localagencies'			=> 'Working with local service agencies',
		'servcommunity'			=> 'Participating in service events in the community',
		'servcampus'			=> 'Participating in service events on campus',
		'servoutofstate'		=> 'Participating in service events outside of the state of Texas',
		'international'			=> 'Participating in an international service/mission trips',
		'philanthropy'			=> 'Supporting a philanthropy through organization activities',
		'other'					=> 'Other',
	),
	
	'fundraising_frequency'	=> $orgmatch_config['frequency_options'],
	'fundraising_types'		=> array(
		'selltomembers'			=> 'Selling items to membership only',
		'selltononmembers'		=> 'Selling items to non-members',
		'selltickets'			=> 'Selling tickets to an event',
		'memberdues'			=> 'Requiring member dues',
		'services'				=> 'Providing a service (i.e. car washes, face painting, etc.)',
		'philanthropy'			=> 'Supporting a philanthropy or outside organization',
		'donations'				=> 'Soliciting donations through donor contact, donor letters, visits to businesses, etc.',
		'endowment'				=> 'Establishing an endowment',
		'auctions'				=> 'Silent or Live Auctions',
		'raffles'				=> 'Raffles (must be 501(c)(3) to conduct)',
		'other'					=> 'Other',
	),
	
	'speakers_frequency'	=> $orgmatch_config['frequency_options'],
	'speakers_types'		=> array(
		'formerstudents'		=> 'Former Students as speakers',
		'facultystaff'			=> 'Faculty/Staff as speakers',
		'community'				=> 'Non-A&M affiliated individuals as speakers (i.e. recruiters, community members, etc.)',
		'speakersinmeetings'	=> 'Speakers/Lectures as part of organizational meetings',
		'conferences'			=> 'Speakers/Lectures as part of a larger program such as a conference',
		'controversial'			=> 'Speakers/Lectures that may be considered controversial in nature',
		'other'					=> 'Other',
	),
	
	'oncampussocial_frequency' => $orgmatch_config['frequency_options'],
	'oncampussocial_types'	=> array(
		'usesfacilities'		=> 'Social events will use campus facilities',
		'opentoall'				=> 'Social events will be open to all A&M students and/or the general public',
		'opentomembers'			=> 'Social events will be open only to organization membership',
		'opentomembersandguests'=> 'Social events will be open only to organization membership and invited guests',
		'includesvendors'		=> 'Social events will include the services of outside vendors (i.e. party rentals, catering companies, production equipment, etc.)',
		'other'					=> 'Other',
	),
	
	'offcampussocial_frequency' => $orgmatch_config['frequency_options'],
	'offcampussocial_types'	=> array(
		'rentedfacilities'		=> 'Social events will use rented facilities',
		'freefacilities'		=> 'Social events will included donated/free facilities',
		'opentoall'				=> 'Social events will be open to all A&M students and/or the general public',
		'opentomembers'			=> 'Social events will be open only to organization membership',
		'opentomembersandguests'=> 'Social events will be open only to organization membership and invited guests',
		'includesvendors'		=> 'Social events will include the services of outside vendors (i.e. party rentals, catering companies, production equipment, etc.)',
		'other'					=> 'Other',
	),
	
	'conferences_frequency'	=> $orgmatch_config['frequency_options'],
	'conferences_types'		=> array(
		'intownconferences'		=> 'In-town conferences',
		'outoftownconferences'	=> 'Out-of-town conferences',
		'internationalconferences' => 'International conferences',
		'opentotamuonly'		=> 'Conferences are for A&M students/faculty/staff only',
		'opentotamu'			=> 'Conferences include non-A&M students/faculty/staff',
		'involvesminors'		=> 'Conferences will involve minors (under 18 years of age, e.g. youth outreach, leadership conferences, etc.)',
		'usesoncampusfacilities' => 'Conferences will use on-campus facilities',
		'usesoffcampusfacilities' => 'Conferences will use off-campus facilities',
		'other'					=> 'Other',
	),

	'specialevents_frequency' => $orgmatch_config['frequency_options'],
	'specialevents_types'	=> array(
		'oncampus'				=> 'Special events will be on-campus',
		'offcampus'				=> 'Special events will be off-campus',
		'opentoall'				=> 'Special events will be open to all A&M students and/or the general public',
		'opentomembers'			=> 'Special events will be open only to organization membership',
		'opentomembersandguests' => 'Special events will be open only to organization membership and invited guests',
		'involvesminors'		=> 'Special events will involve minors (under 18 years of age, e.g. camps, conferences, recruiting events, etc.)',
		'includesvendors'		=> 'Special events will require the services of outside vendors',
		'includescontract'		=> 'Special events will involve contracting with a group or outside vendor',
		'controversial'			=> 'Special events may be considered controversial in nature',
		'other'					=> 'Other',
	),
	
	'orientation_frequency'	=> $orgmatch_config['frequency_options'],
	'orientation_types'		=> array(
		'intownretreats'		=> 'In-town member retreats',
		'outoftownretreats'		=> 'Out-of-town member retreats',
		'pledges'				=> 'New members must pledge with the organization',
		'formalinitiation'		=> 'New members are formally initiated into the organization',
		'informainitiation'		=> 'New members are informally initiated into the organization',
		'specialtraining'		=> 'New members must receive specialized training in order to participate in organization activities',
	),
	
	'alcohol_frequency'		=> $orgmatch_config['frequency_options'],
	'alcohol_types'			=> array(
		'opentoall'				=> 'Events will be open to all A&M students and/or the general public',
		'opentomembers'			=> 'Events will be open only to organization membership',
		'opentomembersandguests' => 'Events will be open only to organization membership and invited guests',
		'includesvendors'		=> 'Events will require the services of outside vendors',
		'other'					=> 'Other',
	),
	
	'travel_instate'		=> $orgmatch_config['frequency_options'],
	'travel_outofstate'		=> $orgmatch_config['frequency_options'],
	'travel_international'	=> $orgmatch_config['frequency_options'],
	'travel_types'			=> array(
		'retrats'				=> 'Retreats',
		'conferences'			=> 'Conferences/Conventions',
		'roadtrips'				=> 'Road trips',
		'serviceevents'			=> 'Service events',
		'fundraising'			=> 'Fundraising activities (i.e. visiting Aggie Mom�s Clubs, donor relations, etc.)',
		'campsandenrichment'	=> 'Camps/Enrichment programs (i.e. programs involving minors)',
		'other'					=> 'Other',
	),
	'travel_modes'			=> array(
		'personalcars'			=> 'Personal Automobiles',
		'commercialcars'		=> 'Commercial Cars/Vans',
		'universitycars'		=> 'University Cars/Vans',
		'airplanes'				=> 'Airplanes',
		'other'					=> 'Other',
	),
	
	'risk_types'			=> array(
		'service'				=> 'Community service projects',
		'fundraising'			=> 'Fundraising events',
		'speakers'				=> 'Speakers/Lectures',
		'oncampussocial'		=> 'On-campus social events',
		'offcampussocial'		=> 'Off-campus social events',
		'conferences'			=> 'Conferences',
		'specialevents'			=> 'Special events',
		'orientation'			=> 'New member orientation/training',
		'alcohol'				=> 'Events with alcohol',
		'travel'				=> 'Events with travel',
		'campsandenrichment'	=> 'Camps/Enrichment programs (i.e. programs involving minors)',
		'other'					=> 'Other',
	),
);

return $orgmatch_config;