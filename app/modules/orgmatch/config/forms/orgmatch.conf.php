<?php
/**
 * OrgMatch Profile
 */

$preference_options = array(
    '0'     => '0 - No Preference',
    '1'     => '1 - Strongly Dislike',
    '2'     => '2 - Dislike',
    '3'     => '3 - Neutral',
    '4'     => '4 - Like',
    '5'     => '5 - Strongly Like',
);

$config = \Zend_Registry::get('config');
$module_config = \Zend_Registry::get('module_config');

$orgmatch_config = $module_config['orgmatch']->general->toArray();

// Generate options.
$classification_elements = array();

$org_config = $config->organizations->toArray();
$org_classifications = array_diff($org_config['classifications'], $org_config['classifications_hidden']);

foreach($org_classifications as $classification)
{
    $key_name = str_replace(' ', '', $classification);
    $classification_elements[$key_name] = array('radio', array(
        'label' => $classification,
        'inline' => true,
        'belongsTo' => 'classifications',
        'multiOptions' => $preference_options,
    ));
}

$public_option_elements = array();
foreach($orgmatch_config['public_options'] as $public_option_key => $public_option)
{
    $public_option_elements[$public_option_key] = array('radio', array(
        'label' => $public_option,
        'inline' => true,
        'belongsTo' => 'public_options',
        'multiOptions' => $preference_options,
    ));
}

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method' => 'post',
        'groups' => array(

            'about_orgmatch' => array(
                'legend' => 'About OrgMatch',
                'elements' => array(

                    'orgmatch_intro' => array('markup', array(
                        'markup' => '<p>OrgMatch is a personal profiler that matches you with ideal organizations based on your personal interests and preferences.</b> After you have completed this quick questionnaire, our system will search the database of over 800 student organizations and find the ones that match your interests on the most levels. You can learn more about these matching groups by reading their profiles, visiting their web sites, and contacting their representatives.</p>
                        <p><b>Note:</b> Information obtained by the OrgMatch system is used solely for the purpose of matching you with organizations. Organizations do not have access to this information, and the Department of Student Activities will not release it to any third parties. For more information, see our <a href="'.\DF\Url::baseUrl().'/help/privacy" target="_blank">Privacy Policy</a>.</p>',
                    )),

                ),
            ),
            
            'basic_info' => array(
                'legend' => 'Basic Information',
                'elements' => array(

                    'personal_classification' => array('select', array(
                        'label' => 'Your Classification',
                        'multiOptions' => $orgmatch_config['classification_options'],
                    )),
                    'affiliation_academic' => array('select', array(
                        'label' => 'Your College Affiliation',
                        'multiOptions' => $orgmatch_config['colleges'],
                    )),
                    'affiliation_nonacademic' => array('multiCheckbox', array(
                        'label' => 'Your Other Affiliations',
                        'multiOptions' => $orgmatch_config['nonacademic_options'],
                    )),

                ),
            ),

            'classifications' => array(
                'legend' => 'Organization Types',
                'description' => 'Each student organization on campus is assigned to a general classification. These are broad descriptors of the overall purpose of the organization. Indicate your interest in each of the classifications below.',
                'elements' => $classification_elements,
            ),
            
            'other_preferences' => array(
                'legend' => 'Other Preferences',
                'description' => 'Indicate your interest in each of the additional criteria below.',
                'elements' => $public_option_elements,
            ),

            'submit_group' => array(
                'elements' => array(
                    'submit_btn' => array('submit', array(
                        'type'  => 'submit',
                        'label' => 'Get Results',
                        'helper' => 'formButton',
                        'class' => 'ui-button',
                    )),
                ),
            ),

        ),
	),
);