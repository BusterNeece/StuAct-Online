<?php
use \Entity\Organization;
use \Entity\User;

use \StuAct\Service\GoogleSearch;

class Orgmatch_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        return TRUE;
    }
    
    public function indexAction()
    {
        $form_config = $this->current_module_config->forms->orgmatch->form->toArray();
        $form = new \DF\Form($form_config);

        if ($_POST && $form->isValid($_POST))
        {
            $orgmatch_profile = $form->getValues();
            $orgmatch_results = \StuAct\OrgMatch::match($orgmatch_profile);
            
            $this->view->orgmatch_results = $orgmatch_results;
            $this->render('results');
            return;
        }

        $this->view->headTitle('OrgMatch');
        $this->renderForm($form, 'edit', 'Questionnaire');
    }
}