<h2>Password Reset Code</h2>

<p>Howdy! This e-mail address recently submitted a request for a password reset. You can reset your password by clicking the link below:</p>
<p>
	To create new forms and view previously submitted ones, visit this address:<br />
    <a href="{$config_data.url_base}/forms/camps/login?email={$email}&amp;code={$code}">{$config_data.url_base}/forms/camps/login?email={$email}&amp;code={$code}</a>
</p>