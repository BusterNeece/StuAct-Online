{include file="messages/forms/camps/form_info.tpl"}

<p>Howdy!</p>

<p>Our records show that you have started a Camp/Enrichment Program application, but have not yet submitted the form for review and approval.</p>

<p><b>The Camp and Enrichment Programs Office within the Department of Student Activities requires applications to be submitted <i>at least</i> 8 weeks prior to the start date of the program.</b> An application submitted less than 8 weeks prior to the start of the requested program will NOT be reviewed for compliance.</p>

<p>For further explanation and documentation regarding the Camps and Enrichment Programs process, please access Texas A&amp;M University Rule 11.99.99.</p>

<p>If you have any questions or concerns regarding your program or application, please contact {$form_settings.contact_name} at {$form_settings.contact_phone} or {mailto address=$form_settings.contact_email}.</p>

<p>
	To visit the online application homepage and complete this form, visit this link:<br />
    {$home_url|link}
</p>

<p>Thanks and Gig 'Em!</p>