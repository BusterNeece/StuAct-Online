<?php
/**
 * Camps form configuration
 */

return array(	
	// Contact information used in e-mail templates and web pages.
	'contact_name'		=> 'Ruthi Hernandez',
	'contact_email'		=> 'camps@stuact.tamu.edu',
	'contact_phone'		=> '979-458-0627',
    
    // E-mail addresses to send incident reports to.
    'notify_incidents' => array(
        'camps@stuact.tamu.edu',
        'insurance@tamu.edu',
    ),

    // Steps involved in form.
	'steps'	=> array(
		'Logistics',
		'Operational',
		'Compliance',
		'Participant Well-Being',
		'Financial',
	),

	// Options used in the form view.
	'options'			=> array(
		'yesno'				=> array(
			'Yes'				=> 'Yes',
			'No'				=> 'No',
		),
		
		'activities'		=> \DF\Utilities::pairs(array(
			"Water Sports",
			"Sports Activities",
			"Climbing",
			"Running",
			"Strength Training",
			"Equestrian",
			"Gymnastics",
			"Other",
		)),
		
		'insurance_required_activities' => \DF\Utilities::pairs(array(
			'Rappelling',
			'Skin Diving',
			'Scuba Diving',
			'Snow Skiing',
			'Water Skiing',
			'Whitewater Rafting',
			'Bungee Jumping',
			'Motorsports (including golf carts, bumper cars, etc.)',
			'Rodeo or any equestrian-related sports',
			'Ballooning',
			'Parachute Jumping',
			'Pyrotechnics',
		)),
		
		'participants_staying'			=> array(
			'On Campus'				=> 'Participants will stay on campus.',
			'Off Campus'			=> 'Participants will be housed in off-campus lodging (not affiliated with Texas A&amp;M University) arranged by the camp or program.',
			'Own Housing'			=> 'Participants will be responsible for arranging their own off campus housing.',
		),
		
		'residence_halls'				=> \DF\Utilities::pairs(array('Appelt', 'Aston', 'Briggs', 'Clements', 'Crocker', 'Davis-Gary', 'Dunn', 'Eppright', 'Fowler', 'Haas', 'Hart', 'Hobby', 'Hughes', 'Keathley', 'Kiest', 'Krueger', 'Lechner', 'Legett', 'McFadden', 'McInnis', 'Moore', 'Moses', 'Mosher', 'Neeley', 'Rudder', 'Schuhmacher', 'Spence', 'Underwood', 'Walton', 'Wells', 'Galveston - Albatross', 'Galveston - Atlantic', 'Galveston - Hullabaloo', 'Galveston - Mariner', 'Galveston - Oceans', 'Galveston - Pacific', 'Galveston - Polaris')),
		
		'offcampus_locations'			=> \DF\Utilities::pairs(array(
			'Hotels',
			'Personal Houses',
		)),
		
		'background_checks'				=> \DF\Utilities::pairs(array(
			'TAMU Background Screening',
			'TAMU-G Background Screening',
			'Other',
		)),
	),
	
	// Number of fields to show in the "Detailed Itinerary" section.
	'itinerary_num' => 10,
	
	// Insurance rate types.
	'rate_types'	=> array(
		'daytime_general_sports'		=> 'Sports - General Liability (Daytime)',
		'daytime_accident_sports'		=> 'Sports - Accident Medical (Daytime)',
		'overnight_general_sports'		=> 'Sports - General Liability (Overnight)',
		'overnight_accident_sports'		=> 'Sports - Accident Medical (Overnight)',
		'daytime_general'				=> 'Non-Sports - General Liability (Daytime)',
		'daytime_accident'				=> 'Non-Sports - Accident Medical (Daytime)',
		'overnight_general'				=> 'Non-Sports - General Liability (Overnight)',
		'overnight_accident'			=> 'Non-Sports - Accident Medical (Overnight)',
		'support'						=> 'Support Service Fee',
	),
	
	// Time ranges for "Matrix" exports.
	'matrix_ranges' => array(
		'winter' => array(
			'name'				=> 'Winter Matrix',
			'start'				=> mktime(0, 0, 0, 12, 1, date('Y')-1),
			'end'				=> mktime(0, 0, 0, 3, 0, date('Y')),
		),
		'spring' => array(
			'name'				=> 'Spring Matrix',
			'start'				=> mktime(0, 0, 0, 3, 1, date('Y')),
			'end'				=> mktime(0, 0, 0, 6, 0, date('Y')),
		),
		'summer' => array(
			'name'				=> 'Summer Matrix',
			'start'				=> mktime(0, 0, 0, 6, 1, date('Y')),
			'end'				=> mktime(0, 0, 0, 9, 0, date('Y')),
		),
		'fall'				=> array(
			'name'				=> 'Fall Matrix',
			'start'				=> mktime(0, 0, 0, 9, 1, date('Y')),
			'end'				=> mktime(0, 0, 0, 12, 0, date('Y')),
		),
	),
	
	// Interval of time to wait before sending another "late submission" notification.
	'late_interval' => (86400 * 3) + 3600,
	
	// Number of days before camp when a form is considered a "late submission".
	'late_threshold' => (86400 * 7) * 8,
);