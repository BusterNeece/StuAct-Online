<?php
use \DF\Utilities;

return array(
	'id' => 'camps_form',
	'method' => 'post',
	'elements'		=> array(
		'program_id' => array('select', array(
			'label' => 'Associated Program',
			'multiOptions' => \Entity\CampsFormProgram::fetchSelect(),
		)),

		'btn_submit' => array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Save Changes',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),
	),
);