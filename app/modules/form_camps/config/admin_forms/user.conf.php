<?php
use \DF\Utilities;

return array(
	'id' => 'camps_form',
	'method' => 'post',
	'elements'		=> array(

		'firstname' => array('text', array(
			'label' => 'First Name',
			'class' => 'half-width',
		)),

		'lastname' => array('text', array(
			'label' => 'Last Name',
			'class' => 'half-width',
		)),

		'email' => array('text', array(
			'label' => 'E-mail Address (Username)',
			'class' => 'full-width',
			'required' => true,
			'validators' => array('EmailAddress'),
		)),

		'password' => array('password', array(
			'label' => 'Set New Password',
			'default' => '',
			'autocomplete' => 'off',
			'description' => 'To leave the password unchanged, leave this field blank.',
		)),

		'programs' => array('multiCheckbox', array(
			'label' => 'Associated Programs',
			'multiOptions' => \Entity\CampsFormProgram::fetchSelect(),
		)),

		'btn_submit' => array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Save Changes',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),
	),
);