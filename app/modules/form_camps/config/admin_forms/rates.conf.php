<?php
use \DF\Utilities;

return array(
	'id' => 'camps_form',
	'method' => 'post',
	'groups' => array(
		'sport_camps' => array(
			'legend' => 'Sports Camps',
			'elements' => array(
				'daytime_general_sports' => array('text', array(
					'label' => 'General Liability Insurance Premium (Daytime)',
					'filters' => array('Float'),
				)),
				'overnight_general_sports' => array('text', array(
					'label' => 'General Liability Insurance Premium (Overnight)',
					'filters' => array('Float'),
				)),

				'daytime_accident_sports' => array('text', array(
					'label' => 'Accident Medical Insurance Fee (Daytime)',
					'filters' => array('Float'),
				)),
				'overnight_accident_sports' => array('text', array(
					'label' => 'Accident Medical Insurance Fee (Overnight)',
					'filters' => array('Float'),
				)),
			),
		),
		'non_sport_camps' => array(
			'legend' => 'Non-Sports Camps',
			'elements' => array(
				'daytime_general' => array('text', array(
					'label' => 'General Liability Insurance Premium (Daytime)',
					'filters' => array('Float'),
				)),
				'overnight_general' => array('text', array(
					'label' => 'General Liability Insurance Premium (Overnight)',
					'filters' => array('Float'),
				)),

				'daytime_accident' => array('text', array(
					'label' => 'Accident Medical Insurance Fee (Daytime)',
					'filters' => array('Float'),
				)),
				'overnight_accident' => array('text', array(
					'label' => 'Accident Medical Insurance Fee (Overnight)',
					'filters' => array('Float'),
				)),
			),
		),
		'ssf' => array(
			'legend' => 'Support Service Rates',
			'elements' => array(
				'support' => array('text', array(
					'label' => 'Support Service Fee',
					'filters' => array('Float'),
				)),
			),
		),
		'submit_grp' => array(
			'elements' => array(
				'btn_submit' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save Changes',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);