<?php
use \DF\Utilities;

return array(
	'method' => 'post',
	'elements'		=> array(
		'user_id' => array('select', array(
			'label' => 'User Name',
			'multiOptions' => \Entity\CampsFormUser::fetchSelect(),
		)),

		'btn_submit' => array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Add User',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),
	),
);