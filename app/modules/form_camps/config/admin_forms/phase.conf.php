<?php
use \DF\Utilities;

return array(
	'id' => 'camps_form',
	'method' => 'post',
	'elements'		=> array(

		'phase' => array('radio', array(
			'label' => 'New Phase',
			'multiOptions' => \Entity\CampsForm::getPhaseNames(),
		)),

		'btn_submit' => array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Set Phase',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),
	),
);