<?php
use \DF\Utilities;

return array(
	'id' => 'camps_form',
	'method' => 'post',
	'elements'		=> array(

		'File_Supplemental' => array('file', array(
			'label' => 'Add Supplemental File',
			'description' => 'Use this section to upload any additional documents associated with this form. There is no limit to the number of files you can associate with the form, but each one must be uploaded individually.',
		)),

		'description' => array('textarea', array(
			'label' => 'File Description',
			'class' => 'full-width half-height',
		)),

		'btn_submit' => array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Add Supplemental File',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),
	),
);