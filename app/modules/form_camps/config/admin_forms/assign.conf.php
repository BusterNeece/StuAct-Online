<?php
use \DF\Utilities;

return array(
	'id' => 'camps_form',
	'method' => 'post',
	'elements' => array(

		'claimant_user_id' => array('radio', array(
			'label' => 'Assign Form To',
			'multiOptions' => array(),
			'required' => true,
		)),
		
		'btn_submit' => array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Save Changes',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),
	),
);