<?php
return array(
    'method' => 'post',
    'groups' => array(
        
        'account_info' => array(
            'legend' => 'Account Information',
            'elements' => array(
                
                'email'	=> array('text', array(
                    'label' => 'E-mail Address',
                    'class' => 'half-width',
                    'required' => true,
                )),
        
                'password'	=> array('password', array(
                    'label' => 'Password',
                    'class' => 'half-width',
                    'required' => true,
                )),
                
            ),
        ),
        
        'general_info' => array(
            'legend' => 'General Information',
            'elements' => array(
                
                'firstname' => array('text', array(
                    'label' => 'First Name',
                    'required' => true,
                )),

                'lastname' => array('text', array(
                    'label' => 'Last Name',
                    'required' => true,
                )),

            ),
        ),
        
        'submit' => array(
            'elements' => array(
                'submit'		=> array('submit', array(
                    'type'	=> 'submit',
                    'label'	=> 'Create Account and Log In',
                    'helper' => 'formButton',
                    'class' => 'ui-button',
                )),
            ),
        ),
        
    ),
);