<?php

return array(
	'id' => 'camps_form',
	'method' => 'post',
	'groups' => array(

		'compliance_general' => array(
			'elements' => array(
				'counselors_chosen' => array('textarea', array(
					'label' => 'How are counselors and staff chosen?',
					'class' => 'full-width half-height',
				)),
			),
		),

		'background_checks_grp' => array(
			'legend' => 'Background Checks',
			'elements' => array(
				'background_checks_info' => array('markup', array(
					'label' => 'About Background Checks',
					'markup' => '<p>While the Camps & Enrichment Programs office does not require that you submit copies of your background checks and/or verifications of completed checks, it is recommended that you retain these records in a secure location for a minimum of three (3) years following your program\'s completion, as well as maintaining copies of the forms on-site during your program. If stored in hard copy form, records must be in a secure facility. If scanned, the hard copies must be shredded and the electronic copies must be password protected in accordance with University encryption policies.</p>

						<p>The Standard Administrative Procedure for Camps and Programs for Minors (<a href="http://rules-saps.tamu.edu/PDFs/24.01.06.M0.01.pdf" target="_blank">24.01.06.M0.01</a>) requires TAMU sponsored programs to complete the TAMU Human Resources background check for each adult employee and volunteer\'s address.</p>

						<p>Third party programs must complete background screenings utilizing both a national criminal history database and sex offender registration database; the background screening process must be acceptable to the individual with designated approval authority.</p>

						<p>All background checks for all CPMs are required to be conducted within the previous year of the start of the program.</p>


						<p>If a staff member or volunteer has completed a background check as part of their role as a professional (i.e. a doctor, schoolteacher, employee of Texas A&M University, etc), a written verification of their completed background check is needed, rather than a new background check. If additional information is required to secure these verifications, see the <a href="http://rules-saps.tamu.edu/PDFs/11.99.99.M1.01.pdf" target="_blank">TAMU Standard Administrative Procedure for Camps & Enrichment Programs</a>.</p>',
				)),

				'background_check_options' => array('markup', array(
					'label' => 'Background Check Options',
					'markup' => '
						<ul>
							<li><b><a href="http://employees.tamu.edu/docs/employment/hiring/404CrimBGCheck.pdf" target="_blank">TAMU Comprehensive Background Check ($5 per check)</a></b><br>
							This background check includes a national criminal and sex offender database search through Texas A&M University\'s Human Resources and Recruitment Office. This check costs $5 per individual checked and requires the individual’s Social Security Number (SSN). For more information about this type of check as well as the required forms, visit the link below. Payment should be made directly to Human Resources and Recruitment (not Student Activities); for any questions about this process, contact the individuals listed below.
						</li>
						<li><b>Other</b><br>
							If you are a third party program conducting a background check outside of the processes outlined above, please describe the check being conducted below, including the provider of the check. This check must include both a national criminal history and sex offender registry check. If you are using more than one provider to conduct your background checks (including the checks listed above), select this option and describe all checks being used in the box below.
						</li>
					</ul>',
				)),

				'background_check_contact' => array('markup', array(
					'label' => 'For More Information',
					'markup' => '<p>For any questions regarding the background check process, contact:</p>
						<blockquote>Cheryl L. McDonald SPHR CCP<br>
							Manager, Recruitment and Workforce Planning<br>
							Texas A&M University<br>
							cmcdonald@vpfn.tamu.edu<br>
							Tel: 979-862-1015 • Fax: 979-847-8877
						</blockquote>
						<p>Or:</p>
						<blockquote>Maria Herrera<br>
							Human Resources<br>
							Texas A&M University<br>
							mherrera@vpfn.tamu.edu<br>
							Tel: 979-862-7164 • Fax: 979-847-8877
						</blockquote>',
				)),

				'background_check' => array('radio', array(
					'label' => 'Select Background Check Method',
					'required' => true,
					'multiOptions' => array(
						'tamu_comprehensive' => 'TAMU Comprehensive Background Check',
						'other' => 'Other (Specify Below)',
					),
				)),

				'background_check_other' => array('textarea', array(
					'label' => 'If "Other", please specify',
					'class' => 'full-width half-height',
				)),

				'background_check_record_initials' => array('text', array(
					'label' => 'The CPM Sponsor understands that the CPM must retain a record that the background checks have been completed and retained for three years after the end of the CPM. Place initials in the box',
					'required' => true,
				)),

				'bringing_own_supervisors' => array('radio', array(
					'label' => 'Will participants bring their own counselors, coaches, parents, or supervisors?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'required' => true,
				)),

				'supervisors_background_checked' => array('radio', array(
					'label' => 'If yes, have these counselors, coaches, parents, or supervisors received a background check?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'class' => 'conditional-decider yesno',
				)),

				'supervisors_which_check' => array('text', array(
					'label' => 'Which check was used?',
					'class' => 'full-width conditional supervisors_background_checked-yes',
				)),
				'supervisors_retention_initials' => array('text', array(
					'label' => 'The CPM Sponsor understands that the CPM must retain a record that the background checks have been completed and retained for three years after the end of the CPM. Place initials in the box',
					'class' => 'conditional supervisors_background_checked-yes',
				)),

				'supervisors_retention_whynot' => array('textarea', array(
					'label' => 'Why was a background check not conducted on these counselors, coaches, parents, or supervisors?',
					'class' => 'conditional supervisors_background_checked-no',
				)),
			),
		),

		'training_grp' => array(
			'legend' => 'Training',
			'elements' => array(

				'cpt_training' => array('markup', array(
					'label' => 'CPT Training',
					'markup' => 'For more information, visit <a href="http://studentactivities.tamu.edu/camps/safety" target="_blank">this page</a>.',
				)),

				'additional_training' => array('textarea', array(
					'label' => 'In addition to Child Protection Training (required by TAMU Camps and Programs for Minors SAP), what additional training has been provided to the program employees/volunteers?',
					'class' => 'full-width half-height',
				)),

				'cpm_retention_initials' => array('text', array(
					'label' => 'The CPM Sponsor understands that the CPM must retain a record that the Child Protection Training has been completed and retained for two years after the end of the CPM. The Texas Department of State Health Services form documenting this training must be submitted to <a href="mailto:camps@stuact.tamu.edu">camps@stuact.tamu.edu</a> no later than 5 business days before the start date of the program. Place initials in the box',
					'required' => true,
				)),

			),
		),

		'codemaroon_grp' => array(
			'legend' => 'Code Maroon',
			'elements' => array(
				'cm_requirements' => array('markup', array(
					'markup' => '<p>You are required to have two individuals who will be present during all camp activities (and overnight, if applicable) that has signed up for Code Maroon alerts. For more information about Code Maroon, see <a href="http://codemaroon.tamu.edu/" target="_blank">codemaroon.tamu.edu</a>.</p>',
				)),

				'codemaroon_person_1' => array('text', array(
					'label' => 'First Individual Signed Up for Code Maroon',
					'required' => true,
					'class' => 'half-width',
				)),
				'codemaroon_person_1_duties' => array('text', array(
					'label' => 'First Individual\'s Responsibilities During Program',
					'required' => true,
					'class' => 'half-width',
				)),

				'codemaroon_person_2' => array('text', array(
					'label' => 'Second Individual Signed Up for Code Maroon',
					'required' => true,
					'class' => 'half-width',
				)),
				'codemaroon_person_2_duties' => array('text', array(
					'label' => 'Second Individual\'s Responsibilities During Program',
					'required' => true,
					'class' => 'half-width',
				)),
			),
		),

		'insurance_grp' => array(
			'legend' => 'Insurance',
			'elements' => array(
				'insurance_req' => array('markup', array(
					'markup' => '
						<p>All TAMU programs must purchase TAMU insurance. Rates are listed <a href="http://studentactivities.tamu.edu/camps/medicalandinsurance" target="_blank">on this web site</a> and are subject to change annually beginning in March.</p>

						<p>TAMU Insurance Only: See attached link below for the letter to participants and claim information.</p>

						<ul>
							<li><a href="http://studentactivities.tamu.edu/camps/medicalandinsurance" target="_blank">Letter to Participants/Claim Information</a></li>
						</ul>
					',
				)),

				'insurance_thirdparty' => array('file', array(
					'label' => 'Third-Party Camps: Attach Insurance Certificate',
				)),
			),
		),

		'waiver_grp' => array(
			'legend' => 'Waiver',
			'elements' => array(
				'waiver_req' => array('markup', array(
					'markup' => '
						<h4>Instructions</h4>
						<p>Each blank space on the waiver must be completed with your program\'s information. The first two spaces should be filled in with your program name and sponsoring department. The third blank, under the Indemnity Clause, should be filled in with a brief list of potential risks that the program participants will be exposed to during program activities. Filling this blank with potential risks such as physical injury or death will not be sufficient; more specific risks need to be communicated in this space. It is also recommended that you think beyond just the physical risks. Consider including emotional risks that the participant may be exposed to or experience as well.</p>

						<p>IMPORTANT: Make sure the language that is bolded, underlined, and italicized remains that way in the releases distributed to your camp participants. Also, the font on the waiver should be at least 10-point font to be sufficient (even if the waiver is part of a brochure). Do not make changes to this form other than inserting the name of your camp/program where indicated.</p>

						<p>As before, this waiver must be signed by each camp participant and his or her parent/guardian and returned to camp staff before the camp begins.</p>

						<p>We will not accept other waivers - the waiver on our website must be used for ALL camps/programs as this waiver has been recently approved by the Office of General Counsel.</p>

						<p>If your program wishes to use a different waiver, verification that the Office of General Counsel has reviewed and approved the waiver must be sent to the Department of Student Activities. Other waivers can be used in addition to the waiver on our website if the camp sponsor/staff feels their waiver is more specific to the event.</p>

						<h4>Retention and Storage of Waivers</h4>
						<p>While the Camps & Programs for Minors Office does not require that you submit copies of your medical and liability waivers, it is recommended that you retain these records in a secure location for a minimum of three (3) years following your program\'s completion, as well as maintaining copies of the forms on-site during your program. If stored in hard copy form, records must be in a secure facility. If scanned, the hard copies must be shredded and the electronic copies must be password protected in accordance with University encryption policies.</p>

						<p>This waiver is for use by both Texas A&M University and Texas A&M University at Galveston.</p>

						<ul>
							<li><a href="https://studentactivities.tamu.edu/site_files/waiver.pdf" target="_blank">Waiver Document - English</a> (PDF)</li>
							<li><a href="https://studentactivities.tamu.edu/site_files/waiver_spanish.pdf" target="_blank">Waiver Document - Spanish</a> (PDF)</li>
						</ul>
					',
				)),

				'liability_waiver' => array('file', array(
					'label' => 'Liability Waiver',
					'description' => 'Upload a copy of the Liability Waiver here. If this document is in hard-copy form, please scan it into a digital format and attach it as a PDF.',
				)),
			),
		),

		'mfn_letter_grp' => array(
			'legend' => 'Medical Facility Notification Letter',
			'elements' => array(
				'medical_facility_notification' => array('file', array(
					'label' => 'Medical Facility Notification Letter',
					'description' => 'Upload the Medical Facility Notification Letter below. An example of this form is available for <a href="http://studentactivities.tamu.edu/files/MedicalLetter.doc" target="_blank">TAMU Camps</a> and <a href="http://studentactivities.tamu.edu/files/3rdPartyMedicalLetter.doc" target="_blank">Third Parties</a>. If the document is in hard-copy form, please scan it into a digital format and attach it as a PDF.',
				)),

				'mfnl_contacts' => array('markup', array(
					'label' => 'Important Contacts for Medical Facility Notification Letter',
					'markup' => '
						<blockquote>
							<b>College Station Medical Center</b><br>
							Marie Craig<br>
							1604 Rock Prairie<br>
							College Station, TX 77845<br>
							Phone: (979) 764-5162<br>
							Fax: (979) 764-5129
						</blockquote>
						<blockquote>
							<b>St. Joseph Regional Health Center</b><br>
							Dr. Bill Bass<br>
							2801 Franciscan Drive<br>
							Bryan, TX 77802<br>
							Phone: (979) 776-5363<br>
							Fax: (979) 776-5957
						</blockquote>
						<blockquote>
							<b>Galveston</b><br>
							Triage Nurse, University of Texas Medical Branch<br>
							901 Harborside Dr.<br>
							Galveston, TX 77555
						</blockquote>
					',
				)),

				'mfnl_initials' => array('text', array(
					'label' => 'Please initial in the box below to indicate that the Medical Facility Notification Letter was sent.',
					'required' => true,
				)),
			),
		),

		'special_considerations_grp' => array(
			'legend' => 'Special Considerations',
			'elements' => array(
				'firearms' => array('radio', array(
					'label' => 'Will any activity of this program involve participant use of, or access to, firearms, bows and arrows, or pressurized projectiles?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'required' => true,
				)),

				'chemicals' => array('radio', array(
					'label' => 'Do any of the activities for this program involve the use of chemicals or other ignitable/noxious gases?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'required' => true,
				)),

				'other_tools' => array('radio', array(
					'label' => 'Do any of the activities for this program involve the operation of hand power tools such as saws, Exacto knives, drills, scissors or scalpels?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'required' => true,
				)),

				'movies' => array('radio', array(
					'label' => 'Will movies be shown at any point during this activity?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'required' => true,
				)),

				'movie_rights' => array('radio', array(
					'label' => 'If Yes, has Swank Motion Pictures been contacted regarding the public viewing of this film?',
					'description' => 'Additional costs payable to Swank for licensing may apply. For more information, visit <a href="http://studentactivities.tamu.edu/orgmanual/eventplanning#film" target="_blank">this page</a>.',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
				)),
			),
		),

		'third_party_contract_grp' => array(
			'legend' => 'Third-Party Capms: Upload Contract',
			'elements' => array(
				'third_party_contract' => array('file', array(
					'label' => 'Upload Contract File',
					'description' => 'For more information, see <a href="http://studentactivities.tamu.edu/camps/thirdparty" target="_blank">this page</a>.',
				)),
			),
		),

		'submit_group' => array(
			'elements' => array(
				'btn_submit' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save and Continue',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);