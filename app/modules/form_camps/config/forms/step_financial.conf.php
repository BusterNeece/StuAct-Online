<?php

return array(
	'id' => 'camps_form',
	'method' => 'post',
	'groups' => array(
		
		'billing_accounts' => array(
			'legend' => 'Billing',
			'description' => 'Please choose accounts and/or payment options for the following.',

			'elements' => array(

				'insurance_method' => array('radio', array(
					'label' => 'Insurance Payment Method',
					'multiOptions' => array(
						'idt' => 'IDT',
						'check' => 'Mailed Check',
						'other' => 'Other',
						'atc' => 'On Campus Approval to Charge Form',
					),
					'required' => true,
				)),

				'ssf_account' => array('text', array(
					'label' => 'TAMU account number to be used for Support Service Fees',
					'class' => 'half-width',
				)),

				'insurance_method_details' => array('text', array(
					'label' => 'TAMU account number to be used for Insurance Fees',
					'class' => 'half-width',
				)),

				'invoice_sent_to' => array('textarea', array(
					'label' => 'Please designate who the invoice should be sent to, if different from the CPM sponsor listed in this form',
					'class' => 'full-width half-height',
				)),

				'sofc_approval_to_charge' => array('file', array(
					'label' => 'Student Organizations with SOFC Accounts, attach Approval to Charge',
					'description' => 'Approval to Charge Form can be downloaded on <a href="http://studentactivities.tamu.edu/site_files/OSF001_0.pdf" target="_blank">this page (PDF)</a>.',
				)),
			),
		),

		'budgeting' => array(
			'legend' => 'Budgeting',
			'elements' => array(
				'budget_initials' => array('text', array(
					'label' => 'Budgeting Requirements (Insurance, Background Check, Support Service Fee) Budgeted For',
					'description' => '<p>The CPM Sponsor understands that the CPM must budget for the following below items in addition to programmatic expenses:</p>
						<ul>
							<li>Insurance</li>
							<li>Background Check</li>
							<li>Support Service Fees</li>
						</ul>
						<p>Please place your initials in the box below.</p>',
					'required' => true,
				)),
			),
		),

		'submit_group' => array(
			'elements' => array(
				'btn_submit' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save and Continue',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);