<?php
return array(
    'method'		=> 'post',
    'elements'		=> array(

        'decision' => array('radio', array(
            'label' => 'Decision',
            'multiOptions' => array(
                1 => 'Approve and Continue',
                0 => 'Decline and Return to Submitter',
            ),
            'required' => true,
        )),

        'reason' => array('textarea', array(
            'label' => 'Reason for Decision (Optional)',
            'class' => 'full-width half-height',
        )),

        'initials' => array('text', array(
            'label' => 'Initials',
            'required' => true,
        )),

        'submit'		=> array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Submit Decision',
			'helper' => 'formButton',
			'class' => 'ui-button',
        )),
    ),
);