<?php

return array(
	'id' => 'camps_form',
	'method' => 'post',
	'groups' => array(

		'op_general' => array(
			'elements' => array(
				'age_range' => array('text', array(
					'label' => 'What is the age range of participants?',
					'class' => 'full-width',
					'required' => true,
				)),

				'ratio' => array('text', array(
					'label' => 'What is the ratio of supervision to participants?',
					'description' => 'Note: Reference Counselor to Participate Ratio information on <a href="http://studentactivities.tamu.edu/camps/backgroundchecks" target="_blank">this web site</a>.',
					'class' => 'full-width',
					'required' => true,
				)),

				'checkin_procedure' => array('textarea', array(
					'label' => 'Explain your check-in procedure, in regards to process, supervision, and parent/guardian verification',
					'class' => 'full-width half-height',
					'required' => true,
				)),

				'checkout_procedure' => array('textarea', array(
					'label' => 'Explain your check-out procedure, in regards to process, supervision, and parent/guardian verification',
					'class' => 'full-width half-height',
					'required' => true,
				)),

				'missing_participant' => array('textarea', array(
					'label' => 'What procedures have been established for managing the situation if a participant is absent and unaccounted for during the program?',
					'description' => 'e.g. what steps will camp/program sponsor take to relocate participants, who should be informed, at what point should security or police authorities be advised, when will parents be notified',
					'required' => true,
					'class' => 'full-width half-height',
				)),
			),
		),

		'housing_grp' => array(
			'legend' => 'Housing',
			'elements' => array(
				'overnight' => array('radio', array(
					'label' => 'Will participants be housed overnight? If any of your program\'s sessions are overnight camps, select this option.', 
					'class' => 'conditional-decider yesno',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'required' => true,
				)),

				'overnight_location' => array('text', array(
					'label' => 'Where will participants be housed?',
					'class' => 'full-width conditional overnight-yes',
				)),

				'overnight_venues' => array('textarea', array(
					'label' => 'Name of Venue(s)',
					'description' => 'If multiple venues, enter each one on a new line.',
					'class' => 'full-width full-height conditional overnight-yes',
				)),

				'overnight_curfew' => array('text', array(
					'label' => 'What time will you conduct curfew/bed check?',
					'class' => 'full-width conditional overnight-yes',
				)),

				'overnight_relocating' => array('textarea', array(
					'label' => 'What procedures have been established for managing the situation if a participant is absent and unaccounted for at the housing area?',
					'description' => 'e.g. what steps will camp/program sponsor take to relocate participants, who should be informed, at what point should security or police authorities be advised, when will parents be notified',
					'class' => 'full-width full-height conditional overnight-yes',
				)),

				'overnight_restricting' => array('text', array(
					'label' => 'How will the housing area be restricted to CPM participants and counselors?',
					'class' => 'full-width conditional overnight-yes',
				)),
			),
		),

		'itinerary_grp' => array(
			'legend' => 'Itinerary and Supporting Documents',
			'elements' => array(
				'itinerary' => array('file', array(
					'label' => 'Upload Itinerary File(s) and/or Supporting Document(s)',
					'multiFile' => 10,
				)),

				'freetime' => array('radio', array(
					'label' => 'At any time during the camp or program, will there be scheduled free time?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'required' => true,
					'class' => 'conditional-decider yesno',
				)),

				'freetime_detail' => array('textarea', array(
					'label' => 'Please describe free time options and supervision',
					'class' => 'full-width half-height conditional freetime-yes',
				)),
			),
		),

		'transportation_grp' => array(
			'legend' => 'Transportation',
			'elements' => array(

				'transportation' => array('radio', array(
					'label' => 'Will Texas A&M University or any of its affiliates play any role in the transport of camp or program participants?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'required' => true,
					'class' => 'conditional-decider yesno',
				)),

				'transportation_seatbelt' => array('markup', array(
					'label' => 'Texas Seatbelt Law',
					'markup' => 'See <a href="http://www.statutes.legis.state.tx.us/docs/tn/htm/tn.545.htm" target="_blank">Texas Seatbelt Laws, Chapter 545 Transportation Code</a>',
					'class' => 'conditional transportation-yes',
				)),

				'transportation_involvement' => array('textarea', array(
					'label' => 'How is the CPM involved in transporting participants?',
					'markup' => 'Locations, driver\'s role in CPM, etc)',
					'class' => 'full-width half-height conditional transportation-yes',
				)),

				'transportation_mode' => array('multiCheckbox', array(
					'label' => 'What are your modes of transportation?',
					'multiOptions' => array(
						'bus' => 'Charter Bus',
						'boat' => 'Boat',
						'priv' => 'Private/Commercial Vehicle',
						'cart' => 'Golf Cart',
						'van' => '11-15 Passenger Van',
					),
					'class' => 'conditional transportation-yes conditional-decider checkbox',
				)),

				'transportation_van_sap' => array('radio', array(
					'label' => 'Has the program sponsor reviewed the University Standard Administrative Procedure 24.01.01.M0.01: Van Safety Procedures prior to transporting the participants?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'class' => 'conditional transportation_mode-van'
				)),

				'transportation_van_reserved' => array('radio', array(
					'label' => 'Has the program sponsor reserved an adequate number of passenger vans for the camp or program?',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'class' => 'conditional transportation_mode-van'
				)),

				'transportation_van_reserved' => array('text', array(
					'label' => 'When did/will drivers complete the required training?',
					'class' => 'full-width conditional transportation_mode-van'
				)),

				'transportation_verification' => array('radio', array(
					'label' => 'Has or will the program sponsor confirm(ed) that the appropriate certifications/insurance is certified according to the mode of transportation and that the certificates or licenses are up to date? This also includes use of golf carts.',
					'multiOptions' => array(0 => 'No', 1 => 'Yes'),
					'class' => 'conditional transportation-yes',
				)),

			),
		),

		'submit_group' => array(
			'elements' => array(
				'btn_submit' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save and Continue',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);