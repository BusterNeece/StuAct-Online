<?php
$settings = \Entity\CampsForm::loadSettings();

return array(
    'method' => 'post',
    'groups' => array(
        
        'program_profile' => array(
            'legend' => 'Program Information',
            'elements' => array(

                'name' => array('text', array(
                    'label' => 'Name of CPM',
                    'class' => 'full-width',
                    'required' => true,
                )),

                'description' => array('textarea', array(
                    'label' => 'Brief Description of CPM Purpose and Activities',
                    'class' => 'full-width half-height',
                )),

                'web' => array('text', array(
                    'label' => 'CPM Website',
                    'class' => 'full-width',
                )),

                'sponsor' => array('radio', array(
                    'label' => 'Sponsor Affiliation',
                    'required' => true,
                    'multiOptions' => array(
                        'Athletics Department'  => 'Athletics Department Program',
                        'Sport Club'            => 'Sport Club Program',
                        'University'            => 'Other University Department (Please specify department below)',
                        'Student Organization'  => 'Student Organization (Please specify organization below)',
                        'Third Party'           => 'Third Party (Please specify department or organization below)',
                    ),
                )),

                'org_id' => array('select', array(
                    'label' => 'If Student Organization, Select',
                    'multiOptions' => \Entity\Organization::fetchSelect('N/A (No Organization)'),
                )),

                'sponsoring_dept' => array('select', array(
                    'label' => 'If Sponsoring Department, Select',
                    'multiOptions' => array('' => 'N/A (No Department)') + $settings['department_options'],
                )),
                
            ),
        ),

        'cpm_sponsor' => array(
            'legend' => 'CPM Sponsor/TAMU Sponsor',
            'description' => 'A CPM Sponsor is the individual representing  the department, college, or 
student organization charged with the direction or operation of the CPM. In the case of Third 
Party CPM, this person serves as the liaison between TAMU and  the Third Party CPM. The
CPM Sponsor must  be a Texas A&M University full-time employee and is responsible for 
completing the CPM application.',

            'elements' => array(
                'cpm_sponsor_name' => array('text', array(
                    'label' => 'Name',
                    'class' => 'full-width',
                )),
                'cpm_sponsor_email' => array('text', array(
                    'label' => 'E-mail Address',
                    'class' => 'half-width',
                    'validators' => array('EmailAddress'),
                )),
                'cpm_sponsor_phone' => array('text', array(
                    'label' => 'Office Phone Number',
                )),
                'cpm_sponsor_cell' => array('text', array(
                    'label' => 'Mobile Phone Number',
                )),
            ),
        ),

        'cpm_contact' => array(
            'legend' => '(Third Parties Only) CPM Contact',
            'elements' => array(
                'cpm_contact_name' => array('text', array(
                    'label' => 'Name',
                    'class' => 'full-width',
                )),
                'cpm_contact_email' => array('text', array(
                    'label' => 'E-mail Address',
                    'class' => 'half-width',
                    'validators' => array('EmailAddress'),
                )),
                'cpm_contact_phone' => array('text', array(
                    'label' => 'Office Phone Number',
                )),
                'cpm_contact_cell' => array('text', array(
                    'label' => 'Mobile Phone Number',
                )),
            ),
        ),
        
        'submit' => array(
            'elements' => array(
                'submit'        => array('submit', array(
                    'type'  => 'submit',
                    'label' => 'Save Profile',
                    'helper' => 'formButton',
                    'class' => 'ui-button',
                )),
            ),
        ),
        
    ),
);