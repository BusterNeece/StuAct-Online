<?php

return array(
	'id' => 'camps_form',
	'method' => 'post',
	'elements' => array(

		'start_date' => array('unixdate', array(
			'label' => 'Session Start Date',
			'blank' => true,
			'required' => true,
		)),

		'end_date' => array('unixdate', array(
			'label' => 'Session End Date',
			'blank' => true,
			'required' => true,
		)),

		'type' => array('radio', array(
			'label' => 'Session Type',
			'multiOptions' => array('D' => 'Daytime-only Session', 'O' => 'Overnight Session'),
			'required' => true,
		)),

		'num_participants' => array('text', array(
			'label' => 'Actual Number of Participants',
			'class' => 'input-small',
			'maxlength' => 8,
			'required' => true,
		)),

		'num_counselors' => array('text', array(
			'label' => 'Number of Counselors Requiring Accident/Medical Insurance Coverage',
			'description' => 'These are the counselors who are not employed by Texas A&M University for the purpose of this event.<br>Counselors who are employed by Texas A&M University for this event are already covered by existing policies.',
			'class' => 'input-small',
			'maxlength' => 8,
			'required' => true,
		)),

		'roster' => array('file', array(
			'label' => 'Upload Session Roster',
			'description' => 'This roster should include all participants and counselors involved with this session.',
		)),

		'btn_submit' => array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Add Session',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),
	),
);