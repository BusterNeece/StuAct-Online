<?php
return array(
    'default' => array(

        'form_camps' => array(
            'label' => 'Camps and Programs for Minors Form',
            'module' => 'form_camps',

            'pages' => array(
                'form_camps_form' => array(
                    'module' => 'form_camps',
                    'controller' => 'form',
                    'action' => 'form',
                ),
            ),
        ),

        'form_camps_manage' => array(
            'label' => 'Manage Camps Forms',
            'module' => 'form_camps',
            'controller' => 'manage',

            'pages' => array(
                'form_camps_phase' => array(
                    'module' => 'form_camps',
                    'controller' => 'manage',
                    'action' => 'index',
                ),
                'form_camps_search' => array(
                    'module' => 'form_camps',
                    'controller' => 'manage',
                    'action' => 'search',
                ),
                'form_camps_view' => array(
                    'module' => 'form_camps',
                    'controller' => 'manage',
                    'action' => 'view',
                ),
                'form_camps_users' => array(
                    'module' => 'form_camps',
                    'controller' => 'manage',
                    'action' => 'users',
                ),
                'form_camps_programs' => array(
                    'module' => 'form_camps',
                    'controller' => 'manage',
                    'action' => 'programs',
                ),
                'form_camps_routing' => array(
                    'module' => 'form_camps',
                    'controller' => 'manage',
                    'action' => 'routing',
                ),
                'form_camps_rates' => array(
                    'module' => 'form_camps',
                    'controller' => 'manage',
                    'action' => 'rates',
                ),
                'form_camps_matrix' => array(
                    'module' => 'form_camps',
                    'controller' => 'manage',
                    'action' => 'matrix',
                ),
            ),
        ),

    ),
);