<?php
use \Entity\CampsForm;
use \Entity\CampsFormIncident;
use \Entity\CampsFormLog;
use \Entity\CampsFormProgram;
use \Entity\CampsFormRouting;
use \Entity\CampsFormUser;
use \Entity\Settings;
use \Entity\User;

// Force mail delivery even on development.
define('DF_FORCE_MAIL', TRUE);

class Form_Camps_IndexController extends \DF\Controller\Action
{
	protected $camps_auth;
	protected $camps_user;
	protected $camps_settings;

	public function permissions()
	{
		return true;
	}

	public function preDispatch()
	{
		parent::preDispatch();

		$this->camps_settings = CampsForm::loadSettings();
		$this->camps_auth = new \StuAct\Auth\Camps;
		$this->camps_user = $this->camps_auth->getLoggedInUser();
		$this->view->camps_user = $this->camps_user;

		$login_actions = array('login', 'register', 'profile');

		if (!in_array($this->_getActionName(), $login_actions))
		{
			if (!$this->camps_user)
			{
				$this->redirectFromHere(array('action' => 'login'));
				return;
			}

			if ($this->camps_user->password_needs_reset)
			{
				$this->alert('<b>Your password has expired and must be reset.</b>', 'yellow');
				$this->redirectFromHere(array('action' => 'profile'));
			}
		}
	}

	public function indexAction()
	{
		$user_programs = $this->_getUserPrograms();
		$this->view->programs = $user_programs;

		$program = $this->_getProgram(FALSE);
		if ($program)
		{
			$this->view->program_id = $program->id;
			$this->view->program = $program;
		}
		else if (count($user_programs) == 1)
		{
			$program_id = key($user_programs);
			$this->redirectFromHere(array('program_id' => $program_id));
			return;
		}
	}

	public function newAction()
	{
		$program = $this->_getProgram(TRUE);

		if (!$program->isUpToDate())
		{
			$this->alert('<b>Please set up the routing information for your Camps Program before submitting a new form.</b><br>Please review your profile below, complete any required fields, and save your changes before continuing.', 'red');
			$this->redirectFromHere(array('action' => 'programprofile'));
			return;
		}

		$form = new CampsForm;
		$form->user = $this->camps_user;
		$form->program = $program;
		$form->data = array(
			'Logistics' => array(
				'name' => $program->name,
				'cpm_sponsor_name' => $program->cpm_sponsor_name,
                'cpm_sponsor_email' => $program->cpm_sponsor_email,
                'cpm_sponsor_phone' => $program->cpm_sponsor_phone,
                'cpm_sponsor_cell' => $program->cpm_sponsor_cell,
			),
		);
		$form->save();

		$this->redirectFromHere(array('program_id' => NULL, 'controller' => 'form', 'action' => 'index', 'code' => $form->access_code));
		return;
	}

	public function programprofileAction()
	{
		$program = $this->_getProgram(TRUE);

		$form = new \DF\Form($this->current_module_config->forms->program);
		$form->setDefaults($program->toArray(TRUE, TRUE));

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			$program->fromArray($data);
			$program->save();

			$this->alert('<b>Program profile updated.</b>', 'green');
			$this->redirectFromHere(array('action' => 'index'));
			return;
		}

		$this->view->headTitle('Edit Program Profile');
		$this->renderForm($form);
	}

	protected function _getUserPrograms()
	{
		$user_programs = array();
		foreach($this->camps_user->programs as $program)
		{
			if ($program->name)
				$user_programs[$program->id] = $program;
		}
		return $user_programs;
	}

	protected function _getProgram($required = false)
	{
		$user_programs = $this->_getUserPrograms();

		$program_id = (int)$this->_getParam('program_id', 0);
		if ($program_id)
		{
			if (!isset($user_programs[$program_id]))
				throw new \DF\Exception\PermissionDenied;

			return $user_programs[$program_id];
		}
		else
		{
			if ($required)
				throw new \DF\Exception\DisplayOnly('No program specified!');
			else
				return NULL;
		}
	}

	/**
	 * Authentication Actions
	 */

	public function registerAction()
	{
		$form = new \DF\Form($this->current_module_config->forms->register);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			$existing_user = CampsFormUser::getRepository()->findOneBy(array('email' => $data['email']));
			if ($existing_user instanceof CampsFormUser)
			{
				$this->alert('<b>An account already exists with the e-mail address you specified.</b><br>Enter your password below to log in to this account.', 'green');
				$this->redirectFromHere(array('action' => 'login'));
				return;
			}
			else
			{
				$record = new CampsFormUser;
				$record->fromArray($data);
				$record->save();

				$this->camps_auth->setUser($record);

				$this->alert('<b>New account created!</b><br>You have been logged in with your new account.' ,'green');
				$this->redirectFromHere(array('action' => 'index'));
				return;
			}
		}

		$this->view->headTitle('Create New Camps Account');
		$this->renderForm($form);
	}

	public function loginAction()
	{
		if ($this->camps_auth->isLoggedIn())
		{
			$this->redirectFromHere(array('action' => 'index'));
			return;
		}

		if ($this->auth->isLoggedIn())
		{
			$user = $this->auth->getLoggedInUser();
			$camps_user = CampsFormUser::getRepository()->findOneBy(array('uin' => $user->uin));

			if (!($camps_user instanceof CampsFormUser))
			{
				$camps_user = new CampsFormUser;
				$camps_user->uin = $user->uin;
			}

			$camps_user->firstname = $user->firstname;
			$camps_user->lastname = $user->lastname;
			$camps_user->email = $user->email;
			$camps_user->type = 'netid';
			$camps_user->save();

			$this->camps_auth->setUser($camps_user);

			$this->redirectFromHere(array('action' => 'index'));
			return;
		}

		$form = new \DF\Form($this->current_module_config->forms->login);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			$camps_user = $this->camps_auth->authenticate($data);

			if ($camps_user)
			{
				$this->redirectFromHere(array('action' => 'index'));
				return;
			}
		}

		$this->view->form = $form;
	}

	public function logoutAction()
	{
		$this->camps_auth->logout();
		$this->redirectHome();
	}

	public function profileAction()
	{
		$form = new \DF\Form($this->current_module_config->forms->profile);
		$form->setDefaults($this->camps_user->toArray(TRUE, TRUE));

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			$this->camps_user->fromArray($data);
			$this->camps_user->save();

			$this->alert('<b>Profile updated!</b>', 'green');
			$this->redirectFromHere(array('action' => 'index'));
			return;
		}

		$this->view->headTitle('Camps Account Profile');
		$this->renderForm($form);
	}
}