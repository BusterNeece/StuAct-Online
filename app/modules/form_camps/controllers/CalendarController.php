<?php
use \Entity\Camp;

class Form_Camps_CalendarController extends \DF\Controller\Action
{
    public function permissions()
    {
        return true;
    }
    
    /**
     * Main display.
     */
    public function indexAction()
    {
        $calendar = new \DF\Calendar($this->_getParam('month'));
		
		// Fetch a list of events.
		$timestamps = $calendar->getTimestamps();

        $records = $this->em->createQuery('SELECT c FROM Entity\Camp c WHERE c.date_start <= :end AND c.date_end >= :start ORDER BY c.name ASC')
            ->setParameter('start', $timestamps['start'])
            ->setParameter('end', $timestamps['end'])
            ->getArrayResult();

		foreach($records as &$record)
		{
            $record['color'] = '#'.strtoupper(substr(md5(serialize($record)), 0, 6));

			$record['start_timestamp'] = $record['date_start'];
			$record['end_timestamp'] = $record['date_end'];
		}
		
		$this->view->records = $records;
		$this->view->calendar = $calendar->fetch($records);
    }

    public function detailAction()
    {
        $id = (int)$this->_getParam('id');
        $camp = Camp::find($id);

        $this->view->camp = $camp;
    }


}