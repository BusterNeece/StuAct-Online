<?php
use \Entity\CampsForm;
use \Entity\CampsFormIncident;
use \Entity\CampsFormLog;
use \Entity\CampsFormProgram;
use \Entity\CampsFormRouting;
use \Entity\CampsFormUser;
use \Entity\CampsFormSession;
use \Entity\Settings;
use \Entity\User;

// Force mail delivery even on development.
define('DF_FORCE_EMAIL', TRUE);

class Form_Camps_FormController extends \DF\Controller\Action
{
	public function permissions()
	{
		return true;
	}

	protected $_code;
	protected $_record;
	protected $_settings;

	public function preDispatch()
	{
		parent::preDispatch();

		if ($_GET && $this->_getParam('origin') == 'legacy')
			$this->redirectFromHere($_GET);

		$code = $this->_getParam('code');
		$form = CampsForm::getRepository()->findOneBy(array('access_code' => $code));

		if (!($form instanceof CampsForm))
			throw new \DF\Exception\DisplayOnly('A form with the ID you specified could not be found!');
		
		$this->_record = $form;
		$this->_code = $form->access_code;
		$this->_settings = CampsForm::loadSettings();

		$this->view->assign(array(
			'record'	=> $form,
			'code'		=> $form->access_code,
			'form_settings' => $this->_settings,
		));
	}

	public function indexAction()
	{
		switch($this->_record->phase)
		{
			case CampsForm::PHASE_INCOMPLETE:
			case CampsForm::PHASE_NEEDS_MODIFICATION:
			case CampsForm::PHASE_SECONDARY_REVIEW:
				$this->redirectFromHere(array('action' => 'begin'));
			break;

			case CampsForm::PHASE_DEPT_REVIEW:
			case CampsForm::PHASE_DEAN_REVIEW:
				$this->redirectFromHere(array('action' => 'review'));
			break;

			case CampsForm::PHASE_APPROVED_NEEDS_ROSTER:
				$this->redirectFromHere(array('action' => 'roster'));
			break;

			default:
				$this->redirectFromHere(array('action' => 'view'));
			break;
		}
	}

	/**
	 * Form Completion Steps
	 */

	public function beginAction()
	{
		$first_step = $this->_settings['steps'][0];
		$this->redirectFromHere(array('action' => 'form', 'step' => $first_step));
		return;
	}

	public function formAction()
	{
		// Verify form status.
		if (!$this->_record->isAllowed('edit'))
		{
			$this->redirectFromHere(array('action' => 'view'));
			return;
		}

		// Verify step.
		$step = $this->_getParam('step');
		$this->view->step = $step;

		$steps = $this->_record->getSteps();
		$this->view->steps = $steps;

		$step_offset = array_search($step, $steps);

		if ($step_offset === FALSE)
			throw new \DF\Exception\DisplayOnly('The step you specified could not be found.');
		
		$next_step = (isset($steps[$step_offset+1])) ? $steps[$step_offset+1] : 'final';
		
		// Load form.
		$form_name = strtolower(preg_replace('/([^a-zA-Z0-9]+)/', '', $step));
		$form_config = $this->current_module_config->forms->{'step_'.$form_name}->toArray();

		$form = new \StuAct\Form($form_config);

		$goto_step = $this->_getParam('goto_step', 'next');
		if ($goto_step != "next")
		{
			foreach($form->getElements() as $element_name => $element)
			{
				$element->setRequired(FALSE);
			}
		}
		else
		{
			$goto_step = $next_step;
		}

		$all_data = $this->_record->data;
		$form->setDefaults((array)$all_data[$step]);

		// Handle incoming submissions.
		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();
			$uploaded_files = $form->processFiles('camps', $this->_code);
			$data = array_merge($data, (array)$uploaded_files);

			// Force version reset when saving new items.
			$this->_record->version = CampsForm::VERSION_DFFORM;

			// Set data in database and trigger index reload.
			$this->_record->replaceData($step, $data);
			$this->_record->reloadIndex();
			$this->_record->save();

			if ($goto_step == 'final')
				$this->redirectFromHere(array('action' => 'confirm', 'step' => NULL));
			else
				$this->redirectFromHere(array('step' => $goto_step));
			return;
		}

		// Render view.
		$this->view->form = $form;
	}

	public function confirmAction()
	{
		if ($this->_hasParam('decision'))
		{
			$decision = $this->_getParam('decision');

			$edit_phases = array(CampsForm::PHASE_INCOMPLETE, CampsForm::PHASE_NEEDS_MODIFICATION);

			if ($this->_record->phase == CampsForm::PHASE_SECONDARY_REVIEW)
			{
				if ($decision == "approve")
				{
					$this->_record->phase = CampsForm::PHASE_DEPT_REVIEW;
					$this->_record->save();

					$this->alert('<b>Form successfully sent forward for review.</b>', 'green');
				}
				else
				{
					$this->_record->phase = CampsForm::PHASE_NEEDS_MODIFICATION;
					$this->_record->save();

					$this->alert('<b>Form successfully sent to submitter for editing.</b>', 'green');
				}
			}
			else if (in_array($this->_record->phase, $edit_phases))
			{
				$phase = ($this->_record->hasSecondaryReviewer()) ? CampsForm::PHASE_SECONDARY_REVIEW : CampsForm::PHASE_DEPT_REVIEW;

				$this->_record->phase = $phase;
				$this->_record->save();

				$this->alert('<b>Form successfully sent forward for review.</b>', 'green');
			}

			$this->redirectToRoute(array('module' => 'form_camps'));
			return;
		}
		else
		{
			$errors = $this->_record->hasErrors();
			if ($errors)
			{
				$this->view->errors = $errors;
				$this->render('confirm_errors');
				return;
			}
		}
	}

	public function deleteAction()
	{
		$this->_record->phase = CampsForm::PHASE_ARCHIVED;
		$this->_record->save();

		$this->alert('<b>Form successfully marked as deleted.</b>', 'green');
		$this->redirectFromHere(array('controller' => 'index', 'action' => 'index', 'code' => NULL));
		return;
	}
	
	public function viewAction()
	{}

	public function printAction()
	{}

	public function reviewAction()
	{
		$current_phase = $this->_record->phase;
		$form_settings = CampsForm::loadSettings();
			
		// Validate the access code submitted.
		$access_code = $this->_record->getPhaseCode();
		$form_access = trim($this->_getParam('access'));
		
		if (!$this->acl->isAllowed('admin_forms_camps') && strcmp($form_access, $access_code) != 0)
		{
			$phase_codes = array();
			foreach($form_settings['phases'] as $phase_num => $phase_name)
			{
				$phase_codes[] = $this->_record->getPhaseCode($phase_num);
			}
		
			if (in_array($form_access, $phase_codes))
				throw new \DF\Exception\DisplayOnly('Form Already Reviewed: You have already reviewed this form and it has been sent to the next phase of approval. No additional action is necessary.');
			else
				throw new \DF\Exception\DisplayOnly('Invalid Access Code Specified: You did not specify a valid access code for this form in its current phase of review. You might have clicked an old link. If you believe this was in error, contact the Information Technology team.');
			return;
		}

		$form = new \DF\Form($this->current_module_config->forms->review);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();
			
			// Store relevant data about the decision in the form's log.
			$admin_data = array(
				'phase_'.$current_phase.'_decision'		=> $data['decision'],
                'phase_'.$current_phase.'_reason'		=> $data['reason'],
				'phase_'.$current_phase.'_initials'		=> $data['initials'],
				'phase_'.$current_phase.'_timestamp'	=> time(),
			);
			$this->_record->setAdminData($admin_data, TRUE);

			if ($data['decision'] == 1)
			{
				$phase_transition = array(
					CampsForm::PHASE_SECONDARY_REVIEW => CampsForm::PHASE_DEPT_REVIEW,
					CampsForm::PHASE_DEPT_REVIEW => CampsForm::PHASE_RISK_INITIAL,
					CampsForm::PHASE_RISK_INITIAL => CampsForm::PHASE_DEAN_REVIEW,
					CampsForm::PHASE_DEAN_REVIEW => CampsForm::PHASE_APPROVED_NEEDS_ROSTER,
				);
			}
			else
			{
				$phase_transition = array(
					CampsForm::PHASE_SECONDARY_REVIEW => CampsForm::PHASE_NEEDS_MODIFICATION,
					CampsForm::PHASE_RISK_INITIAL => CampsForm::PHASE_DEAN_REVIEW,
					CampsForm::PHASE_DEPT_REVIEW => CampsForm::PHASE_NEEDS_MODIFICATION,
					CampsForm::PHASE_DEAN_REVIEW => CampsForm::PHASE_DECLINED,
				);
			}

			$new_phase = $phase_transition[$current_phase];
			$this->_record->setPhase($new_phase, $data['reason']);
			$this->_record->save();

			$this->alert('<b>Form #'.$this->_record->access_code.' successfully reviewed.</b><br>No additional action is required regarding this form.', 'green');
			$this->redirectToRoute(array('module' => 'form_camps'));
			return;
		}

		$this->view->form = $form;
	}

	/**
	 * Roster Steps
	 */

	public function rosterAction()
	{}

	public function rosterdoneAction()
	{
		$this->_record->phase = CampsForm::PHASE_APPROVED_UNPAID;
		$this->_record->save();

		$this->alert('<b>Camp roster(s) have been submitted and the form has been saved.</b>', 'green');
		$this->redirectToRoute(array('module' => 'form_camps'));
		return;
	}

	public function editsessionAction()
	{
		$form = new \DF\Form($this->current_module_config->forms->session);

		$session_id = (int)$this->_getParam('session_id');
		if ($session_id)
		{
			$record = CampsFormSession::find($session_id);

			if ($record->form_id == $this->_record->id)
				$form->setDefaults($record->toArray());
			else
				$record = NULL;
		}

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();
			$uploaded_files = $form->processFiles('camps', $this->_code);
			$data = array_merge($data, (array)$uploaded_files);

			if (!$record)
			{
				$record = new CampsFormSession;
				$record->form = $this->_record;
			}

			$record->fromArray($data);
			$record->save();

			$this->alert('<b>Session updated.</b>', 'green');
			$this->redirectFromHere(array('action' => 'roster', 'session_id' => NULL));
			return;
		}

		$this->view->headTitle('Edit Camp Session');
		$this->renderForm($form);
	}

	public function deletesessionAction()
	{
		$session_id = (int)$this->_getParam('session_id');
		$record = CampsFormSession::find($session_id);

		if ($record->form_id == $this->_record->id)
			$record->delete();

		$this->alert('<b>Session deleted.</b>', 'green');
		$this->redirectFromHere(array('action' => 'roster', 'session_id' => NULL));
		return;
	}

	/**
	 * Incident Reporting
	 */

	public function incidentAction()
	{
		$form = new \DF\Form($this->current_module_config->forms->incident);
		$form->setDefaults(array(
			'app_id'			=> $this->_record->access_code,
			'program_name'		=> $this->_record->program->name,
		));

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			$record = new CampsFormIncident;
			$record->form = $this->_record;
			$record->data = $data;
			$record->save();

			$form_settings = CampsForm::loadSettings();

			\DF\Messenger::send(array(
				'to'		=> array_merge(array($data['email']), $form_settings['notify_incidents']),
				'subject'	=> 'Camps Form #'.$this->_record->access_code.' Incident Report',
				'module'	=> $this->_getModuleName(),
				'template'	=> 'incident_report',
				'vars'		=> array(
					'record'	=> $this->_record,
					'form'		=> $form->populate($data),
				),
			));

			$this->alert('<b>Incident report submitted successfully.</b>', 'green');
			$this->redirectToRoute(array('module' => 'form_camps'));
			return;
		}

		$this->view->headTitle('Submit Incident Report');
		$this->renderForm($form);
	}
}