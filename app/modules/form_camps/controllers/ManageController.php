<?php
use \Entity\Camp;
use \Entity\CampsForm;
use \Entity\CampsFormIncident;
use \Entity\CampsFormLog;
use \Entity\CampsFormProgram;
use \Entity\CampsFormRouting;
use \Entity\CampsFormUser;
use \Entity\Settings;

define('DF_FORCE_EMAIL', TRUE);

class Form_Camps_ManageController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed('admin_forms_camps');
    }

    protected $_settings;
    public function preDispatch()
    {
    	parent::preDispatch();
    	$this->_settings = CampsForm::loadSettings();
    	$this->view->phases = $this->_settings['phases'];
    }

    public function indexAction()
    {
    	$this->view->tab = "phase";

    	$phase = $this->_getParam('phase', CampsForm::PHASE_COMPLETE);
		$this->view->phase = $phase;
		$this->view->phase_name = $this->_settings['phases'][$phase];

    	$query = $this->em->createQuery('SELECT cf FROM Entity\CampsForm cf WHERE cf.phase = :phase ORDER BY cf.timestamp DESC')->setParameter('phase', $phase);
    	$this->view->pager = new \DF\Paginator\Doctrine($query, $this->_getParam('page', 1));
    }

    public function searchAction()
    {
    	if ($_GET)
    		$this->redirectFromHere($_GET);

    	if ($this->_hasParam('q'))
    	{
    		$this->view->q = $q = $this->_getParam('q');
    		$query = $this->em->createQuery('SELECT cf FROM Entity\CampsForm cf WHERE cf.index_name LIKE :q_like OR cf.id = :q_exact OR cf.access_code LIKE :q_like ORDER BY cf.timestamp DESC')
    			->setParameter('q_like', '%'.$q.'%')
    			->setParameter('q_exact', $q);
    	}
    	else if ($this->_hasParam('program_id'))
    	{
    		$pid = (int)$this->_getParam('program_id');
    		$query = $this->em->createQuery('SELECT cf FROM Entity\CampsForm cf WHERE cf.program_id = :pid ORDER BY cf.timestamp DESC')->setParameter('pid', $pid);
    	}

    	if ($query)
    		$this->view->pager = new \DF\Paginator\Doctrine($query, $this->_getParam('page', 1));
    }

    public function myformsAction()
    {
    	$user = $this->auth->getLoggedInUser();

    	$query = $this->em->createQuery('SELECT cf FROM Entity\CampsForm cf WHERE cf.claimant_user_id = :user_id ORDER BY cf.timestamp DESC')
    		->setParameter('user_id', $user->id);
    	$this->view->pager = new \DF\Paginator\Doctrine($query, $this->_getParam('page', 1));
    }

    /**
	 * Single Record Functions
	 */

	protected function _getForm()
	{
		$id = (int)$this->_getParam('id');
		$record = CampsForm::find($id);

		if (!($record instanceof CampsForm))
			throw new \DF\Exception\DisplayOnly('Camps form not found!');

		$this->view->record = $record;
		return $record;
	}

	public function viewAction()
	{
		$record = $this->_getForm();

		$this->view->user_select = CampsFormUser::fetchSelect();

		$data = $record->getData();
		$admin_data = $record->getAdminData();
		$this->view->notes = array(
			'public' => $data['Miscellaneous']['Notes'], 
			'private' => $admin_data['private_notes']
		);
	}

	public function addfileAction()
	{
		$record = $this->_getForm();
		$form = new \DF\Form($this->current_module_config->admin_forms->supplemental_file);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();
			$file_data = $form->processFiles('camps', $record->access_code);

			$files = (array)$record->data['Supplemental_Files']['File_Uploaded_Files'];

			for($i = 0; $i <= 50; $i++)
			{
				if (!isset($files[$i]))
				{
					$files[$i] = array(
						'url'		=> $file_data['File_Supplemental'][1],
						'description' => $data['description'],
					);
					break;
				}
			}

			$record->appendData('Supplemental_Files', array('File_Uploaded_Files' => $files));
			$record->save();

			$this->alert('<b>New supplemental file uploaded.</b>', 'green');
			$this->redirectFromHere(array('action' => 'view'));
			return;
		}

		$this->view->headTitle('Add Supplemental File');
		$this->renderForm($form);
	}

	public function setnotesAction()
	{
		$this->doNotRender();

		$record = $this->_getForm();
		$record->appendData('Miscellaneous', array('Notes' => $_POST['public_notes']));
		$record->setAdminData(array('private_notes' => $_POST['private_notes']));
		$record->save();

		if ($this->isAjax())
		{
			echo \Zend_Json::encode(array(
				'status' 	=> 'success',
				'text'		=> 'Saved: '.date('g:ia'),
			));
		}
		else
		{
			$this->alert('<b>Notes saved.</b>', 'green');
			$this->redirectFromHere(array('action' => 'view'));
		}
		return;
	}

	public function setprogramAction()
	{
		$form = new \DF\Form($this->current_module_config->admin_forms->setprogram);

		$record = $this->_getForm();
		$form->setDefaults($record->toArray());

		if ($_POST && $form->isValid($_POST))
		{
			$record->fromArray($form->getValues());
			$record->save();

			$this->alert('<b>Form program updated.</b>', 'green');
			$this->redirectFromHere(array('action' => 'view'));
			return;
		}

		$this->view->headTitle('Set Form Program');
		$this->renderForm($form);
	}

	public function notifyAction()
	{
		$record = $this->_getForm();
		$record->notify(NULL);

		$this->alert('<b>Form notification sent.</b>', 'green');
		$this->redirectFromHere(array('action' => 'view'));
		return;
	}

	public function setphaseAction()
	{
		$record = $this->_getForm();
		$form = new \DF\Form($this->current_module_config->admin_forms->phase);

		$form->setDefaults(array('phase' => $record->phase));

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			$record->setPhase($data['phase']);
			$record->save();

			$this->alert('<b>Form routing phase changed.</b>', 'green');
			$this->redirectFromHere(array('action' => 'view'));
			return;
		}

		$this->view->headTitle('Set Form Phase');
		$this->renderForm($form);
	}

	public function assignAction()
	{
		$record = $this->_getForm();

		$form_config = $this->current_module_config->admin_forms->assign->toArray();
		$users_with_role = $this->em->createQuery('SELECT u FROM Entity\User u LEFT JOIN u.roles r LEFT JOIN r.actions a WHERE a.name = :action ORDER BY u.lastname ASC')
			->setParameter('action', 'admin_forms_camps')
			->getArrayResult();

		$user_select = array(
			0 => 'Unassigned/No User',
		);
		foreach($users_with_role as $user)
		{
			$user_select[$user['id']] = $user['lastname'].', '.$user['firstname'];
		}

		$form_config['elements']['claimant_user_id'][1]['multiOptions'] = $user_select;
		$form = new \DF\Form($form_config);

		$form->setDefaults(array('claimant_user_id' => $record->claimant_user_id));

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			if ($data['claimant_user_id'] == 0)
			{
				$record->claimant = NULL;
			}
			else
			{
				$user = User::find($data['claimant_user_id']);
				$record->claimant = $user;

				\DF\Messenger::send(array(
					'to'		=> $user->email,
					'subject'	=> 'Camps Form Assigned to You',
					'template'	=> 'assigned',
					'module'	=> $this->_getModuleName(),
					'vars'		=> array(
						'record' => $record,
					),
				));
			}

			$record->save();

			$this->alert('<b>Form assigned to user.</b>', 'green');
			$this->redirectFromHere(array('action' => 'view'));
			return;
		}

		$this->view->headTitle('Assign Form to User');
		$this->renderForm($form);
	}

	public function deleteAction()
	{
		$record = $this->_getForm();
		$record->setPhase(CampsForm::PHASE_ARCHIVED);
		$record->save();

		$this->alert('<b>Form '.$id.' deleted.</b>', 'green');
		$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
		return;
	}

	public function invoiceAction()
	{
		$record = $this->_getForm();
		$form = new \DF\Form($this->current_module_config->admin_forms->invoice);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

        	$show_general = (in_array('general', $data['show']));
			$show_accident = (in_array('accident', $data['show']));
			$show_support = (in_array('support', $data['show']));
			$is_sports = ($data['is_sports'] == 1);
		
			$sessions = $record->data['Sessions'];
			if (!$sessions)
				throw new \DF\Exception\DisplayOnly('No Sessions Listed for This Form: This form has not listed sessions using the new session submission format. Invoices cannot be generated for this form.');
	        
			$invoice_data = array();
			foreach($sessions as $session_num => &$session_info)
			{
				$session_type = ($session_info['Session_type'] == "D") ? 'daytime' : 'overnight';
				
				$session_info['real_num_participants'] = ($session_info['Actual_num_participants'] != 0) ? $session_info['Actual_num_participants'] : $session_info['Num_participants'];
				$session_info['num_insured'] = $session_info['real_num_participants'] + $session_info['Num_insured_counselors'];
	            
	            if ($session_info['num_insured'] > 0)
	            {
	                if ($show_general)
	                {
	                    $field_name = $this->_getRateField('general', $session_type, $is_sports);
	                    $row_info = $session_info;
	                    $row_info['current_rate'] = $rates[$field_name];
	                    $row_info['total'] = $row_info['current_rate'] * $row_info['num_insured'] * $row_info['Num_days'];
	                    
	                    $invoice_data[$field_name]['sessions'][] = $row_info;
	                    $invoice_data[$field_name]['total'] += $row_info['total'];
	                    $invoice_data['total'] += $row_info['total'];
	                }
	                if ($show_accident)
	                {
	                    $field_name = $this->_getRateField('accident', $session_type, $is_sports);
	                    $row_info = $session_info;
	                    $row_info['current_rate'] = $rates[$field_name];
	                    $row_info['total'] = $row_info['current_rate'] * $row_info['num_insured'] * $row_info['Num_days'];
	                    
	                    $invoice_data[$field_name]['sessions'][] = $row_info;
	                    $invoice_data[$field_name]['total'] += $row_info['total'];
	                    $invoice_data['total'] += $row_info['total'];
	                }
	                if ($show_support)
	                {
	                    $field_name = $this->_getRateField('support', $session_type, $is_sports);
	                    $row_info = $session_info;
	                    $row_info['current_rate'] = $rates[$field_name];
	                    $row_info['total'] = $row_info['current_rate'] * $row_info['num_insured'] * $row_info['Num_days'];
	                    
	                    $invoice_data[$field_name]['sessions'][] = $row_info;
	                    $invoice_data[$field_name]['total'] += $row_info['total'];
	                    $invoice_data['total'] += $row_info['total'];
	                }
	            }
			}

			$this->view->form_fields = (array)$record->data;
			$this->view->rates = $rates;
			$this->view->invoice_types = $this->_settings['rate_types'];
			$this->view->invoice_data = $invoice_data;

			$this->render();
			return;
		}

		$this->view->headTitle('Generate Invoice');
		$this->renderForm($form);
	}

	protected function _getRateField($rate_name, $camp_code = "daytime", $is_sports = FALSE)
	{
		if ($rate_name == "support")
			return 'support';
		else
			return ($is_sports) ? $camp_code.'_'.$rate_name.'_sports' : $camp_code.'_'.$rate_name;
	}

    public function usersAction()
    {
    	$users_q = $this->em->createQueryBuilder()
    		->select('fcu')
    		->from('Entity\CampsFormUser', 'fcu')
    		->orderBy('fcu.lastname', 'ASC')
    		->orderBy('fcu.name', 'ASC');

    	if ($this->_hasParam('q'))
    	{
    		$this->view->q = $q = $this->_getParam('q');

    		$users_q->where('fcu.name LIKE :q OR fcu.email LIKE :q OR CONCAT(fcu.firstname, CONCAT(\' \', fcu.lastname)) LIKE :q');
    		$users_q->setParameter('q', '%'.$q.'%');
    	}

    	$this->view->tab = 'users';
    	$this->view->pager = new \DF\Paginator\Doctrine($users_q, $this->_getParam('page', 1));
	}

	public function edituserAction()
	{
		$user_id = (int)$this->_getParam('user_id');
		$record = CampsFormUser::find($user_id);

		$form = new \DF\Form($this->current_module_config->admin_forms->user);
		$form->setDefaults($record->toArray(TRUE, TRUE));

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			$record->fromArray($data);
			$record->save();

			$this->alert('<b>User updated.</b>', 'green');
			$this->redirectFromHere(array('action' => 'users', 'user_id' => NULL));
			return;
		}

		$this->view->headTitle('Edit User');
		$this->renderForm($form);
	}

	public function deleteuserAction()
	{
		$user_id = (int)$this->_getParam('user_id');
		$record = CampsFormUser::find($user_id);
		
		if ($record instanceof CampsFormUser)
			$record->delete();

		$this->alert('<b>User deleted.</b>', 'green');
		$this->redirectFromHere(array('action' => 'users', 'user_id' => NULL));
	}

	public function programsAction()
    {
    	$query = $this->em->createQueryBuilder()
    		->select('fcp')
    		->from('Entity\CampsFormProgram', 'fcp')
    		->orderBy('fcp.name', 'ASC');

    	if ($this->_hasParam('q'))
    	{
    		$this->view->q = $q = $this->_getParam('q');

    		$query->where('fcp.name LIKE :q');
    		$query->setParameter('q', '%'.$q.'%');
    	}

    	if ($this->_getParam('view') == "all")
    	{
    		$export_data = array(array('# Forms', 'Name'));

    		$records = $query->getQuery()->execute();
    		foreach($records as $record)
    		{
    			$export_data[] = array(
    				count($record->forms),
    				$record->name,
    			);
    		}

    		\DF\Export::csv($export_data);
    		return;
    	}
    	else
    	{
    		$this->view->tab = 'programs';
    		$this->view->pager = new \DF\Paginator\Doctrine($query, $this->_getParam('page', 1));
    	}
	}

	public function programusersAction()
    {
    	$program_id = (int)$this->_getParam('program_id');
    	$record = CampsFormProgram::find($program_id);
    	$this->view->record = $record;

    	$form = new \DF\Form($this->current_module_config->admin_forms->program_user);

    	if ($_POST && $form->isValid($_POST))
    	{
    		$data = $form->getValues();
    		$user_id = $data['user_id'];

    		$user = CampsFormUser::find($user_id);
    		$user->programs->add($record);
    		$user->save();

    		$this->alert('<b>User added to program.</b>', 'green');
    		$this->redirectHere();
    		return;
    	}

    	$this->view->form = $form;
	}

	public function removeprogramuserAction()
	{
		$program_id = (int)$this->_getParam('program_id');
		$record = CampsFormProgram::find($program_id);

		$user_id = (int)$this->_getParam('user_id');
		$user = CampsFormUser::find($user_id);

		$user->programs->removeElement($record);
		$user->save();

		$this->alert('<b>User removed from program.</b>', 'green');
		$this->redirectFromHere(array('action' => 'programusers', 'user_id' => NULL));
		return;
	}

	public function editprogramAction()
	{
		$form = new \DF\Form($this->current_module_config->forms->program);

		$program_id = (int)$this->_getParam('program_id');
		if ($program_id != 0)
		{
			$record = CampsFormProgram::find($program_id);
			$form->setDefaults($record->toArray(TRUE, TRUE));
		}

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			if (!($record instanceof CampsFormProgram))
				$record = new CampsFormProgram;

			$record->fromArray($data);
			$record->save();

			$this->alert('<b>Program updated.</b>', 'green');
			$this->redirectFromHere(array('action' => 'programs', 'program_id' => NULL));
			return;
		}

		$this->view->headTitle('Edit Program');
		$this->renderForm($form);
	}

	public function deleteprogramAction()
	{
		$program_id = (int)$this->_getParam('program_id');
		$record = CampsFormProgram::find($program_id);

		if ($record instanceof CampsFormProgram)
			$record->delete();

		$this->alert('<b>Program deleted.</b>', 'green');
		$this->redirectFromHere(array('action' => 'programs', 'program_id' => NULL));
	}

	/**
	 * Configuration
	 */

	public function ratesAction()
	{
		$form = new \DF\Form($this->current_module_config->admin_forms->rates);

		$camps_rates = Settings::getSetting('camps_rates');
		$form->setDefaults((array)$camps_rates);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			Settings::setSetting('camps_rates', $data);

			$this->alert('<b>Rates saved.</b>', 'green');
			$this->redirectHere();
			return;
		}

		$this->view->headTitle('Set Rates');
		$this->renderForm($form);
	}

	public function routingAction()
	{
		$this->view->tab = 'routing';

		$records = $this->em->createQuery('SELECT cfr FROM Entity\CampsFormRouting cfr ORDER BY cfr.name ASC');
		$this->view->pager = new \DF\Paginator\Doctrine($records, $this->_getParam('page', 1));
	}

	public function editroutingAction()
	{
		$form = new \DF\Form($this->current_module_config->admin_forms->routing);

		$routing_id = (int)$this->_getParam('routing_id');
		if ($routing_id != 0)
		{
			$record = CampsFormRouting::find($routing_id);
			$form->setDefaults($record->toArray(TRUE, TRUE));
		}

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();

			if (!($record instanceof CampsFormRouting))
				$record = new CampsFormRouting;

			$record->fromArray($data);
			$record->save();

			$this->alert('<b>Routing area updated.</b>', 'green');
			$this->redirectFromHere(array('action' => 'routing', 'routing_id' => NULL));
			return;
		}

		$this->view->headTitle('Edit Routing Area');
		$this->renderForm($form);
	}

    public function deleteroutingAction()
    {
    	$routing_id = (int)$this->_getParam('routing_id');
    	$record = CampsFormRouting::find($routing_id);

    	if ($record instanceof CampsFormRouting)
    		$record->delete();

    	$this->alert('<b>Routing area deleted.</b>', 'green');
		$this->redirectFromHere(array('action' => 'routing', 'routing_id' => NULL));
		return;
    }

	public function matrixAction()
	{
		$terms = $this->_settings['matrix_ranges'];

		if (!$this->_hasParam('term'))
		{
			$this->view->tab = 'matrix';
			$this->view->terms = $terms;
			$this->render('matrix');
			return;
		}

		ini_set('memory_limit', -1);

		$term = $this->_getParam('term');
		$term_info = $terms[$term];

		$all_forms = $this->em->createQuery('SELECT fc FROM CampsForm fc ORDER BY fc.timestamp ASC')->getArrayResult();
		foreach($all_forms as $form)
		{			
			if ($form['app_data']['Sessions'])
			{
				$latest_session = 0;
				foreach($form['app_data']['Sessions'] as $session)
				{
					$enddate = strtotime($session['End_date'].' 12:00 am');
					if ($enddate > $latest_session)
						$latest_session = $enddate;
				}
				
				if ($latest_session >= $term_info['start'] && $latest_session <= $term_info['end'])
				{
					$selected_forms[] = $form;
				}
			}
		}
		
		$forms_by_type = array();
		foreach($selected_forms as $form)
		{
			$form_info = &$form['app_data'];
			$admin_info = &$form['app_admin_data'];
			
			$show_general = (in_array('general', $admin_info['show']));
			$show_accident = (in_array('accident', $admin_info['show']));
			$show_support = (in_array('support', $admin_info['show']));
			$is_sports = ($admin_info['is_sports'] == 1);
			
			$current_row = 0;
			
			foreach($form_info['Sessions'] as $session_offset => &$session_info)
			{
                if ($session_info['Start_date'] != 0 && $session_offset != "File_Rosters")
                {
                    $current_row++;
                    $session_info['num'] = $current_row;
                    
                    $session_type = ($session_info['Session_type'] == "D") ? 'daytime' : 'overnight';
                    
                    $session_info['real_num_participants'] = ($session_info['Actual_num_participants'] != 0) ? $session_info['Actual_num_participants'] : $session_info['Num_participants'];
                    $session_info['num_insured'] = $session_info['real_num_participants'] + $session_info['Num_insured_counselors'];
                    
                    $rate_types = array('general', 'accident', 'support');
                    foreach($rate_types as $rate_type)
                    {
                        if (in_array($rate_type, $admin_info['show']))
                        {
                            $field_name = getRateField($rate_type, $session_type, $is_sports);
                            $current_rate = $rates[$field_name];
                            $total_fee = $current_rate * $session_info['num_insured'] * $session_info['Num_days'];
                            
                            $session_info[$rate_type.'_rate'] = number_format($current_rate, 2);
                            $session_info[$rate_type.'_fee'] = number_format($total_fee, 2);
                            $session_info['total'] += $total_fee;
                        }
                        else
                        {
                            $session_info[$rate_type.'_rate'] = '';
                            $session_info[$rate_type.'_fee'] = '';
                        }
                    }
                    
                    $session_info['total'] = number_format($session_info['total'], 2);
                }
                else
                {
                    unset($form_info['Sessions'][$session_offset]);
                }
			}
            
            $form_info['num_sessions'] = count($form_info['Sessions']);
			
			if ($is_sports)
				$forms_by_type['Sports'][] = $form;
			else
				$forms_by_type['NonSports'][] = $form;
		}

		$export_data = array();
		$export_data[] = array('Matrix - '.$term_info['name']);
		$export_data[] = arraY(' ');

		foreach($forms_by_type as $form_category => $form_list)
		{
			$export_data[] = array($form_category);
			$export_data[] = array(
				'Name of Camp', 
				'Sessions', 
				'Date Application Received', 
				'Session #',
				'Dates',
				'Actual Days',
				'Daytime/Overnight',
				'Insured Participants',
				'GL Rate',
				'GL Fees',
				'AM Rate',
				'AM Fees',
				'Support Rate',
				'Support Fees',
				'Total Due',
				'Paid By'
			);

			foreach($form_list as $form)
			{
				foreach($form['app_data']['Sessions'] as $session)
				{
					$export_data[] = array(
						$form['app_data']['General_Info']['Program_name'],
						$form['app_data']['num_sessions'],
						date('m/d/Y', $form['time_created']),
						$session['num'],
						$session['Start_date'].' - '.$session['End_date'],
						$session['Num_days'],
						$session['Session_type'],
						$session['num_insured'],
						$session['general_rate'],
						$session['general_fee'],
						$session['accident_rate'],
						$session['accident_fee'],
						$session['support_rate'],
						$session['support_fee'],
						$session['total'],
						' ',
					);
				}
			}

			$export_data[] = array(' ');
		}

		\DF\Export::csv($export_data);
	}

	public function reconcileAction()
	{
		ini_set('memory_limit', -1);
		set_time_limit(0);

		// Reset list of camps.
		$this->em->createQuery('DELETE FROM Entity\Camp c WHERE c.form_id IS NOT NULL')->execute();

		// Create list of active camps.
		$all_camps = $this->em->createQuery('SELECT cf FROM Entity\CampsForm cf WHERE cf.phase IN (:phases)')
			->setParameter('phases', array(CampsForm::PHASE_APPROVED_NEEDS_ROSTER, CampsForm::PHASE_APPROVED_UNPAID, CampsForm::PHASE_COMPLETE, CampsForm::PHASE_ARCHIVED));
		$iterator = $all_camps->iterate();

		foreach($iterator as $iterator_item)
		{
			$row = $iterator_item[0];
			$row->reloadIndex();
			$row->updateCampsDirectory();
		}

		$this->alert('<b>Camps directory updated.</b>', 'green');
		$this->redirectFromHere(array('action' => 'index'));
	}
}