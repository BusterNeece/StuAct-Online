<?php

$categories = array(
	'sact-online'		=> 'StuAct Online General',
	'sact-rec'			=> 'Organizations - Recognition',
	'sact-rec-roster' 	=> 'Organizations - Rosters',
	'sact-sofc'			=> 'Organizations - Finances (SOFC)',
	'sact-it'			=> 'Organizations - Web/E-mail (IT)',
);

return array(	

	'types' => $categories,

	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(

			'type' => array('radio', array(
				'label' => 'Category',
				'multiOptions' => $categories,
				'required' => true,
			)),

			'question' => array('text', array(
				'label' => 'Question',
				'class' => 'full-width',
				'required' => true,
			)),

			'answer' => array('textarea', array(
				'label' => 'Answer',
				'class' => 'full-width full-height',
				'required' => true,
			)),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);