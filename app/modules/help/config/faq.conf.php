<?php
/**
 * FAQ Configuration
 */

return array(
    
    'types'			=> array(
		'sact-online'		=> 'StuAct Online General',
		'sact-rec'			=> 'Organizations - Recognition',
		'sact-rec-roster' 	=> 'Organizations - Rosters',
		'sact-sofc'			=> 'Organizations - Finances (SOFC)',
		'sact-it'			=> 'Organizations - Web/E-mail (IT)',
	),
    
);