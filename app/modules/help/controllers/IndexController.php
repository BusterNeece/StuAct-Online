<?php
class Help_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        return TRUE;
    }
    
    /**
     * Main display.
     */
    public function indexAction()
    {}
    
    public function aboutAction()
    {}
    
    public function termsAction()
    {}
    
    public function privacyAction()
    {}
}