<?php
class Help_ContactController extends \DF\Controller\Action
{
    public function permissions()
    {
        return TRUE;
    }
    
    /**
     * Main display.
     */
    public function indexAction()
    {
        $this->doNotRender();
        $this->redirect('http://studentactivities.tamu.edu/email');
        return;
    }
}