<?php
use \Entity\Faq;

class Help_FaqController extends \DF\Controller\Action
{
    public function permissions()
    {
        return TRUE;
    }
    
    /**
     * Main display.
     */
    public function indexAction()
    {
        $faq_types = $this->current_module_config->forms->faq->types->toArray();
        $this->view->faq_types = $faq_types;
        
        $faqs = array();
        foreach($faq_types as $faq_key => $faq_name)
        {
            $faqs[$faq_key] = array(
                'name'	=> $faq_name,
                'questions' => array(),
            );
        }
        
        $all_faqs = Faq::fetchArray();
        foreach($all_faqs as $faq)
        {
            $faqs[$faq['type']]['questions'][] = $faq;
        }
        
        $this->view->faqs = $faqs;
        $this->view->is_admin = $this->acl->isAllowed('admin_faqs');
    }

    public function editAction()
    {
        $this->acl->checkPermission('admin_faqs');

        $form_config = $this->current_module_config->forms->faq->form->toArray();
        $form = new \StuAct\Form($form_config);

        $id = (int)$this->_getParam('id');
        if ($id != 0)
        {
            $record = Faq::find($id);
            $form->setDefaults($record->toArray(TRUE, TRUE));
        }

        if ($_POST && $form->isValid($_POST))
        {
            $data = $form->getValues();

            if (!($record instanceof Faq))
                $record = new Faq;
            
            $record->fromArray($data);
            $record->save();

            $this->alert('<b>FAQ updated.</b>', 'green');
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
            return;
        }

        $this->view->headTitle('Frequently Asked Questions');
        $this->renderForm($form, 'edit');
        return;
    }

    public function deleteAction()
    {
        $this->acl->checkPermission('admin_faqs');

        $id = (int)$this->_getParam('id');
        $record = Faq::find($id);

        if ($record instanceof Faq)
            $record->delete();
        
        $this->alert('<b>Question deleted.</b>', 'green');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
        return;
    }
}