<?php
class Help_TutorialController extends \DF\Controller\Action
{
    public function permissions()
    {
        return TRUE;
    }
    
    /**
     * Main display.
     */
    public function indexAction()
    {
        $tutorials = array(
            'getstarted' => array(
                'type'			=> 'local',
                'title'			=> 'Get Started with StuAct Online',
                'url'		    => 'http://studentactivities.tamu.edu/online/static/media/tutorials/StuActOnlineGettingStarted.swf',
                'transcript'    => 'http://studentactivities.tamu.edu/online/static/media/tutorials/getting_started.pdf',
            ),
            'drupal' => array(
                'type'			=> 'new',
                'title'			=> 'Getting Started with Drupal',
                'url'		    => 'http://studentactivities.tamu.edu/online/static/media/tutorials/drupal.mp4',
                'transcript'    => 'http://studentactivities.tamu.edu/online/static/media/tutorials/drupal_transcript.pdf',
            ),
        );
        
        if ($this->_hasParam('video'))
        {
            $video_name = $this->_getParam('video');
            $tutorial = $tutorials[$video_name];
            
            $this->view->tutorial = $tutorial;
            
            switch($tutorial['type'])
            {
                case "new":
                    $this->render('player_new');
                    return;
                break;
                
                default:
                    $this->render('player_local');
                    return;
                break;
            }
        }
        
        $this->view->tutorials = $tutorials;
    }
}