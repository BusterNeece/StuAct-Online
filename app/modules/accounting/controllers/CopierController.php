<?php
class Accounting_CopierController extends \DF\Controller\Action
{
	public function permissions()
	{
		return true;
	}
	
	/**
	 * Main display.
	 */
    public function indexAction()
    {
		$this->redirect('http://dsanet.tamu.edu/accounting_copiers');
		return;
	}
}