<?php
use \Entity\OrganizationBatchGroup;
use \Entity\Organization;

class Batch_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        $user = $this->auth->getLoggedInUser();

        if (!$this->acl->isAllowed('is logged in'))
            return false;
        else if ($this->acl->isAllowed('admin_orgs'))
            return true;
        else
            return (count($user->batchgroups) > 0);
    }
    
    protected $_id;
    protected $_batch_group;
    
    public function preDispatch()
    {
        $user = $this->auth->getLoggedInUser();

        if ($this->acl->isAllowed('admin_orgs'))
            $batch_groups_raw = $this->em->createQuery('SELECT obg FROM \Entity\OrganizationBatchGroup obg')->getArrayResult();
        else
            $batch_groups_raw = $user->batchgroups;

        $batch_groups = array();
        foreach($batch_groups_raw as $group)
            $batch_groups[$group['id']] = $group['name'];

        if ($this->_hasParam('id'))
        {
            $id = (int)$this->_getParam('id');
            $group = OrganizationBatchGroup::find($id);

            if (!($group instanceof OrganizationBatchGroup) || !isset($batch_groups[$id]))
                throw new \DF\Exception\PermissionDenied;

            $this->_id = $id;
            $this->_batch_group = $group;
        }
        else
        {
            if (count($batch_groups) == 1)
                $this->redirectFromHere(array('id' => key($batch_groups)));

            $this->view->batch_groups = $batch_groups;
            $this->render('select');
            return;
        }
    }
	
    /**
     * Main display.
     */
    public function indexAction()
    {
        $this->view->org_settings = Organization::loadSettings();
        $this->view->batch_group = $this->_batch_group;
	}
    
    public function bulkAction()
    {
        $this->doNotRender();
        
        // Redirect to handle incoming requests.
        if ($this->_hasParam('ids_raw'))
        {
            $this->redirectFromHere(array(
                'action'        => $this->_getParam('do'),
                'ids'           => implode("|", $this->_getParam('ids_raw')),
            ));
        }
        return;
    }
    
    public function gradesAction()
    {
        if (!$this->_hasParam('term'))
        {
            $this->view->terms = \StuAct\Grades::getAvailableTerms();
            $this->render('grades_select');
            return;
        }
        
        list($org_ids, $batch_id) = $this->_getBulkDetails();
        
        $term = $this->_getParam('term');
        $term_name = \StuAct\Grades::getTermName($term);
        
        $orgs_raw = $this->em->createQuery('SELECT o, oo, u FROM \Entity\Organization o JOIN o.officer_positions oo JOIN oo.user u WHERE o.id IN (:org_ids) ORDER BY o.name ASC, oo.position ASC, u.lastname ASC, u.firstname ASC, u.uin ASC')
            ->setParameter('org_ids', $org_ids)
            ->getArrayResult();
        
        $org_config = $this->config->organizations->toArray();
        
        // Compile officer information into a flattened array.
        $officers = array();
        $officer_uins = array();
        foreach($orgs_raw as $org)
        {
            $org_id = $org['id'];
            
            $org_info = $org;
            unset($org_info['officer_positions']);
            
            foreach((array)$org['officer_positions'] as  $officer)
            {
                if ($officer['position'] == ORG_MEMBER || in_array($officer['position'], $org_config['officer_groups']['gpr']))
                {
                    $user_info = $officer['user'];
                    unset($officer['user']);
                    
                    $officers[$org_id][] = array_merge($officer, $org_info, $user_info);
                    $officer_uins[] = $user_info['uin'];
                }
            }
        }
        
        $all_records = \StuAct\Grades::getBulkStudentRecords($officer_uins, $term);
        
        switch(strtolower($this->_getParam('format', 'csv')))
        {
            case "csv":
            default:
                $export_data = array();
                
                foreach($officers as $org_id => $officer_data)
                {
                    $export_data[] = array(
                        '-- #'.$officer_data[0]['account_number'].': '.$officer_data[0]['name'].' --',
                    );
                    
                    $export_data[] = array(
                        'UIN',
                        'Position',
                        'First Name',
                        'Last Name',
                        $term_name.' GPR',
                        'Cumulative GPR',
                        'Student Status',
                        'Good Standing',
                    );
                    
                    foreach($officer_data as $officer)
                    {
                        $uin = $officer['uin'];
                        $student_record = $all_records[$uin];
                        $conduct = \Entity\Conduct::check($uin);
                        
                        $export_data[] = array(
                            $officer['uin'],
                            $org_config['officer_codes'][$officer['position']],
                            $officer['firstname'],
                            $officer['lastname'],
                            number_format($student_record['gpa'], 2),
                            number_format($student_record['cumul'], 2),
                            $student_record['classification'],
                            ($conduct) ? 'Y' : 'N',
                        );
                    }
                    
                    $export_data[] = array(' ');
                }
                
                $this->doNotRender();
                \DF\Export::csv($export_data);
                return;
            break;
        }
    }
    
    public function rosterAction()
    {   
        list($org_ids, $batch_id) = $this->_getBulkDetails();
        
        $orgs_raw = $this->em->createQuery('SELECT o, oo, u FROM \Entity\Organization o LEFT JOIN o.officer_positions oo INNER JOIN oo.user u WHERE o.id IN (:org_ids) ORDER BY o.name ASC, oo.position ASC, u.lastname ASC, u.firstname ASC, u.uin ASC')
            ->setParameter('org_ids', $org_ids)
            ->getArrayResult();
        
        $this->view->orgs = $orgs_raw;
        $this->view->org_config = $org_config = $this->config->organizations->toArray();
        
        switch(strtolower($this->_getParam('format', 'html')))
        {
            case "csv":
                $export_data = array(array(
                    'Org Name',
                    'Position',
                    'First Name',
                    'Last Name',
                    'UIN',
                    'NetID',
                    'Phone',
                    'E-mail Address',
                ));
                
                foreach($orgs_raw as $org)
                {
                    foreach((array)$org['officer_positions'] as $officer)
                    {
                        $export_data[] = array(
                            $org['name'],
                            $org_config['officer_codes'][$officer['position']],
                            $officer['user']['firstname'],
                            $officer['user']['lastname'],
                            $officer['user']['uin'],
                            $officer['user']['username'],
                            $officer['user']['phone'],
                            $officer['user']['email'],
                        );
                    }
                }
                
                $this->doNotRender();
                \DF\Export::csv($export_data);
                return;
            break;
            
            case 'html':
            default:
                $this->render();
            break;
        }
    }
    
    protected function _getBulkDetails()
    {
        // Establish list of organizations to cross-check.
        $batch_orgs = $this->em->createQuery('SELECT o FROM \Entity\Organization o WHERE o.batch_id = :batch_id')
            ->setParameter('batch_id', $this->_id)
            ->getArrayResult();
        
        $batch_org_ids = array();
        foreach($batch_orgs as $org)
        {
            $batch_org_ids[] = $org['id'];
        }
        
        $org_ids_raw = (array)explode("|", $this->_getParam('ids'));
        $org_ids = array();
        foreach($org_ids_raw as $org_id)
        {
            if (in_array($org_id, $batch_org_ids))
                $org_ids[] = $org_id;
        }
        
        return array($org_ids, $this->_id);
    }
}