<?php
use \Entity\OrganizationBatchGroup;
use \Entity\OrganizationBatchGroupManager;
use \Entity\User;

class Batch_ManageController extends \DF\Controller\Action
{
    public function permissions()
    {
		return \DF\Acl::isAllowed('admin_orgs');
    }
	
    /**
     * Main display.
     */
    public function indexAction()
    {
		// List batch groups.
        $batch_groups = $this->em->createQuery('SELECT obg, u FROM \Entity\OrganizationBatchGroup obg JOIN obg.managers u')
            ->getArrayResult();
		
		$this->view->batch_groups = $batch_groups;
	}
	
	public function managersAction()
	{
        $group = $this->em->createQuery('SELECT obg, u FROM \Entity\OrganizationBatchGroup obg JOIN obg.managers u WHERE obg.id = :id')
            ->setParameter('id', $this->_getParam('id'))
            ->getSingleResult();
		
		if (!($group instanceof OrganizationBatchGroup))
			throw new \DF\Exception\DisplayOnly('Group not found!');
		
		$this->view->group = $group;
	}
	
	public function addmanagerAction()
	{
		$org_batch_id = $this->_getParam('id');
		$user = User::lookUp($_REQUEST['user']);
		
		if ($user)
		{
			$record = OrganizationBatchGroup::find($org_batch_id);
			$record->managers->add($user);
			$record->save();
		}
		
		$this->alert('Member added!');
		$this->redirectFromHere(array('action' => 'managers', 'user_id' => NULL));
		return;
	}
	
	public function deletemanagerAction()
	{
		$org_batch_id = $this->_getParam('id');
		$user_id = $this->_getParam('user_id');
		$user = User::find($user_id);
		
		if ($user_id)
		{
			$record = OrganizationBatchGroup::find($org_batch_id);
			$record->managers->removeElement($user);
			$record->save();
		}
		
		$this->alert('Member deleted!');
		$this->redirectFromHere(array('action' => 'managers', 'user_id' => NULL));
		return;
	}
	
	public function editAction()
	{
		$form = new \StuAct\Form($this->current_module_config->forms->batchtype->form);
		
		if ($this->_hasParam('id'))
		{
            $record = OrganizationBatchGroup::find($this->_getParam('id'));
			$form->setDefaults($record->toArray());
		}
        
        if(!empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();
			
			if (!($record instanceof OrganizationBatchGroup))
				$record = new OrganizationBatchGroup();
			
			$record->fromArray($data);
			$record->save();
			
			$this->alert('Batch type updated.');
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
        }
        
        $this->view->form = $form;
	}
	
	public function deleteAction()
	{
        $group = OrganizationBatchGroup::find($this->_getParam('id'));
		
		if ($group instanceof OrganizationBatchGroup)
		{
            if (count($group->managers) > 0)
            {
                foreach($group->managers as $manager)
                {
                    $manager->delete();
                }
            }
			
			if (count($group->organizations) > 0)
			{
				foreach($group->organizations as $org)
				{
					$org->org_batch_id = 0;
					$org->save();
				}
			}
			
			$group->delete();
		}
			
		$this->alert('Batch type deleted!');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
	}
}