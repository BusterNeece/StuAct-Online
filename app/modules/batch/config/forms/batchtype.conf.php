<?php
/**
 * Edit Batch Type Form
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
					
			'name'		=> array('text', array(
				'label' => 'Batch Type Name',
				'class'	=> 'half-width',
				'required' => true,
	        )),
			
			'short_name' => array('text', array(
				'label' => 'Batch Type Short Name',
				'class' => 'half-width',
				'required' => true,
			)),
			
			'use_generator' => array('radio', array(
				'label' => 'Use Constitution Generator Tool',
				'multiOptions' => array(0 => 'No', 1 => 'Yes'),
			)),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);