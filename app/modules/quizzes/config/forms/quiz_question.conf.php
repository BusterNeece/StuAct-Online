<?php
/**
 * Quiz form definition
 */

$weights = array();
for($i = 0; $i <= 45; $i++)
    $weights[$i] = $i;

$form_config = array(
    // Number of options to show on the administrative page.
    'num_options' => 5,
    
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
            
			'question' => array('text', array(
				'label' => 'Question',
				'required' => true,
                'class' => 'full-width',
	        )),
            
            'url' => array('text', array(
                'label' => 'More Information URL (if applicable)',
                'class' => 'half-width',
            )),
            
            'correct_answer' => array('select', array(
               'label'  => 'Correct Answer',
               'required' => true,
               'multiOptions' => array(),
            )),
			
			
		),
	),
);

/**
 * Assemble the question fields.
 */
$question_elements = array();
for($i = 1; $i <= $form_config['num_options']; $i++)
{
    $option_letter = chr(64+$i);
    
    $form_config['form']['elements']['option_'.$i] = array('text', array(
        'label'     => 'Option '.$option_letter,
        'class'     => 'half-width',
        'maxlength' => 255,
        'required'  => FALSE,
    ));
    
    $form_config['form']['elements']['correct_answer'][1]['multiOptions'][$i] = 'Option '.$option_letter;
}

/**
 * Add submit button.
 */
$form_config['form']['elements']['submit'] = array('submit', array(
    'type'	=> 'submit',
    'label'	=> 'Save Changes',
    'helper' => 'formButton',
    'class' => 'ui-button',
));

return $form_config;