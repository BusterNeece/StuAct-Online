<?php
/**
 * Quiz form definition
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
            
            'short_name' => array('text', array(
                'label' => 'Short Name',
                'description' => 'Letters, numbers, and underscores only. For use when referring to this quiz programmatically.',
                'class' => 'half-width',
            )),
            
			'title'		=> array('text', array(
				'label' => 'Quiz Title',
				'required' => true,
                'class' => 'full-width',
	        )),
            
            'description' => array('textarea', array(
               'label'  => 'Quiz Description',
               'required' => false,
               'class'  => 'full-width full-height',
            )),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);