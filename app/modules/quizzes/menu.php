<?php
return array(
    'default' => array(
        'quizzes' => array(
            'label' => 'Quizzes',
            'module' => 'quizzes',
            'controller' => 'index',
        ),
        
        'quizzes_manage' => array(
            'label' => 'Manage Quizzes',
            'module' => 'quizzes',
            'controller' => 'manage',
            'action' => 'index',
            'permission' => 'manage quizzes',
            
            'pages' => array(
                
                'edit' => array(
                    'module' => 'quizzes',
                    'controller' => 'manage',
                    'action' => 'edit',
                ),
                'results' => array(
                    'module' => 'quizzes',
                    'controller' => 'manage',
                    'action' => 'results',
                ),
                
            ),
        ),
    ),
);