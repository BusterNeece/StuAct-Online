<?php
use \Entity\Quiz;
use \Entity\QuizQuestion;
use \Entity\QuizOption;
use \Entity\QuizResult;

class Quizzes_ManageController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::getInstance()->isAllowed('manage quizzes');
    }
    
    public function indexAction()
    {
        $this->view->all_quizzes = Quiz::fetchAll();
    }
    
    public function resultsAction()
    {
        $quiz_id = (int)$this->_getParam('id');
        
        $quiz_raw = $this->em->createQuery('SELECT q, qr, u FROM \Entity\Quiz q LEFT JOIN q.results qr LEFT JOIN qr.user u WHERE q.id = :quiz_id ORDER BY qr.created_at DESC')
            ->setParameter('quiz_id', $quiz_id)
            ->getArrayResult();
        
        switch($this->_getParam('format', 'html'))
        {
            case "csv":
                $this->doNotRender();
                $quiz = $quiz_raw[0];
                
                $export_data = array(
                    array('Results for Quiz: '.$quiz['title']),
                    array('Date/Time', 'UIN', 'User Name', 'E-mail Address'),
                );
                
                foreach((array)$quiz['results'] as $result)
                {
                    $export_data[] = array(
                        $result['created_at']->format('m/d/Y g:ia'),
                        $result['user']['uin'],
                        $result['user']['firstname'].' '.$result['user']['lastname'],
                        $result['user']['email'],
                    );
                }
                
                \DF\Export::csv($export_data);
                return;
            break;
            
            case "html":
            default:
                $this->view->quiz = $quiz_raw[0];
                $this->render();
                return;
            break;
        }
    }

    public function clearresultsAction()
    {
        $quiz_id = (int)$this->_getParam('id');

        $results = QuizResult::getRepository()->findBy(array('quiz_id' => $quiz_id));

        foreach($results as $result)
        {
            $result->delete();
        }

        $this->alert('<b>Results archived!</b>', 'green');
        $this->redirectFromHere(array('action' => 'results'));
        return;
    }
    
    public function editquizAction()
    {
        $form = new \StuAct\Form($this->current_module_config->forms->quiz->form);
        
        $quiz_id = (int)$this->_getParam('id');
        
        if ($quiz_id != 0)
        {
            $record = Quiz::find($quiz_id);
            $form->setDefaults($record->toArray());
        }
		
		if (!empty($_POST) && $form->isValid($_POST))
		{
			$data = $form->getValues();
            
            if (!$record)
                $record = new Quiz();
            
            $record->fromArray($data);
			$record->save();
			
			$this->alert('New record created!');
			$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
		}

        $this->view->headTitle('Add/Update Quiz');
        $this->renderForm($form);
    }
    
    public function deletequizAction()
    {
        $this->validateToken($this->_getParam('csrf'));
        
        $record = Quiz::find($this->_getParam('id'));
        if ($record)
            $record->delete();
        
        $this->alert('Record deleted!');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
    }
    
    /**
     * View page for a specific quiz.
     */
    public function quizAction()
    {
        $quiz_id = $this->_getParam('id');
        $quiz = Quiz::find($quiz_id);
        
        if (!$quiz)
            throw new \DF\Exception_Warning('Quiz #'.$quiz_id.' not found!');
        
        $this->view->quiz = $quiz;
    }
    
    public function saveorderAction()
    {
        $update_query = $this->em->createQuery('UPDATE \Entity\QuizQuestion qq SET qq.weight = :new_weight WHERE qq.id = :question_id');
        
        $order = explode(',', $_REQUEST['order']);
        foreach((array)$order as $weight => $qid)
        {
            $update_query->setParameters(array('new_weight' => $weight, 'question_id' => $qid))
                ->execute();
        }
        
        $this->alert('Question order saved!');
        $this->redirectFromHere(array('action' => 'quiz'));
    }
    
    public function editquestionAction()
    {
        $quiz_id = $this->_getParam('id');
        $quiz = Quiz::find($quiz_id);
        
        $question_id = $this->_getParam('qid');
        
        $form = new \StuAct\Form($this->current_module_config->forms->quiz_question->form);
        
        if ($question_id != 0)
        {
            $record = QuizQuestion::find($question_id);
            $form_defaults = $record->toArray();
            
            if (count($record->options) > 0)
            {
                $option_ids = array();
                $i = 1;
                foreach($record->options as $option)
                {
                    $form_defaults['option_'.$i] = $option->option_text;
                    $option_ids[$option->id] = $i;
                    $i++;
                }
            }
            
            if ($record->correct_answer_id)
            {
                $form_defaults['correct_answer'] = $option_ids[$record->correct_answer_id];
            }
            
            $form->setDefaults($form_defaults);
        }
        
        if (!empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();
            
            if (!($record instanceof QuizQuestion))
                $record = new QuizQuestion();
            
            $record->quiz = $quiz;
            $record->question = $data['question'];
            $record->url = $data['url'];
            $record->correct_answer_id = NULL;
            $record->save();
            
            // Delete existing question options.
            if (count($record->options) > 0)
                foreach($record->options as $option)
                    $option->delete();
            
            $option_ids = array();
            for($i = 1; $i <= $this->config->forms->quiz_question->num_options; $i++)
            {
                if (!empty($data['option_'.$i]))
                {
                    $option = new QuizOption();
                    $option->question = $record;
                    $option->option_text = $data['option_'.$i];
                    $option->save();
                    $option_ids[$i] = $option->id;
                }
            }
            
            if ($data['correct_answer'] && isset($option_ids[$data['correct_answer']]))
            {
                $answer_id = $option_ids[$data['correct_answer']];
                $record->correct_answer_id = $answer_id;
                $record->save();
            }
            
            $this->alert('Record updated!');
            $this->redirectFromHere(array('action' => 'quiz', 'id' => $quiz_id, 'qid' => NULL));
            return;
        }

        $this->view->headTitle('Add/Update Quiz Question');
        $this->renderForm($form);
    }
    
    public function deletequestionAction()
    {
        $this->validateToken($this->_getParam('csrf'));
        
        $record = QuizQuestion::find($this->_getParam('qid'));
        if ($record)
            $record->delete();
        
        $this->alert('Record deleted!');
        $this->redirectFromHere(array('action' => 'quiz', 'id' => $this->_getParam('id'), 'qid' => NULL, 'csrf' => NULL));
    }
}