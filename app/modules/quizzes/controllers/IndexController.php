<?php
use \Entity\Quiz;

class Quizzes_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::getInstance()->isAllowed('is logged in');
    }
    
    public function indexAction()
    {
        $this->view->all_quizzes = $this->em->createQuery('SELECT q FROM \Entity\Quiz q ORDER BY q.title ASC')
            ->execute();
    }
    
    public function quizAction()
    {
		$quiz_id = (int)$this->_getParam('id');
		$quiz = Quiz::find($quiz_id);
        
        if (!($quiz instanceof Quiz))
            throw new \DF\Exception\DisplayOnly('Quiz not found!');
        
        $this->view->quiz = $quiz;
        $form = $quiz->getForm();
        
        if (!empty($_POST) && $form->isValid($_POST))
        {
            $quiz->registerCredit();
            $this->render('complete');
            return;
        }
        else
        {
            $previous = $quiz->hasPreviousCredit();
            if ($previous != 0)
                $this->alert('<b>You previously completed this quiz on '.date('F j, Y \a\t h:ia', $previous).'.</b>');
            
            $this->view->form = $form;
        }
    }
}