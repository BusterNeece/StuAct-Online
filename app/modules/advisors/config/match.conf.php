<?php
/**
 * Advisor Match configuration
 */

$types_raw = array(
    'Performing Arts',
    'Visual Arts',
    'Campus Life',
    'Community Service',
    'Cultural/International',
    'Global Service',
    'Healthy Living',
    'Honor',
    'Professional/Career',
    'Recreation',
    'Religious',
    'Social/Political Issues',
    'Spirit/Tradition',
    'Technology/Gaming',
);

$types = array();
foreach($types_raw as $type)
{
    $type_key = strtolower(preg_replace("/[^a-zA-Z0-9_]/", "", str_replace(' ', '_', $type)));
    $types[$type_key] = $type;
}

return array(
    'types' => $types,
);