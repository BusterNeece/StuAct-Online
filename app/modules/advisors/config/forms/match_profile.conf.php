<?php
/**
 * Advisor match profile form
 */

$module_config = Zend_Registry::get('module_config');

return array(	
	/* Form Configuration */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
            
			'summary' => array('textarea', array(
                'label' => 'Personal Summary',
				'class'	=> 'full-width half-height',
                'description' => 'This summary is listed on the main directory page and contains the most essential information visitors should know about you. Include any background, personality, or philosophy notes that you feel would be relevant.',
				'required' => true,
	        )),
            
            'types' => array('multiCheckbox', array(
                'label' => 'Preferred Organization Types',
                'multiOptions' => $module_config['advisors']->match->types->toArray(),
            )),
            
            'interests' => array('textarea', array(
                'label' => 'Subject Matter Interests',
                'class'	=> 'full-width half-height',
                'description' => 'Elaborate on the categories you selected above and describe the kind of subject matters that you would be interested in advising.'
            )),
            
            'availability' => array('textarea', array(
                'label' => 'Availability',
                'class'	=> 'full-width half-height',
                'description' => 'How flexible is your schedule? How much time can you dedicate to attending meetings and events, working with student leaders, and completing other advisor duties? Are you available after hours and on weekends?',
            )),
            
            'location' => array('textarea', array(
                'label' => 'Location on Campus',
                'class'	=> 'full-width half-height',
                'description' => 'Where can students most commonly reach you? If there is a particular type of communication that you prefer to use, indicate it here as well.',
            )),
            
            'role' => array('textarea', array(
                'label' => 'Role in University',
                'class'	=> 'full-width half-height',
                'description' => 'If your role with the University could impact your ability to advise an organization, either by providing additional opportunities or imposing limitations, specify this here.',
            )),
            
            'advising_style' => array('textarea', array(
                'label' => 'Advising Style',
                'class'	=> 'full-width half-height',
                'description' => 'Do you prefer a high level of responsibility in the decision-making process, or would you rather fill a more hands-off role? Describe your approach to advising here, including any history of working with other organizations.'
            )),
            
            'comments' => array('textarea', array(
                'label' => 'Other Comments',
                'class'	=> 'full-width half-height',
                'description' => 'If you would like to provide any other information that you were not able to add to the fields above, include it here.',
            )),
            
            'additional_info' => array('markup', array(
                'label' => 'Additional Information Included',
                'markup' => '
                    <p>In order to allow student leaders to contact you for more information, we automatically include the following information from your StuAct Online profile:</p>
                    <ul>
                        <li>First and Last Name</li>
                        <li>Title</li>
                        <li>E-mail Address</li>
                        <li>Phone Number</li>
                    </ul>
                ',
            )),
            
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);