<?php
/**
 * Advisapedia
 */

use \Entity\AdvisorDirectory;

class Advisors_MatchController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::isAllowed('is logged in');
	}
	
	public function indexAction()
	{
        $all_entries = $this->em->createQuery('SELECT ad, u FROM \Entity\AdvisorDirectory ad JOIN ad.user u ORDER BY ad.created_at DESC')
            ->getArrayResult();
        
        $entries_by_type = array();
        $entries_by_user = array();
        
        foreach($all_entries as $entry)
        {
            $types = $entry['types'];
            foreach((array)$types as $type)
            {
                $entries_by_type[$type][] = $entry;
            }
            
            $entries_by_user[$entry['user']['id']] = $entry;
        }
        
        ksort($entries_by_type);
        
        $this->view->types = $this->module_config['advisors']->match->types->toArray();
        $this->view->entries_by_type = $entries_by_type;
        
        $user = \DF\Auth::getLoggedInUser();
        $user_id = $user->id;
        
        if (isset($entries_by_user[$user_id]))
        {
            $this->view->has_entry = TRUE;
        }
        else
        {
            $leader_status = $user->getLeaderStatus();
            $this->view->show_submit = ($leader_status['type'] != "officer");
        }
	}
    
    public function detailAction()
    {
        $user_id = (int)$this->_getParam('id');
        $entry = AdvisorDirectory::getRepository()->findOneBy(array('user_id' => $user_id));
            
        if (!($entry instanceof AdvisorDirectory))
            throw new \DF\Exception_DisplayOnly('Advisor directory entry not found!');
        
        $this->view->entry = $entry;
        
        $form_config = $this->module_config['advisors']->forms->match_profile->form->toArray();
        foreach($form_config['elements'] as &$element)
        {
            unset($element['description']);
        }
        $form = new \StuAct\Form($form_config);
        $form->populate($entry->toArray());
        
        $this->view->form = $form;
    }
	
	public function profileAction()
	{
		$user = \DF\Auth::getLoggedInUser();
        
        $user_entry = AdvisorDirectory::getRepository()->findOneBy(array('user_id' => $user->id));
        
        $form_config = $this->module_config['advisors']->forms->match_profile->form->toArray();
        $form = new \StuAct\Form($form_config);
        
        if ($user_entry instanceof AdvisorDirectory)
            $form->setDefaults($user_entry->toArray());
        
        if( !empty($_POST) && $form->isValid($_POST) )
        {
            $data = $form->getValues();
            
            if (!($user_entry instanceof AdvisorDirectory))
            {
                $user_entry = new AdvisorDirectory();
                $user_entry->user = $user;
            }
            
            $user_entry->fromArray($data);
            $user_entry->save();
            
            $this->alert('Profile saved!');
            $this->redirectFromHere(array('action' => 'index'));
            return;
        }
        
        $this->view->form = $form;
	}
	
	public function deleteAction()
	{
        $user = \DF\Auth::getLoggedInUser();
        $user_entry = AdvisorDirectory::getRepository()->findOneBy(array('user_id' => $user->id));
        
        if ($user_entry instanceof AdvisorDirectory)
        {
            $user_entry->delete();
        }
        
        $this->alert('Directory entry removed.');
        $this->redirectFromHere(array('action' => 'index'));
        return;
	}
}