<?php
return array(
    'default' => array(
        'advisors' => array(
            'label' => 'Advisopedia',
            'module' => 'advisors',
            'pages' => array(
				
				'advisor_match' => array(
					'label' => 'Advisor Matching Directory',
					'module' => 'advisors',
					'controller' => 'match',
					'action' => 'index',
                    'pages' => array(
                        'advisor_match_profile' => array(
                            'module' => 'advisors',
                            'controller' => 'match',
                            'action' => 'detail',
                        ),
                        'advisor_match_submit' => array(
                            'label' => 'Submit Profile',
                            'module' => 'advisors',
                            'controller' => 'match',
                            'action' => 'profile',
                        ),
                    ),
				),
				
            ),
        ),
    ),
);