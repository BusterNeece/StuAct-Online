<?php

$form_config = array(

	'method' => 'post',
	'groups' => array(

		'outbound_journey' => array(
			'legend' => 'Journey to Destination (Outbound)',
			'elements' => array(

				'outbound_departure' => array('unixdatetime', array(
					'label' => 'Departure Date/Time',
					'required' => true,
				)),
				'outbound_arrival' => array('unixdatetime', array(
					'label' => 'Arrival Date/Time',
					'required' => true,
				)),

			),
		),

		'inbound_journey' => array(
			'legend' => 'Journey from Destination (Inbound)',
			'elements' => array(

				'inbound_departure' => array('unixdatetime', array(
					'label' => 'Departure Date/Time',
					'required' => true,
				)),
				'inbound_arrival' => array('unixdatetime', array(
					'label' => 'Arrival Date/Time',
					'required' => true,
				)),

			),
		),

		'submit_group' => array(
			'elements' => array(
				'submit_btn' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Add Travel Date',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);

return $form_config;