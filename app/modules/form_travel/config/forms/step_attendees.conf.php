<?php
$form_config = array(

	'method' => 'post',
	'groups' => array(

		'traveler_info_instructions' => array(
			'legend' => 'Choose Traveler Information Submission Method',
			'elements' => array(
				'instructions' => array('markup', array(
					'markup' => '
						<p>Specific information for each traveler may be submitted below by uploading a document (in an approved format) <b>or</b> by using the form below. If you wish to upload a document, it must include:</p>
						<ul>
							<li>Each traveler\'s full name</li>
							<li>Texas A&M University UIN</li>
							<li>Emergency contact name</li>
							<li>Emergency contact number</li>
						</ul>

						<p><i>Please do not include social security numbers.</i></p>

						<p>Additionally, please indicate if the traveler is designated as faculty, staff, and/or a student.</p>
					',
				)),
			),
		),

		'traveler_file' => array(
			'legend' => 'Option 1: Upload Traveler Information File(s)',
			'elements' => array(

				'travelers_files' => array('file', array(
					'label' => 'Traveler Information File(s)',
					'multiFile' => 4,
				)),

			),
		),

		'traveler_details_staff' => array(
			'legend' => 'Option 2: Specify Inside Form - Faculty/Staff Advisors',
			'elements' => array(),
		),

		'traveler_details_students' => array(
			'legend' => 'Option 2: Specify Inside Form - Students',
			'elements' => array(),
		),

		'submit_group' => array(
			'elements' => array(
				'submit_btn' => array('submit', array(
					'type'	=> 'submit',
					'label'	=> 'Save and Submit',
					'helper' => 'formButton',
					'class' => 'ui-button',
				)),
			),
		),
	),
);

$user_types = array(
	'staff'		=> 5,
	'students'	=> 21,
);

foreach($user_types as $type_name => $type_limit)
{
	for($i = 1; $i <= $type_limit; $i++)
	{
		$form_config['groups']['traveler_details_'.$type_name]['elements'] += array(
			$type_name.'_'.$i.'_name' => array('text', array(
				'label' => $i.' Name',
				'class' => 'floatblock full-width',
				'required' => FALSE,
			)),
			$type_name.'_'.$i.'_uin' => array('text', array(
				'label' => $i.' UIN',
				'class' => 'floatblock full-width',
				'required' => FALSE,
			)),
			$type_name.'_'.$i.'_emergcontact' => array('text', array(
				'label' => $i.' Emerg. Contact Name',
				'class' => 'floatblock full-width',
				'required' => FALSE,
			)),
			$type_name.'_'.$i.'_emergphone' => array('text', array(
				'label' => $i.' Emerg. Contact Phone',
				'class' => 'floatblock full-width',
				'required' => FALSE,
			)),
		);
	}
}

return $form_config;