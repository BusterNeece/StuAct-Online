<?php
/**
 * Forms Center
 */

use \Entity\Organization;
use \Entity\User;
use \Entity\TravelForm;

class Form_Travel_ManageController extends \DF\Controller\Action
{
	public function permissions()
	{
		return $this->acl->isAllowed('admin_forms_cirt');
	}

	public function indexAction()
	{
		$view = $this->_getParam('view', 'today');
		$this->view->view = $view;

		$qb = $this->em->createQueryBuilder()
			->select('tf.id, tf.password, tf.timestamp, tf.index_name, tf.index_purpose, tf.index_startdate, tf.index_enddate')
			->from('Entity\TravelForm', 'tf')
			->where('tf.is_archived = 0')
			->orderBy('tf.index_startdate', 'DESC');
		
		if ($view == "today")
		{
			$today_start = mktime(0, 0, 0);
			$today_end = mktime(23, 59, 59);

			$qb->andWhere('tf.index_startdate < :today_end AND tf.index_enddate > :today_start')
				->setParameter('today_start', mktime(0, 0, 0))
				->setParameter('today_end', mktime(23, 59, 59));
		}
		else if ($view == "search")
		{
			if (isset($_GET['q']))
			{
				$this->redirectFromHere(array('q' => $_GET['q']));
				return;
			}

			$q = $this->_getParam('q');
			$this->view->query = $q;

			if (empty($q))
			{
				// Show no results until query is submitted.
				$qb->andWhere('1 != 1');
			}
			else
			{
				$qb->andWhere('(tf.password LIKE :q OR tf.index_name LIKE :q OR tf.index_purpose LIKE :q)')
					->setParameter('q', '%'.$q.'%');
			}
		}
		
		$page = $this->_getParam('page', 1);
		$pager = new \DF\Paginator\Doctrine($qb, $page);
		$this->view->pager = $pager;
	}

	public function viewAction()
	{
		$id = (int)$this->_getParam('id');
		$record = TravelForm::find($id);

		$this->view->record = $record;

		$form = new \StuAct\Form;

		$contact_form_config = $this->current_module_config->forms->step_contact->toArray();
		$contact_form = new \StuAct\Form\SubForm($contact_form_config);
		$form->addSubForm($contact_form, 'contact');

		$attendees_form_config = $this->current_module_config->forms->step_attendees->toArray();
		$attendees_form = new \StuAct\Form\SubForm($attendees_form_config);
		$form->addSubForm($attendees_form, 'attendees');

		$form->populate((array)$record->data);
		$this->view->form = $form;
	}

	public function lookupAction()
	{
		$this->view->view = "lookup";

		if ($this->_hasParam('uin'))
		{
			$uin = trim($this->_getParam('uin'));
			$this->view->uin = $uin;

			$compass = new \DF\Service\DoitApi;
			$record_raw = $compass->getStudentById($uin);

			if ($record_raw)
			{
				$record = array();
				$fields = $this->config->services->doitapi->student_record_fields->toArray();

				foreach($fields as $db_name => $full_name)
				{
					if (!empty($record_raw[$db_name]))
					{
						$record_value = $record_raw[$db_name];
						if (stristr($db_name, 'date'))
							$record_value = date('m/d/Y', $record_value);

						$record[$full_name] = $record_value;
					}
				}

				$this->view->record = $record;
			}
		}
	}
}