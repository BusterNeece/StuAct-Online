<?php
/**
 * Forms Center
 */

use \Entity\Organization;
use \Entity\User;

use \Entity\TravelForm;
use \Entity\TravelFormSession;

// Force mail delivery even on development.
define('DF_FORCE_MAIL', TRUE);

class Form_Travel_IndexController extends \DF\Controller\Action
{
	public function permissions()
	{
		// No login required.
		return TRUE;
	}

	public function indexAction()
	{
		if ($this->auth->isLoggedIn())
		{
			$user = $this->auth->getLoggedInUser();

			$orgs = $this->em->createQuery('SELECT o FROM Entity\Organization o JOIN o.officer_positions oo WHERE oo.user_id = :user_id ORDER BY o.name ASC')
				->setParameter('user_id', $user->id)
				->getArrayResult();
			$this->view->orgs = $orgs;

			$forms = $this->em->createQuery('SELECT tf FROM Entity\TravelForm tf WHERE tf.user_id = :user_id ORDER BY tf.index_startdate DESC')
				->setParameter('user_id', $user->id)
				->getArrayResult();
			$this->view->forms = $forms;
			return;
		}
		else
		{
			$this->render('index_anonymous');
			return;
		}
	}

	public function createAction()
	{
		$record = new TravelForm;
		$app_data = array();

		if ($this->_hasParam('org_id'))
		{
			$org = Organization::find($this->_getParam('org_id'));
			$record->organization = $org;

			$app_data['org_name'] = $org->org_name;
		}
		
		if ($this->auth->isLoggedIn())
		{
			$user = $this->auth->getLoggedInUser();
			$record->user = $user;

			$app_data['org_contact_email'] = $user->email;
		}

		$record->data = $app_data;
		$record->save();
		
		$this->redirectFromHere(array('action' => 'edit', 'code' => $record->password, 'org_id' => NULL));
		return;
	}

	public function viewAction()
	{
		if ($_GET)
			$this->redirectFromHere($_GET);

		$code = $this->_getParam('code');
		$record = TravelForm::getRepository()->findOneBy(array('password' => $code));

		if (!($record instanceof TravelForm))
			throw new \DF\Exception\DisplayOnly('Form not found!');
		
		$form = new \DF\Form($this->current_module_config->forms->travel_form->form);
		$form->populate((array)$record->data);

		$this->view->form = $form;
		$this->view->record = $record;
	}

	public function editAction()
	{
		if ($_GET)
			$this->redirectFromHere($_GET);

		$code = $this->_getParam('code');
		$record = TravelForm::getRepository()->findOneBy(array('password' => $code));
		$this->view->record = $record;

		if (!($record instanceof TravelForm))
			throw new \DF\Exception\DisplayOnly('Form not found!');

		$step = $this->_getParam('step', 'start');
		switch($step)
		{
			case 'start':
			case 'contact':
				$form = new \DF\Form($this->current_module_config->forms->step_contact);
				$form->populate((array)$record->data);
				$this->view->form = $form;

				if (!empty($_POST) && $form->isValid($_POST))
				{
					$data = $form->getValues();
					$record->addData($data);
					$record->save();

					$this->redirectFromHere(array('step' => 'sessions'));
					return;
				}

				$this->render('edit_contact');
				return;
			break;

			case 'sessions':
				$form = new \DF\Form($this->current_module_config->forms->step_session);
				$this->view->form = $form;

				$command = $this->_getParam('command', '');
				if (!empty($_POST) && $form->isValid($_POST))
				{
					$data = $form->getValues();

					$session = new TravelFormSession;
					$session->form = $record;
					$session->fromArray($data);
					$session->save();

					$record->reloadIndex();

					$this->redirectHere();
					return;
				}
				else if ($command == 'delete')
				{
					$id = (int)$this->_getParam('id');
					foreach($record->sessions as $session)
					{
						if ($session->id == $id)
							$session->delete();
					}

					$this->redirectFromHere(array('command' => NULL, 'id' => NULL));
					return;
				}
				else if ($command == 'next')
				{
					if (count($record->sessions) == 0)
					{
						$this->alert('<b>You have not entered any travel dates!</b><br>Please enter at least one travel date before continuing.', 'red');
						$this->redirectFromHere(array('command' => NULL));
						return;
					}
					else
					{
						$this->redirectFromHere(array('step' => 'attendees', 'command' => NULL));
						return;
					}
				}

				$this->render('edit_sessions');
				return;
			break;

			case 'attendees':
				$form = new \DF\Form($this->current_module_config->forms->step_attendees);
				$form->populate((array)$record->data);
				$this->view->form = $form;

				if (!empty($_POST) && $form->isValid($_POST))
				{
					$data = $form->getValues();

					$uploaded_files = $form->processFiles('travel_form', $record->password);
					foreach($uploaded_files as $element_name => $element_files)
					{
						$data[$element_name] = $element_files;
					}

					$record->addData($data);
					$record->save();

					$always_notify = $this->current_module_config->forms->travel_form->notify->toArray();

					$user = $this->auth->getLoggedInUser();
					$data = (array)$record->data;

					\DF\Messenger::send(array(
						'to'		=> array_merge(array($data['org_contact_email'], $data['advisor_email'], $user->email), $always_notify),
						'subject'	=> 'Travel Information Form - '.$record->index_name.' - '.date('m/d/Y', $record->index_startdate).' to '.date('m/d/Y', $record->index_enddate),
						'module'	=> $this->_getModuleName(),
						'template'	=> 'notification',
						'vars'		=> array(
							'record'	=> $record,
							'form'		=> $form->renderMessage(),
						),
					));

					$this->view->record = $record;
					$this->render('edit_success');
					return;
				}

				$this->render('edit_attendees');
				return;
			break;
		}
	}

}