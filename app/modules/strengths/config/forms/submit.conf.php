<?php
/**
 * Strengths submit form.
 */

$module_config = \Zend_Registry::get('module_config');
$strength_types = $module_config['strengths']->general->types->toArray();
unset($strength_types[0]);

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'groups'		=> array(

			'personal_info' => array(
				'legend' => 'Personal Information',
				'elements' => array(
					
					'firstname' => array('text', array(
						'label' => 'First Name',
						'class' => 'half-width',
						'required' => true,
					)),
					'lastname' => array('text', array(
						'label' => 'Last Name',
						'class' => 'half-width',
						'required' => true,
					)),
					'status' => array('radio', array(
						'label' => 'TAMU Status',
						'multiOptions' => array(
							'Undergraduate'		=> 'Undergraduate Student',
							'Graduate'			=> 'Graduate Student',
							'Faculty'			=> 'Faculty',
							'StuActStaff'		=> 'Student Activities Staff',
							'SAStaff'			=> 'Student Affairs Staff',
							'Staff'				=> 'Other Staff',
							'FormerStudent'		=> 'Former Student',
							'Other'				=> 'Other',
						),
					)),
					'email' => array('text', array(
						'label' => 'E-mail Address',
						'class' => 'half-width',
						'validators' => array('EmailAddress'),
						'required' => true,
					)),

				),
			),

			'seminar_info' => array(
				'legend' => 'StrengthsQuest Details',
				'elements' => array(
					
					'seminar_id' => array('select', array(
						'label' => 'Seminar Name (if applicable)',
						'description' => 'If you are required to submit your Strengths to receive credit for a course, seminar, or workshop, you must choose the corresponding seminar from this list. If the seminar name is not listed, please contact the Department of Student Activities.',
						'multiOptions' => \Entity\StrengthsSeminar::fetchSelect(TRUE),
					)),

					'strength1' => array('select', array(
						'label' => 'Strength 1',
						'multiOptions' => $strength_types,
						'required' => true,
					)),
					'strength2' => array('select', array(
						'label' => 'Strength 2',
						'multiOptions' => $strength_types,
						'required' => true,
					)),
					'strength3' => array('select', array(
						'label' => 'Strength 3',
						'multiOptions' => $strength_types,
						'required' => true,
					)),
					'strength4' => array('select', array(
						'label' => 'Strength 4',
						'multiOptions' => $strength_types,
						'required' => true,
					)),
					'strength5' => array('select', array(
						'label' => 'Strength 5',
						'multiOptions' => $strength_types,
						'required' => true,
					)),

				),
			),

			'submit_group' => array(
				'elements' => array(
					'submit_btn' => array('submit', array(
						'type'	=> 'submit',
						'label'	=> 'Submit Strengths',
						'helper' => 'formButton',
						'class' => 'ui-button',
					)),
				),
			),
		),
	),
);