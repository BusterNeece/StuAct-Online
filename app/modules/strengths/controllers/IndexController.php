<?php
use \Entity\StrengthsSeminar;
use \Entity\StrengthsSubmission;

class Strengths_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        return true;
    }
    
    public function indexAction()
    {
        $form_config = $this->current_module_config->forms->submit->form->toArray();
        $form = new \StuAct\Form($form_config);

        if ($this->auth->isLoggedIn())
        {
            $user = $this->auth->getLoggedInUser();
            $form->setDefaults($user->toArray(TRUE, TRUE));
        }
        else
        {
            $this->alert('<b>You are not currently logged in!</b><br>If you log in with your NetID, your strengths will be saved to your personal profile. Click the link at the top of the page to log in with your NetID.', 'yellow');
        }

        if ($_POST && $form->isValid($_POST))
        {
            $data = $form->getValues();

            if ($data['seminar_id'])
            {
                $strength_types = $this->current_module_config->general->types->toArray();
                $record_data = $data;

                for($i = 1; $i <= 5; $i++)
                {
                    $record_data['strength'.$i] = $strength_types[$record_data['strength'.$i]];
                }

                $record = new StrengthsSubmission;
                $record->fromArray($record_data);
                $record->save();
            }

            if ($this->auth->isLoggedIn())
            {
                $user = $this->auth->getLoggedInUser();
                $user->fromArray($data);
                $user->save();
            }

            $this->alert('<b>Strengths submitted successfully.</b>', 'green');
            $this->redirectHere();
            return;
        }

        $this->view->headTitle('Submit Strengths');
        $this->renderForm($form);
    }
}