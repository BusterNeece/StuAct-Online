<?php
namespace DF\Validate;
class Future extends \Zend_Validate_Abstract
{
	const DATE_NOT_SPECIFIED = 'dateNotSpecified';
    const DATE_NOT_IN_RANGE = 'dateNotInRange';
    
    protected $_messageTemplates = array(
		self::DATE_NOT_SPECIFIED => "No date was specified.",
        self::DATE_NOT_IN_RANGE => "Please provide a date in the future.",
    );
    
    public function isValid($value)
    {
		$this->_setValue($value);
		
		if ($value == 0)
		{
			$this->_error(self::DATE_NOT_SPECIFIED);
			return false;
		}
		else if ($value <= time())
		{
            $this->_error(self::DATE_NOT_IN_RANGE);
            return false;
        }

        return true;
    }
}