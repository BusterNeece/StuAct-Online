<?php
namespace DF\View\Helper;
class PubNub extends \Zend_View_Helper_Abstract
{
	public function pubNub()
    {
		static $is_attached;
		
		if (!$is_attached)
		{
			$config = \Zend_Registry::get('config');
			$settings = $config->services->pubnub->toArray();
			
			$pubnub_settings = array(
				'publish_key' 		=> $settings['pub_key'],
				'subscribe_key' 	=> $settings['sub_key'],
				'origin'			=> 'pubsub.pubnub.com',
				'ssl'				=> (DF_IS_SECURE) ? true : false,
			);
			
			if (DF_IS_SECURE)
				$cdn_url = 'https://pubnub.a.ssl.fastly.net/pubnub-3.4.min.js';
			else
				$cdn_url = 'http://cdn.pubnub.com/pubnub-3.4.min.js';
			
			$this->view->headScript()->appendFile($cdn_url);
			$this->view->headScript()->appendScript('
				var pubnub = PUBNUB.init('.json_encode($pubnub_settings).');
				$.PUBNUB = pubnub;
			');
			
			$is_attached = TRUE;
		}
    }
}