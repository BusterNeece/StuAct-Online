<?php
/**
 * General configuration file.
 */

define('DF_OPT_NO',	0);
define('DF_OPT_YES', 1);

// General - Time Ranges
define('DF_ONE_HOUR', 3600);
define('DF_ONE_DAY', DF_ONE_HOUR*24);
define('DF_ONE_WEEK', DF_ONE_DAY*7);
define('DF_ONE_MONTH', DF_ONE_DAY*30);

// General - Months
define('DF_MONTH_JANUARY', 	1);
define('DF_MONTH_FEBRUARY', 	2);
define('DF_MONTH_MARCH', 		3);
define('DF_MONTH_APRIL', 		4);
define('DF_MONTH_MAY', 		5);
define('DF_MONTH_JUNE', 		6);
define('DF_MONTH_JULY', 		7);
define('DF_MONTH_AUGUST', 		8);
define('DF_MONTH_SEPTEMBER', 	9);
define('DF_MONTH_OCTOBER', 	10);
define('DF_MONTH_NOVEMBER', 	11);
define('DF_MONTH_DECEMBER', 	12);

return array(
	/* General: Options */
	'opts_yesno'		=> array(
		DF_OPT_NO			=> 'No',
		DF_OPT_YES			=> 'Yes',
	),
	
	/* General: Genders */
	'opts_gender'		=> array(
		'M'		=> 'Male',
		'F'		=> 'Female',
	),
	
	/* General: Months */
	'months'			=> array(
		1	=> 'January',
		2	=> 'February',
		3	=> 'March',
		4	=> 'April',
		5	=> 'May',
		6	=> 'June',
		7	=> 'July',
		8	=> 'August',
		9	=> 'September',
		10	=> 'October',
		11	=> 'November',
		12	=> 'December',
	),
	
	/* General: States */
	'states'			=> array(
		'AK' => 'Alaska',
		'AL' => 'Alabama',
		'AR' => 'Arkansas',
		'AS' => 'American Samoa',
		'AZ' => 'Arizona',
		'CA' => 'California',
		'CO' => 'Colorado',
		'CT' => 'Connecticut',
		'DC' => 'D.C.',
		'DE' => 'Delaware',
		'FL' => 'Florida',
		'FM' => 'Micronesia',
		'GA' => 'Georgia',
		'GU' => 'Guam',
		'HI' => 'Hawaii',
		'IA' => 'Iowa',
		'ID' => 'Idaho',
		'IL' => 'Illinois',
		'IN' => 'Indiana',
		'KS' => 'Kansas',
		'KY' => 'Kentucky',
		'LA' => 'Louisiana',
		'MA' => 'Massachusetts',
		'MD' => 'Maryland',
		'ME' => 'Maine',
		'MH' => 'Marshall Islands',
		'MI' => 'Michigan',
		'MN' => 'Minnesota',
		'MO' => 'Missouri',
		'MP' => 'Marianas',
		'MS' => 'Mississippi',
		'MT' => 'Montana',
		'NC' => 'North Carolina',
		'ND' => 'North Dakota',
		'NE' => 'Nebraska',
		'NH' => 'New Hampshire',
		'NJ' => 'New Jersey',
		'NM' => 'New Mexico',
		'NV' => 'Nevada',
		'NY' => 'New York',
		'OH' => 'Ohio',
		'OK' => 'Oklahoma',
		'OR' => 'Oregon',
		'PA' => 'Pennsylvania',
		'PR' => 'Puerto Rico',
		'PW' => 'Palau',
		'RI' => 'Rhode Island',
		'SC' => 'South Carolina',
		'SD' => 'South Dakota',
		'TN' => 'Tennessee',
		'TX' => 'Texas',
		'UT' => 'Utah',
		'VA' => 'Virginia',
		'VI' => 'Virgin Islands',
		'VT' => 'Vermont',
		'WA' => 'Washington',
		'WI' => 'Wisconsin',
		'WV' => 'West Virginia',
		'WY' => 'Wyoming',
	),
);