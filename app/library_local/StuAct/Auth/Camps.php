<?php
namespace StuAct\Auth;

use \Entity\CampsFormUser;

class Camps extends \DF\Auth\Model
{
	public function __construct()
	{
		parent::__construct();
		$this->_adapter = new Adapter\Camps;
	}

	public function getUser()
    {
        if ($this->_user === NULL)
        {
			$user_id = (int)$this->_session->user_id;
			
			if ($user_id == 0)
			{
				$this->_user = FALSE;
				return false;
			}
			
			$user = CampsFormUser::find($user_id);
			if ($user instanceof CampsFormUser)
			{
				$this->_user = $user;
			}
			else
			{
				unset($this->_session->user_id);
				$this->_user = FALSE;
				$this->logout();
				
				throw new \DF\Exception\InvalidUser;
			}
		}
		
		return $this->_user;
    }

    public function isLoggedIn()
    {
        if( php_sapi_name() == 'cli' )
            return false;
        
        $user = $this->getUser();
        return ($user instanceof CampsFormUser);
    }

    public function setUser(CampsFormUser $user)
    {
		// Prevent any previous identity from being used.
		unset($this->_session->identity);
		
		$this->_session->user_id = $user->id;
		$this->_user = $user;
		return true;
    }

	public function authenticate($credentials = null)
	{
		$this->_adapter->setOptions($credentials);
		$response = $this->_adapter->authenticate();

        if($response->isValid())
        {
			$identity = $response->getIdentity();
			$user = CampsFormUser::find($identity['id']);
			
			$this->_session->identity = $identity;
			$this->_session->user_id = $user['id'];
			$this->_user = $user;
            return true;
        }
        else
        {
            if($response->getCode() != \Zend_Auth_Result::FAILURE_UNCATEGORIZED)
            {
                foreach($response->getMessages() as $message)
                    \DF\Flash::addMessage($message);
            }
            return false;
        }
	}
}