<?php
namespace StuAct\Auth\Adapter;

use \Entity\CampsFormUser;

class Camps extends \DF\Auth\Adapter\Model
{
	public function modelAuth($username, $password)
	{
		return CampsFormUser::authenticate($username, $password);
	}
}