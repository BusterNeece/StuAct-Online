<?php
namespace StuAct\Synchronization;

use \Entity\CnameRequest;

class DomainNames extends TaskAbstract
{
	protected function _run()
	{
		$requests = CnameRequest::fetchArray();
		
		if ($requests)
		{
			\DF\Messenger::send(array(
				'subject'		=> 'CNAME Requests ('.date('m/d/y g:ia').')',
				'module'		=> 'organization',
				'template'		=> 'it/cname_requests',
				'to'			=> 'help@doit.tamu.edu',
				'vars' 			=> array(
					'cname_requests' => $requests,
				),
			));

			$this->em->createQuery('DELETE FROM \Entity\CnameRequest')->execute();
		}
	}
}