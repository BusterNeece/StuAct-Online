<?php
namespace StuAct\Synchronization;

use \Entity\Training;
use \Entity\Organization;

class TrainingExpiration extends TaskAbstract
{
	protected function _run()
	{		
		$org_settings = Organization::loadSettings();
		$timestamp_limit = time() - $org_settings['lifespan_training'];

		// Notify all users with expiring credits.	
		$expiring_users = $this->em->createQuery('SELECT u FROM Entity\User u WHERE u.flag_training != 0 AND u.flag_training_timestamp < :threshold')
			->setParameter('threshold', $timestamp_limit)
			->execute();
		
		foreach($expiring_users as $user)
		{
			if (count($user->officer_positions) > 0)
			{
				\DF\Messenger::send(array(
					'to'		=> $user->email,
					'subject'	=> 'Training Expired',
					'module'	=> 'training',
					'template' 	=> 'expiration',
					'vars'		=> array(
						'training_date' => $user->flag_training_timestamp,
					),
				));
			}
			
			$user->flag_training_notification = time();
			$user->flag_training = 0;
			$this->em->persist($user);
		}
		
		$this->em->flush();
		$this->em->clear();
		
		// Notify all users whose training is about to expire.
		$timestamp_notification = $timestamp_limit + $org_settings['training_notification'];
		
		$notify_users = $this->em->createQuery('SELECT u FROM Entity\User u WHERE u.flag_training != 0 AND u.flag_training_timestamp < :threshold AND u.flag_training_notification < :threshold')
			->setParameter('threshold', $timestamp_notification)
			->execute();
		
		foreach($notify_users as $user)
		{
			if (count($user->officer_positions) > 0)
			{
				\DF\Messenger::send(array(
					'to'		=> $user->email,
					'subject'	=> 'Training Will Expire Soon',
					'module'	=> 'training',
					'template' 	=> 'notification',
					'vars'		=> array(
						'training_date' => $user->flag_training_timestamp,
						'expiration_date' => $user->flag_training_timestamp+$org_settings['lifespan_training'],
					),
				));
			}
			
			$user->flag_training_notification = time();
			$this->em->persist($user);
		}
		
		$this->em->flush();
		$this->em->clear();

		return true;
	}
}