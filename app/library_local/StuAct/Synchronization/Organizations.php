<?php
namespace StuAct\Synchronization;

use \Entity\Organization;

class Organizations extends TaskAbstract
{
	const FRACTION_TO_SYNC = 6;

	protected function _run()
	{
		$random_num = mt_rand(0, (self::FRACTION_TO_SYNC-1));
		$timestamp_pre_threshold = time() - 86400; // One day ago.

		$all_orgs = $this->em->createQuery('SELECT o.id, o.date_last_synchronized FROM Entity\Organization o')
			->getArrayResult();
		$orgs_to_sync = array();

		foreach($all_orgs as $org)
		{
			$sync_random = ($org['id'] % self::FRACTION_TO_SYNC == $random_num);
			$sync_threshold = ($org['date_last_synchronized'] < $timestamp_pre_threshold);

			if ($sync_random || $sync_threshold)
				$orgs_to_sync[] = $org['id'];
		}

		$progress_base = count($orgs_to_sync);
		$i = 0;

		foreach($orgs_to_sync as $org_id)
		{
			$i++;

			$org = Organization::find($org_id);
			$org->synchronize();
			
			$this->em->persist($org);

			if ($i % 10 == 0)
			{
				$this->em->flush();
				$this->em->clear();

				$progress_percent = ($i < $progress_base) ? floor(($i / $progress_base) * 100) : 100;
				$this->setProgress($progress_percent, $i.' of '.$progress_base);
			}
		}

		$this->em->flush();
		$this->em->clear();
		return true;
	}
}