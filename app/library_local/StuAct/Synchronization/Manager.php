<?php
namespace StuAct\Synchronization;

/**
 * Manages the two types of synchronization that take place in StuAct Online:
 *  - Hourly: Runs every hour from approx. 7am to 6pm
 *  - Off-Cycle: Runs every 5 minutes and handles quick out-of-band tasks.
 */ 

class Manager
{
	// Distribution divisor:
	//  - 5 results in 1/5 of organizations being processed per synchronization.
	//  - 4 results in 1/4 of organizations being processed per synchronization.
	const SYNC_DISTRIBUTION = 5;
	const SYNC_MORNING_HOUR = 9;
	const SYNC_AFTERNOON_HOUR = 16;

	public static function run($type = 'offcycle')
	{
		$sync = new self;

		if ($type == "hourly")
			return $sync->hourly();
		else
			return $sync->offcycle();
	}

	public static function solo($class_name)
	{
		$class_name = '\StuAct\Synchronization\\'.$class_name;
		$task_to_run = new $class_name;

		$sync = new self;
		return $sync->runTask($task_to_run);
	}

	/**
	 * Sync instance functions
	 */

	protected $_progress;
	public function __construct()
	{
		set_time_limit(0);

		error_reporting(E_ALL & ~E_NOTICE);
		ini_set('log_errors', 1);
		ini_set('error_log', DF_INCLUDE_TEMP.'/synchronization_'.date('Ymd_his').'.txt');

		if (DF_IS_COMMAND_LINE)
		{
			$progress_adapter = new \Zend_ProgressBar_Adapter_Console;
		}
		else
		{
			$progress_adapter = new \Zend_ProgressBar_Adapter_JsPush();
			$progress_adapter->setUpdateMethodName('Zend_ProgressBar_Update');
			$progress_adapter->setFinishMethodName('Zend_ProgressBar_Finish');
		}

		$this->_progress = new \Zend_ProgressBar($progress_adapter, 0, 100);
	}

	public function getProgressManager()
	{
		return $this->_progress;
	}

	public function hourly()
	{
		$current_hour = (int)date('G');
		$current_minute = (int)date('i');
		$current_hour_code = ($current_minute <= 30) ? $current_hour : $current_hour + 1;
		$current_hour_code = ($current_hour_code == 24) ? 0 : (int)$current_hour_code;

		// Tasks that always run.
		$this->runTask(new Organizations);
		$this->runTask(new TrainingExpiration);
		$this->runTask(new Chat);

		// Both morning and afternoon tasks.
		if ($current_hour_code == self::SYNC_MORNING_HOUR || $current_hour_code == self::SYNC_AFTERNOON_HOUR)
		{

		}

		// Morning-only tasks.
		if ($current_hour_code == self::SYNC_MORNING_HOUR)
		{
			$this->runTask(new TouchNet);
		}

		// Afternoon-only tasks.
		if ($current_hour_code == self::SYNC_AFTERNOON_HOUR)
		{
			$this->runTask(new DomainNames);
			$this->runTask(new Listservs);
		}
	}

	public function offcycle()
	{
		// $this->runTask(new TextRelay);
	}

	public function runTask($obj)
	{
		$obj->setManager($this);
		return $obj->run();
	}
}