<?php
namespace StuAct\Synchronization;

use \Entity\Upay;
use \Entity\StrengthsOrderForm;

class TouchNet extends TaskAbstract
{
	protected function _run()
	{
		$start_time = strtotime('yesterday 12:00 am');
		$end_time = strtotime('today 12:00 am');
		return $this->pushToFamis($start_time, $end_time);
	}

	public function pushToFamis($start_time, $end_time)
	{
		$upay_settings = $this->config->services->upay->toArray();
		
		set_time_limit(300);
		
		$transaction_path = DF_INCLUDE_TEMP.'/TRANSACTIONS-'.date('Y-m-d', $start_time).'.txt';
		$result_data = '';
        
		if ($upay_settings['enable_postback'])
		{
			// Pull transactions from entire previous day.
			$transactions = $this->em->createQuery('SELECT u FROM Entity\Upay u WHERE (u.time_created >= :start AND u.time_created <= :end AND u.payment_status = :success)')
				->setParameter('success', 'success')
				->setParameter('start', $start_time)
				->setParameter('end', $end_time)
				->getArrayResult();

			if (!$transactions)
				return FALSE;

			$transaction_log = array();
			
			foreach($transactions as $trans)
			{
				// Look up corresponding order.
				$sq_id = StrengthsOrderForm::generateOrderId($trans['user_id']);
				$sq_order = StrengthsOrderForm::find($sq_id);
				
				if ($sq_order instanceof StrengthsOrderForm)
				{
					$trans_string = '';
					
					$trans_string .= str_pad($upay_settings['vendor_id'], 10, '0', STR_PAD_LEFT);
					$trans_string .= str_pad($trans['payment_order_id'], 20, '0', STR_PAD_LEFT);
					$trans_string .= date('Ymd', $trans['time_created']);
					$trans_string .= date('His', $trans['time_created']);
					
					$payment_amount = round($trans['payment_amount'] * 100);
					
					$trans_string .= str_pad($payment_amount, 12, '0', STR_PAD_LEFT);
					
					if ($sq_order['is_tax_exempt'] == 1)
					{
						$amount_1 = $payment_amount;
						$amount_2 = 0;
					}
					else
					{
						// Calculate portion of purchase that is tax.
						$total_amount = $trans['payment_amount'];
						
						$amount_before_tax = ($sq_order['price_codes'] * $sq_order['num_codes']) + ($sq_order['price_books'] * $sq_order['num_books']);
						$amount_1 = ceil($amount_before_tax * 100);
						$amount_2 = $payment_amount - $amount_1;
					}

					$trans_string .= str_pad(str_replace('-', '', $upay_settings['famis_account_1']), 20, ' ', STR_PAD_RIGHT);
					$trans_string .= str_pad($amount_1, 12, '0', STR_PAD_LEFT);
					
					$trans_string .= str_pad(str_replace('-', '', $upay_settings['famis_account_2']), 20, ' ', STR_PAD_RIGHT);
					$trans_string .= str_pad($amount_2, 12, '0', STR_PAD_LEFT);
					
					for($i = 0; $i < 18; $i++)
					{
						$trans_string .= str_repeat(' ', 20);
						$trans_string .= str_repeat('0', 12);
					}
					
					if (strlen($trans_string) == 696)
					{
						$transaction_log[] = $trans_string;
					}
				}
			}
				
			$transaction_string = implode("\n", $transaction_log);
			
			if (!empty($transaction_string))
			{
				file_put_contents($transaction_path, $transaction_string);
				$file_path = $transaction_path;
				
				$ch = curl_init();
				$fp = fopen($file_path, 'r');
				
				curl_setopt($ch, CURLOPT_URL, $upay_settings['postback_path'].basename($file_path));
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
				curl_setopt($ch, CURLOPT_FTP_SSL, CURLFTPSSL_TRY);
				curl_setopt($ch, CURLOPT_FTPSSLAUTH, 1);
				curl_setopt($ch, CURLOPT_UPLOAD, 1);
				curl_setopt($ch, CURLOPT_INFILE, $fp);
				curl_setopt($ch, CURLOPT_TRANSFERTEXT, 1);
				curl_setopt($ch, CURLOPT_INFILESIZE, filesize($file_path));
				
				$result = curl_exec($ch);
				$error_no = curl_errno($ch);
				curl_close($ch);
				fclose($fp);		
			}
		}

		return $error_no;
	}
}