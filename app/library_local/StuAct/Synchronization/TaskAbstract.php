<?php
namespace StuAct\Synchronization;

class TaskAbstract
{
	protected $manager;
	protected $em;

	public function __construct($manager = null)
	{
		$this->em = \Zend_Registry::get('em');
		$this->config = \Zend_Registry::get('config');
	}

	public function setManager($manager)
	{
		$this->manager = $manager;
	}

	public function run()
	{
		$this->setProgress(1, 'Starting...');

		try
		{
			$this->_run();
		}
		catch(\Exception $e)
		{
			trigger_error('Exception on '.$e->getFile().' line '.$e->getLine().': '.$e->getMessage(), E_USER_WARNING);
			return false;
		}

		$this->setProgress(100, 'Complete!');
		return true;
	}

	public function setProgress($number, $message)
	{
		$class_name = get_called_class();
		$class_name = substr($class_name, strrpos($class_name, '\\')+1);

		if ($number > 100) $number = 100;
		if ($number < 0) $number = 0;
		
		$progress_meter = $this->manager->getProgressManager();
		$progress_meter->update($number, 'Sync Task '.$class_name.': '.$message);
	}
}