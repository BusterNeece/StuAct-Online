<?php
/**
 * Miscellaneous Utilities Class
 **/

namespace StuAct;

class Utilities
{
	/**
	 * RSS Feed Consumption
	 */
	public static function getNewsFeed($feed_url, $cache_name = NULL, $cache_expires = 900)
	{
		if (!is_null($cache_name))
		{
			$feed_cache = \DF\Cache::get('feed_'.$cache_name);
		}
		
		if (!$feed_cache)
		{
			// Catch the occasional error when the RSS feed is malformed or the HTTP request times out.
			try
			{
				$http_client = \Zend_Feed::getHttpClient();
				$http_client->setConfig(array('timeout' => 60));
				
				$news_feed = new \Zend_Feed_Rss($feed_url);
			}
			catch(Exception $e)
			{
				$news_feed = NULL;
			}
			
			if (!is_null($news_feed))
			{	
				$latest_news = array();
				$article_num = 0;
				
				foreach ($news_feed as $item)
				{
					$article_num++;
					
					// Process categories.
					$categories_raw = (is_array($item->category)) ? $item->category : array($item->category);
					$categories = array();
					
					foreach($categories_raw as $category)
					{
						$categories[] = $category->__toString();
					}
				
					// Process main description.
					$description = trim(strip_tags($item->description(), '<a><b><i><img>')); // Remove extraneous tags.
					$description = preg_replace('/[^(\x20-\x7F)]+/',' ', $description); // Strip "exotic" non-ASCII characters.
					$description = preg_replace('/<a[^(>)]+>read more<\/a>/i', '', $description); // Remove "read more" link.
					
					$news_item = array(
						'num'			=> $article_num,
						'title'			=> $item->title(),
						'timestamp'		=> strtotime($item->pubDate()),
						'description'	=> $description,
						'link'			=> $item->link(),
						'categories'	=> $categories,
					);
					
					$latest_news[] = $news_item;
				}
				
				if (!is_null($cache_name))
				{
					\DF\Cache::set($latest_news, 'feed_'.$cache_name, array('feeds', $cache_name));
				}
			}
		}
		else
		{
			$latest_news = $feed_cache;
		}
		
		return $latest_news;
	}
	
	/**
	 * Text Formatting
	 */
	public static function formatUrl($raw_url)
	{
		if (!empty($raw_url))
		{
			return 'http://'.str_replace('http://', '', $raw_url);
		}
		else
		{
			return '';
		}
	}
	
	/**
	 * Security
	 */
	public static function generatePassword($char_length = 8)
	{
		// String of all possible characters. Avoids using certain letters and numbers that closely resemble others.
		$numeric_chars = str_split('234679');
		$uppercase_chars = str_split('ACDEFGHJKLMNPQRTWXYZ');
		$lowercase_chars = str_split('acdefghjkmnpqrtwxyz');
		
		$chars = array($numeric_chars, $uppercase_chars, $lowercase_chars);
		
		$password = '';
		for($i = 1; $i <= $char_length; $i++)
		{
			$char_array = $chars[$i % 3];
			$password .= $char_array[mt_rand(0, count($char_array)-1)];
		}
		
		return str_shuffle($password);
	}
	
	public static function checkPasswordStrength($pwd)
	{
		$errors = array();
		
		if (strlen($pwd) < 8)
			$errors[] = 'Password is too short. Passwords must be 8 characters long.';
		
		if (strlen($pwd) > 20)
			$errors[] = 'Password is too long. Passwords must be less than 20 characters long.';
		
		if (!preg_match("#[0-9]+#", $pwd))
			$errors[] = 'Password must include at least one number.';
		
		if (!preg_match("#[a-z]+#", $pwd))
			$errors[] = 'Password must include at least one letter.';
		
		if (!preg_match("#[A-Z]+#", $pwd))
			$errors[] = 'Password must include at least one capital letter.';
		
		if (!preg_match("#\W+#", $pwd))
			$errors[] = 'Password must include at least one symbol!';
			
		if (!empty($errors))
		{
			return array(
				'is_valid' 	=> FALSE,
				'errors'	=> $errors,
			);
		}
		else
		{
			return array(
				'is_valid'	=> TRUE,
			);
		}
	}
	
	/**
	 * Time Functions
	 */
	 
	// Retrieve the current fiscal year.
	public static function getFiscalYear($timestamp = NULL)
	{
		$timestamp = (!is_null($timestamp)) ? $timestamp : time();
		
		if ($timestamp != 0)
		{
			$fiscal_year = intval(date('Y', $timestamp));
			$fiscal_month = intval(date('m', $timestamp));
			
			if ($fiscal_month >= 9)
				$fiscal_year++;
			return $fiscal_year;
		}
		return false;
	}
	
	// Get the plain-english value of a given timestamp.
	public static function timeToText($timestamp)
	{
		return self::timeDifferenceText(0, $timestamp);
	}
	
	// Get the plain-english difference between two timestamps.
	public static function timeDifferenceText($timestamp1, $timestamp2)
	{
		$time_diff = abs($timestamp1 - $timestamp2);
		$diff_text = "";
		
		if ($time_diff < 60)
		{
			$time_num = intval($time_diff);
			$time_unit = 'second';
		}
		else if ($time_diff >= 60 && $time_diff < 3600)
		{
			$time_num = round($time_diff / 60, 1);
			$time_unit = 'minute';
		}
		else if ($time_diff >= 3600 && $time_diff < 216000)
		{
			$time_num = round($time_diff / 3600, 1);
			$time_unit = 'hour';
		}
		else if ($time_diff >= 216000 && $time_diff < 10368000)
		{
			$time_num = round($time_diff / 86400);
			$time_unit = 'day';
		}
		else
		{
			$time_num = round($time_diff / 2592000);
			$time_unit = 'month';
		}
		
		$diff_text = $time_num.' '.$time_unit.(($time_num != 1)?'s':'');
		
		return $diff_text;
	}
	
	// Convert one number to a numeric percentage (out of 100) of the second number provided.
	public static function findPercent($num_1, $num_2, $decimal = 0)
	{
		$fraction = $num_1 / $num_2;
		return round($fraction * 100, $decimal);
	}
	
	// Replacement for print_r.
	public static function print_r($var, $return = FALSE, $use_graphical = TRUE)
	{
		$return_value = '<pre>'.print_r($var, TRUE).'</pre>';
		
		if ($return)
			return $return_value;
		else
			echo $return_value;
	}
	
	/*
		Paul's Simple Diff Algorithm v 0.1
		(C) Paul Butler 2007 <http://www.paulbutler.org/>
		May be used and distributed under the zlib/libpng license.
	*/
	
	public static function diff($old, $new){
		foreach($old as $oindex => $ovalue){
			$nkeys = array_keys($new, $ovalue);
			foreach($nkeys as $nindex){
				$matrix[$oindex][$nindex] = isset($matrix[$oindex - 1][$nindex - 1]) ?
					$matrix[$oindex - 1][$nindex - 1] + 1 : 1;
				if($matrix[$oindex][$nindex] > $maxlen){
					$maxlen = $matrix[$oindex][$nindex];
					$omax = $oindex + 1 - $maxlen;
					$nmax = $nindex + 1 - $maxlen;
				}
			}	
		}
		if($maxlen == 0) return array(array('d'=>$old, 'i'=>$new));
		return array_merge(
			self::diff(array_slice($old, 0, $omax), array_slice($new, 0, $nmax)),
			array_slice($new, $nmax, $maxlen),
			self::diff(array_slice($old, $omax + $maxlen), array_slice($new, $nmax + $maxlen)));
	}
	
	public static function htmlDiff($old, $new){
		$diff = self::diff(explode("\n", $old), explode("\n", $new));
		foreach($diff as $k){
			if(is_array($k))
			{
				$ret .= (!empty($k['d'])?"-- ".implode("\n-- ",$k['d'])."\n":'').
					(!empty($k['i'])?"++ ".implode("\n++ ",$k['i'])."\n":'');
			}
			else
			{
				$ret .= $k . "\n";
			}
		}
		return $ret;
	}
	
	/**
	 * Truncate text (adding "..." if needed)
	 */
	public static function truncateText($text, $limit = 80, $pad = '...')
	{
		if (strlen($text) <= $limit)
		{
			return $text;
		}
		else
		{
			$wrapped_text = wordwrap($text, $limit, "{N}", TRUE);
			$shortened_text = substr($wrapped_text, 0, strpos($wrapped_text, "{N}"));
			
			// Prevent the padding string from bumping up against punctuation.
			$punctuation = array('.',',',';','?','!');
			if (in_array(substr($shortened_text, -1), $punctuation))
			{
				$shortened_text = substr($shortened_text, 0, -1);
			}
			
			return $shortened_text.$pad;
		}
	}
	
	/**
	 * Encodes the UIN according to security standards.
	 */
	public static function encodeUIN($uin)
	{
		$config = \Zend_Registry::get('config');
		return sha1($uin.$config->general->encryption_hash_secret);
	}
	
	/**
	 * Strengths text processing.
	 */
	public static function addStrengthText(&$var_to_modify, $origin_var = NULL)
	{
		global $strength_types, $strength_descriptions;
		
		if (is_null($origin_var))
		{
			$origin_var = $var_to_modify;
		}
		
		for($i = 1; $i <= 5; $i++)
		{
			$strength_id = $origin_var['strength'.$i];
			$strength_name = $strength_types[$strength_id];
			
			$var_to_modify['strengths'][$i] = array(
				'name'			=> $strength_name,
				'description'	=> $strength_descriptions[$strength_name],
			);
			$var_to_modify['strength'.$i.'_text'] = $strength_name;
		}
	}
}