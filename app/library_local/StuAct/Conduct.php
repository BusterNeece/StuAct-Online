<?php
/**
 * Static class that facilitates checking of conduct reports.
 */

namespace StuAct;

use \Entity\User;

class Conduct
{	
	// Returns true unless a conduct violation exists.
	public static function checkConduct(User $user)
	{
		$conduct_row = self::fetchConduct($user->uin);
		
		if ($conduct_row)
		{
			if ($conduct_row['effective_start'] <= time() && $conduct_row['effective_end'] >= time())
				return false;
			else
				return true;
		}
		
		return true;
	}
	
	public static function addRecord($uin, $effective_start, $effective_end)
	{
		$record = \Entity\Conduct::getOrCreate($uin);
		$record->effective_start = $effective_start;
		$record->effective_end = $effective_end;
		$record->save();
		
		return $record;
	}
	public static function deleteRecord($uin)
	{
		\Entity\Conduct::clear($uin);
	}
	
	public static function fetchConduct($uin)
	{
		$record = \Entity\Conduct::fetchByUin($uin);
		return ($record instanceof \Entity\Conduct) ? $record : NULL;
	}
}