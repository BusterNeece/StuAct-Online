<?php
namespace StuAct\Organization;

use \Entity\Organization;

class Notification extends OrganizationAbstract
{
	public function notify($notification_type, $vars = array())
	{
		$settings = self::loadSettings();

		$org_settings = Organization::loadSettings();
		$organization_officer_groups = $org_settings['officer_groups'];

		// Compile arrays of special e-mails based on positions.
		$address_group_positions = array(
			'advisors'			=> $organization_officer_groups['advisors'],
			'advisors_and_csl' 	=> array_merge($organization_officer_groups['advisors'], $organization_officer_groups['csl']),
			'leaders'			=> $organization_officer_groups['leaders'],
			'webmasters'		=> array_merge($organization_officer_groups['leaders'], $organization_officer_groups['webmasters']),
			'student_leaders' 	=> $organization_officer_groups['student_leaders'],
			'officers'			=> $organization_officer_groups['officers'],
		);
		
		if (isset($vars['position']))
			$address_group_positions['special'] = array($vars['position']);
		
		$address_groups = array();
		foreach($address_group_positions as $address_group_name => $positions)
		{
			$address_groups[$address_group_name][] = array();

			foreach($this->_org->officer_positions as $position)
			{
				if (in_array($position->position, $positions))
					$address_groups[$address_group_name][] = $position->user->email;
			}
		}

		$notifications = array(
		
			// Enhanced Expectations Annual Reminder
			'enhanced_annual'			=> array(
				'addresses'			=> $settings['enhanced_annual'],
				'template'			=> 'risk/enhanced_annual',
				'subject'			=> 'Enhanced Expectations Annual Reminder',
			),
			
			// Enhanced Expectations File Upload Notification
			'enhanced_upload'			=> array(
				'addresses'			=> $settings['enhanced_annual'],
				'template'			=> 'risk/enhanced_upload',
				'subject'			=> 'Enhanced Expectations File Uploaded',
			),
			
			// Notification to SOFC of Special Profile Question Answers
			'sofc_profile_answers'		=> array(
				'addresses'			=> $settings['sofc_profile_answers'],
				'template'			=> 'finances/profile_answers',
				'subject'			=> 'Organization Banking Off-Campus',
			),
			
			// Warning to SOFC if an upcoming status change might be dependent on manual processing.
			'status_warning_sofc'		=> array(
				'addresses'			=> $settings['status_warning_sofc'],
				'template'			=> 'recognition/status_warning_sofc',
				'subject'			=> 'Status Change Warning - SOFC Signature Card',
			),
			
			// Warning to Recognition if an upcoming status change might be dependent on manual processing.
			'status_warning_rec'		=> array(
				'addresses'			=> $settings['status_warning_rec'],
				'template'			=> 'recognition/status_warning_rec',
				'subject'			=> 'Status Change Warning - Constitution',
			),
						
			// Notify an officer of their addition.
			'member_added'				=> array(
				'addresses'			=> $address_groups['special'],
				'template'			=> 'members/added',
				'subject'			=> 'Added to Organization',
			),
			
			// Notify an officer of their removal.
			'member_removed'			=> array(
				'addresses'			=> $address_groups['special'],
				'template'			=> 'members/removed',
				'subject'			=> 'Removed from Organization',
			),
			
			// Notify an officer who is due for renewal.
			'member_renew' => array(
				'addresses'			=> $address_groups['special'],
				'template'			=> 'members/renew',
				'subject'			=> 'Complete Officer Transition Process',
			),
			
			// Notify someone who is suggested as a successor for an officer position.
			'member_renew_successor' => array(
				'addresses'			=> $vars['email'],
				'template'			=> 'members/renew_successor',
				'subject'			=> 'Submit Request for Officer Position',
			),
			
			// Notify organization leaders of ineligible officer.
			'ineligible_officer'		=> array(
				'addresses'			=> $address_groups['advisors_and_csl'],
				'template'			=> 'members/ineligible_org',
				'subject'			=> 'Organization Officer(s) Ineligible',
			),
			
			// Notify the individual student that they are ineligible.
			'ineligible_individual'		=> array(
				'addresses'			=> $address_groups['special'],
				'template'			=> 'members/ineligible_student',
				'subject'			=> 'Ineligible to Fill Officer Role',
			),

			// Notify organization leaders of resolution of ineligible officer issue.
			'ineligible_resolved'		=> array(
				'addresses'			=> $address_groups['advisors_and_csl'],
				'template'			=> 'members/ineligible_org_resolved',
				'subject'			=> 'Officer Ineligibility Resolved',
			),

			// Notify Recognition team of newly uploaded constitution.
			'new_constitution'			=> array(
				'addresses'			=> $settings['new_constitution'],
				'template'			=> 'constitution/submitted',
				'subject'			=> 'New Constitution Uploaded',
			),
			
			// Notify organization of approved constitution.
			'constitution_approved'		=> array(
				'addresses'			=> $address_groups['leaders'],
				'template'			=> 'constitution/approved',
				'subject'			=> 'Constitution Approved',
			),
			
			// Notify organization of declined constitution.
			'constitution_declined'		=> array(
				'addresses'			=> $address_groups['leaders'],
				'template'			=> 'constitution/declined',
				'subject'			=> 'Constitution Declined',
			),
			
			// Notify organization of a processed SOFC signature card.
			'sofc_sigcard_received'		=> array(
				'addresses'			=> $address_groups['leaders'],
				'template'			=> 'finances/sigcard_received',
				'subject'			=> 'SOFC Signature Card Received',
			),
			
			// Notify advisors of a new leader request.
			'new_leader'				=> array(
				'addresses'			=> $address_groups['advisors'],
				'template'			=> 'members/new_leader',
				'subject'			=> 'New Leader Request',
			),
			
			// Notify organization of a new member request.
			'new_member'				=> array(
				'addresses'			=> $address_groups['student_leaders'],
				'template'			=> 'members/new_officer',
				'subject'			=> 'New Officer Request',
			),
			
			// Notify organization of a status change.
			'status_change'				=> array(
				'addresses'			=> $address_groups['student_leaders'],
				'template'			=> 'recognition/status_change',
				'subject'			=> 'Organization Status Change',
			),
			
			// Notify department staff of a status change.
			'status_change_staff'		=> array(
				'addresses'			=> $settings['status_changes'],
				'template'			=> 'recognition/status_change',
				'subject'			=> 'Organization Status Change',
			),
			
			// Notify organization if it was restricted as a result of losing one of its recognition requirements.
			'restrict_flags'			=> array(
				'addresses'			=> array_merge($address_groups['student_leaders'], $settings['status_changes']),
				'template'			=> 'recognition/restrict_flags',
				'subject'			=> 'Organization Restricted Due to Recognition Requirements',
			),
			
			// Notify SOFC of unrecognized status.
			'sofc_unrecognized'			=> array(
				'addresses'			=> $settings['sofc_unrecognized'],
				'template'			=> 'finances/mailslot_unrecognized',
				'subject'			=> 'Organization Not Recognized',
			),
			
			// Notify SOFC of mail slot being made available.
			'sofc_mailslot_available'	=> array(
				'addresses'			=> $settings['sofc_unrecognized'],
				'template'			=> 'finances/mailslot_available',
				'subject'			=> 'Mail Slot Unassigned',
			),			
			
			// Notification to student leaders that the public profile should be updated soon.
			'profile_needs_update'		=> array(
				'addresses'			=> $address_groups['leaders'],
				'template'			=> 'recognition/profile_needs_update',
				'subject'			=> 'Update Public Profile Soon',
			),
			
			// Cycle change approaching message.
			'cycle_change_approaching'	=> array(
				'addresses'			=> $address_groups['leaders'],
				'template'			=> 'recognition/cycle_change_approaching',
				'subject'			=> 'Early Bird Recognition Open',
			),
			
			// Recognition cycle change notification.
			'cycle_change'				=> array(
				'addresses'			=> $address_groups['leaders'],
				'template'			=> 'recognition/cycle_change',
				'subject'			=> 'Recognition Cycle Change',
			),
			
			// Restriction notification.
			'autorestrict'				=> array(
				'addresses'			=> $address_groups['leaders'],
				'template'			=> 'recognition/restrict',
				'subject'			=> 'Organization Restricted',
			),
			
			// Pre-restriction warning.
			'autorestrict_warning'		=> array(
				'addresses'			=> $address_groups['leaders'],
				'template'			=> 'recognition/restrict_warning',
				'subject'			=> 'Warning! Restriction Approaching',
			),
			
			// Suspension notification.
			'autosuspend'				=> array(
				'addresses'			=> $address_groups['leaders'],
				'template'			=> 'recognition/suspend',
				'subject'			=> 'Organization Suspended',
			),
			
			// Pre-suspension warning.
			'autosuspend_warning'		=> array(
				'addresses'			=> $address_groups['leaders'],
				'template'			=> 'recognition/suspend_warning',
				'subject'			=> 'Warning! Suspension Approaching',
			),
			
			// Unrecognition notification.
			'autounrecognize'			=> array(
				'addresses'			=> $address_groups['leaders'],
				'template'			=> 'recognition/unrecognize',
				'subject'			=> 'Organization Recognition Revoked',
			),
			
			// Pre-unrecognition warning.
			'autounrecognize_warning'	=> array(
				'addresses'			=> $address_groups['leaders'],
				'template'			=> 'recognition/unrecognize_warning',
				'subject'			=> 'Warning! Revocation of Recognized Status Approaching',
			),
			
			// Web/e-mail account information change.
			'itchange'					=> array(
				'addresses'			=> $address_groups['webmasters'],
				'template'			=> 'it/account_change',
				'subject'			=> 'Web/E-mail Account Information',
			),

			// Web/e-mail account information change.
			'it_new_account' 			=> array(
				'addresses'			=> $vars['email'],
				'template'			=> 'it/new_account',
				'subject'			=> 'New Organization Web Site Request',
			),
			
			// Getting Started with Drupal guide.
			'it_drupal'					=> array(
				'addresses'			=> $address_groups['webmasters'],
				'template'			=> 'it/drupal',
				'subject'			=> 'Getting Started with Drupal',
			),
			
			// Drupal uninstallation notice
			'it_drupal_uninstalled'		=> array(
				'addresses'			=> $address_groups['webmasters'],
				'template'			=> 'it/drupal_uninstalled',
				'subject'			=> 'Drupal Uninstalled from Web Space',
			),
			
			// Notification of web/e-mail account rejection due to taken address.
			'it_reject_taken'			=> array(
				'addresses'			=> $address_groups['webmasters'],
				'template'			=> 'it/request_reject_taken',
				'subject'			=> 'Web/E-mail Account Request Rejected: Address Taken',
			),
			
			// New SOFC statement available.
			'sofcstatement'				=> array(
				'addresses'			=> $address_groups['leaders'],
				'template'			=> 'finances/statement',
				'subject'			=> 'New SOFC Statements Available',
			),

			// SOFC Audit notification.
			'sofc_audit'				=> array(
				'addresses'			=> $address_groups['leaders'],
				'template'			=> 'finances/audit',
				'subject'			=> 'SOFC Audit Notification',
			),

			// Requests for follow-up from leadership.
			'request_follow_up'			=> array(
				'addresses'			=> $address_groups['student_leaders'],
				'template'			=> 'members/follow_up',
				'subject'			=> 'Visitor Follow-Up Request',
			),
			
			// Pre-Event Planning Form advisor approval e-mail.
			'preeventplanning_advisor'	=> array(
				'addresses'			=> $address_groups['advisors'],
				'module'			=> 'form_preevent',
				'template'			=> 'approval',
				'subject'			=> 'Pre-Event Planning Form Requires Approval',
			),
			
			// Pre-Event Planning Form advisor approval e-mail.
			'preeventplanning_change'	=> array(
				'addresses'			=> $address_groups['advisors'],
				'module'			=> 'form_preevent',
				'template'			=> 'change',
				'subject'			=> 'Pre-Event Planning Form Status Updated',
			),
			
			// Pre-Event Planning Form declined.
			'preeventplanning_decline'	=> array(
				'addresses'			=> (isset($vars['email'])) ? $vars['email'] : $address_groups['student_leaders'],
				'module'			=> 'form_preevent',
				'template'			=> 'decline',
				'subject'			=> 'Pre-Event Planning Form Requires Revision',
			),
			
			// Pre-Event Planning Form post-event assessment.
			'preeventplanning_assessment' => array(
				'addresses'			=> ($vars['email']) ? $vars['email'] : $address_groups['student_leaders'],
				'module'			=> 'form_preevent',
				'template'			=> 'assessment',
				'subject'			=> 'Pre-Event Planning Form Assessment Reminder',
			),
		);
		
		if (isset($notifications[$notification_type]))
		{
			$notification = $notifications[$notification_type];

			if ($notification['addresses'])
			{
				$vars = array_merge((array)$vars, array(
					'additional_info' => $vars,
					'org'	=> $this->_org,
					'organization' => $this->_org,
					'org_id' => $this->_org_id,
				));

				$email_to = $notification['addresses'];
				if (!is_array($email_to))
					$email_to = array($email_to);

				// $email_to[] = 'bneece@doit.tamu.edu';

				$email_config = array(
					'to'		=> $email_to,
					'from'		=> 'recognition@stuact.tamu.edu',
					'subject'	=> $notification['subject'],
					'template'	=> $notification['template'],
					'module'	=> (isset($notification['module'])) ? $notification['module'] : 'organization',
					'reply_to'	=> (isset($notification['reply_to'])) ? $notification['reply_to'] : NULL,
					'vars'		=> $vars,
				);
				\DF\Messenger::send($email_config);
			}
		}
	}

	// Static settings manager function.
	public static function loadSettings()
	{
		static $settings;
        
        if (!$settings)
        {
            $config = \Zend_Registry::get('config');
            $settings = $config->email->toArray();
        }
        
        return $settings;
	}
}