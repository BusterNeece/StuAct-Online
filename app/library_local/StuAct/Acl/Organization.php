<?php
namespace StuAct\Acl;

class Organization
{
	public static function getInstance()
	{
		static $instance;
		if ($instance === NULL)
			$instance = new static;
		
		return $instance;
	}

	protected $_settings;
	protected $_user_permissions;

	public function __construct()
	{
		$this->_settings = \Entity\Organization::loadSettings();
		$this->_user_permissions = array();
	}
	
	public function canModifyPosition($position_num, \Entity\Organization $org = null, \Entity\User $user = null)
	{
		$auth = \Zend_Registry::get('auth');
		$acl = \Zend_Registry::get('acl');

		// Can optionally pass an OrganizationOfficer instance for both pieces of info.
		if ($position_num instanceof \Entity\OrganizationOfficer)
		{
			$org = $position_num->organization;
			$position_num = $position_num->position;
		}
		
		if ($user === NULL)
			$user = $auth->getLoggedInUser();
		
		if (!($user instanceof \Entity\User) || !($org instanceof \Entity\Organization))
			return false;
		
		if ($acl->userAllowed('admin_orgs', $user))
			return true;
		
		$position = $org->getUserPosition($user);
		
		if (!$position)
			return false;
		
		if ($position->position == $position_num)
			return true;
			
		$is_advisor = $position->isInGroup('advisors');
		$is_leader = $position->isInGroup('leaders');
		$is_officer = $position->isInGroup('officers');
		
		// Advisors can manage themselves or Chief Student Leaders.
		// Leaders and advisors can manage any other positions (including the Treasurer position).
		
		if (in_array($position_num, $this->_settings['officer_groups']['advisors']) || $position_num == ORG_OFFICER_CHIEF_STUDENT_LEADER)
			return $is_advisor;
		else
			return ($is_advisor || $is_leader);
	}

	public function isAllowed($priv_name, \Entity\Organization $org)
	{
		$auth = \Zend_Registry::get('auth');
		$permissions = $this->_settings['permissions'];

		if ($auth->isLoggedIn())
		{
			$user = $auth->getLoggedInUser();
			return $this->isUserAllowed($priv_name, $user, $org);
		}
		else
		{
			return (in_array($priv_name, $permissions['anonymous']));
		}
	}

	public function isUserAllowed($priv_name, \Entity\User $user, \Entity\Organization $org)
	{
		$permissions = $this->_settings['permissions'];

		// Always grant anonymous permissions.
		if (in_array($priv_name, $permissions['anonymous']))
			return TRUE;

		// Check global administrator access.
		$acl = \Zend_Registry::get('acl');
		
		if ($acl->userAllowed(array('admin_orgs', 'admin_sofc'), $user))
			return TRUE;

		// Special IT administrator access.
		if ($acl->userAllowed('administer web sites', $user) && in_array($priv_name, $permissions['it']))
			return TRUE;
		
		// Check batch groups.
		$batch_groups = $user->batchgroups;
		$is_batch_manager = FALSE;

		if ($batch_groups)
		{
			foreach($batch_groups as $group)
			{
				if ($group->organizations->contains($org))
					$is_batch_manager = TRUE;
			}
		}

		// Check batch manager and limited admin.
		if (($acl->userAllowed(array('staff_staffprofiles','admin_orgs_limited'), $user) || $is_batch_manager) && !in_array($priv_name, $permissions['admin']))
			return TRUE;
		
		$org_permissions = $this->getUserPermissions($user);
		$org_id = (int)$org->id;

		if (isset($org_permissions[$org_id]) && isset($org_permissions[$org_id][$priv_name]))
			return TRUE;
		
		return FALSE;
	}

	public function getUserPermissions(\Entity\User $user)
	{
		if ($this->_user_permissions[$user->id] === NULL)
		{
			$settings = \Entity\Organization::loadSettings();
			$positions = $user->officer_positions;
			$permissions = array();

			if ($positions)
			{
				foreach($positions as $position)
				{
					$privs = $position->getPermissions();
					$org_perms = array_keys($privs['granted']);
					
					$permissions[$position->org_id] = array_combine($org_perms, $org_perms);
				}
			}

			$this->_user_permissions[$user->id] = $permissions;
		}

		return $this->_user_permissions[$user->id];
	}

	public function checkPermission($action, \Entity\Organization $org)
	{
		$auth = \Zend_Registry::get('auth');
		
		if (!$this->isAllowed($action, $org))
		{
			if (!$auth->isLoggedIn())
				throw new \DF\Exception\NotLoggedIn();
			else
				throw new \DF\Exception\PermissionDenied();
		}
	}
}