<?php
/**
 * OrgMatch Manager Class
 **/

namespace StuAct;

use \Entity\Organization;

class OrgMatch
{
	public static function match($orgmatch_profile)
	{
		$em = \Zend_Registry::get('em');
		$all_orgs = $em->createQuery('SELECT o FROM \Entity\Organization o WHERE o.status >= :min_status ORDER BY o.status DESC, o.name ASC')
			->setParameter('min_status', ORG_STATUS_RESTRICTED)
			->getArrayResult();
		
		/*
		 * Matching Algorithm
		 */
		
		foreach($all_orgs as $org)
		{
			$org_score = 0;
					
			if ($org['orgmatch_profile'])
			{
				$org_profile = $org['orgmatch_profile'];
				
				// Personal classification
				if ($orgmatch_profile['personal_classification'] == 'undergraduate')
				{
					if (is_array($org_profile['membership_policies']) && in_array('undergraduate', $org_profile['membership_policies']))
					{
						$org_score += 20;
					}
				}
				else if ($orgmatch_profile['personal_classification'] == 'graduate')
				{
					if (is_array($org_profile['membership_policies']) && in_array('graduate', $org_profile['membership_policies']))
					{
						$org_score += 20;
					}
				}
				else if ($orgmatch_profile['personal_classification'] == 'faculty' || $orgmatch_profile['personal_classification'] == 'staff')
				{
					if (is_array($org_profile['membership_policies']) && in_array('facultystaff', $org_profile['membership_policies']))
					{
						$org_score += 20;
					}
				}
				else if ($orgmatch_profile['personal_classification'] == 'community')
				{
					if (is_array($org_profile['membership_policies']) && in_array('community', $org_profile['membership_policies']))
					{
						$org_score += 20;
					}
				}
				
				// Academic affiliation
				
				if ($orgmatch_profile['affiliation_academic'] && $orgmatch_profile['affiliation_academic'] != 'none')
				{
					if ($org_profile['affiliation_academic'] == $orgmatch_profile['affiliation_academic'])
					{
						$org_score += 10;
					}
				}
				
				// Non-academic affiliation
				
				if (is_array($orgmatch_profile['affiliation_nonacademic']))
				{
					foreach($orgmatch_profile['affiliation_nonacademic'] as $affiliation_nonacademic)
					{
						if($org_profile['affiliation_nonacademic'] == $affiliation_nonacademic)
						{
							$org_score += 10;
						}
					}
				}
				
				// Public options
				if ($orgmatch_profile['public_options']['membership_small'])
				{
					if ($org_profile['membership_population'] == '30' || $org_profile['membership_population'] == '50')
					{
						$org_score += $orgmatch_profile['public_options']['membership_small'] * 5;
					}
				}
				if ($orgmatch_profile['public_options']['membership_large'])
				{
					if ($org_profile['membership_population'] != '30' && $org_profile['membership_population'] != '50')
					{
						$org_score += $orgmatch_profile['public_options']['membership_large'] * 5;
					}
				}
				if ($orgmatch_profile['public_options']['timerequirement_small'])
				{
					if ($org_profile['membership_timerequirement'] == '3' || $org_profile['membership_timerequirement'] == '5')
					{
						$org_score += $orgmatch_profile['public_options']['timerequirement_small'] * 5;
					}
				}
				if ($orgmatch_profile['public_options']['timerequirement_large'])
				{
					if ($org_profile['membership_timerequirement'] != '3' && $org_profile['membership_timerequirement'] != '5')
					{
						$org_score += $orgmatch_profile['public_options']['timerequirement_large'] * 5;
					}
				}
				if ($orgmatch_profile['public_options']['commserv'])
				{
					if (!empty($org_profile['commserv_frequency']) && $org_profile['commserv_frequency'] != 'never')
					{
						$org_score += $orgmatch_profile['public_options']['commserv'] * 5;
					}
				}
				if ($orgmatch_profile['public_options']['fundraising'])
				{
					if (!empty($org_profile['fundraising_frequency']) && $org_profile['fundraising_frequency'] != 'never')
					{
						$org_score += $orgmatch_profile['public_options']['fundraising'] * 5;
					}
				}
				if ($orgmatch_profile['public_options']['speakers'])
				{
					if (!empty($org_profile['speakers_frequency']) && $org_profile['speakers_frequency'] != 'never')
					{
						$org_score += $orgmatch_profile['public_options']['speakers'] * 5;
					}
				}
				if ($orgmatch_profile['public_options']['oncampussocial'])
				{
					if (!empty($org_profile['oncampussocial_frequency']) && $org_profile['oncampussocial_frequency'] != 'never')
					{
						$org_score += $orgmatch_profile['public_options']['oncampussocial'] * 5;
					}
				}
				if ($orgmatch_profile['public_options']['offcampussocial'])
				{
					if (!empty($org_profile['offcampussocial_frequency']) && $org_profile['offcampussocial_frequency'] != 'never')
					{
						$org_score += $orgmatch_profile['public_options']['fundraising'] * 5;
					}
				}
				if ($orgmatch_profile['public_options']['conferences'])
				{
					if (!empty($org_profile['conferences_frequency']) && $org_profile['conferences_frequency'] != 'never')
					{
						$org_score += $orgmatch_profile['public_options']['conferences'] * 5;
					}
				}
				if ($orgmatch_profile['public_options']['travel_instate'])
				{
					if (!empty($org_profile['travel_instate']) && $org_profile['travel_instate'] != 'never')
					{
						$org_score += $orgmatch_profile['public_options']['travel_instate'] * 5;
					}
				}
				if ($orgmatch_profile['public_options']['travel_outofstate'])
				{
					if (!empty($org_profile['travel_outofstate']) && $org_profile['travel_outofstate'] != 'never')
					{
						$org_score += $orgmatch_profile['public_options']['travel_outofstate'] * 5;
					}
				}
				if ($orgmatch_profile['public_options']['travel_international'])
				{
					if (!empty($org_profile['travel_international']) && $org_profile['travel_international'] != 'never')
					{
						$org_score += $orgmatch_profile['public_options']['travel_international'] * 5;
					}
				}
			}
					
			// Classification
			if (is_array($orgmatch_profile['classifications']))
			{
				foreach($orgmatch_profile['classifications'] as $classification_name => $classification_score)
				{
					if ($org['org_classification'] == $classification_name)
					{
						$org_score += intval($classification_score) * 10;
					}
				}
			}
			
			$orgs[] = array(
				'score' => $org_score,
				'info'	=> $org,
			);
		}
		
		// Sort the organizations by score, from highest to lowest.
		usort($orgs, array(self, 'sortByScore'));
		$orgs = array_slice($orgs, 0, 15);
		
		$org_search_results = array();
		foreach($orgs as $org)
		{
			$org_search_results[] = Organization::find($org['info']['id']);
		}
		
		return $org_search_results;
	}
	
	public static function sortByScore($a, $b)
	{
		if ($a['score'] == $b['score']) {
			return 0;
		}
		return ($a['score'] > $b['score']) ? -1 : 1;
	}
}