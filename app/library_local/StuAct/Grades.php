<?php
/**
 * COMPASS Grade management functions
 */

namespace StuAct;

use \Entity\User;
use \Entity\Settings;

class Grades
{
	// Exemption types.
	const EXEMPT_TEMPORARY = 0;
	const EXEMPT_PERMANENT = 1;

	// GPR cutoffs.
	const CUTOFF_UNDERGRADUATE = 2.0;
	const CUTOFF_GRADUATE = 3.0;
	
	// Name of the cumulative GPR field returned by Compass.
	const CUMULATIVE_GPR = 'CUMUL';
	
	// Internal use variables.
	protected static $compass;
	protected static $is_compass_available = NULL;

	// Check a student's grade using the current active "Check My Eligibility" term.
	public static function checkGradeUsingAlternateTerm(User $user)
	{
		$current_alternate_term = self::getCurrentAlternateTerm();
		return self::checkGrade($user, $current_alternate_term);
	}
	
	public static function checkGrade(User $user, $term = NULL)
	{
		$acl = \Zend_Registry::get('acl');
		if ($acl->userAllowed('flag_gpr', $user))
			return true;

		$student_record = self::getStudentRecord($user->uin, $term);
		
		if (strcmp($student_record['classification'], 'NA') == 0)
		{
			// If Compass is down, temporarily consider the user eligible.
			return true;
		}
		else
		{
			// Determine whether student is held to Graduate or Undergraduate requirements.
			$is_grad = ($student_record['classification'] == "GR");
			$grade_cutoff = ($is_grad) ? self::CUTOFF_GRADUATE : self::CUTOFF_UNDERGRADUATE;
			
			$semester_gpa = max((float)$student_record['gpa_alt'], (float)$student_record['gpa']);

			if ($is_grad && (int)$student_record['hours_attempted'] == 0)
				$meets_semester = true;
			else
				$meets_semester = ($semester_gpa >= $grade_cutoff);
			
			$cumul_gpa = $student_record['cumul'];
			$meets_cumul = ($cumul_gpa >= $grade_cutoff);
			
			return $meets_cumul && $meets_semester;
		}
	}
	
	/**
	 * New GPR System: General Use Functions
	 */
	 
	public static function getCurrentActiveTerm()
	{
		return Settings::getSetting('gpr_active_term');
 	}
	public static function setCurrentActiveTerm($term)
	{
		Settings::setSetting('gpr_active_term', $term);
		self::clearCache();
	}
	
	public static function getCurrentAlternateTerm()
	{
		return Settings::getSetting('gpr_alternate_term');
	}
	
	public static function setCurrentAlternateTerm($term)
	{
		Settings::setSetting('gpr_alternate_term', $term);
	}
	
	public static function getStudentRecord($uin, $current_term = NULL)
	{
		$grades = self::getBulkStudentRecords(array($uin), $current_term);
		return $grades[$uin];
	}
	
	public static function getBulkStudentRecords($uins, $current_term = NULL)
	{
		$current_term = (is_null($current_term)) ? self::getCurrentActiveTerm() : $current_term;

		// Extend the script's running time.
		$uins_filtered = array();
		foreach((array)$uins as $uin_raw)
		{
			$uin = self::getUIN($uin_raw);
			$uins_filtered[$uin] = $uin;
		}
		$uins_filtered = array_filter($uins_filtered);

		$all_grades = self::fetchBulkGrades($uins_filtered);
		$grades = array();

		foreach($uins_filtered as $uin)
		{
			if (isset($all_grades[$uin]))
			{
				$grade_record = $all_grades[$uin];

				$term_year = substr($current_term, 0, 4);
				$term_semester = substr($current_term, 4, 1);
	            $term_campus_code = substr($current_term, 5, 1);

	            if ($term_semester == 1)
	            	$alt_term = $term_year.'2'.$term_campus_code;
	            else
	            	$alt_term = NULL;

	            if ($alt_term && isset($grade_record[$alt_term]))
	            	$alt_gpa = $grade_record[$alt_term]['gpa'];
	            else
	            	$alt_gpa = 0;

				$grades[$uin] = array(
					'gpa'			=> $grade_record[$current_term]['gpa'],
					'gpa_alt'		=> $alt_gpa,
					'cumul'			=> $grade_record[self::CUMULATIVE_GPR]['gpa'],
					
					'complete_current' => $grade_record[$current_term],
					'complete_cumul' => $grade_record[self::CUMULATIVE_GPR],

					'classification' => $grade_record[$current_term]['classification'],
				);
			}
			else
			{
				$grades[$uin] = array(
					'gpa'		=> 0,
					'gpa_alt'	=> 0,
					'cumul'		=> 0,

					'complete_current' => array(),
					'complete_cumul' => array(),

					'classification' => 'NA',
				);
			}
		}
		
		return $grades;
	}
	
	public static function addToCache($uin, $term, $data)
	{ }
	
	public static function checkCache($uin, $term)
	{ }
	
	public static function clearCache($uin = NULL)
	{ }

	public static function getTermName($term_code = NULL)
	{
		$term_code = (is_null($term_code)) ? self::getCurrentActiveTerm() : $term_code;
	
		$semesters = array(
			1		=> 'Spring',
			2		=> 'Summer',
			3		=> 'Fall',
		);
        $campus_codes = array(
            1       => 'College Station',
            2       => 'Galveston',
            3       => 'Qatar',
        );
		
		if ($term_code == self::CUMULATIVE_GPR)
		{
			return 'Cumulative';
		}
		else if (!is_null($term_code))
		{
			$term_year = substr($term_code, 0, 4);
			$term_semester = substr($term_code, 4, 1);
            $term_campus_code = substr($term_code, 5, 1);
            
            $term_name = $semesters[$term_semester].' '.$term_year;
            
            if ($term_campus_code != 1)
				$term_name .= ' ('.$campus_codes[$term_campus_code].')';
			
			return $term_name;
		}
		else
		{
			return '';
		}
	}
	
	public static function getValidTermName($term_code = NULL)
	{
		$term_year = intval(substr($term_code, 0, 4));
		$term_semester = substr($term_code, 4, 1);
		
		if ($term_semester == 1)
			return 'Summer/Fall '.$term_year;
		else if ($term_semester == 2)
			return 'Fall '.$term_year;
		else if ($term_semester == 3)
			return 'Spring '.($term_year+1);
	}
	
	/**
	 * COMPASS API Functions
	 */
	
	protected static function getCompass()
	{}

	protected static function getDb()
	{
		static $db;

		if ($db === NULL)
		{
			$config = \Zend_Registry::get('config');

			$dbal_config = new \Doctrine\DBAL\Configuration;
			$conn_config = $config->services->doitapidirect->conn->toArray();

			$db = \Doctrine\DBAL\DriverManager::getConnection($conn_config, $dbal_config);
		}

		return $db;
	}
	
	public static function isCompassAvailable()
	{
		return true;
	}
	
	public static function fetchGradesForAllTerms($uin)
	{
		$grades = self::fetchBulkGrades(array($uin));
		return $grades[$uin];
	}
	
	public static function fetchBulkGrades($uins)
	{
		$conn = self::getDb();

		try
		{
			$query = $conn->prepare('SELECT g.* FROM gpa AS g WHERE g.uin = :uin');

			$grades = array();
			foreach($uins as $uin)
			{
				$query->bindValue('uin', $uin);
				$query->execute();
				
				$results = $query->fetchAll();

				$grade_result = self::processCompassGradeRow($results);
				if ($grade_result)
					$grades[$uin] = $grade_result;
			}

			return $grades;
		}
		catch (\Exception $e)
		{
			return NULL;
		}
	}
	
	public static function processCompassGradeRow($grade_data)
	{	
		$grades = array();
		
		foreach($grade_data as $grade_row)
		{
			$term = $grade_row['term'];
			
			$term_campus_code = substr($term, -1);
			if ($term != self::CUMULATIVE_GPR && $term_campus_code != 1)
				continue;

			if ($grade_row['classification'] == "GR" || !isset($grades[$term]))
				$grades[$term] = $grade_row;
		}
		
		return $grades;
	}
	
	public static function fetchGradeForTerm($uin, $term = self::CUMULATIVE_GPR)
	{
		$all_term_grades = self::fetchGradesForAllTerms($uin);
		
		if (!is_null($all_term_grades) && isset($all_term_grades[$term]))
			return $all_term_grades[$term];
		else
			return 0;
	}
	
	public static function getAvailableTerms()
	{
		$conn = self::getDb();
		$terms_raw = $conn->fetchAll('SELECT g.term FROM gpa AS g GROUP BY g.term ORDER BY g.term ASC');

		$terms = array();
		foreach($terms_raw as $term_raw)
		{
			$term_num = $term_raw['term'];

			if ($term_num != self::CUMULATIVE_GPR && substr($term_num, -1) == 1)
				$terms[$term_num] = self::getTermName($term_num);
		}

		return $terms;
	}
	
	public static function getStudentStatus($uin)
	{
		$student_details = self::getStudentDetails($uin);
		return (isset($student['classification'])) ? $student['classification'] : NULL;
	}
	
	public static function getStudentDetails($uin)
	{
		$conn = self::getDb();
		return $conn->fetchArray('SELECT s.* FROM students AS s WHERE s.uin = ?', array($uin));
	}

	public static function getQualityPoints($uins)
	{
		if (!is_array($uins))
			$uins = array($uins);

		$conn = self::getDb();
		$qps_raw = $conn->executeQuery('SELECT s.uin, s.quality_points, s.gpa_credits FROM students s WHERE s.uin IN (?)', array($uins), array(\Doctrine\DBAL\Connection::PARAM_STR_ARRAY))
			->fetchAll(\PDO::FETCH_ASSOC);

		$qps = array();
		foreach((array)$qps_raw as $qp)
		{
			$qps[$qp['uin']] = array(
				'qp' => $qp['quality_points'],
				'gpacr' => $qp['gpa_credits'],
			);
		}

		return $qps;
	}

	/**
	 * Generic Functions
	 */
	
	public static function getUIN($uin_raw)
	{
		return intval(substr(str_replace('-', '', trim($uin_raw)), 0, 9));
	}
}