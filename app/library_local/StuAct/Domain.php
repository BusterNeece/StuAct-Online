<?php
namespace StuAct;

require_once 'Net/DNS.php';

class Domain
{
	const DNS_SERVER_1 = '8.8.8.8';
	const DNS_SERVER_2 = '8.8.4.4';
	
	// Check connectivity to the DNS server by attempting to establish a connection with it.
	public static function checkConnectivity()
	{
		$fp = @fsockopen(self::DNS_SERVER_1, 53, $errno, $errstr, 30);
		return ($fp !== FALSE);
	}
	
	// Look up a domain's DNS records and return any association with known servers.
	public static function lookup($url)
	{
		$resolver = new \Net_DNS_Resolver();
		$resolver->nameservers = array(self::DNS_SERVER_1, self::DNS_SERVER_2);
		
		$response = $resolver->rawQuery(strtolower($url));
		
		if ($response)
		{
			if ((count($response->answer) == 0) || ($response->header->rcode == "NXDOMAIN"))
			{
				return array('available' => TRUE, 'current' => NULL);
			}
			else
			{
				foreach($response->answer as $rr)
				{
					if ($rr->type == "CNAME")
						$holding_domain = $rr->cname;
					else if ($rr->type == "A" && !$holding_domain)
						$holding_domain = $url;
				}
				
				return array('available' => FALSE, 'current' => $holding_domain);
			}
		}
		else
		{
			return array('available' => TRUE, 'current' => NULL);
		}
	}
}