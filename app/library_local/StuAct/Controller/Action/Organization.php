<?php
namespace StuAct\Controller\Action;

class Organization extends \DF\Controller\Action
{
	protected $_org_id;
	protected $_organization;
	protected $_current_position;
	protected $_settings;
	protected $_org_acl;

	protected $_allow_no_org;

    public function preInit()
    {
		// Load organization settings.
		$this->_settings = \Entity\Organization::loadSettings();
		$this->view->org_settings = $this->_settings;

		$this->_org_acl = \StuAct\Acl\Organization::getInstance();
		$this->view->org_acl = $this->_org_acl;
		
		// Locate an organization based on the ID specified.
		$id_string = $this->_getParam('id');

		if (!is_numeric($id_string))
			$id_string = base64_decode($id_string);
		
		if (strlen($id_string) == 6)
			$org = \Entity\Organization::getRepository()->findOneBy(array('account_number' => $id_string));
		else
			$org = \Entity\Organization::find((int)$id_string);

		if ($org instanceof \Entity\Organization)
		{
			$this->_organization = $org;
			$this->_org_id = $org->id;

			$this->view->organization = $org;
			$this->view->org_id = $org->id;
		}
		else if (!$this->_allow_no_org)
		{
			$this->alert('<b>The organization you specified could not be found.</b><br>You can search the list of organizations using the links on this page.', 'red');
			$this->redirectToRoute(array('module' => 'organization', 'controller' => 'index'));
			return;
		}
    }

    public function preDispatch()
    {
    	// Set controller-wide title.
    	if ($this->_org_id)
    	{
    		// Set title bar.
    		$org_title = $this->_organization->name;
	    	if ($this->_isAllowed('home') && $this->_organization->account_number)
	    		$org_title .= ' ('.$this->_organization->account_number.')';
	    	$this->view->headTitle($org_title);

	    	$this->view->layout()->show_subtitle = true;

	    	// Hide pages from users if organization is unrecognized.
			if (!defined('VISIBLE_WHEN_UNRECOGNIZED') && ($this->_organization->status == ORG_STATUS_NOT_RECOGNIZED) && (!$this->_isAllowed('profile_staff_edit')))
				throw new \DF\Exception\DisplayOnly('Oragnization Not Active: The organization you requested is currently not actively recognized. No actions can be performed on it until it is recognized again.');
			
			// Show current position in org (if applicable)
			if ($this->auth->isLoggedIn())
			{
				$this->_current_position = $this->_organization->getUserPosition();
				$this->view->current_position = $this->_current_position;
			}
		}

		parent::preDispatch();
    }

    public function postDispatch()
    {
    	// Set header tabs.
    	if ($this->_org_id && $this->_isAllowed('home'))
    		$this->view->layout()->tabs = $this->view->renderHere('tabs', TRUE);
    	
    	parent::postDispatch();
    }
    
    /**
     * Internal organization permission check shortcuts.
     */

    protected function _isAllowed($action)
    {
    	return $this->_org_acl->isAllowed($action, $this->_organization);
    }
    protected function _checkPermission($action)
    {
    	if (!$this->_isAllowed($action))
		{
			if (!$this->auth->isLoggedIn())
				throw new \DF\Exception\NotLoggedIn();
			else
				throw new \DF\Exception\PermissionDenied();
		}
    }
}