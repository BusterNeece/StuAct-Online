<?php
/**
 * Legacy Form Handling Code
 */

namespace StuAct\Form;

class Legacy
{
	/* Render a full HTML view of a given dataset. */
	public static function renderView($info, $raw = FALSE, $single_group = FALSE)
	{
		if ($single_group)
			$info = array('data' => $info);

		$fields = self::encodeFields($info);

		$output = array();
		foreach($fields as $field_header => $field_items)
		{
			$view = '';

			if (!$raw)
				$view .= '<h2>'.$field_header.'</h2>';

			$view .= '<dl>';
			foreach((array)$field_items as $field_item_name => $field_item_value)
			{
				if ($field_item_value)
				{
					$view .= '<dt>'.$field_item_name.':</dt>';
					$view .= '<dd>'.nl2br($field_item_value).'</dd>';
				}
			}

			$view .= '</dl>';

			$output[$field_header] = $view;
		}

		if ($single_group)
			return $output['data'];
		elseif ($raw)
			return $output;
		else
			return implode('', $output);
	}

	/* Convert field group from raw array to encoded array. */
	public static function encodeFields($info)
	{
		$encoded_fields = array();
		foreach($info as $section_name => $section_fields)
		{
			$encoded_section_name = self::encodeFieldName($section_name);
			$encoded_fields[$encoded_section_name] = self::encodeFieldArray($section_fields);
		}
		return $encoded_fields;
	}

	public static function encodeFieldArray($field_array)
	{
		$encoded_fields = array();
		$row_num = 0;
		foreach((array)$field_array as $field_name => $field_value)
		{
			$row_num++;
			
			if (stristr($field_name, 'File_'))
			{
				$field_values = (is_array($field_value)) ? $field_value : array($field_value);
				$new_field_values = array();
				foreach($field_values as $offset => $file_info)
				{
                    if (is_array($file_info))
                    {
                        $file_url = $file_info['url'];
                        $file_description = $file_info['description'];
                    }
                    else
                    {
                        $file_url = $file_description = $file_info;
                    }
                    
					$full_file_url = 'https://studentactivities.tamu.edu/uploads/'.$file_url;
					$new_field_values[] = '<a class="file_download" data-offset="'.$offset.'" data-field="'.$field_name.'" href="'.$full_file_url.'">'.$file_description.'</a>';
				}
				$new_field_value = implode("\n", $new_field_values);
			}
			else if (stristr($field_name, 'Date_'))
			{
                if (!is_numeric($field_value))
                    $field_value = strtotime($field_value);
                
				$new_field_value = ($field_value != 0) ? date('F j, Y h:ia', $field_value) : NULL;
			}
			else if (is_array($field_value))
			{
				if (is_int($field_name))
				{
					$field_name = '#'.$row_num;	
				}
				
				$new_field_value = array();
				$encoded_array = self::encodeFieldArray($field_value);
				$is_assoc = (array_keys($encoded_array) !== range(0, count($encoded_array) - 1));
				foreach($encoded_array as $sub_field_name => $sub_field_val)
				{
					if ($is_assoc)
						$new_field_value[] = '<b>'.$sub_field_name.':</b> '.$sub_field_val;
					else
						$new_field_value[] = $sub_field_val;
				}
				
				$new_field_value = implode("\n", $new_field_value);
			}
			else
			{
				$new_field_value = $field_value;
			}
			
			$encoded_field_name = self::encodeFieldName($field_name);
			$encoded_fields[$encoded_field_name] = $new_field_value;
		}
		return $encoded_fields;
	}
	
	public static function encodeFieldName($field_name)
	{
		$replacement_patterns = array(
			'File_'		=> '[File] ',
			'Date_'		=> '[Date] ',
			'__'		=> ' - ',
			'_'			=> ' ',
		);
		
		foreach($replacement_patterns as $original_string => $new_string)
		{
			$field_name = str_replace($original_string, $new_string, $field_name);
		}
		return $field_name;
	}
}