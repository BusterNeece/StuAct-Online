<?php
namespace StuAct\Form;
class SponsAuth extends \DF\Form\Custom
{
	protected $_app_type;
	protected $_record;

	public function __construct($record = NULL, $type = NULL, $admin_view = FALSE)
	{
		// Set up form.
		$module_config = \Zend_Registry::get('module_config');
		$config = $module_config['sponsauth'];
		
		$form_config = $config->forms->request->form->toArray();
        
        if ($admin_view)
        {
            unset($form_config['groups']['submit_group']);
            
            $admin_form = $config->forms->request_admin->form->toArray();
            $form_config['groups'] += $admin_form['groups'];
        }
        
        // Set application type.
        if ($record)
			$this->_app_type = $record['event_type'];
		else if ($type)
			$this->_app_type = $type;
		else
			throw new \DF\Exception\DisplayOnly('No event type specified!');
        
        // Type-specific field changes
        if ($this->_app_type == "sponsored")
        {
            unset($form_config['groups']['event_info']['elements']['course_name']);
        }
        
		parent::__construct($form_config);
        
        if ($record)
        {
            $this->_record = $record;
            $this->populate($record->toArray());
        }
	}
	
    public function save($other_data = array())
    {
		$data = $this->getValues();
		
		if (!($this->_record instanceof \Entity\SponsAuth))
		{
			$this->_record = new \Entity\SponsAuth();
		}
		
		$updated_data = array(
			'event_type'		=> $this->_app_type,
		);
		$updated_data += $data;
        $updated_data += $other_data;
		
		$this->_record->fromArray($updated_data);
		$this->_record->save();
		
		return $this->_record;
    }
    
    public function renderSpecial($view = NULL, $view_mode = 'edit')
    {
		$this->populate($this->_record->toArray(TRUE, TRUE));
		return parent::renderSpecial($view, $view_mode);
    }
}