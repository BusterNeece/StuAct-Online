<?php
namespace StuAct\Service;

use \DF\Cache;

class GoogleSearch
{
	public static function getSearchUrl($query)
	{
		$search_config = self::loadSettings();

		$url_base = 'https://www.google.com/search';

		$params = array(
			'q' => htmlspecialchars($query).' site:'.$search_config['search_site'],
		);

		return $url_base.'?'.http_build_query($params);
	}

	public static function search($query)
	{
		$search_config = self::loadSettings();
		$search_params = array(
			'site'	=> 'default_collection',
			'client' => 'TAMU_frontend'
		);
		
		$full_query = htmlspecialchars($query).' site:'.$search_config['search_site'];
		$query_hash = 'search_'.md5($full_query);
		
		if (Cache::test($query_hash))
			return Cache::load($query_hash);
		
		$client = new \Zend_Http_Client;
		$client->setUri($search_config['appliance_uri']);
		$client->setParameterGet($search_params);
		$client->setParameterGet('q', $full_query);
		$client->setParameterGet('output', 'xml_no_dtd');
					
		try
		{
			$response = $client->request('GET');
		}
		catch(\Exception $e)
		{
			return false;
		}
				
		if ($response->isSuccessful())
		{
			$raw_results = $response->getBody();

			$dom = new \Zend_Dom_Query();
			$dom->setDocumentXml($raw_results);
			
			$result_set = $dom->query('RES')->current();
			
			if (!$result_set)
			{
				$results = array(
					'totalResultsReturned'	=> 0,
					'totalResultsAvailable' => 0,
					'moreResults'			=> NULL,
					'Result'				=> array(),
				);
			}
			else
			{
				$results = array();
				$result_elements = $result_set->getElementsByTagName("R");
								
				if (count($result_elements) > 0)
				{
					foreach($result_elements as $result_el)
					{
						$result = array();
						$result['DisplayUrl'] = $result['ClickUrl'] = $result_el->getElementsByTagName("U")->item(0)->nodeValue;
						$result['Title'] = $result_el->getElementsByTagName("T")->item(0)->nodeValue;
						$result['Summary'] = $result_el->getElementsByTagName("S")->item(0)->nodeValue;

						$results[] = $result;
					}
				}
			}
			
			Cache::save($results, $query_hash, array('search'), 3600);

			return $results;
		}
	}

	public static function loadSettings()
	{
		static $settings;

		if (!$settings)
		{
			$config = \Zend_Registry::get('config');
			$settings = $config->services->search->toArray();
		}

		return $settings;
	}
}