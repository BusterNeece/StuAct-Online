<?php
namespace StuAct\WebHostManager;

class SSH
{
	private $_server_index;
	private $_connection;
	
	public function __construct()
	{
        $config = \Zend_Registry::get('config');
        $settings = $config->services->cpanel->toArray();
        
        $this->_server_index = $server_index;
        $this->_connection = new \Net_SSH2($settings['host']);
        
		if (!$this->_connection->login($settings['user'], $settings['pass']))
			throw new \DF\Exception\DisplayOnly('Could Not Authenticate to SSH: An error occurred when attempting to log in to the cPanel server.');
	}
	
	public function command($command_name)
	{
		set_time_limit(300);
		
		return $this->_connection->exec($command_name);
	}
	
	public function generateRandomFilename()
	{
		$random_filename = time();
		$random_filename .= substr(md5(mt_rand()), 0, 10);
		
		return '~/'.$random_filename;
	}
	
	/**
	 * Static utility functions.
	 */
	
	public static function getUserDirPath($username, $subdirectory = NULL)
	{
		if (!is_null($subdirectory))
		{
			$subdirectory .= '/';
		}
		
		return '/home/'.$username.'/'.$subdirectory;
	}	
}